/* 
    Documento  : app.js
    Criado em  : 07/12/2015
    Autor      : UX Solucoes em TI
    Descrição  : JS - Site noGlobo.com
 */

$(window).resize(function(){
    redimensionar();    
})
$(document).ready(function(){
    //initMap();
    var anterior;
    //navigator.geolocation.getCurrentPosition(MostrarPosicao);
    $('#frmPesq\\:iptPesq_input').focus();        
    paceOptions = {
        elements: true
    };    
    redimensionar();         
    // Textos da home
    var textos = ["Produtos.", "Serviços.", "Empresas."];
    var atual = 0;
    $('#texto-principal').text(textos[atual++]);
    setInterval(function() {
        $('#texto-principal').fadeOut(function() {
            if (atual >= textos.length) atual = 0;
            $('#texto-principal').text(textos[atual++]).fadeIn();
        });
    }, 4000);    
    
    // Rolagem suave
    $("html").niceScroll({
        scrollspeed: 30,
        cursorwidth: 12
    });    
    
    $("#slider-fotos").owlCarousel({
        navigation : false, 
        slideSpeed : 200,
        paginationSpeed : 400,
        singleItem: true
    });            
})
var map;
function initMap() {
    map = new google.maps.Map(document.getElementById('map'), {
        center: {
            lat: -34.397, 
            lng: 150.644
        },
        zoom: 8
    });
}

function girarCartao(){
    if ($(".frente").hasClass("girar")) {
        $(".frente").removeClass("girar");
        $(".verso").removeClass("girar");
    } else {
        $(".frente").addClass("girar");
        $(".verso").addClass("girar");
    }   
}



function destacar(id){
    var nomeCampo = id.toString().split(":");
    nomeCampo = nomeCampo[1].substr(1, nomeCampo[1].length);
    var saida = "t" + nomeCampo;  
    if (this.anterior != undefined) {
        document.getElementById("t" + this.anterior).className = "";
    }
    if (nomeCampo == "CVV") {
        girarCartao();
        document.getElementById(saida).className = "destacar";
    } else {        
        
        if ($(".frente").hasClass("girar")) {
            $(".frente").removeClass("girar");
            $(".verso").removeClass("girar");
        }        
        document.getElementById(saida).className = "destacar";
    }
    this.anterior = nomeCampo;    
}

function mudarBandeira(tipo, valor) {
    //console.log("Trim: " + trim(valor));
    if (tipo == "select") {
        if (valor != "selecione") {
            $("#bandeira").attr("src", "resources/assets/ico/pagamentos/128x128/" + valor.toString() + ".png");
        }        
    } else {   
        if (verificarBandeira(trim(valor)) != '') {
            $("#bandeira").attr("src", "resources/assets/ico/pagamentos/128x128/" + verificarBandeira(trim(valor)) + ".png");
        } else {
            $("#bandeira").attr("src", "");
        }            
    }
}

function verificarBandeira(number){
    var re = {
        electron: /^(4026|417500|4405|4508|4844|4913|4917)\d+$/,
        maestro: /^(5018|5020|5038|5612|5893|6304|6759|6761|6762|6763|0604|6390)\d+$/,
        dankort: /^(5019)\d+$/,
        interpayment: /^(636)\d+$/,
        unionpay: /^(62|88)\d+$/,
        visa: /^4[0-9]{12}(?:[0-9]{3})?$/,
        mastercard: /^5[1-5][0-9]{14}$/,
        amex: /^3[47][0-9]{13}$/,
        diners: /^3(?:0[0-5]|[68][0-9])[0-9]{11}$/,
        discover: /^6(?:011|5[0-9]{2})[0-9]{12}$/,
        jcb: /^(?:2131|1800|35\d{3})\d{11}$/
    };
    if (re.electron.test(number)) {
        return 'electron';
    } else if (re.maestro.test(number)) {
        return 'maestro';
    } else if (re.dankort.test(number)) {
        return 'danfort';
    } else if (re.interpayment.test(number)) {
        return 'interpayment';
    } else if (re.unionpay.test(number)) {
        return 'unionpay';
    } else if (re.visa.test(number)) {
        return 'visa';
    } else if (re.mastercard.test(number)) {
        return 'mastercard';
    } else if (re.amex.test(number)) {
        return 'amex';
    } else if (re.diners.test(number)) {
        return 'diners';
    } else if (re.discover.test(number)) {
        return 'discover';
    } else if (re.jcb.test(number)) {
        return 'jcb';
    } else {
        return '';
    }
}

function trim(e){ 
    espacos = /\s/g; 
    return e.replace(espacos, "");
}

function MostrarPosicao(position) {
    console.log("Latitude: " + position.coords.latitude + ". Longitude: " + position.coords.longitude);
}

function consultaCep(valor) {
    var cep = valor.replace(/\D/g, '');
    if (cep != "") {
        var validacep = /^[0-9]{8}$/;
        var logradouro = "";
        if(validacep.test(cep)) {
            $.getJSON("//viacep.com.br/ws/"+ cep +"/json/?callback=?", function(dados) {
                if (!("erro" in dados)) {
                    logradouro = dados.logradouro.split(" ");
                    $('#frmCompra\\:tipo').val(logradouro[0]);                    
                    $('#frmCompra\\:tipoLogradouroCadastro').val(logradouro[1]);                    
                    $('#frmCompra\\:tipoLogradouroCadastro').val(logradouro[0]);
                    $('#frmCompra\\:logradouroCadastro').val(dados.logradouro.replace(logradouro[0], "").substr(1, dados.logradouro.replace(logradouro[0], "").length));
                    $('#frmCompra\\:bairroCadastro').val(dados.bairro);
                    $('#frmCompra\\:cidadeCadastro').val(dados.localidade + " - " + dados.uf);
                    $('#frmCompra\\:numeroCadastro').focus();
                } 
                else {
                    alert("CEP não encontrado.");
                }
            });
        } 
    } 
};

function redimensionar(){
    $(function(){
        if ($(window).width() < 768) {
            if ($(window).width() < 450) {
                $("#frmBarraPesq\\:iptPesq_input").css("min-width", $(window).width()- 100);
                $("#frmBarraPesq\\:iptPesq_input").css("width", $(window).width()- 100);
                
                $("#frmPesq\\:iptPesq_input").css("min-width", $(window).width() - 120);
                $("#frmPesq\\:iptPesq_input").css("width", $(window).width()- 120);
            } else {
                $("#frmBarraPesq\\:iptPesq_input").css("min-width", ($(window).width()/2) + 100);
                $("#frmBarraPesq\\:iptPesq_input").css("width", ($(window).width()/2) + 100);
                
                $("#frmPesq\\:iptPesq_input").css("min-width", ($(window).width()/2) + 200);
                $("#frmPesq\\:iptPesq_input").css("width", ($(window).width()/2) + 200);
            }            
        } else {
            if ($(window).width() > 1400) {
                $("#frmBarraPesq\\:iptPesq_input").css("width",  $(window).width()/4);
            } else {
                $("#frmBarraPesq\\:iptPesq_input").css("width",  $(window).width()/3);
            }
        //            $("#frmPesq\\:iptPesq_input").css("max-width", $(window).width()/2 );
        //            $("#frmPesq\\:iptPesq_input").css("min-width", '800');
        //            $("#frmPesq\\:iptPesq_input").css("width",  $(window).width()/2);
        }
    });
}

var Distancia = {
    distanciaEntreDoisPontos : function( pontoInicial, pontoFinal ){
        var R = 6371; // Radio da Terra
        var dLat = this.graus2Radianos( pontoFinal.latitude - pontoInicial.latitude ); 
        var dLon = this.graus2Radianos( pontoFinal.longitude - pontoInicial.longitude ); 
        var a = Math.sin( dLat/2 ) * Math.sin( dLat/2 ) + Math.cos( this.graus2Radianos( pontoInicial.latitude ) ) * Math.cos( this.graus2Radianos( pontoFinal.latitude ) ) * Math.sin( dLon/2 ) * Math.sin( dLon/2 ); 
        var c = 2 * Math.atan2( Math.sqrt( a ), Math.sqrt( 1-a ) ); 
        var d = R * c; 
        return d;
    },
    graus2Radianos : function( graus ){
        return graus * ( Math.PI/180 )
    }
};

function tog(v){
    return v?'addClass':'removeClass';
} 
$(document).on('input', '.clearable', function(){
    $(this)[tog(this.value)]('x');
}).on('mousemove', '.x', function( e ){
    $(this)[tog(this.offsetWidth-18 < e.clientX-this.getBoundingClientRect().left)]('onX');
}).on('touchstart click', '.onX', function( ev ){
    ev.preventDefault();
    $(this).removeClass('x onX').val('').change();
});

function f2(){
    var valor = document.getElementById("frmPesq:iptPesq_input").value;
    if (valor == "" || valor == null){
        document.getElementById("spn").style.display = "block";
        document.getElementById("frmPesq:iptPesq_input").focus();
        return false
    }else{
        return true;
    }
}

function verificarCampo(){
    var valor = document.getElementById("frmBarraPesq:iptPesq_input").value;
    if (valor == "" || valor == null){
        document.getElementById("frmBarraPesq:iptPesq_input").style.border= "1px solid #red !important";
        return false
    }else{
        return true;
    }
}

function pegarValorCampo(campo){
    console.info(campo);
    if (document.getElementById(campo).value != null) {
        var valor = document.getElementById(campo).value;
        console.log(valor);
        return valor;    
    }
    return "";
}

function mudarUrl(title, url) {
    if (typeof (history.pushState) != "undefined") {
        var obj = {
            Title: title, 
            Url: url
        };
        history.pushState(obj, obj.Title, obj.Url);
    } else {
        alert("Browser does not support HTML5.");
    }
}