/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ux.util;

import java.net.URL;
import java.util.Iterator;
import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.Element;
import org.dom4j.io.SAXReader;

/**
 *
 * @author Renato
 */
public class ConsultaCEP {

    public ConsultaCEP() {
    }

    

    public Document getDocumento(URL url) throws DocumentException {
        SAXReader reader = new SAXReader();
        Document document = reader.read(url);
        return document;
    }
}
