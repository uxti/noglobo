/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ux.util;

import com.ux.config.Configuracoes;
import com.ux.model.Empresa;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.Reader;
import java.io.StringWriter;
import java.io.UnsupportedEncodingException;
import java.io.Writer;
import java.math.BigDecimal;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.DateFormat;
import java.text.Normalizer;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.InputMismatchException;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Random;
import java.util.Scanner;
import javax.ejb.Asynchronous;
import javax.ejb.EJBException;
import javax.faces.context.FacesContext;
import javax.imageio.ImageIO;
import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.text.WordUtils;
import org.apache.commons.mail.EmailException;
import org.apache.commons.mail.HtmlEmail;
import org.apache.commons.mail.SimpleEmail;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.Element;
import org.dom4j.io.SAXReader;
import org.primefaces.event.FileUploadEvent;

/**
 *
 * @author Renato
 */
public class UXUtil {

    public static Date ultimoDiaDoMes() {
        Calendar data = new GregorianCalendar();
        int ultimo_dia_mes = data.getActualMaximum(Calendar.DAY_OF_MONTH);
        data.set(Calendar.DAY_OF_MONTH, ultimo_dia_mes);
        return data.getTime();
    }

    public static Date primeiroDiaDoMes() {
        Calendar data = new GregorianCalendar();
        int ultimo_dia_mes = data.getActualMinimum(Calendar.DAY_OF_MONTH);
        data.set(Calendar.DAY_OF_MONTH, ultimo_dia_mes);
        return data.getTime();
    }

    public static Date primeiroDiaDaSemana() {
        Calendar data = new GregorianCalendar();
        int ultimo_dia_mes = data.getActualMinimum(Calendar.DAY_OF_WEEK);
        data.set(Calendar.DAY_OF_WEEK, ultimo_dia_mes);
        return data.getTime();
    }

    public static Date ultimoDiaDaSemana() {
        Calendar data = new GregorianCalendar();
        int ultimo_dia_mes = data.getActualMaximum(Calendar.DAY_OF_WEEK);
        data.set(Calendar.DAY_OF_WEEK, ultimo_dia_mes);
        return data.getTime();
    }

    public static Date primeiroDiaDoAno() {
        Calendar data = new GregorianCalendar();
        int primeiro_dia_ano = data.getActualMinimum(Calendar.DAY_OF_YEAR);
        data.set(Calendar.DAY_OF_YEAR, primeiro_dia_ano);
        return data.getTime();
    }

    public static Date primeiroDiaDoAno(Integer ano) {
        Calendar data = new GregorianCalendar();
        int primeiro_dia_ano = data.getActualMinimum(Calendar.DAY_OF_YEAR);
        data.set(Calendar.DAY_OF_YEAR, primeiro_dia_ano);
        return data.getTime();
    }

    public static Date ultimoDiaDoAno() {
        Calendar data = new GregorianCalendar();
        int ultimo_dia_ano = data.getActualMaximum(Calendar.DAY_OF_YEAR);
        data.set(Calendar.DAY_OF_YEAR, ultimo_dia_ano);
        return data.getTime();
    }

    public static Date addMes(Date data, int qtd) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(data);
        cal.add(Calendar.MONTH, qtd);
        return cal.getTime();
    }

    public static Date addDia(Date data, int qtd) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(data);
        cal.add(Calendar.DAY_OF_MONTH, qtd);
        return cal.getTime();
    }

    public static Map<String, String> getParametros() {
        return FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap();
    }

    public static Date addAno(Date data, int qtd) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(data);
        cal.add(Calendar.YEAR, qtd);
        return cal.getTime();
    }

    public static String formatarMoeda(Double val) {
        return NumberFormat.getCurrencyInstance().format(val);
    }

    public static String formatarMoeda(String val) {
        return NumberFormat.getCurrencyInstance().format(val);
    }

    public static String formatarMoeda(BigDecimal val) {
        return NumberFormat.getCurrencyInstance().format(val);
    }

    public static String formataData(Date d) {
        DateFormat df = new SimpleDateFormat("dd/MM/yyyy", new Locale("pt", "BR"));
        if (d != null) {
            return df.format(d);
        } else {
            return "";
        }
    }

    public static String generateAleatorio() {
        String d = String.valueOf(new Date().getTime());
        String s = String.valueOf(new Random().nextInt());
        return d + s;
    }

    public static String formataDataYYYYMMDD(Date d) {
        DateFormat df = new SimpleDateFormat("yyyy-MM-dd", new Locale("pt", "BR"));
        if (d != null) {
            return df.format(d);
        } else {
            return "";
        }
    }

    public static String genSlug(String par) {
        String saida = par.toLowerCase().replaceAll(" ", "-");
        saida = Normalizer.normalize(saida, Normalizer.Form.NFD);;
        saida = saida.replaceAll("[^\\p{ASCII}]", "");
        return saida;

    }

    public static List<String> delimitar(String valor, String delimitador) {
        List<String> result = new ArrayList<String>();
        if (valor != null) {
            Scanner scan = new Scanner(valor);
            scan.useDelimiter(delimitador);
            while (scan.hasNext()) {
                result.add(scan.next());
            }
            return result;
        } else {
            return new ArrayList<String>();
        }
    }

//    public static Empresa retornaEmpresaLogada(String paramName) {
//        HttpServletRequest request = (HttpServletRequest) FacesContext
//                .getCurrentInstance().getExternalContext().getRequest();
//        Empresa e = empEJB.SelecionarPorID(Integer.parseInt(request.getParameter(paramName)));
//        return e;
//    }
//
//    public static Empresa retornaEmpresaLogadaPorID(Integer id) {
//        Empresa e = empEJB.SelecionarPorID(id);
//        return e;
//    }
    public static Integer retornaTamanhoString(String s) {
        return s.length();
    }

    public static String formataDataDDMMYYYY(Date date) {
        DateFormat df = new SimpleDateFormat("dd/MM/yyyy");
        if (date != null) {
            return df.format(date);
        }
        return "";
    }

    public static String formataDataDDMMYYYYHHMM(Date date) {
        DateFormat df = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
        if (date != null) {
            return df.format(date);
        }
        return "";
    }

    public static Date removeHoraMinutoSegundoDeData(Date date) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        calendar.set(Calendar.MILLISECOND, 0);
        calendar.set(Calendar.SECOND, 0);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.HOUR_OF_DAY, 0);
        return calendar.getTime();

    }

    public static boolean betweenDates(Date date, Date dateStart, Date dateEnd) {
        if (date != null && dateStart != null && dateEnd != null) {
            if ((date.after(dateStart) || date.equals(dateStart)) && (date.before(dateEnd) || date.equals(dateEnd))) {
                return true;
            } else {
                return false;
            }
        }
        return false;
    }

    public static String getLastTree(String s) {
        if (s.length() >= 3) {
            int le = s.length();
            return s.substring(le - 3, le);
        } else {
            return s;
        }
    }

    public long retornaQuantidadeDeDiasDiferenteDaDataAtual(Date dtt) {
//        Calendar c = Calendar.getInstance();
//        c.set(2015, 9, 23);
//        System.out.println(c.getTime());
        Date d2 = dtt;
        Date d1 = new Date();
        long dt = (d2.getTime() - d1.getTime()) + 3600000;
        return (dt / 86400000L);
    }

    public static void escreveLogErro(Exception e) {
        System.out.println("O erro ocorrido é:" + e);
        System.out.println(e);
    }

    public static Date retornaPrimeiroData(int mes, int ano) {
        Calendar c = new GregorianCalendar(ano, mes, 1);
        int dia = c.getMinimum(Calendar.DAY_OF_MONTH);
        Calendar ca = new GregorianCalendar(ano, mes, dia);
        return ca.getTime();
    }

    public static Date retornaUltimoData(int mes, int ano) {
        Calendar c = new GregorianCalendar(ano, mes, 1);
        int dia = c.getActualMaximum(Calendar.DAY_OF_MONTH);
        Calendar ca = new GregorianCalendar(ano, mes, dia);
        return ca.getTime();
    }

    public static String retornaMes(int i) {
        if (i == 0) {
            return "Jan";
        } else if (i == 1) {
            return "Fev";
        } else if (i == 2) {
            return "Mar";
        } else if (i == 3) {
            return "Abr";
        } else if (i == 4) {
            return "Mai";
        } else if (i == 5) {
            return "Jun";
        } else if (i == 6) {
            return "Jul";
        } else if (i == 7) {
            return "Ago";
        } else if (i == 8) {
            return "Set";
        } else if (i == 9) {
            return "Out";
        } else if (i == 10) {
            return "Nov";
        } else if (i == 11) {
            return "Dez";
        } else {
            return "INVÁLIDO";
        }
    }

    public static String getRandomImageName() {
        int i = (int) (Math.random() * 10000000);
        String a = new Date().toString();
        return String.valueOf(i) + a;
    }

    public static void descobreErroDeEJBException(EJBException e) {
        @SuppressWarnings("ThrowableResultIgnored")
        Exception cause = e.getCausedByException();
        if (cause instanceof ConstraintViolationException) {
            @SuppressWarnings("ThrowableResultIgnored")
            ConstraintViolationException cve = (ConstraintViolationException) e.getCausedByException();
            for (Iterator<ConstraintViolation<?>> it = cve.getConstraintViolations().iterator(); it.hasNext();) {
                ConstraintViolation<? extends Object> v = it.next();
                System.err.println(v);
                System.err.println("==>>" + v.getMessage());
            }
        }
    }

    public static String upload(FileUploadEvent event, String destino, int largura, int altura) {
        String filename = Normalizer.normalize(((new Date().getTime()) + new Random().nextInt(50) + event.getFile().getFileName().replaceAll(" ", "_")).trim(), Normalizer.Form.NFD).replaceAll("[^\\p{ASCII}]", "");
        String fileEntrada = destino + "/" + filename;
        String fileNameSaida = largura + "_" + altura + "_" + filename;
        String fileSaida = destino + "/" + fileNameSaida;
        System.out.println("Destino: " + destino);
        try {
            copyFile(filename, event.getFile().getInputstream(), destino);
            redimensionarImagem(largura, altura, fileEntrada, fileSaida);
//               new File(fileEntrada).delete();
            return fileNameSaida;
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }

    public static String upload(FileUploadEvent event, String destino) {
        String filename = Normalizer.normalize(((new Date().getTime()) + new Random().nextInt(50) + event.getFile().getFileName().replaceAll(" ", "_")).trim(), Normalizer.Form.NFD).replaceAll("[^\\p{ASCII}]", "");
        String fileNameSaida = filename;
//        String fileSaida = destino + "/" + fileNameSaida;
        System.out.println("Destino: " + destino);
        try {
            copyFile(filename, event.getFile().getInputstream(), destino);
            return fileNameSaida;
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }

    private static void copyFile(String fileName, InputStream in, String destino) {
        try {
            // write the inputStream to a FileOutputStream
            OutputStream out = new FileOutputStream(new File(destino + fileName));
            int read = 0;
            byte[] bytes = new byte[1024];

            while ((read = in.read(bytes)) != -1) {
                out.write(bytes, 0, read);
            }
            in.close();
            out.flush();
            out.close();
//            System.out.println("New file created!");
        } catch (IOException e) {
            System.out.println(e.getMessage());
        }

    }

    private static BufferedImage resizeImage(BufferedImage originalImage, int type, int width, int height) {
        BufferedImage resizedImage = new BufferedImage(width, height, type);
        Graphics2D g = resizedImage.createGraphics();
        g.drawImage(originalImage, 0, 0, width, height, null);
        g.dispose();
        return resizedImage;
    }

    public static void redimensionarImagem(int height, int width, String fenter, String fexit) {
        try {
            BufferedImage originalImage = ImageIO.read(new File(fenter));
            int type = originalImage.getType() == 0 ? BufferedImage.TYPE_INT_ARGB : originalImage.getType();

            BufferedImage resizeImageJpg = resizeImage(originalImage, type, height, width);
            ImageIO.write(resizeImageJpg, "png", new File(fexit));

        } catch (IOException ex) {
            System.out.println(ex);
        }
    }

    @Asynchronous
    public static void enviarEmail(String hostMail, String destinatario, String nomeDestinatario, String emailDe, String nomeDe, String senha, String assunto, String mensagem) throws EmailException {
        SimpleEmail email = new SimpleEmail();
        System.out.println(hostMail);
        email.setHostName(hostMail);
        email.setSmtpPort(465);
        System.out.println(destinatario);
        email.addTo(destinatario, nomeDestinatario);
        System.out.println(emailDe + nomeDe);
        email.setFrom(emailDe, nomeDe);
        email.setSubject(assunto);
        email.setMsg(mensagem);
        email.setSSL(true);
        email.setAuthentication(emailDe, senha);
        email.send();
    }

    @Asynchronous
    public static void enviarEmailConfigurado(String destinatario, String nomeDestinatario, String assunto, String mensagem) throws EmailException {
        HtmlEmail email = new HtmlEmail();
        email.setHostName(Configuracoes.host);
//        email.setSmtpPort(465);
        email.setSmtpPort(465);
        System.out.println(destinatario);
        email.addTo(destinatario, nomeDestinatario);
        email.setFrom(Configuracoes.email, Configuracoes.nomeDe);
        email.setSubject(assunto);
        email.setHtmlMsg(mensagem);
        email.setSSL(true);
        email.setAuthentication(Configuracoes.email, Configuracoes.senha);
        email.send();
        System.out.println("enviou o email");
    }

    public static String formatString(String s) {
        String temp = Normalizer.normalize(s, java.text.Normalizer.Form.NFD);
        return temp.replaceAll("[^\\p{ASCII}]", "").replace("/", "").replace(" ", "-").replace("---", "-").replace("--", "-").replace(" - ", "-").replaceFirst(" ", " ").toLowerCase();
    }

    public static String reFormatString(String s) {
        String temp = Normalizer.normalize(s, java.text.Normalizer.Form.NFD);
        return temp.replaceAll("[^\\p{ASCII}]", "").replace("-", " ").replace("  ", " ").toLowerCase();
    }

    public static String curl(Map<String, Object> params) {
        StringBuilder sb = new StringBuilder();
        for (Object key : params.keySet()) {
            Object value = params.get(key);
            if (sb.length() == 0) {
                if (!sb.toString().contains("?")) {
                    sb.append("?");
                }
                sb.append(key).append("=").append(value);
            } else {
                sb.append("&").append(key).append("=").append(value);
            }
        }
        return sb.toString();
    }

    public static String inputStreamToString(InputStream is) throws IOException {
        if (is != null) {
            Writer writer = new StringWriter();

            char[] buffer = new char[1024];
            try {
                Reader reader = new BufferedReader(new InputStreamReader(is, "UTF-8"));
                int n;
                while ((n = reader.read(buffer)) != -1) {
                    writer.write(buffer, 0, n);
                }
            } finally {
                is.close();
            }
            return writer.toString();
        } else {
            return "";
        }
    }

    public static String executarPost(String url2, String oauth, String json) throws MalformedURLException, IOException {
        URL url = new URL(url2);
        // 2. Open connection
        HttpURLConnection conn = (HttpURLConnection) url.openConnection();
//        / 3. Specify POST method
        conn.setRequestMethod("POST");
        // 4. Set the headers
        conn.setRequestProperty("Content-Type", "application/json");
        conn.setRequestProperty("Authorization", oauth);
        conn.setDoOutput(true);
        // 5.2 Get connection output stream
        OutputStream wr = conn.getOutputStream();
        wr.write(json.getBytes());
        // 5.4 Send the request
        wr.flush();
        // 5.5 close
        wr.close();

        BufferedReader in = new BufferedReader(
                new InputStreamReader(conn.getInputStream()));
        String inputLine;
        StringBuffer response = new StringBuffer();

        while ((inputLine = in.readLine()) != null) {
            response.append(inputLine);
        }
        in.close();
        return response.toString();
    }

    public static String post(String json, String url, String oauth) throws UnsupportedEncodingException, IOException {
        CloseableHttpClient client = HttpClients.createDefault();
        HttpPost httpPost = new HttpPost(url);
//        HttpPost httpPost = new HttpPost("https://sandbox.moip.com.br/v2/accounts");
        StringEntity entity = new StringEntity(json);
        httpPost.setEntity(entity);
        httpPost.setHeader("Accept", "application/json");
        httpPost.setHeader("Content-type", "application/json");
        httpPost.setHeader("Authorization", oauth);
//        httpPost.setHeader("Authorization", "OAuth gjlbnjd9s541r361hj52e03jm4pksw9");
        CloseableHttpResponse response = client.execute(httpPost);
        String saida = inputStreamToString(response.getEntity().getContent());
        client.close();
        return (saida);

    }

    public static boolean isCNPJ(String CNPJ) {
        if (CNPJ.equals("") || CNPJ == null) {
            CNPJ = "";
        } else {
            CNPJ = CNPJ.replace(".", "").replace("-", "").replace("/", "");
        }

        // considera-se erro CNPJ's formados por uma sequencia de numeros iguais 
        if (CNPJ.equals("00000000000000")
                || CNPJ.equals("11111111111111")
                || CNPJ.equals("22222222222222")
                || CNPJ.equals("33333333333333")
                || CNPJ.equals("44444444444444")
                || CNPJ.equals("55555555555555")
                || CNPJ.equals("66666666666666")
                || CNPJ.equals("77777777777777")
                || CNPJ.equals("88888888888888")
                || CNPJ.equals("99999999999999")
                || (CNPJ.length() != 14)) {
            return (false);
        }
        char dig13, dig14;
        int sm, i, r, num, peso;
        // "try" - protege o código para eventuais erros de conversao de tipo (int) 
        try {
            // Calculo do 1o. Digito Verificador 
            sm = 0;
            peso = 2;
            for (i = 11; i >= 0; i--) {
                // converte o i-ésimo caractere do CNPJ em um número: 
                // por exemplo, transforma o caractere '0' no inteiro 0
                // (48 eh a posição de '0' na tabela ASCII) 
                num = (int) (CNPJ.charAt(i) - 48);
                sm = sm + (num * peso);
                peso = peso + 1;
                if (peso == 10) {
                    peso = 2;
                }
            }
            r = sm % 11;
            if ((r == 0) || (r == 1)) {
                dig13 = '0';
            } else {
                dig13 = (char) ((11 - r) + 48);
            }
            // Calculo do 2o. Digito Verificador 
            sm = 0;
            peso = 2;
            for (i = 12; i >= 0; i--) {
                num = (int) (CNPJ.charAt(i) - 48);
                sm = sm + (num * peso);
                peso = peso + 1;
                if (peso == 10) {
                    peso = 2;
                }
            }
            r = sm % 11;
            if ((r == 0) || (r == 1)) {
                dig14 = '0';
            } else {
                dig14 = (char) ((11 - r) + 48);
            }
            // Verifica se os dígitos calculados conferem com os dígitos informados. 
            if ((dig13 == CNPJ.charAt(12)) && (dig14 == CNPJ.charAt(13))) {
                return (true);
            } else {
                return (false);
            }
        } catch (InputMismatchException erro) {
            return (false);
        }
    }

    public static boolean isCPF(String CPF) {
        if (CPF == null) {
            CPF = "";
        } else {
            CPF = CPF.replace(".", "").replace("-", "");
        }


        // considera-se erro CPF's formados por uma sequencia de numeros iguais
        if (CPF.equals("00000000000") || CPF.equals("11111111111") || CPF.equals("22222222222") || CPF.equals("33333333333") || CPF.equals("44444444444") || CPF.equals("55555555555") || CPF.equals("66666666666") || CPF.equals("77777777777") || CPF.equals("88888888888") || CPF.equals("99999999999") || (CPF.length() != 11)) {
            return (false);
        }
        char dig10, dig11;
        int sm, i, r, num, peso;
        // "try" - protege o codigo para eventuais erros de conversao de tipo (int) 
        try {
            // Calculo do 1o. Digito Verificador
            sm = 0;
            peso = 10;
            for (i = 0; i < 9; i++) {
                // converte o i-esimo caractere do CPF em um numero:
                // por exemplo, transforma o caractere '0' no inteiro 0 
                // (48 eh a posicao de '0' na tabela ASCII) 
                num = (int) (CPF.charAt(i) - 48);
                sm = sm + (num * peso);
                peso = peso - 1;
            }
            r = 11 - (sm % 11);
            if ((r == 10) || (r == 11)) {
                dig10 = '0';
            } else {
                dig10 = (char) (r + 48);
            }
            // converte no respectivo caractere numerico 
            // Calculo do 2o. Digito Verificador 
            sm = 0;
            peso = 11;
            for (i = 0; i < 10; i++) {
                num = (int) (CPF.charAt(i) - 48);
                sm = sm + (num * peso);
                peso = peso - 1;
            }
            r = 11 - (sm % 11);
            if ((r == 10) || (r == 11)) {
                dig11 = '0';
            } else {
                dig11 = (char) (r + 48);
            }
            // Verifica se os digitos calculados conferem com os digitos informados. 
            if ((dig10 == CPF.charAt(9)) && (dig11 == CPF.charAt(10))) {
                return (true);
            } else {
                return (false);
            }
        } catch (InputMismatchException erro) {
            return (false);
        }


    }

    public static int getNumeroMes(Date date) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        return cal.get(Calendar.MONTH);
    }

    public static int getAno() {
        Calendar cal = Calendar.getInstance();
        cal.setTime(new Date());
        return cal.get(Calendar.YEAR);
    }

    public static boolean isValidCardNumber(String ccNumber) {
        try {
            ccNumber = ccNumber.replaceAll("\\D", "");
            char[] ccNumberArry = ccNumber.toCharArray();
            int checkSum = 0;
            for (int i = ccNumberArry.length - 1; i >= 0; i--) {
                char ccDigit = ccNumberArry[i];
                if ((ccNumberArry.length - i) % 2 == 0) {
                    int doubleddDigit = Character.getNumericValue(ccDigit) * 2;
                    checkSum += (doubleddDigit % 9 == 0 && doubleddDigit != 0) ? 9 : doubleddDigit % 9;
                } else {
                    checkSum += Character.getNumericValue(ccDigit);
                }
            }
            return (checkSum != 0 && checkSum % 10 == 0);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    public static String ltrim(String s) {
        return s.replaceAll("\\s+$", "");
    }

    public static String rtrim(String s) {
        return s.replaceAll("\\s+$", "");
    }

    public static String retornaTelefone(Empresa emp) {
        String telefones = "";
        telefones = "(" + emp.getDddTelefone() + ") " + emp.getTelefone();
//        if (emp.getDddSegundoTelefone() != null) {
//            telefones += " - (" + emp.getDddSegundoTelefone() + ") " + emp.getSegundoTelefone();
//        }       
        return telefones;
    }

    public static int cotarQuantosExistemNaLista(List<String> str, String val) {
        int total = 0;
        for (String s : str) {
            if (s.equals(val)) {
                total = total + 1;
            }
        }
        return total;
    }

    public static String retornaNumeroComZerosAEsquerda(String numero, int qtd, String caracter) {
        return StringUtils.leftPad(numero, qtd, caracter);
    }

    public static void redirecionar(String url) throws IOException {
        FacesContext.getCurrentInstance().getExternalContext().redirect(FacesContext.getCurrentInstance().getExternalContext().getRequestContextPath() + "/" + url);
    }

    public static void consultarCEP(String CEP) {
        String tipoLogradouro, logradouro, bairro, cidade, estado;
        try {
            URL url = new URL("http://cep.republicavirtual.com.br/web_cep.php?cep=" + CEP + "&formato=xml");
            Document document = getDocumento(url);
            Element root = document.getRootElement();
            for (Iterator i = root.elementIterator(); i.hasNext();) {
                Element element = (Element) i.next();
                System.out.println(element);               
                
                if (element.getQualifiedName().equals("resultado")) {
                    //  setResultado(Integer.parseInt(element.getText()));
                }
                if (element.getQualifiedName().equals("resultado_txt")) {
                    //  setResultado_txt(element.getText());
                }   
                if (element.getQualifiedName().equals("tipoLogradouro")) {
                    tipoLogradouro = element.getText();
                }
                if (element.getQualifiedName().equals("logradouro")) {
                    logradouro = element.getText();
                }
                if (element.getQualifiedName().equals("bairro")) {
                    bairro = element.getText();
                }
                if (element.getQualifiedName().equals("cidade")) {
                    cidade = element.getText();
                }
                if (element.getQualifiedName().equals("uf")) {
                    estado = element.getText();
                }
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public static Document getDocumento(URL url) throws DocumentException {
        SAXReader reader = new SAXReader();
        Document document = reader.read(url);
        return document;
    }
}
