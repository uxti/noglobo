/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ux.controller;

import com.ux.facade.FacadeEJB;
import com.ux.model.Compra;
import com.ux.model.Empresa;
import com.ux.model.Usuario;
import com.ux.pojo.Pedido;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.Query;

/**
 *
 * @author Renato
 */
@Stateless
public class CompraEJB extends FacadeEJB<Compra> {

    public CompraEJB() {
        super(Compra.class);
    }

    public void atualizarComprasPorLote(String lote, String order) {
        Query q = em.createQuery("UPDATE Compra c set c.orderCode = :ord where c.lote = :lot");
        q.setParameter("ord", order);
        q.setParameter("lot", lote);
        q.executeUpdate();
        
    }

    public void atualizarStatusPorLote(String status, String lote) {
        Query q = em.createQuery("UPDATE Compra c set c.status = :ord where c.lote = :lot");
        q.setParameter("ord", status);
        q.setParameter("lot", lote);
        q.executeUpdate();
    }

    public void atualizarPagamentoComprasPorLote(String lote, String pay, String link) {
        Query q = em.createQuery("UPDATE Compra c set c.paymentCode = :pay, c.linkBoleto = :link where c.lote = :lot");
        q.setParameter("pay", pay);
        q.setParameter("link", link);
        q.setParameter("lot", lote);
        q.executeUpdate();
    }

    public List<Compra> comprasPorLote(String lote) {
        Query q = em.createQuery("Select c From Compra c where c.lote = :lote");
        q.setParameter("lote", lote);
        return q.getResultList();
    }

    public List<Compra> listarComprasPorUsuario(Usuario u) {
        Query q = em.createQuery("Select c From Compra c where c.idUsuario.idUsuario = :idu ORDER BY c.dtCompra DESC");
        q.setParameter("idu", u.getIdUsuario());
        return q.getResultList();
    }
    
//    public List<Pedido> listarPedidosPorUsuario(Usuario u) {
//        em.getEntityManagerFactory().getCache().evictAll();
//        Query query = em.createQuery("Select p.idEmpresa, p.dtCompra, p.lote, p.situacaoEntrega, p.status, p.valorTotal,p.idUsuario From Compra p where p.idUsuario.idUsuario = :ide  group by  p.idEmpresa, p.dtCompra, p.lote, p.situacaoEntrega, p.status, p.valorTotal,p.idUsuario  order by p.dtCompra desc");
//        query.setParameter("ide", u.getIdUsuario());
//        List<Object[]> compras = query.getResultList();
//        List<Pedido> pedidos = new ArrayList<Pedido>();
//        for (Object[] c : compras) {
//            Pedido p = new Pedido();
//            p.setEmpresa((Empresa) c[0]);
//            p.setDt_venda((Date) c[1]);
//            p.setLote((String) c[2]);
//            p.setSituacaoEntrega((String) c[3]);
//            p.setStatus((String) c[4]);
//            p.setValor((BigDecimal) c[5]);
//            p.setUsuario((Usuario) c[6]);
//            pedidos.add(p);
//        }
//        return pedidos;
//    }

    public List<Pedido> listarPedidosPorUsuario(Usuario u) {
        Query q = em.createQuery("Select c From Compra c where c.idUsuario.idUsuario = :idu ORDER BY c.dtCompra DESC");
        q.setParameter("idu", u.getIdUsuario());
        List<Compra> compras = q.getResultList();
        List<Pedido> pedidos = new ArrayList<Pedido>();
        for (Compra c : compras) {
            Pedido p = new Pedido();
            p.setEmpresa(c.getIdEmpresa());
            p.setDt_venda(c.getDtCompra());
            p.setLote(c.getLote());
            p.setSituacaoEntrega(c.getSituacaoEntrega());
            p.setStatus(c.getStatus());
            p.setValor(c.getValorTotal());
            p.setUsuario(c.getIdUsuario());
            if (pedidos.indexOf(p) < 0) {
                pedidos.add(p);
            }
        }
        return pedidos;
    }
    
    public List<Pedido> listarPedidosPorEmpresa(Empresa u) {
        Query q = em.createQuery("Select c From Compra c where c.idEmpresa.idEmpresa = :ide ORDER BY c.dtCompra DESC");
        q.setParameter("ide", u.getIdEmpresa());
        List<Compra> compras = q.getResultList();
        List<Pedido> pedidos = new ArrayList<Pedido>();
        for (Compra c : compras) {
            Pedido p = new Pedido();
            p.setEmpresa(c.getIdEmpresa());
            p.setDt_venda(c.getDtCompra());
            p.setLote(c.getLote());
            p.setSituacaoEntrega(c.getSituacaoEntrega());
            p.setStatus(c.getStatus());
            p.setValor(c.getValorTotal());
            if (pedidos.indexOf(p) < 0) {
                pedidos.add(p);
            }
        }
        return pedidos;
    }

    public List<Compra> listarComprasPorLote(String lote) {
        Query query = em.createQuery("Select c From Compra c where c.lote = :lote");
        query.setParameter("lote", lote);
        return query.getResultList();
    }

    public List<Compra> comprasParaAtualizarStatus(Usuario u) {
        Query q = em.createQuery("Select c From Compra c where c.idUsuario.idUsuario = :idu and c.status IN ('Aguardando pagamento','IN_ANALYSIS')");
        q.setParameter("idu", u.getIdUsuario());
        return q.getResultList();
    }

    public List<Compra> comprasParaAtualizarStatus() {
        Query q = em.createQuery("Select c From Compra c where  c.status IN ('Aguardando pagamento','IN_ANALYSIS','AUTHORIZED')");
        return q.getResultList();
    }

    public List<Compra> comprasParaAtualizarStatusDelivery() {
        Query q = em.createQuery("Select c From Compra c where  c.status IN ('Aguardando pagamento','IN_ANALYSIS','AUTHORIZED') and c.tipoFrete in ('Entrega','Retirada')");
        return q.getResultList();
    }


}
