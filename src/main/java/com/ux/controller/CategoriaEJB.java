/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ux.controller;

import com.ux.facade.FacadeEJB;
import com.ux.model.Categoria;
import com.ux.util.UXUtil;
import java.util.ArrayList;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.Query;

/**
 *
 * @author JOAOPAULO
 */
@Stateless
public class CategoriaEJB extends FacadeEJB<Categoria> {

    public CategoriaEJB() {
        super(Categoria.class);
    }

    public void salvarCategoria(Categoria c) {
        System.out.println(c.getTags());
        em.merge(c);
    }

    public List<String> listarTags(String par) {
        Query query = em.createQuery("Select c.tags From Categoria c where c.tags like :pa");
        query.setParameter("pa", "%" + par + "%");
        Query query2 = em.createQuery("select t.descricao From TipoEmpresa t where t.descricao LIKE :par");
        query2.setParameter("par", "%" + par + "%");
//        

        StringBuilder sb = new StringBuilder();
        List<String> ss = query.getResultList();
        List<String> sss = new ArrayList<String>();
        List<String> ssss = new ArrayList<String>();

        for (String s : ss) {
            sb.append(s).append(",");
        }
        sss = UXUtil.delimitar(sb.toString(), ",");

        sss.addAll(query2.getResultList());
//        sss.addAll(query3.getResultList());

        for (String s : sss) {
            if (s.contains(par)) {
                ssss.add(s);
            }
        }
        return ssss;

    }

    public List<String> getNamesByRelacionados(String par) {
        Query query3 = em.createQuery("select t.nome From Produto t where t.nome LIKE :pare");
        query3.setParameter("pare", "%" + par + "%");
        query3.setMaxResults(3);
        return query3.getResultList();
    }
}
