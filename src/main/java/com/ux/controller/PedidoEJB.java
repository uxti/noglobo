/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ux.controller;

import com.ux.model.Empresa;
import com.ux.model.Usuario;
import com.ux.pojo.Pedido;
import com.ux.util.Conexao;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.Query;

/**
 *
 * @author Renato
 */
@Stateless
public class PedidoEJB extends Conexao {

    public List<Pedido> listarPedidos(Empresa empresa) {
        Query query = em.createQuery("Select v.idEmpresa, v.idUsuario, v.valorTotal, v.valorFrete, v.lote, "
                + "v.dtCompra, v.status, v.situacaoEntrega, v.codigoRetirada, v.responsavel "
                + "From Compra v where v.idEmpresa.idEmpresa = :ide and v.status = 'Pago' and v.situacaoEntrega in ('Aguardando Envio' , 'Aguardando Retirada') GROUP BY  v.idEmpresa, v.idUsuario, v.valorTotal, v.valorFrete, v.lote, v.dtCompra, v.status, v.situacaoEntrega, v.codigoRetirada order by  v.dtCompra");
        query.setParameter("ide", empresa.getIdEmpresa());
        List<Object[]> resultList = query.getResultList();
        List<Pedido> pedidos = new ArrayList<Pedido>();
        for (Object[] obj : resultList) {
            Pedido p = new Pedido();
            p.setEmpresa((Empresa) obj[0]);
            p.setUsuario((Usuario) obj[1]);
            p.setValor((BigDecimal) obj[2]);
            p.setFrete((BigDecimal) obj[3]);
            p.setLote((String) obj[4]);
            p.setDt_venda((Date) obj[5]);
            p.setStatus((String) obj[6]);
            p.setSituacaoEntrega((String) obj[7]);
            p.setCodRetirada((String) obj[8]);
            p.setTelefone((String) obj[9]);
            pedidos.add(p);
        }
        return pedidos;
    }
}
