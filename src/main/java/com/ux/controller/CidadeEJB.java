/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ux.controller;

import com.ux.facade.FacadeEJB;
import com.ux.model.Cidade;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.Query;

/**
 *
 * @author JOAOPAULO
 */
@Stateless
public class CidadeEJB extends FacadeEJB<Cidade> {

  

    public CidadeEJB() {
        super(Cidade.class);
    }

    public List<Cidade> listarCidadesComEmpresas() {
        Query q = em.createQuery("SELECT c.idCidade From Empresa c GROUP BY c.idCidade");
        return q.getResultList();
    }

    public List<Cidade> pesquisarCidadePorNome(String nome) {
        Query q = em.createQuery("Select c From Cidade c where c.nome = :cid");
        q.setParameter("cid", nome);
        return q.getResultList();
    }
}
