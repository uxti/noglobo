/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ux.controller;

import br.com.moip.Authentication;
import br.com.moip.Moip;
import br.com.moip.OAuth;
import br.com.moip.resource.Payment;
import com.ux.config.Config;
import com.ux.model.Compra;
import com.ux.config.Configuracoes;
import com.ux.util.UXUtil;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.ejb.Asynchronous;
import javax.ejb.EJB;
import javax.ejb.Schedule;
import javax.ejb.Stateless;
import org.apache.commons.mail.EmailException;
import org.primefaces.context.RequestContext;
import sun.misc.Request;

/**
 *
 * @author Renato
 */
@Stateless
public class ConsultarPagamentos {

     @EJB
     CompraEJB cEJB;

     @Schedule(hour = "*", minute = "*/10")
     @Asynchronous
     public void atualizarStatusCompras() throws EmailException {
          System.out.println("consultando os ecommerce");
          List<Compra> csAtualizar = cEJB.comprasParaAtualizarStatus();
          List<String> lotesEnviados = new ArrayList<String>();
          for (Compra compra : csAtualizar) {
               lotesEnviados.add(compra.getLote());
               if (compra.getPaymentCode() != null) {
                    System.out.println("Achou um pagamento para consultar!");
                    Authentication auth = new OAuth(compra.getIdEmpresa().getOauth());
                    Moip moip = new Moip(auth, Config.endpoint);
//                System.out.println("Order Code " + compra.getOrderCode());
//                System.out.println("Payme Code " + compra.getPaymentCode());
                    if (compra.getValidoAte() != null) {
                         if (compra.getValidoAte().before(new Date())) {
                              compra.setStatus("Cancelada");
                              cEJB.Atualizar(compra);
                              if (UXUtil.cotarQuantosExistemNaLista(lotesEnviados, compra.getLote()) <= 1) {
                                   UXUtil.enviarEmailConfigurado(compra.getIdUsuario().getEmail(), compra.getIdUsuario().getIdCliente().getPrimeiroNome(), "O pedido " + compra.getLote() + " foi cancelado!", Configuracoes.mensagemCancelamentoPedido(compra));
                              }
                         } else {
                              Payment payment = moip.orders().get(compra.getOrderCode()).payments().get(compra.getPaymentCode());
                              System.out.println(payment);
                              if (payment.getStatus().equals("AUTHORIZED")) {
                                   compra.setStatus("Pago");
                                   compra.setSituacaoEntrega("Aguardando Envio");
                                   cEJB.Atualizar(compra);
                                   if (UXUtil.cotarQuantosExistemNaLista(lotesEnviados, compra.getLote()) <= 1) {
                                        UXUtil.enviarEmailConfigurado(compra.getIdUsuario().getEmail(), compra.getIdUsuario().getIdCliente().getPrimeiroNome(), "Pagamento do pedido " + compra.getLote() + " foi confirmado!", Configuracoes.mensagemAutorizacaoPagamento(compra, cEJB.listarComprasPorLote(compra.getLote())));
                                        UXUtil.enviarEmailConfigurado(compra.getIdProduto().getIdEmpresa().getEmail(), compra.getIdUsuario().getIdCliente().getPrimeiroNome(), "Pagamento do pedido " + compra.getLote() + " foi confirmado!", Configuracoes.mensagemAutorizacaoPagamento(compra, cEJB.listarComprasPorLote(compra.getLote())));
                                   }

                              } else if (payment.getStatus().equals("CANCELLED") || payment.getStatus().equals("Cancelled") ) {
                                   compra.setStatus("Cancelado");
                                   cEJB.Atualizar(compra);
                              }
                         }
                    } else {
                         compra.setValidoAte(UXUtil.addDia(compra.getDtCompra(), 5));
                         cEJB.Atualizar(compra);
                    }
               } else {
                    Authentication auth = new OAuth(compra.getIdEmpresa().getOauth());
                    Moip moip = new Moip(auth, Config.endpoint);
                    Payment payment = moip.orders().get(compra.getOrderCode()).payments();
                    //System.out.println("Pagamento id moip " + payment.getId());
                    if (compra.getValidoAte() != null) {
                         if (compra.getValidoAte().before(new Date())) {
                              compra.setStatus("Cancelada");
                              cEJB.Atualizar(compra);
                              if (UXUtil.cotarQuantosExistemNaLista(lotesEnviados, compra.getLote()) <= 1) {
                                   UXUtil.enviarEmailConfigurado(compra.getIdUsuario().getEmail(), compra.getIdUsuario().getIdCliente().getPrimeiroNome(), "O pedido " + compra.getLote() + " foi cancelado!", Configuracoes.mensagemCancelamentoPedido(compra));
                              }
                         }
                    } else {
                         compra.setValidoAte(UXUtil.addDia(compra.getDtCompra(), 5));
                         cEJB.Atualizar(compra);
                         if (compra.getValidoAte().before(new Date())) {
                              compra.setStatus("Cancelada");
                              cEJB.Atualizar(compra);
                              if (UXUtil.cotarQuantosExistemNaLista(lotesEnviados, compra.getLote()) <= 1) {
                                   UXUtil.enviarEmailConfigurado(compra.getIdUsuario().getEmail(), compra.getIdUsuario().getIdCliente().getPrimeiroNome(), "O pedido " + compra.getLote() + " foi cancelado!", Configuracoes.mensagemCancelamentoPedido(compra));
                              }
                         }
                    }
               }
          }
     }

     @Schedule(hour = "*", minute = "*/3")
     @Asynchronous
     public void atualizarStatusComprasDelivery() throws EmailException {
          System.out.println("Ué Delivery");
          List<Compra> csAtualizar = cEJB.comprasParaAtualizarStatusDelivery();
          List<String> lotesEnviados = new ArrayList<String>();
          for (Compra compra : csAtualizar) {
               lotesEnviados.add(compra.getLote());
               if (compra.getPaymentCode() != null) {
                    System.out.println("o payment nao está nulo");
                    Authentication auth = new OAuth(compra.getIdEmpresa().getOauth());
                    Moip moip = new Moip(auth, Config.endpoint);
                    if (compra.getValidoAte() != null) {
                         if (compra.getValidoAte().before(new Date())) {
                              System.out.println("a data validade esta anterior");
                              compra.setStatus("Cancelada");
                              cEJB.Atualizar(compra);
                         } else {
                              System.out.println("a data validade nao esta anterior");
                              Payment payment = moip.orders().get(compra.getOrderCode()).payments().get(compra.getPaymentCode());
                              System.out.println(payment);
                              if (payment.getStatus().equals("AUTHORIZED")) {
                                   compra.setStatus("Pago");
                                   if (compra.getTipoFrete().equals("Retirada")) {
                                        compra.setSituacaoEntrega("Aguardando Retirada");
                                   } else {
                                        compra.setSituacaoEntrega("Aguardando Envio");
                                   }
                                   cEJB.Atualizar(compra);
                                   if (UXUtil.cotarQuantosExistemNaLista(lotesEnviados, compra.getLote()) <= 1) {
                                        if (compra.getIdEmpresa().getSegmento().equals("Delivery")) {
                                             UXUtil.enviarEmailConfigurado(compra.getIdUsuario().getEmail(), compra.getIdUsuario().getIdCliente().getPrimeiroNome(), "Pagamento do pedido " + compra.getLote() + " foi confirmado!", Configuracoes.mensagemAutorizacaoPagamentoDelivery(compra, cEJB.listarComprasPorLote(compra.getLote())));
                                             UXUtil.enviarEmailConfigurado(compra.getIdProduto().getIdEmpresa().getEmail(), compra.getIdUsuario().getIdCliente().getPrimeiroNome(), "Pagamento do pedido " + compra.getLote() + " foi confirmado!", Configuracoes.mensagemAutorizacaoPagamento(compra, cEJB.listarComprasPorLote(compra.getLote())));
                                        } else {
                                             UXUtil.enviarEmailConfigurado(compra.getIdUsuario().getEmail(), compra.getIdUsuario().getIdCliente().getPrimeiroNome(), "Pagamento do pedido " + compra.getLote() + " foi confirmado!", Configuracoes.mensagemAutorizacaoPagamento(compra, cEJB.listarComprasPorLote(compra.getLote())));
                                             UXUtil.enviarEmailConfigurado(compra.getIdProduto().getIdEmpresa().getEmail(), compra.getIdUsuario().getIdCliente().getPrimeiroNome(), "Pagamento do pedido " + compra.getLote() + " foi confirmado!", Configuracoes.mensagemAutorizacaoPagamento(compra, cEJB.listarComprasPorLote(compra.getLote())));
                                        }

                                   }
//                                   RequestContext rc = RequestContext.getCurrentInstance();
//                                   rc.update(":frmPedidos:tblPedidos");
                              }else if (payment.getStatus().equals("CANCELLED")) {
                                   compra.setStatus("Cancelado");
                                   cEJB.Atualizar(compra);
                              }
                         }
                    } else {
                         compra.setValidoAte(UXUtil.addDia(compra.getDtCompra(), 5));
                         cEJB.Atualizar(compra);
                    }
               } else {
                    Authentication auth = new OAuth(compra.getIdEmpresa().getOauth());
                    Moip moip = new Moip(auth, Config.endpoint);
                    Payment payment = moip.orders().get(compra.getOrderCode()).payments();
                    //System.out.println("Pagamento id moip " + payment.getId());
                    compra.setPaymentCode(payment.getId());
                    cEJB.Atualizar(compra);
                    if (compra.getValidoAte() != null) {
                         if (compra.getValidoAte().before(new Date())) {
                              compra.setStatus("Cancelada");
                              cEJB.Atualizar(compra);
                              if (UXUtil.cotarQuantosExistemNaLista(lotesEnviados, compra.getLote()) <= 1) {
                                   UXUtil.enviarEmailConfigurado(compra.getIdUsuario().getEmail(), compra.getIdUsuario().getIdCliente().getPrimeiroNome(), "O pedido " + compra.getLote() + " foi cancelado!", Configuracoes.mensagemCancelamentoPedido(compra));
                              }
                         }
                    } else {
                         compra.setValidoAte(UXUtil.addDia(compra.getDtCompra(), 5));
                         cEJB.Atualizar(compra);
                         if (compra.getValidoAte().before(new Date())) {
                              compra.setStatus("Cancelada");
                              cEJB.Atualizar(compra);
                              if (UXUtil.cotarQuantosExistemNaLista(lotesEnviados, compra.getLote()) <= 1) {
                                   UXUtil.enviarEmailConfigurado(compra.getIdUsuario().getEmail(), compra.getIdUsuario().getIdCliente().getPrimeiroNome(), "O pedido " + compra.getLote() + " foi cancelado!", Configuracoes.mensagemCancelamentoPedido(compra));
                              }
                         }
                    }
               }
          }

     }
}
