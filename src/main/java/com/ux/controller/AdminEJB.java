/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ux.controller;

import com.ux.model.Empresa;
import com.ux.pojo.Vendas;
import com.ux.util.Conexao;
import com.ux.util.UXUtil;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.Query;

/**
 *
 * @author Renato
 */
@Stateless
public class AdminEJB extends Conexao {

    public List<Empresa> listarEmpresasEcommerce() {
        Query query = em.createQuery("Select e From Empresa e where e.ecommerce = 1");
        return query.getResultList();
    }

    public List<Vendas> listarVendasPorEmpresa(Empresa emp, Date ini, Date fim) {
        Query query = em.createQuery("Select c.lote, c.dtCompra, (SUM(c.valorCompra) + c.valorFrete)total, c.valorFrete, c.idEmpresa, c.status, c.situacaoEntrega From Compra c where c.idEmpresa.idEmpresa = :ide and c.status = 'Pago' and c.dtCompra BETWEEN :ini and :fim GROUP BY c.lote, c.dtCompra,  c.valorFrete, c.idEmpresa, c.status, c.situacaoEntrega");
        query.setParameter("ide", emp.getIdEmpresa());
        query.setParameter("ini", ini);
        query.setParameter("fim", fim);
        List<Object[]> resultList = query.getResultList();
        List<Vendas> vendas = new ArrayList<Vendas>();
        for (Object[] obj : resultList) {
            Vendas venda = new Vendas();
            venda.setLote((String) obj[0]);
            venda.setDtCompra((Date) obj[1]);
            venda.setValorTotal((BigDecimal) obj[2]);
            venda.setValorFrete((BigDecimal) obj[3]);
            venda.setEmpresa((Empresa) obj[4]);
            venda.setStatus((String) obj[5]);
            venda.setSituacao((String) obj[6]);
            venda.setMes(UXUtil.getNumeroMes(venda.getDtCompra()));
            vendas.add(venda);
        }
        return vendas;
    }

    public List<Integer> anosQueHouveramVendas() {
        Query query = em.createQuery("Select c.dtCompra From Compra c where c.status = 'Pago'");
        List<Date> resultList = query.getResultList();
        List<Integer> anos = new ArrayList<Integer>();
        for (Date obj : resultList) {
            Date d = obj;
            System.out.println(d);
            Calendar c = Calendar.getInstance();
            c.setTime(d);
            if (anos.indexOf(c.get(Calendar.YEAR)) < 0) {
                anos.add(c.get(Calendar.YEAR));
            }
        }
        return anos;
    }
}
