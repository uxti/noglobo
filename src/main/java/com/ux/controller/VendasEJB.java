/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ux.controller;

import com.ux.model.Compra;
import com.ux.model.Empresa;
import com.ux.model.Usuario;
import com.ux.pojo.Pedido;
import com.ux.util.Conexao;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.persistence.Query;

/**
 *
 * @author Renato
 */
@Stateless
public class VendasEJB extends Conexao {

    @EJB
    UsuarioEJB uEJB;

    public List<Compra> listarVendas() {
        Query q = em.createQuery("Select c From Compra c where c.idEmpresa.idEmpresa = :ide");
        q.setParameter("ide", uEJB.EmpresaLogado().getIdEmpresa());
        return q.getResultList();
    }

    public void efetuarCompra(Compra c) {
        try {
            em.merge(c);
        } catch (Exception e) {
            System.out.println(e);
        }
    }

    public void atualizar(Compra c) {
        em.merge(c);
    }

    public List<Compra> listarVendasPorPeriodo(Empresa e, Date ini, Date fim) {
        Query query = em.createQuery("Select c From Compra c where c.idEmpresa.idEmpresa = :ide and c.dtCompra BETWEEN :ini and :fim");
        query.setParameter("ide", e.getIdEmpresa());
        query.setParameter("ini", ini);
        query.setParameter("fim", fim);
        return query.getResultList();
    }

    public List<Compra> listarComprasAguardandoEnvio() {
        try {
            Query q = em.createQuery("Select c From Compra c where c.idEmpresa.idEmpresa = :ide and c.status = 'Pago' and c.situacaoEntrega ='Aguardando Envio'");
            q.setParameter("ide", uEJB.EmpresaLogado().getIdEmpresa());
            return q.getResultList();
        } catch (Exception e) {
            return new ArrayList<Compra>();
        }
    }

    public List<Compra> listarComprasAguardandoRetirada() {
        try {
            Query q = em.createQuery("Select c From Compra c where c.idEmpresa.idEmpresa = :ide and c.status = 'Pago' and c.situacaoEntrega ='Aguardando Retirada'");
            q.setParameter("ide", uEJB.EmpresaLogado().getIdEmpresa());
            return q.getResultList();
        } catch (Exception e) {
            return new ArrayList<Compra>();
        }
    }

    public Integer comprasAguardandoRetirada(Empresa emp) {
        Query query = em.createQuery("Select c.lote From Compra c where c.idEmpresa.idEmpresa = :ide and c.status = 'Pago' and c.situacaoEntrega = 'Aguardando Retirada' group by c.lote");
        query.setParameter("ide", emp.getIdEmpresa());
        return query.getResultList().size();
    }
    
    public Integer comprasAguardandoEnvio(Empresa emp) {
        Query query = em.createQuery("Select c.lote From Compra c where c.idEmpresa.idEmpresa = :ide and c.status = 'Pago' and c.situacaoEntrega = 'Aguardando Envio' GROUP BY c.lote");
        query.setParameter("ide", emp.getIdEmpresa());
        return query.getResultList().size();
    }
    
    public Integer totalDeVendas(Empresa emp) {
        Query query = em.createQuery("Select c.lote From Compra c where c.idEmpresa.idEmpresa = :ide and c.status = 'Pago'  GROUP BY c.lote");
        query.setParameter("ide", emp.getIdEmpresa());
        return query.getResultList().size();
    }

    public Number contarComprasRegistro(String clausula) {
        em.getEntityManagerFactory().getCache().evictAll();
        Query query = em.createQuery("Select COUNT(p.idCompra) From Compra p " + clausula);
        return (Number) query.getSingleResult();
    }

    public List<Compra> listarComprasLazyModeWhere(int primeiro, int qtd, String clausula, Empresa emp) {
        em.getEntityManagerFactory().getCache().evictAll();
        Query query = em.createQuery("Select p From Compra p where p.idEmpresa.idEmpresa = :ide " + clausula);
        query.setParameter("ide", emp.getIdEmpresa());
        query.setFirstResult(primeiro);
        query.setMaxResults(qtd);
        return query.getResultList();
    }

    public List<Pedido> listarComprasLazyModeWherePedido(int primeiro, int qtd, String clausula, Empresa emp) {
        em.getEntityManagerFactory().getCache().evictAll();
        Query query = em.createQuery("Select p.idEmpresa, p.dtCompra, p.lote, p.situacaoEntrega, p.status, p.valorTotal,p.idUsuario From Compra p where p.idEmpresa.idEmpresa = :ide " + clausula + " group by  p.idEmpresa, p.dtCompra, p.lote, p.situacaoEntrega, p.status, p.valorTotal,p.idUsuario  order by p.dtCompra desc");
        query.setParameter("ide", emp.getIdEmpresa());
        query.setFirstResult(primeiro);
        query.setMaxResults(qtd);
        List<Object[]> compras = query.getResultList();
        List<Pedido> pedidos = new ArrayList<Pedido>();
        for (Object[] c : compras) {
            Pedido p = new Pedido();
            p.setEmpresa((Empresa) c[0]);
            p.setDt_venda((Date) c[1]);
            p.setLote((String) c[2]);
            p.setSituacaoEntrega((String) c[3]);
            p.setStatus((String) c[4]);
            p.setValor((BigDecimal) c[5]);
            p.setUsuario((Usuario) c[6]);
            pedidos.add(p);
        }
        return pedidos;
    }
}
