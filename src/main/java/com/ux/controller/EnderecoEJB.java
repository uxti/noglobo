/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ux.controller;

import com.ux.model.Cidade;
import com.ux.model.Estado;
import com.ux.util.Conexao;
import javax.ejb.Stateless;
import javax.persistence.Query;

/**
 *
 * @author Renato
 */
@Stateless
public class EnderecoEJB extends Conexao {

    public Cidade getCidadeByName(String cidade) {
        Query q = em.createQuery("Select c From Cidade c where c.nome = :nome");
        q.setParameter("nome", cidade);
        return (Cidade) q.getSingleResult();
    }

    public Estado getEstadoBySigla(String estado) {
        Query q = em.createQuery("Select c From Estado c where c.sigla= :nome");
        q.setParameter("nome", estado);
        return (Estado) q.getSingleResult();
    }
}
