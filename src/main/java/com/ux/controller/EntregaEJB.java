/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ux.controller;

import com.ux.facade.FacadeEJB;
import com.ux.model.Empresa;
import com.ux.model.Entrega;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.Query;

/**
 *
 * @author Renato
 */
@Stateless
public class EntregaEJB extends FacadeEJB<Entrega> {

    public EntregaEJB() {
        super(Entrega.class);
    }

    public List<Entrega> listarEntregas(Empresa empresa) {
        Query query = em.createQuery("Select e From Entrega e where e.idEmpresa.idEmpresa = :ide");
        query.setParameter("ide", empresa.getIdEmpresa());
//        System.out.println(query.getResultList().size());
        return query.getResultList();
    }
}
