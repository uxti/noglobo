/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ux.controller;

import com.ux.facade.FacadeEJB;
import com.ux.model.TipoEmpresa;
import javax.ejb.Stateless;

/**
 *
 * @author Renato
 */
@Stateless
public class TipoEmpresaEJB extends FacadeEJB<TipoEmpresa> {

    public TipoEmpresaEJB() {
        super(TipoEmpresa.class);
    }
}
