/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ux.controller;

import com.ux.config.Config;
import com.ux.facade.FacadeEJB;
import com.ux.model.Empresa;
import com.ux.model.Foto;
import com.ux.model.Frete;
import com.ux.model.Produto;
import com.ux.model.Tags;
import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Random;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.persistence.NoResultException;
import javax.persistence.Query;

/**
 *
 * @author Renato
 */
@Stateless
public class ProdutoEJB extends FacadeEJB<Produto> {

     @EJB
     UsuarioEJB uEJB;
     private double totalReg;
     private double numPaginas;

     public ProdutoEJB() {
          super(Produto.class);
     }

     public void excluir(Frete f) {
          f = em.getReference(Frete.class, f.getIdFrete());
          em.remove(f);
     }

     public void excluirTag(Tags t) {
          t = em.getReference(Tags.class, t.getIdTag());
          em.remove(t);
     }

     public void excluirFoto(Foto t) {
          t = em.getReference(Foto.class, t.getIdFoto());
          em.remove(t);
     }

     public void atualizarProduto(Produto p) {
          em.merge(p);
          for (Foto f : p.getFotoList()) {
               f.setIdProduto(p);
               em.merge(f);
          }
//        for (Tags t : p.getTagsList()) {
//            t.setIdProduto(p);
//            em.merge(t);
//        }
     }

     public List<Produto> listarProdutosPorEmpresa(Empresa e) {
          Query q = em.createQuery("Select p From Produto p where p.idEmpresa.idEmpresa = :ide");
          System.out.println("Pesquisa da " + e);
          q.setParameter("ide", e.getIdEmpresa());
          return q.getResultList();
     }

     public Number contarProdutosRegistro(String param, String tipo, String cidade, String param2) {
          em.getEntityManagerFactory().getCache().evictAll();
          Query q = em.createQuery("Select COUNT(p.idProduto) From Produto p where p.nome = :pa or  p.nome LIKE :pa or p.slug LIKE :pa or p.nome = :pa or p.slug = :pa or p.metaTag like :pa and  p.tipo = 'produtos'");
          q.setParameter("pa", "%" + param + "%");
          return (Number) q.getSingleResult();
     }

     public List<Produto> listarProdutosLazyModeWhere(int primeiro, int qtd, String param, String tipo, String cidade, String param2) {
          em.getEntityManagerFactory().getCache().evictAll();
          if (param == null || param == "") {
               return new ArrayList<Produto>();
          } else {
               Query q = em.createQuery("Select p FROM Produto p where p.nome = :pa or  p.nome LIKE :pa or p.slug LIKE :pa or p.nome = :pa or p.slug = :pa or p.metaTag like :pa and  p.tipo = 'produtos'");
               q.setFirstResult(primeiro);
               q.setMaxResults(qtd);
               q.setParameter("pa", "%" + param + "%");
               return q.getResultList();
          }
     }

     public List<Produto> pesquisarProdutos(String param, String tipo, String cidade, String param2) {
          if (param.endsWith("s")) {
               param = param.substring(0, param.length() - 1);
          }
          System.out.println("Param: " + param);
          em.getEntityManagerFactory().getCache().evictAll();
//        System.out.println("Parametro na EJB " + param);
          if (param == null || param == "") {
               return new ArrayList<Produto>();
          } else {
               Query q = em.createQuery("Select p FROM Produto p where (p.metaTag like :pa or p.slug LIKE :pa or p.nome = :pa or p.nome LIKE :pa) and p.tipo = 'produtos'");
               q.setParameter("pa", "%" + param + "%");
               List<Produto> ps = q.getResultList();
               Collections.shuffle(ps);
               return ps;
          }
     }

     public List<Produto> pesquisarServicos(String param, String tipo, String cidade, String param2) {
          em.getEntityManagerFactory().getCache().evictAll();
          System.out.println("Parametro na EJB " + param);
          if (param == null || param == "") {
               return new ArrayList<Produto>();
          } else {
               Query q = em.createQuery("Select p FROM Produto p where (p.metaTag like :pa or p.slug LIKE :pa or p.nome LIKE :pa or p.nome = :pa) and p.tipo != 'produtos'");
               q.setParameter("pa", "%" + param + "%");
               System.out.println(q.getResultList().size());
               return q.getResultList();
          }
     }

     public List<String> slugsRelacionadas(String param) {
          if (param == null || param.equals("")) {
               return new ArrayList<String>();
          } else {
               Query q = em.createQuery("Select p.nome FROM Produto p where p.idCategoria.descricao like :pa  ");
               q.setParameter("pa", "%" + param + "%");
               q.setMaxResults(3);
               return q.getResultList();
          }

     }

     public List<Produto> listarProdutosReferentesPorEmpresa(Integer idProduto, String idCategoria, Integer idEmpresa, Integer limit) {
          Query query = em.createQuery("Select p From Produto p where p.idCategoria.idCategoria LIKE :idc and p.idEmpresa.idEmpresa = :ide and p.idProduto != :idp");
          query.setParameter("idp", idProduto);
          query.setParameter("idc", idCategoria);
          query.setParameter("ide", idEmpresa);
          query.setMaxResults(limit);
          List<Produto> list = query.getResultList();
          Collections.shuffle(list);
          return list;
     }

     public List<Produto> listarProdutosReferentesPorEmpresaPatrocinadas(Integer limit) {
          List<Produto> list = new ArrayList<>();
          while (list.size() < limit) {
               Query query = em.createQuery("Select COUNT(p.idEmpresa) From Empresa p WHERE p.ativo = 1");
               Random random = new Random();
               int number = random.nextInt(Integer.valueOf(query.getSingleResult().toString()));

               Query query2 = em.createQuery("Select p From Produto p WHERE p.idEmpresa.idEmpresa = :id");
               query2.setParameter("id", number);
               query2.setMaxResults(limit);
               list = query2.getResultList();
               Collections.shuffle(list);
          }
          return list;
     }

     public Integer afterID() {
          Query query = em.createQuery("select MAX(c.idProduto) From Produto c");
          return 1 + (Integer) query.getSingleResult();
     }

     public void excluirProduto(Produto p) {
          p = em.getReference(Produto.class, p.getIdProduto());
          new File(Config.caminhoProduto + "\\" + p.getFotoPrincipal()).delete();
          for (Foto f : p.getFotoList()) {
               f = em.getReference(Foto.class, f.getIdFoto());
               new File(Config.caminhoProduto + "\\" + f.getUrl()).delete();
               em.remove(f);
          }
          for (Frete frete : p.getFreteList()) {
               frete = em.getReference(Frete.class, frete.getIdFrete());
               em.remove(frete);
          }
          for (Tags t : p.getTagsList()) {
               t = em.getReference(Tags.class, t.getIdTag());
               em.remove(t);
          }
          em.remove(p);
     }

     public Produto getProdutoBySlug(String slug) {
          em.getEntityManagerFactory().getCache().evictAll();
          try {
               Query q = em.createQuery("Select p From Produto p where p.slug = :sl");
               q.setParameter("sl", slug);
               q.setMaxResults(1);
               return (Produto) q.getSingleResult();
          } catch (NoResultException e) {
               return new Produto();
          }
     }

     public List<Produto> listarProdutosEmPromocao() {
          Query query = em.createQuery("Select p From Produto p where p.promocao = 1");
          List<Produto> ps = query.getResultList();
          Collections.shuffle(ps);
          return ps;
     }

     public double getTotalReg() {
          return totalReg;
     }

     public void setTotalReg(double totalReg) {
          this.totalReg = totalReg;
     }

     public double getNumPaginas() {
          return numPaginas;
     }

     public void setNumPaginas(double numPaginas) {
          this.numPaginas = numPaginas;
     }
}
