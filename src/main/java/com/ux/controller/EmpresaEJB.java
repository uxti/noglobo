/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ux.controller;

import com.google.gson.Gson;
import com.moip.pojo.Address;
import com.moip.pojo.Cliente;
import com.moip.pojo.Email;
import com.moip.pojo.Person;
import com.moip.pojo.Phone;
import com.moip.pojo.TaxDocument;
import com.ux.config.Config;
import com.ux.facade.FacadeEJB;
import com.ux.model.Comentario;
import com.ux.model.Compra;
import com.ux.model.Empresa;
import com.ux.model.Foto;
import com.ux.model.Frete;
import com.ux.model.Produto;
import com.ux.model.Tags;
import com.ux.model.Usuario;
import com.ux.util.UXUtil;
import java.io.File;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.Query;

/**
 *
 * @author Renato
 */
@Stateless
public class EmpresaEJB extends FacadeEJB<Empresa> {

     public EmpresaEJB() {
          super(Empresa.class);
     }

     public boolean verificarEmailExiste(String email) {
          Query q = em.createQuery("Select e From Usuario e where e.email = :em");
          q.setParameter("em", email);
          try {
               Usuario e = (Usuario) q.getSingleResult();
               return true;
          } catch (javax.persistence.NoResultException e) {
               return false;
          }
     }

     public void salvarEmpresa(Empresa emp) {
          System.out.println(emp.getValorEntrega());
          em.merge(emp);
          for (Foto f : emp.getFotoList()) {
               f.setIdEmpresa(emp);
               em.merge(f);
          }
//        for (Produto p : emp.getProdutoList()) {
//            p.setSlug(p.getNome().replace(" ", "-"));
//            p.setIdEmpresa(emp);
//            em.merge(p);
//            for (Foto f : p.getFotoList()) {
//                f.setIdProduto(p);
//                em.merge(f);
//            }
//        }
     }

     public Empresa listarEmpresasPatrocinadas(String param) {
          Query query = em.createQuery("Select e From Empresa e where e.linkPatrocinado = 1 ");
//        query.setParameter("par", "%" + param + "%");
          List<Empresa> emps = new ArrayList<Empresa>();
          emps = query.getResultList();
//        System.out.println("teve no patrocinados" + emps);
          if (emps.isEmpty()) {
               return new Empresa();
          } else {
               Collections.shuffle(emps);
               return emps.get(0);
          }
     }

     public void excluirProduto(Produto p) {
          p = em.getReference(Produto.class, p.getIdProduto());
          new File(Config.caminhoProduto + "\\" + p.getFotoPrincipal()).delete();
          for (Foto f : p.getFotoList()) {
               f = em.getReference(Foto.class, f.getIdFoto());
               new File(Config.caminhoProduto + "\\" + f.getUrl()).delete();
               em.remove(f);
          }
//        for (Frete frete : p.getFreteList()) {
//            frete = em.getReference(Frete.class, frete.getIdFrete());
//            em.remove(frete);
//        }
//        for (Tags t : p.getTagsList()) {
//            t = em.getReference(Tags.class, t.getIdTag());
//            em.remove(t);
//        }
          em.remove(p);
     }

     public Empresa getEmpresaByIdMoip(String id) {
          Query query = em.createQuery("Select e From Empresa e where e.idMoip = :idm");
          query.setParameter("idm", id);
          return (Empresa) query.getSingleResult();
     }
     
     public List<Empresa> empresasQueVendem(){
         Query query = em.createQuery("Select e From Empresa e ");
         return query.getResultList();
     }

     public void excluirEmpresa(Empresa emp) {
          emp = em.getReference(Empresa.class, emp.getIdEmpresa());
          for (Produto p : emp.getProdutoList()) {
               p = em.getReference(Produto.class, p.getIdProduto());
               new File(Config.caminhoProduto + "\\" + p.getFotoPrincipal()).delete();
               for (Foto f : p.getFotoList()) {
                    f = em.getReference(Foto.class, f.getIdFoto());
                    new File(Config.caminhoProduto + "\\" + f.getUrl()).delete();
                    em.remove(f);
               }
               for (Frete frete : p.getFreteList()) {
                    frete = em.getReference(Frete.class, frete.getIdFrete());
                    em.remove(frete);
               }
               for (Tags t : p.getTagsList()) {
                    t = em.getReference(Tags.class, t.getIdTag());
                    em.remove(t);
               }
               em.remove(p);
          }
          new File(Config.caminhoEmpresa + "\\" + emp.getCapa()).delete();
          new File(Config.caminhoEmpresa + "\\" + emp.getLogo()).delete();
          for (Foto f : emp.getFotoList()) {
               f = em.getReference(Foto.class, f.getIdFoto());
               new File(Config.caminhoEmpresa + "\\" + f.getUrl()).delete();
               em.remove(f);
          }
          Usuario u = em.getReference(Usuario.class, emp.getIdUsuario().getIdUsuario());
          new File(Config.caminhoUsuario + "\\" + u.getImagem()).delete();
          em.remove(u);
          em.remove(emp);
     }

     public Integer afterID() {
          Query query = em.createQuery("select MAX(c.idEmpresa) From Empresa c");
          return 1 + (Integer) query.getSingleResult();
     }

     public Empresa getEmpresaBySlug(String slug) {
          Query q = em.createQuery("Select e From Empresa e where e.slug = :slug");
          q.setParameter("slug", slug);
          return (Empresa) q.getSingleResult();
     }

     public List<Empresa> pesquisarEmpresas(String param, String tipo, String cidade) {
          if (param == null || param == "") {
               return new ArrayList<Empresa>();
          } else {
               Query q = em.createQuery("Select e FROM Empresa e where e.idCidade.slug = :slug and e.nome LIKE :pa");
               q.setParameter("pa", "%" + param + "%");
               q.setParameter("slug", cidade);
               List<Empresa> empresas = q.getResultList();
               Collections.shuffle(empresas);
               return empresas;
          }
     }

     public Number contarEmpresasRegistro(String clausula) {
          em.getEntityManagerFactory().getCache().evictAll();
          Query query = em.createQuery("Select COUNT(p.idEmpresa) From Empresa p " + clausula);
          return (Number) query.getSingleResult();
     }

     public List<Empresa> listarEmpresasLazyModeWhere(int primeiro, int qtd, String clausula) {
          em.getEntityManagerFactory().getCache().evictAll();
          Query query = em.createQuery("Select p From Empresa p " + clausula);
          query.setFirstResult(primeiro);
          query.setMaxResults(qtd);
          return query.getResultList();
     }

     public List<String> listarTagsEmpresas(String param) {
          Query query = em.createQuery("Select s.tagKeywords From Empresa s where s.tagKeywords like :par");
          query.setParameter("par", "%" + param + "%");
          List<String> retornos = query.getResultList();
          List<String> saida = query.getResultList();
          StringBuilder sb = new StringBuilder();
          for (String s : retornos) {
               saida = UXUtil.delimitar(s, ",");
          }
          return saida;

     }

     public String registroMoip(String email, String cpf, String rua, String numero, String bairro, String cep, String cidade, String estado, String ddd, String fone,
             String nome, String sobrenome, String url, String oauth) throws UnsupportedEncodingException, IOException {
          if (email == null) {
               return "Informe o email!";
          } else if (cpf == null) {
               return "Informe o cpf!";
          } else if (rua == null) {
               return "Informe a rua!";
          } else if (numero == null) {
               return "Informe o número!";
          } else if (bairro == null) {
               return "Informe o bairro!";
          } else if (cep == null) {
               return "Informe o cep!";
          } else if (cidade == null) {
               return "Informe a cidade!";
          } else if (estado == null) {
               return "Informe o estado!";
          } else if (ddd == null) {
               return "Informe o ddd!";
          } else if (fone == null) {
               return "Informe o telefone!";
          } else if (nome == null) {
               return "Informe o nome!";
          } else if (sobrenome == null) {
               return "Informe o sobrenome!";
          } else if (url == null) {
               return "Informe a url da config!";
          } else if (oauth == null) {
               return "Informe o oauth!";
          } else {
               Cliente cliente = new Cliente();
               cliente.setEmail(new Email(email));
               TaxDocument taxDocument = new TaxDocument("CPF", cpf);
               Address address = new Address(rua, numero, bairro, cep, cep, cidade, estado, "BRA");
               Phone phone = new Phone("55", ddd, fone);
               Person person = new Person();
               person.setName(nome.length() > 8 ? nome.substring(0, 8) : nome);
               person.setLastName(sobrenome.length() > 8 ? sobrenome.substring(0, 8) :  sobrenome);
               person.setTaxDocument(taxDocument);
               person.setBirthDate("1990-01-01");
               person.setAddress(address);
               person.setPhone(phone);
               cliente.setPerson(person);
               cliente.setType("MERCHANT");
               Gson gson = new Gson();
               String json = gson.toJsonTree(cliente).toString();
               System.out.println(json);
               System.out.println("\n\n\n\n");
               return UXUtil.post(json, url, oauth);
          }
     }

     public List<Compra> listarVendasPorEmpresa(Empresa e) {
          Query query = em.createQuery("Select e From Compra e where e.idEmpresa.idEmpresa = :ide");
          query.setParameter("ide", e.getIdEmpresa());
          return query.getResultList();
     }

     public List<Comentario> listarComentarios(Empresa e) {
//        System.out.println("teve aqui");
//        System.out.println(e.getIdEmpresa());
          Query query = em.createQuery("Select c From Comentario c where c.idProduto.idEmpresa.idEmpresa = :ide and c.tipo = 'C' and c.idComentarioReplica is null ORDER BY c.dtComentario DESC");
          query.setParameter("ide", e.getIdEmpresa());
//        System.out.println(query.getResultList().size());
          return query.getResultList();
     }
}
