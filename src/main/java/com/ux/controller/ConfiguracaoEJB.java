/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ux.controller;

import com.ux.facade.FacadeEJB;
import com.ux.model.Configuracao;
import javax.ejb.Stateless;

/**
 *
 * @author Renato
 */
@Stateless
public class ConfiguracaoEJB extends FacadeEJB<Configuracao> {

     public ConfiguracaoEJB() {
          super(Configuracao.class);
     }
}
