/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ux.controller;

import com.ux.facade.FacadeEJB;
import com.ux.model.Cliente;
import javax.ejb.Stateless;
import javax.persistence.Query;

/**
 *
 * @author Renato
 */
@Stateless
public class ClienteEJB extends FacadeEJB<Cliente> {

    public ClienteEJB() {
        super(Cliente.class);
    }

    public boolean verificarCPFExiste(String CPF) {
        Query q = em.createQuery("Select e From Cliente e where e.cpfcnpj = :em");
        q.setParameter("em", CPF);
        try {
            Cliente e = (Cliente) q.getSingleResult();
            return true;
        } catch (javax.persistence.NoResultException e) {
            return false;
        }
    }
}
