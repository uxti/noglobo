/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ux.controller;

import com.ux.facade.FacadeEJB;
import com.ux.model.Estado;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.Query;

/**
 *
 * @author JOAOPAULO
 */
@Stateless
public class EstadoEJB extends FacadeEJB<Estado> {

    public EstadoEJB() {
        super(Estado.class);
    }

    public List<Estado> pesquisarEstado(String sigla) {
        Query q = em.createQuery("Select e From Estado e where e.sigla = :sig");
        q.setParameter("sig", sigla);
        return q.getResultList();
    }
}
