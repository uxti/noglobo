/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ux.controller;

import com.ux.facade.FacadeEJB;
import com.ux.model.Comentario;
import com.ux.model.Empresa;
import java.util.ArrayList;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.Query;

/**
 *
 * @author Renato
 */
@Stateless
public class ComentarioEJB extends FacadeEJB<Comentario> {

    public ComentarioEJB() {
        super(Comentario.class);
    }

    public List<Comentario> listarComentariosSemRespostas(Empresa e) {
        if (e != null) {
            Query q = em.createQuery("SELECT c From Comentario c where c.idComentarioReplica is null and c.idProduto.idEmpresa.idEmpresa = :ide and c.tipo = 'C'");
            q.setParameter("ide", e.getIdEmpresa());
            return q.getResultList();
        } else {
            return new ArrayList<Comentario>();
        }
    }

    public void inserirComentarioResposta(Comentario c) {
        Query q = em.createNamedQuery("INSERT INTO COMENTARIO SET  id_produto = :pro, comentario = :com, dt_comentario = :dt, id_cliente = :cli , tipo = 'R'");
        q.setParameter("pro", c.getIdProduto().getIdProduto());
        q.setParameter("com", c.getComentario());
        q.setParameter("dt", c.getDtComentario());
        q.setParameter("cli", c.getIdCliente().getIdCliente());
        q.executeUpdate();
    }
}
