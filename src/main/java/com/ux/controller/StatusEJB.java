/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ux.controller;

import com.ux.facade.FacadeEJB;
import com.ux.model.Status;
import javax.ejb.Stateless;

/**
 *
 * @author JOAOPAULO
 */
@Stateless
public class StatusEJB extends FacadeEJB<Status> {

    public StatusEJB(){
        super(Status.class);
    }

}
