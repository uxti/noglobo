/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ux.controller;

import com.ux.util.Conexao;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.Query;

/**
 *
 * @author Charles
 */
@Stateless
public class BuscaEJB extends Conexao{

    public List<String> pesquisarSugestoesBuscaProdutos(String busca){
        Query q = em.createQuery("SELECT p.nome FROM Produto p WHERE  p.slug LIKE '%" + busca + "%' or p.nome LIKE '%" + busca + "%' ORDER by p.nome");
        return q.getResultList();
    }
    
    public List<String> pesquisarSugestoesBuscaEmpresas(String busca){
        Query q = em.createQuery("SELECT e.nome FROM Empresa e WHERE  e.slug LIKE '%" + busca + "%' or e.nome LIKE '%" + busca + "%' ORDER by e.nome");
        return q.getResultList();
    }
    
    public List<String> pesquisarSugestoesBuscaServiços(String busca){
        Query q = em.createQuery("SELECT p.nome FROM Produto p WHERE  p.tipo = 'servicos' and (p.slug LIKE '%" + busca + "%' or p.nome LIKE '%" + busca + "%') ORDER by p.nome");
        return q.getResultList();
    }

}
