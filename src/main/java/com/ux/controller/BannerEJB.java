/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ux.controller;

import com.ux.facade.FacadeEJB;
import com.ux.model.Banner;
import javax.ejb.Stateless;
import javax.persistence.Query;

/**
 *
 * @author Renato
 */
@Stateless
public class BannerEJB extends FacadeEJB<Banner> {

    public BannerEJB() {
        super(Banner.class);
    }

    public Integer afterID() {
        Query query = em.createQuery("select MAX(c.idBanner) From Banner c");
        try {
            return 1 + (Integer) query.getSingleResult();
        } catch (NullPointerException e) {
            return 1;
        }

    }
}
