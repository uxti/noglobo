/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ux.controller;

import com.ux.facade.FacadeEJB;
import com.ux.model.Cliente;
import com.ux.model.Empresa;
import com.ux.model.Perfil;
import com.ux.model.Usuario;
import com.ux.util.Security;
import java.util.List;
import javax.ejb.Stateless;
import javax.faces.context.FacesContext;
import javax.faces.view.facelets.FaceletContext;
import javax.persistence.NoResultException;
import javax.persistence.Query;

/**
 *
 * @author Renato
 */
@Stateless
public class UsuarioEJB extends FacadeEJB<Usuario> {

     public UsuarioEJB() {
          super(Usuario.class);
     }

     public List<Perfil> listarPerfis() {
          return em.createQuery("Select p From Perfil p").getResultList();
     }

     public Perfil perfilEmpresa() {
          Query q = em.createQuery("Select p From Perfil p where p.descricao= 'Empresa'");
          q.setMaxResults(1);
          return (Perfil) q.getSingleResult();
     }

     public Usuario getUserByName(String email, String senha) {
          Query q = em.createQuery("Select e From Usuario e where e.email = :em and e.senha = :pas");
          q.setParameter("em", email);
          q.setParameter("pas", senha);
          try {
               return (Usuario) q.getSingleResult();
          } catch (NoResultException e) {
               return new Usuario();
          }
     }

     public Usuario getUserByEmail(String email) {
          Query q = em.createQuery("Select e From Usuario e where e.email = :em");
          q.setParameter("em", email);
          try {
               return (Usuario) q.getSingleResult();
          } catch (NoResultException e) {
               return new Usuario();
          }
     }

     public Usuario usuarioLogado() {
          FaceletContext faceletContext = (FaceletContext) FacesContext.getCurrentInstance().getAttributes().get(FaceletContext.FACELET_CONTEXT_KEY);
          String formId = (String) faceletContext.getAttribute("usuarioLogado");
          if (formId != null) {
               Query q = em.createQuery("Select u From Usuario u where u.email = :em");
               q.setParameter("em", formId);
               return (Usuario) q.getSingleResult();
          } else {
               return new Usuario();
          }
     
     }
     
     
     public boolean verificarEmailExiste(String email) {
          Query q = em.createQuery("Select e From Usuario e where e.email = :em");
          q.setParameter("em", email);
          try {
               Usuario e = (Usuario) q.getSingleResult();
               return true;
          } catch (javax.persistence.NoResultException e) {
               return false;
          }
     }
     
     public Empresa EmpresaLogado() {
          FaceletContext faceletContext = (FaceletContext) FacesContext.getCurrentInstance().getAttributes().get(FaceletContext.FACELET_CONTEXT_KEY);
          try {
               String formId = (String) faceletContext.getAttribute("usuarioLogado");
               if (formId != null) {
                    Query q = em.createQuery("Select u From Usuario u where u.email = :em");
                    q.setParameter("em", formId);
                    Usuario u = (Usuario) q.getSingleResult();
                    return u.getIdEmpresa();
               } else {
                    return new Empresa();
               }
          } catch (NullPointerException e) {
               return new Empresa();
          }

     }

     public Integer afterID() {
          Query query = em.createQuery("select MAX(c.idUsuario) From Usuario c");
          return 1 + (Integer) query.getSingleResult();
     }

     public void cadastrarUsuario(Usuario u, Cliente c) throws Exception {
          Query perfil = em.createQuery("Select p From Perfil p where p.descricao = 'Cliente'");
          perfil.setMaxResults(1);
          Perfil p = (Perfil) perfil.getSingleResult();
          u.setIdPerfil(p);
          u.setIdCliente(c);
          u.setSenha(Security.encrypt(u.getSenha()));
          em.merge(c);
          em.merge(u);
     }
}
