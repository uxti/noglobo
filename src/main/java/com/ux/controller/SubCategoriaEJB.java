/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ux.controller;

import com.ux.facade.FacadeEJB;
import com.ux.model.SubCategoria;
import javax.ejb.Stateless;

/**
 *
 * @author JOAOPAULO
 */
@Stateless
public class SubCategoriaEJB extends FacadeEJB<SubCategoria> {

    public SubCategoriaEJB(){
        super(SubCategoria.class);
    }

}
