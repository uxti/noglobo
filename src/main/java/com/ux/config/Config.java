/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ux.config;

/**
 *
 * @author Renato
 */
public class Config {

    public static final String caminhoProduto = "C:\\noglobo\\upload\\Produto\\";
    public static final String caminhoNoImage = "C:\\noglobo\\upload\\no-image.jpg";
    public static final String caminhoEmpresa = "C:\\noglobo\\upload\\Empresa\\";
    public static final String caminhoUsuario = "C:\\noglobo\\upload\\Usuario\\";
    public static final String caminhoCidade  = "C:\\noglobo\\upload\\Cidade\\";
    public static final String caminhoBanner  = "C:\\noglobo\\upload\\Banner\\";
    public static final String host = "";
    public static final String user = "";
    public static final String pass = "";
    public static final String base = "";
    public static final String moipCriarConta = "https://sandbox.moip.com.br/v2/accounts";
    
    public static final String oAuth = "OAuth sfmkjvcp5z5f0bkbfxy97ml1vu2lj22";
    public static final String appID = "APP-8R23TXP8TCIU"; //Informar o app gerado do aplicativo
    public static final String secretApp = "n76hivo25sx12e02ot4y7avm98r8vk9";
    public static final String idMoip = "MPA-CF70BEC961BB"; //MPA da Conta noGlobo neste caso ela é secundário no recebimento de pedidos
    
    public static final String endpoint = "https://sandbox.moip.com.br";
    
    public static final String uriRetorno = "http://localhost:8080/noglobo/Restrito/Registro/"; //deve ser a mesma registrada no app
    public static final String linkRegApp = "https://sandbox.moip.com.br/oauth/authorize?responseType=CODE&appId=" + appID + "&redirectUri=" + uriRetorno + "&scope=CREATE_ORDERS|VIEW_ORDERS|CREATE_PAYMENTS|VIEW_PAYMENTS";
    
    public static final String indexModel = "C:\\noglobo\\upload\\index.xhtml" ;
}
