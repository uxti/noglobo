/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ux.config;

import com.ux.controller.CompraEJB;
import com.ux.model.Compra;
import com.ux.model.Usuario;
import com.ux.pojo.Pedido;
import java.math.BigDecimal;
import java.util.List;
import javax.ejb.EJB;

/**
 *
 * @author Renato
 */
public class Configuracoes {

     public static String email = "renato@uxsolucoes.com.br";
     public static String host = "mail.uxsolucoes.com.br";
     public static String nomeDe = "noGlobo";
     public static String senha = "12345";

     public static String mensagemEnvioDelivery(Pedido p, List<Compra> cs) {
          StringBuilder sb = new StringBuilder();
          sb.append("<center><img src=\"http://noglobo.com.br/img/logomarca.png\"</center>");
          sb.append("<br/>");
          sb.append("<h3>Olá " + p.getUsuario().getIdCliente().getPrimeiroNome() + "</h3>");
          sb.append("O seu pedido Nº " + p.getLote() + " foi enviado."
                  + "<br/> Dentro de alguns minutos receberá em casa seu produto<br/>"
                  + "Agradecemos pela preferência<br/>");


          sb.append("<br/>");
          sb.append("<h2>Produtos</h2>");
          sb.append("<table width=\"100%\" cellspacing=\"0\" border=\"0\" cellpadding=\"0\" "
                  + "style=\"border-collapse:collapse;\""
                  + " class=\"ecxtable-order ecxinfo\">");
          sb.append("<thead><tr ><th style=\"text-align:left\">Produto</th><th style=\"text-align:left\">Quantidade</th><th style=\"text-align:left\">Valor</th></tr></thead>");
          sb.append("<tbody>");
          for (Compra com : cs) {
               sb.append("<tr>");
               sb.append("<td>" + com.getIdProduto().getNome() + "</td>");
               sb.append("<td>" + com.getQuantidade() + "</td>");
               sb.append("<td>" + com.getValorCompra() + "</td>");
               sb.append("</tr>");
          }
          sb.append("</tbody>");
          sb.append("</table>");

          return sb.toString();
     }

     public static String mensagemRetiradaDelivery(Pedido p, List<Compra> cs) {
          StringBuilder sb = new StringBuilder();
          sb.append("<center><img src=\"http://noglobo.com.br/img/logomarca.png\"</center>");
          sb.append("<br/>");
          sb.append("<h3>Olá " + p.getUsuario().getIdCliente().getPrimeiroNome() + "</h3>");
          sb.append("O seu pedido Nº " + p.getLote() + " foi retirado.<br/>"
                  + "Agradecemos pela preferência<br/>");
          
           sb.append("<br/>");
          sb.append("<h2>Produtos</h2>");
          sb.append("<table width=\"100%\" cellspacing=\"0\" border=\"0\" cellpadding=\"0\" "
                  + "style=\"border-collapse:collapse;\""
                  + " class=\"ecxtable-order ecxinfo\">");
          sb.append("<thead><tr ><th style=\"text-align:left\">Produto</th><th style=\"text-align:left\">Quantidade</th><th style=\"text-align:left\">Valor</th></tr></thead>");
          sb.append("<tbody>");
          for (Compra com : cs) {
               sb.append("<tr>");
               sb.append("<td>" + com.getIdProduto().getNome() + "</td>");
               sb.append("<td>" + com.getQuantidade() + "</td>");
               sb.append("<td>" + com.getValorCompra() + "</td>");
               sb.append("</tr>");
          }
          sb.append("</tbody>");
          sb.append("</table>");

          return sb.toString();
     }

     public static String mensagemDelivery(Compra c, List<Compra> cs) {
          StringBuilder sb = new StringBuilder();
          sb.append("<center><img src=\"http://noglobo.com.br/img/logomarca.png\"</center>");
          sb.append("<br/>");
          sb.append("<h3>Olá " + c.getIdUsuario().getIdCliente().getPrimeiroNome() + "</h3>");
          sb.append("Recebemos o seu pedido Nº " + c.getLote() + " e agora aguardamos a confirmação do pagamento. Assim que confirmado, iniciaremos sua entrega. "
                  + "<br/> Veja abaixo informações sobre o seu pedido:<br/>");

          sb.append("<table width=\"100%\" cellspacing=\"0\" border=\"0\" cellpadding=\"0\" "
                  + "style=\"border-collapse:collapse;\""
                  + " class=\"ecxtable-order ecxinfo\">"
                  + "<thead class=\"ecxfirst\"><tr><th align=\"center\" bgcolor=\"#f4f4f6\" "
                  + "style=\"background:#f4f4f6;border:2px solid white;color:#333333;font-family:'trebuchet ms', "
                  + "sans-serif;font-size:12px;padding:10px 15px;text-align:left;\" colspan=\"2\">"
                  + "Informações do pedido "
                  + "Nº" + c.getLote()
                  + "</th>"
                  + "</tr></thead><tbody><tr>"
                  + "<td style=\"background:#f4f4f6;border-collapse:collapse;border:2px solid white;color:#333333;font-family:"
                  + "'trebuchet ms', sans-serif;font-size:12px;padding:10px 15px;vertical-align:top;\" "
                  + "bgcolor=\"#f4f4f6\" valign=\"top\"><p style=\"color:#333333 !important;line-height:1.3 !important;\" "
                  + "class=\"ecxtitle-address\"><strong>Endereço Principal</strong></p><p style=\"color:#333333 !important;line-height:1.3"
                  + " !important;\">"
                  + "<strong>" + c.getIdUsuario().getIdCliente().getPrimeiroNome() + " " + c.getIdUsuario().getIdCliente().getSobrenome() + ""
                  + "</strong>"
                  + "<br>"
                  + "</p> Endereço do estabelecimento "
                  + "Bairro: " + c.getIdEmpresa().getBairro()
                  + "<br>				"
                  + "Cidade: " + c.getIdEmpresa().getIdCidade().getNome() + "&nbsp; - &nbsp;" + c.getIdEmpresa().getIdEstado().getSigla()
                  + "<br>				"
                  + "Brasil			"
                  + "</td>			"
                  + "<td style=\"background:#f4f4f6;border-collapse:collapse;border:2px solid white;color:#333333;"
                  + "font-family:'trebuchet ms', sans-serif;font-size:12px;padding:10px 15px;vertical-align:top;"
                  + "\" valign=\"top\" bgcolor=\"#f4f4f6\">				"
                  + "<p style=\"color:#333333 !important;line-height:1.3 !important;\" "
                  + "class=\"ecxtitle-address\"><strong>"
                  + "Forma de pagamento"
                  + "</strong>"
                  + "</p>				"
                  + "<p style=\"color:#333333 !important;line-height:1.3 !important;\">			"
                  + c.getFormaPagamento() + "<br>					"
                  + c.getNumParcela() + " x R$ " + c.getValorTotal().divide(new BigDecimal(c.getNumParcela())) + "<br>					"
                  + "2,99% am<br>							"
                  + "</p>			"
                  + "</td>		"
                  + "</tr>	"
                  + "</tbody>"
                  + "</table>");

          sb.append("<br/>");
          sb.append("<h2>Produtos</h2>");
          sb.append("<table width=\"100%\" cellspacing=\"0\" border=\"0\" cellpadding=\"0\" "
                  + "style=\"border-collapse:collapse;\""
                  + " class=\"ecxtable-order ecxinfo\">");
          sb.append("<thead><tr ><th style=\"text-align:left\">Produto</th><th style=\"text-align:left\">Quantidade</th><th style=\"text-align:left\">Valor</th></tr></thead>");
          sb.append("<tbody>");
          for (Compra com : cs) {
               sb.append("<tr>");
               sb.append("<td>" + com.getIdProduto().getNome() + "</td>");
               sb.append("<td>" + com.getQuantidade() + "</td>");
               sb.append("<td>" + com.getValorCompra() + "</td>");
               sb.append("</tr>");
          }
          sb.append("</tbody>");
          sb.append("</table>");


          return sb.toString();
     }

     public static String mensagem(Compra c, List<Compra> cs) {
          StringBuilder sb = new StringBuilder();
          sb.append("<center><img src=\"http://noglobo.com.br/img/logomarca.png\"</center>");
          sb.append("<br/>");
          sb.append("<h3>Olá " + c.getIdUsuario().getIdCliente().getPrimeiroNome() + "</h3>");
          sb.append("Recebemos o seu pedido Nº " + c.getLote() + " e agora aguardamos a confirmação do pagamento. Assim que confirmado, iniciaremos sua entrega. "
                  + "<br/> Veja abaixo informações sobre o seu pedido:<br/>");



          sb.append("<table width=\"100%\" cellspacing=\"0\" border=\"0\" cellpadding=\"0\" "
                  + "style=\"border-collapse:collapse;\""
                  + " class=\"ecxtable-order ecxinfo\">"
                  + "<thead class=\"ecxfirst\"><tr><th align=\"center\" bgcolor=\"#f4f4f6\" "
                  + "style=\"background:#f4f4f6;border:2px solid white;color:#333333;font-family:'trebuchet ms', "
                  + "sans-serif;font-size:12px;padding:10px 15px;text-align:left;\" colspan=\"2\">"
                  + "Informações do pedido "
                  + "Nº" + c.getLote()
                  + "</th>"
                  + "</tr></thead><tbody><tr>"
                  + "<td style=\"background:#f4f4f6;border-collapse:collapse;border:2px solid white;color:#333333;font-family:"
                  + "'trebuchet ms', sans-serif;font-size:12px;padding:10px 15px;vertical-align:top;\" "
                  + "bgcolor=\"#f4f4f6\" valign=\"top\"><p style=\"color:#333333 !important;line-height:1.3 !important;\" "
                  + "class=\"ecxtitle-address\"><strong>Endereço Principal</strong></p><p style=\"color:#333333 !important;line-height:1.3"
                  + " !important;\">"
                  + "<strong>" + c.getIdUsuario().getIdCliente().getPrimeiroNome() + " " + c.getIdUsuario().getIdCliente().getSobrenome() + ""
                  + "</strong>"
                  + "<br>"
                  + c.getEnderecoEntrega() + ", " + c.getNumeroEntrega()
                  + "</p>					"
                  + "Bairro: " + c.getBairroEntrega()
                  + "<br>				"
                  + "Cidade: " + c.getIdCidadeEntrega().getNome() + "&nbsp; - &nbsp;" + c.getIdEstadoEntrega().getSigla()
                  + "<br>				"
                  + "CEP: " + c.getCepEntrega()
                  + "<br>				"
                  + "Brasil			"
                  + "</td>			"
                  + "<td style=\"background:#f4f4f6;border-collapse:collapse;border:2px solid white;color:#333333;"
                  + "font-family:'trebuchet ms', sans-serif;font-size:12px;padding:10px 15px;vertical-align:top;"
                  + "\" valign=\"top\" bgcolor=\"#f4f4f6\">				"
                  + "<p style=\"color:#333333 !important;line-height:1.3 !important;\" "
                  + "class=\"ecxtitle-address\"><strong>"
                  + "Forma de pagamento"
                  + "</strong>"
                  + "</p>				"
                  + "<p style=\"color:#333333 !important;line-height:1.3 !important;\">			"
                  + c.getFormaPagamento() + "<br>					"
                  + c.getNumParcela() + " x R$ " + c.getValorTotal().divide(new BigDecimal(c.getNumParcela())) + "<br>					"
                  + "2,99% am<br>							"
                  + "</p>			"
                  + "</td>		"
                  + "</tr>	"
                  + "</tbody>"
                  + "</table>");

          sb.append("<br/>");
          sb.append("<h2>Produtos</h2>");
          sb.append("<table width=\"100%\" cellspacing=\"0\" border=\"0\" cellpadding=\"0\" "
                  + "style=\"border-collapse:collapse;\""
                  + " class=\"ecxtable-order ecxinfo\">");
          sb.append("<thead><tr ><th style=\"text-align:left\">Produto</th><th style=\"text-align:left\">Quantidade</th><th style=\"text-align:left\">Valor</th></tr></thead>");
          sb.append("<tbody>");
          for (Compra com : cs) {
               sb.append("<tr>");
               sb.append("<td>" + com.getIdProduto().getNome() + "</td>");
               sb.append("<td>" + com.getQuantidade() + "</td>");
               sb.append("<td>" + com.getValorCompra() + "</td>");
               sb.append("</tr>");
          }
          sb.append("</tbody>");
          sb.append("</table>");
          sb.append("<br/><br/>");

          sb.append("<table width=\"100%\" cellspacing=\"0\" border=\"0\" cellpadding=\"0\" "
                  + "style=\"border-collapse:collapse;\" class=\"ecxresult\"> "
                  + "<tbody><tr>"
                  + "<td style=\"background:#f4f4f6;border-collapse:collapse;border:2px solid white;"
                  + "color:#333333;font-family:'trebuchet ms', sans-serif;font-size:12px;font-weight:bold;padding:10px 15px;\""
                  + " bgcolor=\"#f4f4f6\">"
                  + "Prazo de entrega&nbsp;noGlobo"
                  + "</td><td style=\"background:#e3e3e3;border-collapse:collapse;border:2px solid white;"
                  + "color:#333333;font-family:'trebuchet ms', sans-serif;font-size:12px;font-weight:bold;"
                  + "padding:10px 15px;width:130px;\" bgcolor=\"#e3e3e3\" class=\"ecxtotal\">"
                  + c.getFrete()
                  + "</td></tr><tr><td style=\"background:#f4f4f6;border-collapse:collapse;border:2px solid white;color:#333333;"
                  + "font-family:'trebuchet ms', sans-serif;font-size:11px;font-weight:bold;padding:10px 15px;\""
                  + " class=\"ecxmi\" colspan=\"2\" bgcolor=\"#f4f4f6\">"
                  + "O prazo é sempre informado em dias úteis e começa a ser contado a partir da data de confirmação do pagamento."
                  + "</td></tr></tbody>"
                  + "</table>");
          System.out.println(sb);
          return sb.toString();
     }

     public static String mensagemAutorizacaoPagamento(Compra c, List<Compra> cs) {
          StringBuilder sb = new StringBuilder();
          sb.append("<center><img src=\"http://noglobo.com.br/img/logomarca.png\"</center>");
          sb.append("<br/>");
          sb.append("<h3>Olá " + c.getIdUsuario().getIdCliente().getPrimeiroNome() + "</h3>");
          sb.append("Está confirmado o pagamento do pedido Nº " + c.getLote() + ". "
                  + "<br/> Sua mercadoria será enviada! Após o envio você receberá um email para acompanhar a entrega!");
          
          
           sb.append("<br/>");
          sb.append("<h2>Produtos</h2>");
          sb.append("<table width=\"100%\" cellspacing=\"0\" border=\"0\" cellpadding=\"0\" "
                  + "style=\"border-collapse:collapse;\""
                  + " class=\"ecxtable-order ecxinfo\">");
          sb.append("<thead><tr ><th style=\"text-align:left\">Produto</th><th style=\"text-align:left\">Quantidade</th><th style=\"text-align:left\">Valor</th></tr></thead>");
          sb.append("<tbody>");
          for (Compra com : cs) {
               sb.append("<tr>");
               sb.append("<td>" + com.getIdProduto().getNome() + "</td>");
               sb.append("<td>" + com.getQuantidade() + "</td>");
               sb.append("<td>" + com.getValorCompra() + "</td>");
               sb.append("</tr>");
          }
          sb.append("</tbody>");
          sb.append("</table>");
          return sb.toString();
     }

     public static String mensagemAutorizacaoPagamentoDelivery(Compra c, List<Compra> cs) {
          StringBuilder sb = new StringBuilder();
          sb.append("<center><img src=\"http://noglobo.com.br/img/logomarca.png\"</center>");
          sb.append("<br/>");
          sb.append("<h3>Olá " + c.getIdUsuario().getIdCliente().getPrimeiroNome() + "</h3>");
          sb.append("Está confirmado o pagamento do pedido Nº " + c.getLote() + ". "
                  + "<br/> Caso deseje retirar o produto no local ultilize o código <strong>" + c.getCodigoRetirada() + "</strong>");
          
           sb.append("<br/>");
          sb.append("<h2>Produtos</h2>");
          sb.append("<table width=\"100%\" cellspacing=\"0\" border=\"0\" cellpadding=\"0\" "
                  + "style=\"border-collapse:collapse;\""
                  + " class=\"ecxtable-order ecxinfo\">");
          sb.append("<thead><tr ><th style=\"text-align:left\">Produto</th><th style=\"text-align:left\">Quantidade</th><th style=\"text-align:left\">Valor</th></tr></thead>");
          sb.append("<tbody>");
          for (Compra com : cs) {
               sb.append("<tr>");
               sb.append("<td>" + com.getIdProduto().getNome() + "</td>");
               sb.append("<td>" + com.getQuantidade() + "</td>");
               sb.append("<td>" + com.getValorCompra() + "</td>");
               sb.append("</tr>");
          }
          sb.append("</tbody>");
          sb.append("</table>");
          return sb.toString();
     }

     public static String mensagemCancelamentoPedido(Compra c) {
          StringBuilder sb = new StringBuilder();
          sb.append("<center><img src=\"http://noglobo.com.br/img/logomarca.png\"</center>");
          sb.append("<br/>");
          sb.append("<h3>Olá " + c.getIdUsuario().getIdCliente().getPrimeiroNome() + "</h3>");
          sb.append("O seu pedido de nº " + c.getLote() + " foi cancelado!"
                  + "<br/> O motivo foi de nosso sistema não localizar o pagamento!");
          return sb.toString();
     }

     public static String mensagemRecuperacaoSenha(String senha, Usuario u) {
          StringBuilder sb = new StringBuilder();
          sb.append("<center><img src=\"http://noglobo.com.br/img/logomarca.png\"</center>");
          sb.append("<br/>");
          sb.append("<h3>Olá " + u.getIdCliente().getPrimeiroNome() + "</h3>");
          sb.append("<h4>Sua senha foi recuperada com sucesso!<h4>"
                  + "<br/> Senha provisória é <strong>" + senha + "</strong>"
                  + "<br/> Lembre-se de alterá la por sua segurança.");
          return sb.toString();
     }

     public static String mensagemEnvioMercadoria(Compra c, List<Compra> cs) {
          StringBuilder sb = new StringBuilder();
          sb.append("<center><img src=\"http://noglobo.com.br/img/logomarca.png\"</center>");
          sb.append("<br/>");
          sb.append("<h3>Olá " + c.getIdUsuario().getIdCliente().getPrimeiroNome() + "</h3>");
          sb.append("O pedido Nº " + c.getLote() + " foi enviado! "
                  + "<br/> Faça o acompanhamento da sua mercadoria por <a href=\"http://websro.correios.com.br/sro_bin/txect01$.QueryList?P_LINGUA=001&P_TIPO=001&P_COD_UNI=" + c.getCodigoRastreamentoEntrega() + "\" > aqui</a>");
          
           sb.append("<br/>");
          sb.append("<h2>Produtos</h2>");
          sb.append("<table width=\"100%\" cellspacing=\"0\" border=\"0\" cellpadding=\"0\" "
                  + "style=\"border-collapse:collapse;\""
                  + " class=\"ecxtable-order ecxinfo\">");
          sb.append("<thead><tr ><th style=\"text-align:left\">Produto</th><th style=\"text-align:left\">Quantidade</th><th style=\"text-align:left\">Valor</th></tr></thead>");
          sb.append("<tbody>");
          for (Compra com : cs) {
               sb.append("<tr>");
               sb.append("<td>" + com.getIdProduto().getNome() + "</td>");
               sb.append("<td>" + com.getQuantidade() + "</td>");
               sb.append("<td>" + com.getValorCompra() + "</td>");
               sb.append("</tr>");
          }
          sb.append("</tbody>");
          sb.append("</table>");
          return sb.toString();
     }

     public static String mensagemCompra(Compra c, List<Compra> cs) {
          StringBuilder sb = new StringBuilder();
          sb.append("<center><img src=\"http://noglobo.com.br/img/logomarca.png\"</center>");
          sb.append("<br/>");
          sb.append("<h3>Olá " + c.getIdUsuario().getIdCliente().getPrimeiroNome() + "</h3>");
          sb.append("O pedido Nº " + c.getLote() + " foi enviado! "
                  + "<br/> Faça o acompanhamento da sua mercadoria por <a href=\"http://websro.correios.com.br/sro_bin/txect01$.QueryList?P_LINGUA=001&P_TIPO=001&P_COD_UNI=" + c.getCodigoRastreamentoEntrega() + "\" > aqui</a>");
          
           sb.append("<br/>");
          sb.append("<h2>Produtos</h2>");
          sb.append("<table width=\"100%\" cellspacing=\"0\" border=\"0\" cellpadding=\"0\" "
                  + "style=\"border-collapse:collapse;\""
                  + " class=\"ecxtable-order ecxinfo\">");
          sb.append("<thead><tr ><th style=\"text-align:left\">Produto</th><th style=\"text-align:left\">Quantidade</th><th style=\"text-align:left\">Valor</th></tr></thead>");
          sb.append("<tbody>");
          for (Compra com : cs) {
               sb.append("<tr>");
               sb.append("<td>" + com.getIdProduto().getNome() + "</td>");
               sb.append("<td>" + com.getQuantidade() + "</td>");
               sb.append("<td>" + com.getValorCompra() + "</td>");
               sb.append("</tr>");
          }
          sb.append("</tbody>");
          sb.append("</table>");
          return sb.toString();
     }

     public static String getEmail() {
          return email;
     }

     public static String getHost() {
          return host;
     }

     public static String getNomeDe() {
          return nomeDe;
     }

     public static String getSenha() {
          return senha;
     }
}
