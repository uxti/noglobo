/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ux.view;

import com.ux.config.Config;
import com.ux.controller.ComentarioEJB;
import com.ux.controller.EmpresaEJB;
import com.ux.controller.UsuarioEJB;
import com.ux.controller.ProdutoEJB;
import com.ux.model.Comentario;
import com.ux.model.Compra;
import com.ux.model.Empresa;
import com.ux.model.Foto;
import com.ux.model.Frete;
import com.ux.model.Produto;
import com.ux.model.Tags;
import com.ux.model.Usuario;
import com.ux.util.MensageFactory;
import com.ux.util.Security;
import com.ux.util.UXUtil;
import java.io.File;
import java.io.IOException;
import java.io.Serializable;
import java.io.UnsupportedEncodingException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import org.apache.commons.io.FileUtils;
import org.primefaces.context.RequestContext;
import org.primefaces.event.FileUploadEvent;
import org.primefaces.event.FlowEvent;
import org.primefaces.json.JSONException;
import org.primefaces.json.JSONObject;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;
import org.primefaces.model.UploadedFile;

/**
 *
 * @author Renato
 */
@ManagedBean
@ViewScoped
public class EmpresaMB implements Serializable {

     @EJB
     EmpresaEJB eEJB;
     @EJB
     UsuarioEJB uEJB;
     private Empresa empresa;
     private Usuario usuario;
     @EJB
     ProdutoEJB pEJB;
     private Produto produto;
     private UploadedFile file;
     private List<String> diasFuncionamento;
     private String tag;
     private boolean skip;
     private Comentario comentario, comentarioResposta;
     private LazyDataModel<Empresa> empresasLazy;
     private List<Empresa> listaDeEmpresas;
     private String parametroConfirmacaoMoip;
     @EJB
     ComentarioEJB cEJB;

     public EmpresaMB() {
          diasFuncionamento = new ArrayList<String>();
          novo();
     }

     public void pegaPar() {
          Map<String, String> par = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap();
          System.out.println(par);
          parametroConfirmacaoMoip = par.get("code");
          System.out.println(parametroConfirmacaoMoip);

     }

     public void confirmarCadastroMOip() throws UnsupportedEncodingException, IOException, JSONException {
          try {
               String code = UXUtil.getParametros().get("code");
               System.out.println("CODE" + code);
               String json = "{     \"appId\": \"" + Config.appID + " \",     \"appSecret\": \"" + Config.secretApp + "\",    \"redirectUri\": \"http://localhost:8080/noglobo/\",    \"grantType\": \"authorization_code\",    \"code\": \"" + parametroConfirmacaoMoip + "\"}";
               System.out.println(json);
               String retorno = UXUtil.post(json, "https://sandbox.moip.com.br/oauth/accesstoken", Config.oAuth);
               if (json.startsWith("{\"errors\":")) {
                    MensageFactory.warn("Aconteceu um erro! Verifique o log!", null);
               } else {
                    System.out.println(retorno);
                    JSONObject userObject = new JSONObject(retorno);
                    String oauth = userObject.getString("accessToken");
                    String moipIP = userObject.getString("moipAccountId");
                    Empresa em = eEJB.getEmpresaByIdMoip(moipIP);
                    em.setOauth(oauth);
                    em.setConfirmacaoMoip(true);
                    eEJB.Atualizar(em);
                    MensageFactory.info("Registro confirmado! Você já pode começar a vender.", null);

               }
          } catch (Exception e) {
               System.out.println(e);
          }

     }

     public void novo() {
          empresa = new Empresa();
          usuario = new Usuario();
          produto = new Produto();
          comentario = new Comentario();
          comentarioResposta = new Comentario();
          verificarExisteCaminhos();
          listaDeEmpresas = new ArrayList<Empresa>();
          empresasLazy = new LazyDataModel<Empresa>() {
               private static final long serialVersionUID = 1L;

               @Override
               public List<Empresa> load(int first, int pageSize, String sortField, SortOrder sortOrder, Map<String, Object> filters) {
                    String Clausula = "";
                    StringBuffer sf = new StringBuffer();
                    int contar = 0;
                    for (Iterator<String> it = filters.keySet().iterator(); it.hasNext();) {
                         String filterProperty = it.next(); // table column name = field name  
                         Object filterValue = filters.get(filterProperty);
                         if (contar == 0) {
                              if (sf.toString().contains("where")) {
                                   sf.append("and p.ativo = TRUE and p.bloqueado = FALSE and p." + filterProperty + " like'%" + filterValue + "%'");
                              } else {
                                   sf.append(" where p." + filterProperty + " like'%" + filterValue + "%' and p.ativo = TRUE and p.bloqueado = FALSE");
                              }
                         } else {
                              sf.append(" and p." + filterProperty + " like'%" + filterValue + "%' and p.ativo = TRUE and p.bloqueado = FALSE");
                         }
                         contar = contar + 1;
                    }
                    if (sortOrder.toString().contains("ASCENDING") && sortField != null) {
                         sf.append(" order by p." + sortField + " asc");
                    }
                    if (sortOrder.toString().contains("DESCENDING") && sortField != null) {
                         sf.append(" order by p." + sortField + " desc");
                    }
                    Clausula = sf.toString();

                    if (Clausula.contains("_")) {
                         Clausula = Clausula.replace("_", "");
                    }
                    if (Clausula.contains("() -")) {
                         Clausula = Clausula.replace("() -", "");
                    }
                    if (Clausula.contains("..")) {
                         Clausula = Clausula.replace("..", "");
                    }
                    setRowCount(Integer.parseInt(eEJB.contarEmpresasRegistro(Clausula).toString()));
                    listaDeEmpresas = eEJB.listarEmpresasLazyModeWhere(first, pageSize, Clausula);
                    return listaDeEmpresas;
               }
          };
     }

     public void novoProduto() {
          produto = new Produto();
     }

     private void verificarExisteCaminhos() {
          if (!new File(Config.caminhoProduto).exists()) {
               new File(Config.caminhoProduto).mkdirs();
          }
          if (!new File(Config.caminhoCidade).exists()) {
               new File(Config.caminhoCidade).mkdirs();
          }
          if (!new File(Config.caminhoEmpresa).exists()) {
               new File(Config.caminhoEmpresa).mkdirs();
          }
          if (!new File(Config.caminhoUsuario).exists()) {
               new File(Config.caminhoUsuario).mkdirs();
          }
     }

     public List<Empresa> listarEmpresas() {
          return eEJB.ListarTodos();
     }

     public void cadastrarContaMoip(Empresa empresa) throws UnsupportedEncodingException, IOException, JSONException {
          if (empresa.getEmail() == null) {
               MensageFactory.warn("O email é obrigatório para o cadastro moip!", null);
          } else if (empresa.getCpf() == null) {
               MensageFactory.warn("O cpf é obrigatório para o cadastro moip!", null);
          } else if (empresa.getLogradouro() == null) {
               MensageFactory.warn("O logradouro é obrigatório para o cadastro moip!", null);
          } else if (empresa.getNumero() == null) {
               MensageFactory.warn("O número é obrigatório para o cadastro moip!", null);
          } else if (empresa.getBairro() == null) {
               MensageFactory.warn("O bairro é obrigatório para o cadastro moip!", null);
          } else if (empresa.getCep() == null) {
               MensageFactory.warn("O cep é obrigatório para o cadastro moip!", null);
          } else if (empresa.getIdCidade().getNome() == null) {
               MensageFactory.warn("O cidade é obrigatório para o cadastro moip!", null);
          } else if (empresa.getIdEstado().getSigla() == null) {
               MensageFactory.warn("O estado é obrigatório para o cadastro moip!", null);
          } else if (empresa.getDddTelefone() == null) {
               MensageFactory.warn("O ddd é obrigatório para o cadastro moip!", null);
          } else if (empresa.getTelefone() == null) {
               MensageFactory.warn("O telefone é obrigatório para o cadastro moip!", null);
          } else if (empresa.getNome() == null) {
               MensageFactory.warn("O nome é obrigatório para o cadastro moip!", null);
          } else {
               String retorno = eEJB.registroMoip(
                       empresa.getEmail(),
                       empresa.getCpf(),
                       empresa.getLogradouro(),
                       empresa.getNumero(),
                       empresa.getBairro(),
                       empresa.getCep(),
                       empresa.getIdCidade().getNome(),
                       empresa.getIdEstado().getSigla(),
                       empresa.getDddTelefone(),
                       empresa.getTelefone(),
                       empresa.getNome(),
                       empresa.getNome(),
                       Config.moipCriarConta,
                       Config.oAuth);
               System.out.println("retorno" + retorno);
               if (retorno.startsWith("{\"errors\":[{\"code\":\"REG-002\"")) {
                    JSONObject userObject = new JSONObject(retorno);
                    empresa.setLoginMoip(userObject.getJSONObject("additionalInfo").getJSONObject("account").getString("login"));
                    empresa.setIdMoip(userObject.getJSONObject("additionalInfo").getJSONObject("account").getString("id"));
                    empresa.setConfirmacaoMoip(false);
//                    empresa.setLinkSenha(userObject.getJSONObject("_links").getJSONObject("setPassword").getString("href"));
                    eEJB.Atualizar(empresa);
                    String s = "https:\\sandbox.moip.com.br/oauth/authorize?responseType=CODE&appId=" + Config.appID + "&redirectUri=" + Config.uriRetorno + "&scope=CREATE_ORDERS|VIEW_ORDERS|CREATE_PAYMENTS|VIEW_PAYMENTS";
                    ExternalContext externalContext = FacesContext.getCurrentInstance().getExternalContext();
                    externalContext.redirect(s.trim());
//                    System.out.println("Retorno no IF \n");
//                    System.out.println(retorno);
               } else if (retorno.startsWith("{\"errors\":[{\"code\":\"REG-001\"")) {
                    JSONObject userObject = new JSONObject(retorno);
                    empresa.setLoginMoip(userObject.getJSONObject("additionalInfo").getJSONObject("account").getString("login"));
                    empresa.setIdMoip(userObject.getJSONObject("additionalInfo").getJSONObject("account").getString("id"));
                    empresa.setConfirmacaoMoip(false);
//                    empresa.setLinkSenha(userObject.getJSONObject("_links").getJSONObject("setPassword").getString("href"));
                    eEJB.Atualizar(empresa);
                    String s = "https:\\sandbox.moip.com.br/oauth/authorize?responseType=CODE&appId=" + Config.appID + "&redirectUri=" + Config.uriRetorno + "&scope=CREATE_ORDERS|VIEW_ORDERS|CREATE_PAYMENTS|VIEW_PAYMENTS";
                    ExternalContext externalContext = FacesContext.getCurrentInstance().getExternalContext();
                    externalContext.redirect(s.trim());
//                    System.out.println("Retorno no ELSE IF \n");
//                    System.out.println(retorno);
               } else if (retorno.startsWith("{ \"ERROR\" : \"Ops... We were not waiting for it\" }")) {
                    MensageFactory.warn("{ \"ERROR\" : \"Ops... We were not waiting for it\" }", null);
               } else if (retorno.startsWith("Informe")) {
                    MensageFactory.warn(retorno, null);
               } else {
                    JSONObject userObject = new JSONObject(retorno);
                    empresa.setLoginMoip(userObject.getString("id"));
                    empresa.setIdMoip(userObject.getString("id"));
                    empresa.setLinkSenha(userObject.getJSONObject("_links").getJSONObject("setPassword").getString("href"));
                    empresa.setConfirmacaoMoip(false);
                    eEJB.Atualizar(empresa);

////                    String s = "https:\\sandbox.moip.com.br/oauth/authorize?responseType=CODE&appId=" + Config.appID + "&redirectUri=" + Config.uriRetorno + "&scope=CREATE_ORDERS|VIEW_ORDERS|CREATE_PAYMENTS|VIEW_PAYMENTS";
                    ExternalContext externalContext = FacesContext.getCurrentInstance().getExternalContext();
                    externalContext.redirect(empresa.getLinkSenha().trim());
                    MensageFactory.info("A conta desta empresa foi associada ao noGlobo", null);
               }
          }

//        System.out.println(retorno);
     }

     public void verificarCadastroMoip(Empresa e) {
          if (e != null) {
               e = eEJB.SelecionarPorID(e.getIdEmpresa());
               if (e.getConfirmacaoMoip() == false) {
                    RequestContext rc = RequestContext.getCurrentInstance();
                    rc.execute("PF('dlgPasso1').show()");
               }
          }
     }

     public void passo1() throws IOException {
          String s = "https:\\sandbox.moip.com.br/oauth/authorize?responseType=CODE&appId=" + Config.appID + "&redirectUri=" + Config.uriRetorno + "&scope=CREATE_ORDERS|VIEW_ORDERS|CREATE_PAYMENTS|VIEW_PAYMENTS";
          ExternalContext externalContext = FacesContext.getCurrentInstance().getExternalContext();
          externalContext.redirect(s.trim());

     }

     public void regrasCorreios() {
          if (produto.getFormatoEncomenda().equals("3")) {
               produto.setAlturaCm(BigDecimal.ZERO);
          } else if (produto.getFormatoEncomenda().equals("2")) {
               produto.setAlturaCm(BigDecimal.ZERO);
          }
     }

     public void regrasCorreiosAltura() {
          if (!produto.getFormatoEncomenda().equals("3")) {
               if (produto.getAlturaCm().longValue() > 105) {
                    MensageFactory.warn("O valor máximo do altura são 105 centímetros!", "");
//                RequestContext rc = RequestContext.getCurrentInstance();
//                rc.execute("alert('O valor máximo do altura são 105 centímetros!')");
                    produto.setAlturaCm(new BigDecimal(105));
               } else if (produto.getAlturaCm().longValue() < 2) {
                    MensageFactory.warn("O valor mínimo do altura são 2 centímetros!", "");
//                RequestContext rc = RequestContext.getCurrentInstance();
//                rc.execute("alert('O valor mínimo do altura são 2 centímetros!')");
                    produto.setAlturaCm(new BigDecimal(2));
               }

          }
     }

     public void regrasCorreiosLargura() {
          if (produto.getLarguraCm().longValue() > 105) {
               MensageFactory.warn("O valor máximo da largura são 105 centímetros!", "");
               produto.setLarguraCm(new BigDecimal(105));
          } else if (produto.getLarguraCm().longValue() < 11) {
               MensageFactory.warn("O valor mínimo da largura são 11 centímetros!", "");

               produto.setLarguraCm(new BigDecimal(11));
          }
     }

     public void regrasCorreiosDiametro() {
          if (produto.getDiametroCm().longValue() > 91) {
               MensageFactory.warn("O valor máximo do diametro são 91 centímetros!", "");
               produto.setDiametroCm(new BigDecimal(91));
          }
     }

     public boolean eecommerce() {
          if (empresa.getEcommerce() != null) {
               if (empresa.getEcommerce()) {
                    return true;
               } else {
                    return false;
               }
          } else {
               return false;
          }
     }

     public void regrasCorreiosPeso() {
          if (produto.getFormatoEncomenda().equals("3")) {
               if (produto.getPesoKg().longValue() > 1) {
                    produto.setPesoKg(BigDecimal.ONE);
               }
          }
     }

     public void validarComprimento() {
          if (produto.getComprimentoCm().longValue() > 105) {
               MensageFactory.warn("O valor máximo do comprimento são 105 centímetros!", "");
               produto.setComprimentoCm(new BigDecimal(105));
          } else if (produto.getComprimentoCm().longValue() < 16) {
               MensageFactory.warn("O valor mínimo do comprimento são 16 centímetros!", "");
               produto.setComprimentoCm(new BigDecimal(16));
          }
     }

     public Empresa getEmpresa() {

          if (empresa.getDiasFechados() != null) {
               diasFuncionamento = UXUtil.delimitar(empresa.getDiasFechados(), ",");
          }
          return empresa;
     }

     public void versegmento() {
          System.out.println(empresa.getSegmento());
     }

     public void verificarEmailExiste() {
          if (eEJB.verificarEmailExiste(usuario.getEmail())) {
               MensageFactory.warn("Este email já se encontra em uso! Informe um email diferente.", null);
          }
     }

     public List<Compra> listarCompras() {
          return eEJB.listarVendasPorEmpresa(empresa);
     }

     public void salvar() {
          empresa.setSlug(empresa.getNome().replace(" ", "-").toLowerCase());
          StringBuilder sb = new StringBuilder();
          System.out.println(diasFuncionamento.size());
          for (String string : diasFuncionamento) {
               sb.append(string).append(",");
          }
          empresa.setDiasFechados(sb.toString());
          if (empresa.getIdEmpresa() == null) {
               try {
                    usuario.setIdPerfil(uEJB.perfilEmpresa());
                    empresa.setIdUsuario(usuario);
                    usuario.setIdEmpresa(empresa);
                    usuario.setUsk(Security.encrypt(usuario.getEmail()));
                    usuario.setSenha(Security.encrypt(usuario.getSenha()));
                    eEJB.salvarEmpresa(empresa);
//                uEJB.Atualizar(usuario);
                    MensageFactory.info("Empresa salva com sucesso!", null);
                    novo();
               } catch (Exception e) {
                    MensageFactory.warn("Não foi possivel salvar a empresa!", null);
                    System.out.println(e);
               }
          } else {
               try {
                    usuario.setIdPerfil(uEJB.perfilEmpresa());
                    empresa.setIdUsuario(usuario);
                    eEJB.salvarEmpresa(empresa);
                    usuario.setIdEmpresa(empresa);
                    usuario.setSenha(Security.encrypt(usuario.getSenha()));
                    usuario.setUsk(Security.encrypt(usuario.getEmail()));
                    System.out.println("Atualizando user " + usuario.getUsk());
                    uEJB.Atualizar(usuario);
                    MensageFactory.info("Empresa atualizada com sucesso!", null);
                    novo();
               } catch (Exception e) {
                    MensageFactory.warn("Não foi possivel atualizar a empresa!", null);
                    System.out.println(e);
               }
          }
     }

     public void addFrete() {
          try {
               produto.getFreteList().add(new Frete());
          } catch (Exception e) {
               produto.setFreteList(new ArrayList<Frete>());
               produto.getFreteList().add(new Frete());
          }
     }

     public void excluirFrete(Frete f, int index) {
          produto.getFreteList().remove(index);
          if (f.getIdFrete() != null) {
               pEJB.excluir(f);
          }
     }

     public void checarFrete(Frete f) {
          if (f.getDescricao().equals("SEDEX")) {
               f.setCodigo("40010");
          }
          if (f.getDescricao().equals("SEDEX à Cobrar")) {
               f.setCodigo("40045");
          }
          if (f.getDescricao().equals("SEDEX 10")) {
               f.setCodigo("40215");
          }
          if (f.getDescricao().equals("SEDEX Hoje")) {
               f.setCodigo("40290");
          }
          if (f.getDescricao().equals("PAC")) {
               f.setCodigo("41106");
          }
          if (f.getDescricao().equals("Frete Grátis")) {
               f.setCodigo("00000");
          }
          if (f.getDescricao().equals("Retirar")) {
               f.setCodigo("00001");
          }
          if (f.getDescricao().equals("Combinar")) {
               f.setCodigo("00002");
          }
     }

     public void addTag() {
          Tags t = new Tags();
          t.setDescricao(tag);
          try {
               produto.getTagsList().add(t);
          } catch (Exception e) {
               produto.setTagsList(new ArrayList<Tags>());
               produto.getTagsList().add(t);
          }
          setTag("");
     }

     public void excluirTag(Tags t, int index) {
          produto.getTagsList().remove(index);
          if (t.getIdTag() != null) {
               pEJB.excluirTag(t);
          }
     }

     public void excluirFoto(Foto f) {
          if (new File(Config.caminhoProduto + f.getUrl()).exists()) {
               new File(Config.caminhoProduto + f.getUrl()).delete();
          }
          int indice = 0;
          for (Foto ff : produto.getFotoList()) {
               if (ff.getUrl().equals(f.getUrl())) {
                    break;
               } else {
                    indice++;
               }
          }
          produto.getFotoList().remove(indice);
          if (f.getIdFoto() != null) {
               pEJB.excluirFoto(f);
          }
     }

     public void excluirFotoEmpresa(Foto f) {
          if (new File(Config.caminhoEmpresa + f.getUrl()).exists()) {
               new File(Config.caminhoEmpresa + f.getUrl()).delete();
          }
          int indice = 0;
          for (Foto ff : empresa.getFotoList()) {
               if (ff.getUrl().equals(f.getUrl())) {
                    break;
               } else {
                    indice++;
               }
          }
          if (f.getIdFoto() != null) {
               pEJB.excluirFoto(f);
          }
          empresa.getFotoList().remove(indice);

     }

     public void uploadEmpresaLogo(FileUploadEvent event) throws IOException {
          if (new File(Config.caminhoEmpresa + empresa.getLogo()).exists()) {
               new File(Config.caminhoEmpresa + empresa.getLogo()).delete();
          }
          if (empresa.getIdEmpresa() == null) {
               new File(Config.caminhoEmpresa + eEJB.afterID()).mkdir();
               FileUtils.copyFile(new File(Config.indexModel), new File(Config.caminhoEmpresa + eEJB.afterID() + "\\index.xhtml"));
               empresa.setLogo(UXUtil.upload(event, Config.caminhoEmpresa + eEJB.afterID() + "\\", 400, 300));
          } else {
               if (!new File(Config.caminhoEmpresa + empresa.getIdEmpresa()).exists()) {
                    new File(Config.caminhoEmpresa + empresa.getIdEmpresa()).mkdir();
                    FileUtils.copyFile(new File(Config.indexModel), new File(Config.caminhoEmpresa + empresa.getIdEmpresa() + "\\index.xhtml"));
               }
               empresa.setLogo(UXUtil.upload(event, Config.caminhoEmpresa + empresa.getIdEmpresa() + "\\", 400, 300));
          }
     }

     public void uploadEmpresaCapa(FileUploadEvent event) throws IOException {
          if (new File(Config.caminhoEmpresa + empresa.getCapa()).exists()) {
               new File(Config.caminhoEmpresa + empresa.getCapa()).delete();
          }
          if (empresa.getIdEmpresa() == null) {
               new File(Config.caminhoEmpresa + eEJB.afterID()).mkdir();
               FileUtils.copyFile(new File(Config.indexModel), new File(Config.caminhoEmpresa + eEJB.afterID() + "\\index.xhtml"));
               empresa.setCapa(UXUtil.upload(event, Config.caminhoEmpresa + eEJB.afterID() + "\\", 750, 255));
          } else {
               if (!new File(Config.caminhoEmpresa + empresa.getIdEmpresa()).exists()) {
                    new File(Config.caminhoEmpresa + empresa.getIdEmpresa()).mkdir();
                    FileUtils.copyFile(new File(Config.indexModel), new File(Config.caminhoEmpresa + empresa.getIdEmpresa() + "\\index.xhtml"));
               }
               empresa.setCapa(UXUtil.upload(event, Config.caminhoEmpresa + empresa.getIdEmpresa() + "\\", 750, 255));
          }
     }

     public void uploadFotosEmpresa(FileUploadEvent event) throws IOException {
          Foto f = new Foto();
          if (empresa.getIdEmpresa() == null) {
               new File(Config.caminhoEmpresa + eEJB.afterID()).mkdir();
               FileUtils.copyFile(new File(Config.indexModel), new File(Config.caminhoEmpresa + eEJB.afterID() + "\\index.xhtml"));
               f.setUrl(UXUtil.upload(event, Config.caminhoEmpresa + eEJB.afterID() + "\\", 400, 300));
               System.out.println(f.getUrl());
          } else {
               if (!new File(Config.caminhoEmpresa + empresa.getIdEmpresa()).exists()) {
                    new File(Config.caminhoEmpresa + empresa.getIdEmpresa()).mkdir();
                    FileUtils.copyFile(new File(Config.indexModel), new File(Config.caminhoEmpresa + eEJB.afterID() + "\\index.xhtml"));
               }
               f.setUrl(UXUtil.upload(event, Config.caminhoEmpresa + empresa.getIdEmpresa() + "\\", 400, 300));
               System.out.println(f.getUrl());
          }
          try {
               empresa.getFotoList().add(f);
          } catch (Exception e) {
               empresa.setFotoList(new ArrayList<Foto>());
               empresa.getFotoList().add(f);
          }
     }

     public void upload(FileUploadEvent event) throws IOException {
          if (new File(Config.caminhoUsuario + usuario.getImagem()).exists()) {
               new File(Config.caminhoUsuario + usuario.getImagem()).delete();
          }
          if (usuario.getIdUsuario() == null) {
               new File(Config.caminhoUsuario + uEJB.afterID()).mkdir();
               FileUtils.copyFile(new File(Config.indexModel), new File(Config.caminhoUsuario + uEJB.afterID() + "\\index.xhtml"));
               usuario.setImagem(UXUtil.upload(event, Config.caminhoUsuario + "\\" + uEJB.afterID() + "\\", 400, 400));
          } else {
               if (!new File(Config.caminhoUsuario + usuario.getIdUsuario()).exists()) {
                    new File(Config.caminhoUsuario + usuario.getIdUsuario()).mkdir();
                    FileUtils.copyFile(new File(Config.indexModel), new File(Config.caminhoUsuario + usuario.getIdUsuario() + "\\index.xhtml"));
               }
               usuario.setImagem(UXUtil.upload(event, Config.caminhoUsuario + usuario.getIdUsuario() + "\\", 400, 400));
          }
     }

     public void uploadProdutoPrincipal(FileUploadEvent event) throws IOException {
          if (produto.getIdProduto() == null) {
               new File(Config.caminhoProduto + pEJB.afterID()).mkdir();
               FileUtils.copyFile(new File(Config.indexModel), new File(Config.caminhoProduto + pEJB.afterID() + "\\index.xhtml"));
               produto.setFotoPrincipal(UXUtil.upload(event, Config.caminhoProduto + pEJB.afterID() + "\\", 400, 300));
          } else {
               if (!new File(Config.caminhoProduto + produto.getIdProduto()).exists()) {
                    new File(Config.caminhoProduto + produto.getIdProduto()).mkdir();
                    FileUtils.copyFile(new File(Config.indexModel), new File(Config.caminhoProduto + produto.getIdProduto() + "\\index.xhtml"));
               }

               produto.setFotoPrincipal(UXUtil.upload(event, Config.caminhoProduto + produto.getIdProduto() + "\\", 400, 300));
          }


     }

     public void excluirEmpresa(Empresa emp) {
          try {
               eEJB.excluirEmpresa(emp);
               MensageFactory.info("Empresa excluída com sucesso!", null);
          } catch (Exception e) {
               MensageFactory.error("Não foi possível excluir a empresa!", null);
               System.out.println(e);
          }
     }

     public void excluirProduto(Produto p) {
          try {
               eEJB.excluirProduto(p);
               MensageFactory.info("Produto excluído com sucesso!", null);
          } catch (Exception e) {
               MensageFactory.error("Não foi possível excluir o produto!", null);
               System.out.println(e);
          }
     }

     public Integer getidTemp() {
          if (produto.getIdProduto() == null) {
               return pEJB.afterID();
          } else {
               return produto.getIdProduto();
          }
     }

     public Integer getidEmpresaTemp() {
          if (empresa.getIdEmpresa() == null) {
               return eEJB.afterID();
          } else {
               return empresa.getIdEmpresa();
          }
     }

     public Integer getidUsuarioTemp() {
          if (usuario.getIdUsuario() == null) {
               return uEJB.afterID();
          } else {
               return usuario.getIdUsuario();
          }
     }

     public void uploadFotosProdutoPrincipal(FileUploadEvent event) throws IOException {
          Foto f = new Foto();
          if (produto.getIdProduto() == null) {
               new File(Config.caminhoProduto + pEJB.afterID()).mkdir();
               FileUtils.copyFile(new File(Config.indexModel), new File(Config.caminhoProduto + pEJB.afterID() + "\\index.xhtml"));
               f.setUrl(UXUtil.upload(event, Config.caminhoProduto + pEJB.afterID() + "\\", 800, 400));
          } else {
               f.setUrl(UXUtil.upload(event, Config.caminhoProduto + produto.getIdProduto() + "\\", 800, 400));
          }
          try {
               produto.getFotoList().add(f);
          } catch (Exception e) {
               produto.setFotoList(new ArrayList<Foto>());
               produto.getFotoList().add(f);
          }
     }

     public List<Comentario> listarComentarios() {
          return eEJB.listarComentarios(empresa);
     }

     public String onFlowProcess(FlowEvent event) {
//        System.out.println("teve aqui");
          RequestContext rc = RequestContext.getCurrentInstance();
          rc.update(":frmPrincipal:msgPag");
          if (skip) {
               skip = false;   //reset in case user goes back
               return "confirm";
          } else {
               return event.getNewStep();
          }
     }

     public void addProduto() {
          System.out.println(produto.getValorFreteFixo());
          produto.setIdEmpresa(empresa);
          produto.setSlug(UXUtil.formatString(produto.getNome() + "-" + produto.getIdEmpresa().getNome()));
          produto.setTipo("produtos");
          if (produto.getMetaTag() == null || produto.getMetaTag() == "") {
               produto.setMetaTag(produto.getNome());
          }

          if (empresa.getIdEmpresa() == null) {
               try {
                    if (empresa.getProdutoList().contains(produto)) {
                         int index = empresa.getProdutoList().indexOf(produto);
                         empresa.getProdutoList().set(index, produto);
                         eEJB.salvarEmpresa(empresa);
                    } else {
                         empresa.getProdutoList().add(produto);
                         eEJB.salvarEmpresa(empresa);
                    }
                    produto = new Produto();
               } catch (Exception e) {
                    System.out.println("Ocorreu exception");
                    System.out.println(e);
                    empresa.setProdutoList(new ArrayList<Produto>());
                    if (empresa.getProdutoList().contains(produto)) {
                         int index = empresa.getProdutoList().indexOf(produto);
                         empresa.getProdutoList().set(index, produto);
                         eEJB.salvarEmpresa(empresa);
                    } else {
                         empresa.getProdutoList().add(produto);
                         eEJB.salvarEmpresa(empresa);
                    }
                    produto = new Produto();
               }
               MensageFactory.info("Salvo com sucesso!", null);
          } else {
               System.out.println(produto.getValorFreteFixo());
               if (produto.getIdProduto() != null) {
                    eEJB.salvarEmpresa(empresa);
                    pEJB.atualizarProduto(produto);
               } else {
                    try {
                         if (empresa.getProdutoList().contains(produto)) {
                              int index = empresa.getProdutoList().indexOf(produto);
                              empresa.getProdutoList().set(index, produto);
                              eEJB.salvarEmpresa(empresa);
                         } else {

                              empresa.getProdutoList().add(produto);
                              eEJB.salvarEmpresa(empresa);
                         }
                         produto = new Produto();
                    } catch (Exception e) {
                         System.out.println("Ocorreu exception");
                         System.out.println(e);
                         empresa.setProdutoList(new ArrayList<Produto>());
                         if (empresa.getProdutoList().contains(produto)) {
                              int index = empresa.getProdutoList().indexOf(produto);
                              empresa.getProdutoList().set(index, produto);
                              eEJB.salvarEmpresa(empresa);
                         } else {
                              empresa.getProdutoList().add(produto);
                              eEJB.salvarEmpresa(empresa);
                         }
                         produto = new Produto();
                    }
               }
               MensageFactory.info("Editado com sucesso!", null);
          }

     }

     public Produto getProduto() {
          return produto;
     }

     public void setProduto(Produto produto) {
          this.produto = produto;
     }

     public Usuario getUsuario() throws Exception {
          return usuario;
     }

     public void setUsuario(Usuario usuario) throws Exception {

          this.usuario = usuario;
     }

     public UploadedFile getFile() {
          return file;
     }

     public void setFile(UploadedFile file) {
          this.file = file;
     }

     public void setEmpresa(Empresa empresa) throws Exception {
          usuario = empresa.getIdUsuario();
          if (usuario.getSenha() != null) {
               usuario.setSenha(Security.decrypt(usuario.getSenha()));
          }
          this.empresa = empresa;
     }

     public String getTag() {
          return tag;
     }

     public void setTag(String tag) {
          this.tag = tag;
     }

     public boolean isSkip() {
          return skip;
     }

     public void setSkip(boolean skip) {
          this.skip = skip;
     }

     public Comentario getComentario() {
          return comentario;
     }

     public void setComentario(Comentario comentario) {
          this.comentario = comentario;
     }

     public Comentario getComentarioResposta() {
          return comentarioResposta;
     }

     public void setComentarioResposta(Comentario comentarioResposta) {
          this.comentarioResposta = comentarioResposta;
     }

     public void corrigirArquivosIndex() throws IOException {

          File dir = new File("C:\\noglobo\\upload\\");
          if (dir.isDirectory()) {
               String[] filhos = dir.list();
               for (int i = 0; i < filhos.length; i++) {
                    File dir2 = new File("C:\\noglobo\\upload\\" + filhos[i]);
                    if (dir2.isDirectory()) {
                         String[] filhos2 = dir2.list();
                         for (int y = 0; y < filhos2.length; y++) {
                              System.out.println();
                              File dir3 = new File("C:\\noglobo\\upload\\" + filhos[i] + "\\" + filhos2[y] + "\\");
                              if (dir3.isDirectory()) {
                                   System.out.println(dir3);
                                   FileUtils.copyFile(new File(Config.indexModel), new File("C:\\noglobo\\upload\\" + filhos[i] + "\\" + filhos2[y] + "\\index.xhtml"));
                              }
                         }
                    }
               }
          }
     }

     public void enviarComentario() {
          comentarioResposta.setDtComentario(new Date());
          comentarioResposta.setTipo("R");
          comentarioResposta.setIdCliente(comentario.getIdCliente());
          comentarioResposta.setIdProduto(comentario.getIdProduto());
//        cEJB.Atualizar(comentarioResposta);
          this.comentario.setIdComentarioReplica(comentarioResposta);
          cEJB.Atualizar(this.comentario);
          this.comentario = new Comentario();
          comentarioResposta = new Comentario();
          MensageFactory.info("Resposta enviada com sucesso!", null);
     }

     public LazyDataModel<Empresa> getEmpresasLazy() {
          return empresasLazy;
     }

     public void setEmpresasLazy(LazyDataModel<Empresa> empresasLazy) {
          this.empresasLazy = empresasLazy;
     }

     public List<Empresa> getListaDeEmpresas() {
          return listaDeEmpresas;
     }

     public void setListaDeEmpresas(List<Empresa> listaDeEmpresas) {
          this.listaDeEmpresas = listaDeEmpresas;
     }

     public List<String> getDiasFuncionamento() {
          return diasFuncionamento;
     }

     public void setDiasFuncionamento(List<String> diasFuncionamento) {
          this.diasFuncionamento = diasFuncionamento;
     }
}
