/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ux.view;

import com.ux.controller.EstadoEJB;
import com.ux.model.Estado;
import com.ux.util.MensageFactory;
import java.io.Serializable;
import java.util.List;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

/**
 *
 * @author JOAOPAULO
 */
@ManagedBean
@ViewScoped
public class EstadoMB implements Serializable{

    @EJB
    EstadoEJB eEJB;
    private Estado estado;
    
    public EstadoMB() {
        novo();
    }
    
    public void novo(){
        estado = new Estado();
    }
    
    public void salvar(){
        if(estado.getIdEstado()==null){
            try {
                eEJB.Salvar(estado);
                novo();
                MensageFactory.info("Estado salvo com sucesso!", null);
            } catch (Exception e) {
                MensageFactory.error("Erro ao tentar salvar!", null);
                System.out.println("O erro ocorrido foi: "+ e);
            }
        }else{
            try {
                eEJB.Atualizar(estado);
                novo();
                MensageFactory.info("Estado atualizado com sucesso!", null);
            } catch (Exception e) {
                MensageFactory.error("Erro ao tentar atualizar!", null);
                System.out.println("O erro ocorrido foi: "+ e);
            }
        }
    }
    
    public void excluir() {
        try {
            eEJB.Excluir(estado, estado.getIdEstado());
            novo();
            MensageFactory.info("Categoria excluida com sucesso!", null);
        } catch (Exception e) {
            MensageFactory.error("Erro ao tentar excluir!", null);
            System.out.println("Erro ocorrido é: " + e);
        }
    }
    
    public List<Estado> listarEstados(){
        return eEJB.ListarTodos();
    }
    //get e set

    public Estado getEstado() {
        return estado;
    }

    public void setEstado(Estado estado) {
        this.estado = estado;
    }
    
}
