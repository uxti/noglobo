/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ux.view;

import com.ux.config.Config;
import com.ux.controller.ProdutoEJB;
import com.ux.controller.UsuarioEJB;
import com.ux.model.Empresa;
import com.ux.model.Foto;
import com.ux.model.Frete;
import com.ux.model.Produto;
import com.ux.model.Tags;
import com.ux.util.MensageFactory;
import com.ux.util.UXUtil;
import java.io.File;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import org.primefaces.event.FileUploadEvent;

/**
 *
 * @author Renato
 */
@ManagedBean
@ViewScoped
public class ProdutoMB {

     @EJB
     ProdutoEJB pEJB;
     @EJB
     UsuarioEJB uEJB;
     private Produto produto;
     private String tag;

     /**
      * Creates a new instance of ProdutoMB
      */
     public ProdutoMB() {
          novo();
     }

     public void novo() {
          produto = new Produto();
     }

     public void addFrete() {
//        System.out.println("chamou aqui");
          try {
               produto.getFreteList().add(new Frete());
          } catch (Exception e) {
               produto.setFreteList(new ArrayList<Frete>());
               produto.getFreteList().add(new Frete());
          }
     }

     public void checarFrete(Frete f) {
          if (f.getDescricao().equals("SEDEX")) {
               f.setCodigo("40010");
          }
          if (f.getDescricao().equals("SEDEX à Cobrar")) {
               f.setCodigo("40045");
          }
          if (f.getDescricao().equals("SEDEX 10")) {
               f.setCodigo("40215");
          }
          if (f.getDescricao().equals("SEDEX Hoje")) {
               f.setCodigo("40290");
          }
          if (f.getDescricao().equals("PAC")) {
               f.setCodigo("41106");
          }
          if (f.getDescricao().equals("Frete Grátis")) {
               f.setCodigo("00000");
          }
          if (f.getDescricao().equals("Retirar")) {
               f.setCodigo("00001");
          }
          if (f.getDescricao().equals("Combinar")) {
               f.setCodigo("00002");
          }
     }

     public void excluirFrete(Frete f, int index) {
          produto.getFreteList().remove(index);
          if (f.getIdFrete() != null) {
               pEJB.excluir(f);
          }
     }

     public Integer getidTemp() {
          if (produto.getIdProduto() == null) {
               return pEJB.afterID();
          } else {
               return produto.getIdProduto();
          }
     }

     public void excluirTag(Tags t, int index) {
          produto.getTagsList().remove(index);
          if (t.getIdTag() != null) {
               pEJB.excluirTag(t);
          }
     }

     public void uploadProdutoPrincipal(FileUploadEvent event) {
          if (new File(Config.caminhoProduto + produto.getFotoPrincipal()).exists()) {
               new File(Config.caminhoProduto + produto.getFotoPrincipal()).delete();
          }
          if (produto.getIdProduto() == null) {
               if (!new File(Config.caminhoProduto + pEJB.afterID()).exists()) {
                    new File(Config.caminhoProduto + pEJB.afterID()).mkdir();
               }
               produto.setFotoPrincipal(UXUtil.upload(event, Config.caminhoProduto + pEJB.afterID() + "\\", 400, 300));
          } else {
               if (!new File(Config.caminhoProduto + produto.getIdProduto()).exists()) {
                    new File(Config.caminhoProduto + produto.getIdProduto()).mkdir();
               }
               produto.setFotoPrincipal(UXUtil.upload(event, Config.caminhoProduto + produto.getIdProduto() + "\\", 400, 300));
          }
     }

     public void excluirFoto(Foto f) {
          if (new File(Config.caminhoProduto + f.getUrl()).exists()) {
               new File(Config.caminhoProduto + f.getUrl()).delete();
          }
          int indice = 0;
          for (Foto ff : produto.getFotoList()) {
               if (ff.getUrl().equals(f.getUrl())) {
                    break;
               } else {
                    indice++;
               }
          }
          produto.getFotoList().remove(indice);
          if (f.getIdFoto() != null) {
               pEJB.excluirFoto(f);
          }
     }

     public void salvar() {
          produto.setTipo("produtos");
          produto.setIdEmpresa(uEJB.usuarioLogado().getIdEmpresa());
          produto.setSlug(UXUtil.formatString(produto.getNome() + produto.getIdEmpresa().getNome()));
          if (produto.getMetaTag() == null || produto.getMetaTag() == "") {
               produto.setMetaTag(produto.getNome());
          }
          if (produto.getIdProduto() == null) {
               pEJB.atualizarProduto(produto);
               MensageFactory.info("Produto salvo com sucesso!", null);
               produto = new Produto();
          } else {
               pEJB.atualizarProduto(produto);
               MensageFactory.info("Produto Atualizado com suceso!", null);
               produto = new Produto();
          }
     }

     public void addTag() {
          Tags t = new Tags();
          t.setDescricao(tag);
          try {
               produto.getTagsList().add(t);
          } catch (Exception e) {
               produto.setTagsList(new ArrayList<Tags>());
               produto.getTagsList().add(t);
          }
          setTag("");
     }

     public void uploadFotosProdutoPrincipal(FileUploadEvent event) {
          Foto f = new Foto();
          if (produto.getIdProduto() == null) {
               new File(Config.caminhoProduto + pEJB.afterID()).mkdir();
               f.setUrl(UXUtil.upload(event, Config.caminhoProduto + pEJB.afterID() + "\\", 400, 300));
          } else {
               f.setUrl(UXUtil.upload(event, Config.caminhoProduto + produto.getIdProduto() + "\\", 400, 300));
          }
          try {
               produto.getFotoList().add(f);
          } catch (Exception e) {
               produto.setFotoList(new ArrayList<Foto>());
               produto.getFotoList().add(f);
          }
     }

     public List<Produto> listaDeProdutos(Empresa e) {
          return pEJB.listarProdutosPorEmpresa(e);
     }

     public void excluirProduto(Produto p) {
          try {
               pEJB.excluirProduto(p);
               MensageFactory.info("Produto excluído com sucesso!", null);
          } catch (Exception e) {
               MensageFactory.error("Não foi possível excluir o produto!", null);
               System.out.println(e);
          }
     }

     public void regrasCorreios() {
          if (produto.getFormatoEncomenda().equals("3")) {
               produto.setAlturaCm(BigDecimal.ZERO);
          }
     }

     public void regrasCorreiosAltura() {
          if (!produto.getFormatoEncomenda().equals("3")) {
               if (produto.getAlturaCm().longValue() > 105) {
                    MensageFactory.warn("O valor máximo do altura são 105 centímetros!", "");
//                RequestContext rc = RequestContext.getCurrentInstance();
//                rc.execute("alert('O valor máximo do altura são 105 centímetros!')");
                    produto.setAlturaCm(new BigDecimal(105));
               } else if (produto.getAlturaCm().longValue() < 2) {
                    MensageFactory.warn("O valor mínimo do altura são 2 centímetros!", "");
//                RequestContext rc = RequestContext.getCurrentInstance();
//                rc.execute("alert('O valor mínimo do altura são 2 centímetros!')");
                    produto.setAlturaCm(new BigDecimal(2));
               }

          }
     }

     public void regrasCorreiosLargura() {
          if (produto.getLarguraCm().longValue() > 105) {
//            RequestContext rc = RequestContext.getCurrentInstance();
//            rc.execute("alert('O valor máximo da largura são 105 centímetros!')");
               MensageFactory.warn("O valor máximo da largura são 105 centímetros!", "");
               produto.setLarguraCm(new BigDecimal(105));
          } else if (produto.getLarguraCm().longValue() < 11) {
//            RequestContext rc = RequestContext.getCurrentInstance();
//            rc.execute("alert('O valor mínimo da largura são 11 centímetros!')");
               MensageFactory.warn("O valor mínimo da largura são 11 centímetros!", "");
               produto.setLarguraCm(new BigDecimal(11));
          }
     }

     public void regrasCorreiosDiametro() {
          if (produto.getDiametroCm().longValue() > 91) {
//            RequestContext rc = RequestContext.getCurrentInstance();
//            rc.execute("alert('O valor máximo do diametro são 91 centímetros!')");
               MensageFactory.warn("O valor máximo do diametro são 91 centímetros!", "");
               produto.setDiametroCm(new BigDecimal(91));
          }
     }

     public void regrasCorreiosPeso() {
          if (produto.getFormatoEncomenda().equals("3")) {
               if (produto.getPesoKg().longValue() > 1) {
                    produto.setPesoKg(BigDecimal.ONE);
               }
          }
     }

     public void validarComprimento() {
          if (produto.getComprimentoCm().longValue() > 105) {
//            RequestContext rc = RequestContext.getCurrentInstance();
//            rc.execute("alert('O valor máximo do comprimento são 105 centímetros!')");
               MensageFactory.warn("O valor máximo do comprimento são 105 centímetros!", "");
               produto.setComprimentoCm(new BigDecimal(105));
          } else if (produto.getComprimentoCm().longValue() < 16) {
//            RequestContext rc = RequestContext.getCurrentInstance();
//            rc.execute("alert('O valor mínimo do comprimento são 16 centímetros!')");
               MensageFactory.warn("O valor mínimo do comprimento são 16 centímetros!", "");
               produto.setComprimentoCm(new BigDecimal(16));
          }
     }

     public Produto getProduto() {
          return produto;
     }

     public void setProduto(Produto produto) {
          this.produto = produto;
     }

     public String getTag() {
          return tag;
     }

     public void setTag(String tag) {
          this.tag = tag;
     }
}
