/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ux.view;

import com.ux.controller.EntregaEJB;
import com.ux.model.Empresa;
import com.ux.model.Entrega;
import com.ux.util.MensageFactory;
import java.util.List;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

/**
 *
 * @author Renato
 */
@ManagedBean
@ViewScoped
public class EntregaMB {

    @EJB
    EntregaEJB eEJB;
    private Entrega entrega;

    public EntregaMB() {
        novo();
    }

    public void salvar(Empresa empresa) {
        try {
            entrega.setIdEmpresa(empresa);
            eEJB.Atualizar(entrega);
            novo();
            MensageFactory.info("Salvo com sucesso!", null);
        } catch (Exception e) {
            MensageFactory.warn("Não foi possível salvar!", null);
            System.out.println(e);
        }
    }

    public List<Entrega> listarEntregas(Empresa empresa) {
        return eEJB.listarEntregas(empresa);
    }

    public void excluir(Entrega entrega) {
        try {
            eEJB.Excluir(entrega, entrega.getIdTaxa());
            MensageFactory.info("Excluído com sucesso!", null);
        } catch (Exception e) {
            MensageFactory.warn("Não foi possível excluir!", null);
            System.out.println(e);
        }
    }

    public void novo() {
        entrega = new Entrega();
    }

    public Entrega getEntrega() {
        return entrega;
    }

    public void setEntrega(Entrega entrega) {
        this.entrega = entrega;
    }
}
