/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ux.view;

import com.ux.controller.StatusEJB;
import com.ux.model.Status;
import com.ux.util.MensageFactory;
import java.io.Serializable;
import java.util.List;
import java.util.Map;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;

/**
 *
 * @author JOAOPAULO
 */
@ManagedBean
@ViewScoped
public class StatusMB implements Serializable {

    @EJB
    StatusEJB sEJB;
    private Status status;

    public StatusMB() {
        novo();
    }

    public void novo() {
        status = new Status();
    }

    public void salvar() {
        if (status.getIdStatus() == null) {
            try {
                sEJB.Salvar(status);
                novo();
                MensageFactory.info("Status Salvo com sucesso!", null);
            } catch (Exception e) {
                MensageFactory.info("Erro ao tentar Salvar", null);
                System.out.println("O erro foi: " + e);
            }
        } else {
            try {
                sEJB.Atualizar(status);
                novo();
                MensageFactory.info("Editado com sucesso", null);
            } catch (Exception e) {
                MensageFactory.info("Erro ao tentar Editar", null);
                System.out.println("O erro foi: " + e);
            }
        }
    }

    public void exlcuir() {
        try {
            sEJB.Excluir(status, status.getIdStatus());
            novo();
            MensageFactory.info("Categoria excluida com sucesso!", null);
        } catch (Exception e) {
            MensageFactory.error("Erro ao tentar excluir!", null);
            System.out.println("Erro ocorrido é: " + e);
        }
    }

    public List<Status> listarStatus() {
        return sEJB.ListarTodos();
    }

    //Get e Set
    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public void oi() {
        Map<String, String> params = FacesContext.getCurrentInstance().
                getExternalContext().getRequestParameterMap();
        String parameterOne = params.get("token");
        if (parameterOne != null) {
            //aqui ele vai pesquisar e salvar as informações desta transação!
            System.out.println("oie");
        }
    }
}
