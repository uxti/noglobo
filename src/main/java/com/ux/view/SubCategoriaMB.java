/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ux.view;

import com.ux.controller.CategoriaEJB;
import com.ux.controller.SubCategoriaEJB;
import com.ux.model.Categoria;
import com.ux.model.SubCategoria;
import com.ux.util.MensageFactory;
import java.io.Serializable;
import java.util.List;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

/**
 *
 * @author JOAOPAULO
 */
@ManagedBean
@ViewScoped
public class SubCategoriaMB implements Serializable {

    @EJB
    SubCategoriaEJB scEJB;
    private SubCategoria subCategoria;
    @EJB
    CategoriaEJB cEJB;
    private Categoria categoria;

    public SubCategoriaMB() {
        categoria = new Categoria();
        novo();
    }

    public void novo() {
        subCategoria = new SubCategoria();
    }

    public void salvar() {
        categoria = subCategoria.getIdCategoria();
//        if (categoria != null) {
//            subCategoria.setIdCategoria(categoria);
//        }
        if (subCategoria.getIdSubCategoria() == null) {
            try {
                scEJB.Salvar(subCategoria);
                novo();
                MensageFactory.info("Salvo com sucesso!", null);
            } catch (Exception e) {
                MensageFactory.info("Erro ao tentar salvar!", null);
                System.out.println("O erro ocorrido foi: " + e);
            }
        } else {
            try {
                scEJB.Atualizar(subCategoria);
                novo();
                MensageFactory.info("Editado com sucesso!", null);
            } catch (Exception e) {
                MensageFactory.error("Erro ao tentar atualizar!", null);
                System.out.println("O erro ocorrido foi: " + e);
            }
        }
        subCategoria.setIdCategoria(categoria);
    } 

    public void salvarFlash() {
        try {
//            System.out.println(subCategoria.getIdCategoria().getDescricao());
            if (categoria.getDescricao() != null) {
                subCategoria.setIdCategoria(categoria);
                scEJB.Atualizar(subCategoria);
                subCategoria = new SubCategoria();
                MensageFactory.info("Adicionada à categoria " + categoria.getDescricao() + " com sucesso!", "");
            }
        } catch (Exception e) {
            MensageFactory.warn("Erro ao tentar salvar!", null);
            System.out.println("O erro ocorrido foi: " + e);
            System.out.println(e);
        }

    }

    public void excluir() {
        try {
            scEJB.Excluir(subCategoria, subCategoria.getIdSubCategoria());
            novo();
            MensageFactory.info("Excluído com sucesso!", null);
        } catch (Exception e) {
            MensageFactory.error("Erro ao tentar excluir!", null);
            System.out.println("Erro ocorrido é: " + e);
        }
    }

    public List<SubCategoria> listarSubCategorias() {
        return scEJB.ListarTodos();
    }

    //Gets e sets
    public SubCategoria getSubCategoria() {
        try {
            if (subCategoria != null) {
                categoria = subCategoria.getIdCategoria();
            }
        } catch (Exception e) {
            System.out.println("A subCategoria veio nula");
        }
        return subCategoria;
    }

    public void setSubCategoria(SubCategoria subCategoria) {
        this.subCategoria = subCategoria;
    }

    public Categoria getCategoria() {
        return categoria;
    }

    public void setCategoria(Categoria categoria) {
        this.categoria = categoria;
    }
}
