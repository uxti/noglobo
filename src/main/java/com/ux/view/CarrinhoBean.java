/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ux.view;

import br.com.moip.Authentication;
import br.com.moip.BasicAuth;
import com.ux.model.Produto;
import com.ux.pojo.Carrinho;
import com.ux.pojo.CarrinhoEmpresa;
import com.ux.pojo.Endereco;
import com.ux.pojo.EnderecoEntrega;
import com.ux.pojo.FreteWS;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.ejb.Stateless;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

/**
 *
 * @author Renato
 */
@ManagedBean
@SessionScoped
public class CarrinhoBean implements Serializable {

    Authentication auth = new BasicAuth("X3HAS9Y8JB78QGHNHCOWROFBIZUJBT7F", "UXY5QSOSQDAP6NS0MYLMZUNPSUUDAUQ26NWOMPSE");
    String endpoint = "https://sandbox.moip.com.br";
    String chavePublica = "-----BEGIN PUBLIC KEY-----MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAgv0H+6/1EdUOy7yPVhKq7KlxHuCkij5W1StiSDHwengFNZr3+VM9SmbItSvAMZZN5HNG0yR1Dbt1Coz7bqbZCN6GfYAGOwQdkScOGnbjLiSdV+8I0de7B2z9TofJ1bKDvbBYmzzhVl2MzEC6ygMjCxCAyRHVcr4LM/wkDiavrVasZZ0qN7tr1d5+K+0g92Yop+UgLeUq4aueuCotzQNJ/sltOmT+hPl0pzCQr0zQ5x36aivqMERcWwgoEsVbaAbcTDKCafjB3WkaGV2vB+rDA5aoHiF4zr+VqfA8d5nDnIaw7oxZ6EXK1XqJE1DvakXQBEYRT1C6r3X2y64tzHv1UQIDAQAB-----END PUBLIC KEY-----";
    private List<CarrinhoEmpresa> itensDoCarrinhoPorEmpresa;
    private List<Carrinho> carrinho;
    private List<FreteWS> fretesDisponiveis;
    private FreteWS freteSelecionado;
    private Endereco enderecoWS;
    private EnderecoEntrega enderecoEntrega;
    private String tipoServico;
    private String cep;

    public CarrinhoBean() {
        itensDoCarrinhoPorEmpresa = new ArrayList<CarrinhoEmpresa>();
        carrinho = new ArrayList<Carrinho>();
    }

    public void addProdutoCarrinho(Produto p, int quantidade) {
        Carrinho c = new Carrinho();
        c.setProduto(p);
        c.setQuantidade(quantidade);
        c.setEmpresa(p.getIdEmpresa());
        carrinho.add(c);
    }

    public List<CarrinhoEmpresa> getItensDoCarrinhoPorEmpresa() {
        return itensDoCarrinhoPorEmpresa;
    }

    public void setItensDoCarrinhoPorEmpresa(List<CarrinhoEmpresa> itensDoCarrinhoPorEmpresa) {
        this.itensDoCarrinhoPorEmpresa = itensDoCarrinhoPorEmpresa;
    }

    public List<Carrinho> getCarrinho() {
        return carrinho;
    }

    public void setCarrinho(List<Carrinho> carrinho) {
        this.carrinho = carrinho;
    }
}
