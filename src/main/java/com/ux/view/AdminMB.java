/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ux.view;

import com.ux.controller.AdminEJB;
import com.ux.model.Empresa;
import com.ux.pojo.Vendas;
import com.ux.util.UXUtil;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

/**
 *
 * @author Renato
 */
@ManagedBean
@ViewScoped
public class AdminMB {

    @EJB
    AdminEJB aEJB;
    private Integer ano, mes;

    public AdminMB() {
        mes = 0;
    }

    public List<Empresa> listarEcommerces() {
        return aEJB.listarEmpresasEcommerce();
    }

    public List<Vendas> listarComprasPagasPorEmpresa(Empresa em) {
        if (ano == null) {
            ano = UXUtil.getAno();
        }
        Calendar calendar = Calendar.getInstance();
        calendar.set(ano, 0, 1);
        Calendar calendar2 = Calendar.getInstance();
        calendar2.set(ano, 11, 31);
        return aEJB.listarVendasPorEmpresa(em, calendar.getTime(), calendar2.getTime());
    }

    public List<Integer> anosComMovimentacao() {
        return aEJB.anosQueHouveramVendas();
    }

    public Integer getAno() {
        return ano;
    }

    public void setAno(Integer ano) {
        this.ano = ano;
    }

    public Integer getMes() {
        return mes;
    }

    public void setMes(Integer mes) {
        this.mes = mes;
    }
    
    
}
