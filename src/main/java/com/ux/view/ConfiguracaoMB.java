/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ux.view;

import com.ux.config.Config;
import com.ux.controller.ConfiguracaoEJB;
import com.ux.model.Configuracao;
import com.ux.util.MensageFactory;
import com.ux.util.UXUtil;
import java.io.IOException;
import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import org.primefaces.event.FileUploadEvent;

/**
 *
 * @author Renato
 */
@ManagedBean
@ViewScoped
public class ConfiguracaoMB implements Serializable {

     @EJB
     ConfiguracaoEJB cEJB;
     private Configuracao configuracao;
     private String teste;

     public ConfiguracaoMB() {
          configuracao = new Configuracao();
     }

     public void salvar() {
          cEJB.Atualizar(configuracao);
          MensageFactory.info("Configuração atualizada com sucesso!", null);
     }

     public Configuracao getConfiguracao() {
          if (cEJB.Contar() >= 1 && configuracao.getIdConfiguracao() == null) {
               System.out.println("teve aqui");
               return configuracao = cEJB.ListarTodos().get(0);
          } else {
               return configuracao;
          }
     }

     public void setConfiguracao(Configuracao configuracao) {
          this.configuracao = configuracao;
     }

     public void uploadTopo(FileUploadEvent event) throws IOException {
          configuracao.setBannerTopoPromocao(UXUtil.upload(event, Config.caminhoBanner));
     }

     public void removerTopo() {
          configuracao.setBannerTopoPromocao(null);
     }

     public void uploadLateral1(FileUploadEvent event) throws IOException {
          configuracao.setBannerLateralPromocao1(UXUtil.upload(event, Config.caminhoBanner));
     }

     public void removerLateral() {
          configuracao.setBannerLateralPromocao1("");
     }

     public void uploadLateral2(FileUploadEvent event) throws IOException {
          configuracao.setBannerLateralPromocao2(UXUtil.upload(event, Config.caminhoBanner));
     }

     public void removerLateral2() {
          configuracao.setBannerLateralPromocao2("");
     }

     public void uploadBusca1(FileUploadEvent event) throws IOException {
          configuracao.setBannerBusca1(UXUtil.upload(event, Config.caminhoBanner));
     }

     public void removerBusca() {
          configuracao.setBannerBusca1("");
     }

     public void uploadBusca2(FileUploadEvent event) throws IOException {
          configuracao.setBannerBusca2(UXUtil.upload(event, Config.caminhoBanner));
     }
     public void removerBusca2() {
          configuracao.setBannerBusca2("");
     }

//        RequestContext rc = RequestContext.getCurrentInstance();
//        rc.update(":frmBanner:image");
     public boolean verificarOfensa(String ofensa) {
          List<String> palavras = UXUtil.delimitar(ofensa.toUpperCase(), " ");
          List<String> xingamen = UXUtil.delimitar(getConfiguracao().getOfensas(), ",");
          int saida = 0;
//          System.out.println(palavras);
//          System.out.println(xingamen);
          for (String s : palavras) {
               for (String x : xingamen) {
                    if (x.equals(s)) {
                         saida = 1;
                         break;
                    }
               }
          }
          if (saida == 1) {
               MensageFactory.warn("O uso de palavras de baixo calão não é aceito!", "");
               return true;
          } else {
               return false;
          }
     }

     public String getTeste() {
          return teste;
     }

     public void setTeste(String teste) {
          this.teste = teste;
     }
}
