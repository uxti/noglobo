/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ux.view;

import com.ux.controller.CidadeEJB;
import com.ux.controller.EstadoEJB;
import com.ux.model.Cidade;
import com.ux.model.Estado;
import com.ux.util.MensageFactory;
import com.ux.util.UXUtil;
import java.io.Serializable;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;
import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.naming.InitialContext;
import javax.sql.DataSource;

/**
 *
 * @author JOAOPAULO
 */
@ManagedBean
@ViewScoped
public class CidadeMB implements Serializable {

    @EJB
    CidadeEJB cEJB;
    private Cidade cidade;
    @EJB
    EstadoEJB eEJB;
    private Estado estado;

    public CidadeMB() {
        novo();
    }


    public void novo() {
        cidade = new Cidade();
        estado = new Estado();
    }

    public void salvar() {
        if (estado != null) {
            cidade.setIdEstado(estado);
        }
        cidade.setSlug(UXUtil.formatString(cidade.getNome()));
        if (cidade.getIdCidade() == null) {
            try {
                cidade.setSlug(UXUtil.formatString(cidade.getNome()));
                cEJB.Salvar(cidade);
                novo();
                MensageFactory.info("Cidade salva com sucesso!", null);
            } catch (Exception e) {
                MensageFactory.info("Erro ao tentar salvar!", null);
                System.out.println("O erro ocorrido foi: " + e);
            }
        } else {
            try {
                cEJB.Atualizar(cidade);
                novo();
                MensageFactory.info("Estado atualizado com sucesso!", null);
            } catch (Exception e) {
                MensageFactory.error("Erro ao tentar atualizar!", null);
                System.out.println("O erro ocorrido foi: " + e);
            }
        }
    }

    public void excluir() {
        try {
            cEJB.Excluir(cidade, cidade.getIdCidade());
            novo();
            MensageFactory.info("Categoria excluida com sucesso!", null);
        } catch (Exception e) {
            MensageFactory.error("Erro ao tentar excluir!", null);
            System.out.println("Erro ocorrido é: " + e);
        }
    }

    public List<Cidade> listarCidades() {
        return cEJB.ListarTodos();
    }

    //get e set
    public Cidade getCidade() {
        try {
            if (cidade != null) {
                estado = cidade.getIdEstado();
            }
        } catch (Exception e) {
            System.out.println("a cidade veio nula");
        }
        return cidade;
    }

    public void setCidade(Cidade cidade) {
        this.cidade = cidade;
    }

    public Estado getEstado() {
        return estado;
    }

    public void setEstado(Estado estado) {
        this.estado = estado;
    }
  
}
