/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ux.view;

import com.ux.config.Config;
import com.ux.controller.ClienteEJB;
import com.ux.controller.CompraEJB;
import com.ux.controller.EmpresaEJB;
import com.ux.controller.EnderecoEJB;
import com.ux.controller.ProdutoEJB;
import com.ux.controller.UsuarioEJB;
import com.ux.controller.VendasEJB;
import com.ux.model.Cliente;
import com.ux.model.Compra;
import com.ux.model.Empresa;
import com.ux.model.Foto;
import com.ux.model.Usuario;
import com.ux.pojo.Carrinho;
import com.ux.pojo.CarrinhoEmpresa;
import com.ux.pojo.Endereco;
import com.ux.pojo.EnderecoEntrega;
import com.ux.pojo.Pedido;
import com.ux.config.Configuracoes;
import com.ux.controller.ConfiguracaoEJB;
import com.ux.model.Configuracao;
import com.ux.util.MensageFactory;
import com.ux.util.Security;
import com.ux.util.UXUtil;
import java.io.File;
import java.io.IOException;
import java.io.Serializable;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletRequest;
import org.apache.commons.io.FileUtils;
import org.apache.commons.mail.EmailException;
import org.primefaces.context.RequestContext;
import org.primefaces.event.FileUploadEvent;

/**
 *
 * @author Renato
 */
@ManagedBean
@SessionScoped
public class controleSessaoMB implements Serializable {

    @EJB
    UsuarioEJB uEJB;
    @EJB
    ProdutoEJB pEJB;
    @EJB
    EmpresaEJB eEJB;
    @EJB
    EnderecoEJB endEJB;
    @EJB
    VendasEJB vEJB;
    @EJB
    CompraEJB cEJB;
    @EJB
    ClienteEJB cliEJB;
    private Usuario usuario;
    private static Usuario usuarioLogado;
    private Empresa empresaLogada;
    private boolean loggedIn;
    private List<String> diasFuncionamento;
    private List<Carrinho> carrinho;
    private List<CarrinhoEmpresa> itensDoCarrinhoPorEmpresa;
    private String tipoServico;
    private String cep;
    private Endereco enderecoWS;
    private EnderecoEntrega enderecoEntrega;
    private String buscaCorrente;
    private Cliente cliente;
    private String novaSenha, senhaAtual;
    private String keyword;
    @EJB
    ConfiguracaoEJB conEJB;
    private Configuracao configuracao;

    /**
     * Creates a new instance of controleSessaoMB
     */
    public controleSessaoMB() {
        usuario = new Usuario();
        usuarioLogado = new Usuario();
        empresaLogada = new Empresa();
        carrinho = new ArrayList<Carrinho>();
        itensDoCarrinhoPorEmpresa = new ArrayList<CarrinhoEmpresa>();
        enderecoEntrega = new EnderecoEntrega();
        enderecoWS = new Endereco();
        cliente = new Cliente();
    }

    public controleSessaoMB(List<CarrinhoEmpresa> car, Usuario u, List<Carrinho> cars) {
        itensDoCarrinhoPorEmpresa = car;
        usuarioLogado = u;
        carrinho = cars;
    }

    public void zerarSenha() {
        senhaAtual = "";
    }

    public Configuracao getConfiguracao() {
        if (cEJB.Contar() >= 1) {
//               System.out.println("trouxe do banco");
            return configuracao = conEJB.ListarTodos().get(0);
        } else {
//               System.out.println("nao contou nenhum");
            return configuracao;
        }
    }

    public void setConfiguracao(Configuracao configuracao) {
        this.configuracao = configuracao;
    }

    public void alterarSenha() throws Exception {
        usuarioLogado.setSenha(Security.encrypt(novaSenha));
        uEJB.Atualizar(usuarioLogado);
        MensageFactory.info("Sua senha foi alterada!", "");
        senhaAtual = "";
        novaSenha = "";
    }

    public void logarUsuarioOauth() throws Exception {

        Map<String, String> par = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap();
        if (par.get("usk") != null) {
            if (!par.get("usk").isEmpty() && !par.get("ksu").isEmpty()) {
                String user = Security.decrypt(par.get("usk"));
                String senha = par.get("ksu");
                vai(user, senha);
            }
        }


    }

    public void atualizarDados() {
        cliEJB.Atualizar(usuarioLogado.getIdCliente());
        MensageFactory.info("Seu dados foram atualizados com sucesso!", null);
    }

    public String vai() throws URISyntaxException, Exception {
        usuario.setSenha(Security.encrypt(usuario.getSenha()));
        usuarioLogado = uEJB.getUserByName(usuario.getEmail(), usuario.getSenha());
        if (usuarioLogado.getEmail() != null) {
            loggedIn = true;
            HttpServletRequest request = (HttpServletRequest) FacesContext
                    .getCurrentInstance().getExternalContext().getRequest();
            request.setAttribute("usuario", usuarioLogado);
//            System.out.println("achou usuario");
            if (usuarioLogado.getIdPerfil().getDescricao().equals("Cliente")) {
//                FacesContext ctxt = FacesContext.getCurrentInstance();
//                ExternalContext ext = ctxt.getExternalContext();

//                URI uri = new URI(ext.getRequestScheme(),
//                        null, ext.getRequestServerName(), ext.getRequestServerPort(),
//                        ext.getRequestContextPath(), null, null);
//                System.out.println(ext.getRequestScheme());
//                System.out.println(ext.getRequestServerName());
//                System.out.println(ext.getRequestServerPort());
//                System.out.println(ext.getRequestContextPath());
//                System.out.println(uri.toASCIIString());
                return "index.xhtml?faces-redirect=true";
            } else {
                return "/Restrito/index.xhtml?faces-redirect=true";
            }
        } else {
            FacesMessage msg = new FacesMessage("Usuário ou senha inválidos!", "ERROR MSG");
            usuario.setSenha("");
            msg.setSeverity(FacesMessage.SEVERITY_ERROR);
            FacesContext.getCurrentInstance().addMessage(null, msg);
            return "";
        }
    }

    public String vai(String user, String pass) throws URISyntaxException, Exception {
//          System.out.println("user " + user);
//          System.out.println("pass " + pass);
        usuarioLogado = uEJB.getUserByName(user, pass);
        if (usuarioLogado.getEmail() != null) {
            loggedIn = true;
            HttpServletRequest request = (HttpServletRequest) FacesContext
                    .getCurrentInstance().getExternalContext().getRequest();
            request.setAttribute("usuario", usuarioLogado);
//            System.out.println("achou usuario");
            if (usuarioLogado.getIdPerfil().getDescricao().equals("Cliente")) {
                FacesContext ctxt = FacesContext.getCurrentInstance();
                ExternalContext ext = ctxt.getExternalContext();

                URI uri = new URI(ext.getRequestScheme(),
                        null, ext.getRequestServerName(), ext.getRequestServerPort(),
                        ext.getRequestContextPath(), null, null);
//                    System.out.println(ext.getRequestScheme());
//                    System.out.println(ext.getRequestServerName());
//                    System.out.println(ext.getRequestServerPort());
//                    System.out.println(ext.getRequestContextPath());
//                    System.out.println(uri.toASCIIString());
                return "/index.xhtml?faces-redirect=true";
            } else {
                return "/Restrito/index.xhtml?faces-redirect=true";
            }
        } else {
            FacesMessage msg = new FacesMessage("Login error!", "ERROR MSG");
            msg.setSeverity(FacesMessage.SEVERITY_ERROR);
            FacesContext.getCurrentInstance().addMessage(null, msg);
            return "";
        }
    }

    public String loginPagamento() throws URISyntaxException {
        try {
//               System.out.println(usuario.getEmail());
//               System.out.println(usuario.getSenha());
            usuario.setSenha(Security.encrypt(usuario.getSenha()));
            usuarioLogado = uEJB.getUserByName(usuario.getEmail(), usuario.getSenha());
            if (usuarioLogado.getEmail() != null) {
                loggedIn = true;
                HttpServletRequest request = (HttpServletRequest) FacesContext
                        .getCurrentInstance().getExternalContext().getRequest();
                request.setAttribute("usuario", usuarioLogado);
                System.out.println("achou usuario");
                System.out.println(usuarioLogado.getIdPerfil().getDescricao());
                
                if (usuarioLogado.getIdPerfil().getDescricao().equals("Cliente")) {
                    FacesContext ctxt = FacesContext.getCurrentInstance();
                    ExternalContext ext = ctxt.getExternalContext();

                    URI uri = new URI(ext.getRequestScheme(),
                            null, ext.getRequestServerName(), ext.getRequestServerPort(),
                            ext.getRequestContextPath(), null, null);
//                         System.out.println(ext.getRequestScheme());
//                         System.out.println(ext.getRequestServerName());
//                         System.out.println(ext.getRequestServerPort());
//                         System.out.println(ext.getRequestContextPath());
//                         System.out.println(uri.toASCIIString());
                    return "";
                } else {
                    return "";
                }
            } else {
                FacesMessage msg = new FacesMessage("Falha ao logar!", "ERROR MSG");
                msg.setSeverity(FacesMessage.SEVERITY_ERROR);
                FacesContext.getCurrentInstance().addMessage(null, msg);
                return "";
            }
        } catch (Exception ex) {
            Logger.getLogger(controleSessaoMB.class.getName()).log(Level.SEVERE, null, ex);
        }
        return "";
    }

    public String doLogout() {
        loggedIn = false;
        usuarioLogado = new Usuario();
        usuario = new Usuario();
        // Set logout message
        FacesMessage msg = new FacesMessage("Logout success!", "INFO MSG");
        msg.setSeverity(FacesMessage.SEVERITY_INFO);
        FacesContext.getCurrentInstance().addMessage(null, msg);
        return "/index.xhtml?faces-redirect=true";
    }

    public String minhaConta() {
        if (usuarioLogado.getIdPerfil().getDescricao().equals("Administrador") || usuarioLogado.getIdPerfil().getDescricao().equals("Empresa")) {
            return "Restrito";
        } else if (usuarioLogado.getIdCliente() != null) {
            return "minhaconta";
        } else {
            return "";
        }
    }

    public void verificarSenhaAtualComDigitada() throws Exception {
        if (usuarioLogado.getIdUsuario() != null) {
//               System.out.println("ta logando");
            if (Security.decrypt(usuarioLogado.getSenha()).equals(senhaAtual)) {
                senhaAtual = "1";
//                    System.out.println(senhaAtual);
            } else {
                MensageFactory.warn("A senha atual não confere! Verifique-a e digite novamente!", "");
            }
        }
    }

    public void recuperarSenha() throws EmailException, Exception {
        Usuario u = uEJB.getUserByEmail(usuario.getEmail());
        if (u.getEmail() == null) {
            MensageFactory.warn("Atenção! O email que você digitou não foi localizado em nossa base.", "");
        } else {
            String senhaTemporaria = String.valueOf(Math.random() * 2L);
//               System.out.println(senhaTemporaria);
            UXUtil.enviarEmailConfigurado(u.getEmail(), u.getIdCliente().getPrimeiroNome(), "Recuperação de Senha noGlobo", Configuracoes.mensagemRecuperacaoSenha(senhaTemporaria, u));
            u.setSenha(Security.encrypt(senhaTemporaria));
            uEJB.Atualizar(u);
            MensageFactory.info("Senha recuperada com sucesso. Verifique seu email.", "");
        }
    }

    public void uploadEmpresaLogo(FileUploadEvent event) throws IOException {
        if (empresaLogada.getIdEmpresa() == null) {
            new File(Config.caminhoEmpresa + eEJB.afterID()).mkdir();
            FileUtils.copyFile(new File(Config.indexModel), new File(Config.caminhoEmpresa + eEJB.afterID() + "\\index.xhtml"));
            empresaLogada.setLogo(UXUtil.upload(event, Config.caminhoEmpresa + eEJB.afterID() + "\\", 400, 300));
        } else {
            if (!new File(Config.caminhoEmpresa + empresaLogada.getIdEmpresa()).exists()) {
                new File(Config.caminhoEmpresa + empresaLogada.getIdEmpresa()).mkdir();
                FileUtils.copyFile(new File(Config.indexModel), new File(Config.caminhoEmpresa + empresaLogada.getIdEmpresa() + "\\index.xhtml"));
            }
            empresaLogada.setLogo(UXUtil.upload(event, Config.caminhoEmpresa + empresaLogada.getIdEmpresa() + "\\", 400, 300));
        }
    }

    public void uploadEmpresaCapa(FileUploadEvent event) throws IOException {
        if (empresaLogada.getIdEmpresa() == null) {
            new File(Config.caminhoEmpresa + eEJB.afterID()).mkdir();
            FileUtils.copyFile(new File(Config.indexModel), new File(Config.caminhoEmpresa + eEJB.afterID() + "\\index.xhtml"));
            empresaLogada.setCapa(UXUtil.upload(event, Config.caminhoEmpresa + eEJB.afterID() + "\\", 400, 300));
        } else {
            if (!new File(Config.caminhoEmpresa + empresaLogada.getIdEmpresa()).exists()) {
                new File(Config.caminhoEmpresa + empresaLogada.getIdEmpresa()).mkdir();
                FileUtils.copyFile(new File(Config.indexModel), new File(Config.caminhoEmpresa + empresaLogada.getIdEmpresa() + "\\index.xhtml"));
            }
            empresaLogada.setCapa(UXUtil.upload(event, Config.caminhoEmpresa + empresaLogada.getIdEmpresa() + "\\", 400, 300));
        }
    }

    public void uploadFotosEmpresa(FileUploadEvent event) {
        Foto f = new Foto();
        f.setUrl(UXUtil.upload(event, Config.caminhoEmpresa + empresaLogada.getIdEmpresa() + "\\", 400, 300));
        try {
            empresaLogada.getFotoList().add(f);
//               System.out.println(empresaLogada.getFotoList().size());
        } catch (Exception e) {
            empresaLogada.setFotoList(new ArrayList<Foto>());
            empresaLogada.getFotoList().add(f);
//               System.out.println(empresaLogada.getFotoList().size());
        }
    }

    public void excluirFotoEmpresa(Foto f) {
        if (new File(Config.caminhoEmpresa + f.getUrl()).exists()) {
            new File(Config.caminhoEmpresa + f.getUrl()).delete();
        }
        int indice = 0;
        for (Foto ff : empresaLogada.getFotoList()) {
            if (ff.getUrl().equals(f.getUrl())) {
                break;
            } else {
                indice++;
            }
        }
        if (f.getIdFoto() != null) {
            pEJB.excluirFoto(f);
        }
        empresaLogada.getFotoList().remove(indice);

    }

    public Usuario getUsuario() {
        return usuario;
    }

    public void setUsuario(Usuario usuario) {
        this.usuario = usuario;
    }

    public boolean isLoggedIn() {
        return loggedIn;
    }

    public void setLoggedIn(boolean loggedIn) {
        this.loggedIn = loggedIn;
    }

    public Usuario getUsuarioLogado() {
        return usuarioLogado;
    }

    public void setUsuarioLogado(Usuario usuarioLogado) {
        this.usuarioLogado = usuarioLogado;
    }

    public List<String> getDiasFuncionamento() {
        return diasFuncionamento;
    }

    public void setDiasFuncionamento(List<String> diasFuncionamento) {
        this.diasFuncionamento = diasFuncionamento;
    }

    public Empresa getEmpresaLogada() {
        empresaLogada = usuarioLogado.getIdEmpresa();
        try {
            if (empresaLogada.getDiasFechados() != null) {
                if (diasFuncionamento == UXUtil.delimitar(empresaLogada.getDiasFechados(), ",")) {
                    System.out.println("teve aqui");
                    diasFuncionamento = UXUtil.delimitar(empresaLogada.getDiasFechados(), ",");
                }
            } else if (empresaLogada.getDiasFechados() == null && diasFuncionamento.size() > 0) {
                diasFuncionamento = UXUtil.delimitar(empresaLogada.getDiasFechados(), ",");
            }
        } catch (Exception e) {
        }
        return empresaLogada;
    }

    public String subString(String val) {
        return val.substring(0, 5);
    }

    public String subString(String val, int lenght) {
        return val.substring(0, lenght);
    }

    public void verDia() {
        System.out.println(diasFuncionamento);
    }

    public void atualizarConfiguracoes() {
        System.out.println("yeahh" + diasFuncionamento);
        StringBuilder sb = new StringBuilder();
        try {
            for (String string : diasFuncionamento) {
                System.out.println("Dia" + string);
                sb.append(string).append(",");
            }
            empresaLogada.setDiasFechados(sb.toString());
            eEJB.salvarEmpresa(empresaLogada);
            MensageFactory.info("Configurações atualizadas com sucesso!", null);
        } catch (Exception e) {
            MensageFactory.error("Não foi possível atualizar as configurações!", null);
            System.out.println(e);
        }
    }

    public void cadastrarUsuario() {
        System.out.println("ddoododod");
        try {
            System.out.println("=D");
            cliente.setDtCadastro(new Date());
            cliente.setEmail(usuario.getEmail());
            usuario.setInativo(true);
            uEJB.cadastrarUsuario(usuario, cliente);
            MensageFactory.info("Usuário cadastrado com sucesso!", null);
            usuario = new Usuario();
            cliente = new Cliente();
        } catch (Exception e) {
            MensageFactory.warn("Não foi possivel cadastrar!", null);
            System.out.println(e);
        }
    }
    
    
//    public void addProdutoCarrinho(Produto p, int quantidade) {
//        Carrinho c = new Carrinho();
//        c.setProduto(p);
//        c.setQuantidade(quantidade);
//        carrinho.add(c);
//    }
//    public void consultarFreteCarrinho(List<Carrinho> carrinho) throws MalformedURLException, IOException {
////        enderecoEntrega = new EnderecoEntrega();
////        for (Carrinho c : carrinho) {
////            FreteWS f = new FreteWS();
////            Map<String, Object> params = new LinkedHashMap<String, Object>();
////            params.put("nCdEmpresa", ""); //nao é obrigatorio
////            params.put("sDsSenha", ""); // nao é obrigatorio
////
////            params.put("sCdMaoPropria", "s");
////            params.put("sCdAvisoRecebimento", "n");
////            params.put("StrRetorno", "xml");
////            params.put("nCdServico", tipoServico);
////            //Obrigatorios
////            params.put("sCepOrigem", c.getProduto().getIdEmpresa().getCep().replace("-", ""));
////            params.put("sCepDestino", cep.replace("-", ""));
////            params.put("nVlPeso", c.getProduto().getPesoKg().toString().replace(".00", ""));
////            params.put("nCdFormato", c.getProduto().getFormatoEncomenda().toString().replace(".00", ""));
////            params.put("nVlComprimento", c.getProduto().getComprimentoCm().toString().replace(".00", ""));
////            params.put("nVlAltura", c.getProduto().getAlturaCm().toString().replace(".00", ""));
////            params.put("nVlLargura", c.getProduto().getLarguraCm().toString().replace(".00", ""));
////            params.put("nVlDiametro", c.getProduto().getDiametroCm().toString().replace(".00", ""));
////            params.put("nVlValorDeclarado", (c.getProduto().getPreco().multiply(new BigDecimal(c.getQuantidade())).toString().replace(".00", "")));
////            c.setFrete(f.consultarFrete(params));
////
////
////            if (f.getErro() == null) {
////                MensageFactory.info("Frete calculado com sucesso!", null);
////                Endereco e = new Endereco();
////                e = e.pesquisarEnderecoPorCep(cep);
////                enderecoWS = e;
////                enderecoEntrega.setBairro(e.getBairro());
////                enderecoEntrega.setCidade(e.getCidade());
////                enderecoEntrega.setLogradouro(e.getLogradouro());
////                enderecoEntrega.setTipo_logradouro(e.getTipo_logradouro());
////                enderecoEntrega.setUf(e.getUf());
////                System.out.println(enderecoEntrega.getLogradouro());
////
////            } else {
////                MensageFactory.warn("Não foi possível calcular o frete!", null);
////            }
////
////        }
////    }
//
//   
//
//    public BigDecimal subTotal(List<Carrinho> cs) {
//        BigDecimal total = new BigDecimal(BigInteger.ZERO);
//        for (Carrinho c : cs) {
//            total = total.add(c.getProduto().getPreco().multiply(new BigDecimal(c.getQuantidade())));
//        }
//        return total;
//    }
//
//    public BigDecimal totalFrete(List<Carrinho> cs) {
//        BigDecimal total = new BigDecimal(BigInteger.ZERO);
//        try {
//            for (Carrinho c : cs) {
//                total = total.add(new BigDecimal(c.getFrete().getValor().replace(",", ".")).multiply(new BigDecimal(c.getQuantidade())));
//            }
//            return total;
//        } catch (Exception e) {
//            return new BigDecimal(BigInteger.ZERO);
//        }
//    }
//
//    public BigDecimal totalFinal(List<Carrinho> cs) {
//        BigDecimal subTotal = subTotal(cs);
//        BigDecimal totalFrete = totalFrete(cs);
//        return subTotal.add(totalFrete);
//    }
    public void setEmpresaLogada(Empresa empresaLogada) {
        empresaLogada = empresaLogada;
    }

    public List<Carrinho> getCarrinho() {
        return carrinho;
    }

    public void setCarrinho(List<Carrinho> carrinho) {
        this.carrinho = carrinho;
    }

    public String getTipoServico() {
        return tipoServico;
    }

    public void setTipoServico(String tipoServico) {
        this.tipoServico = tipoServico;
    }

    public String getCep() {
        return cep;
    }

    public void setCep(String cep) {
        this.cep = cep;
    }

    public void atualizarDadosMinhaConta() {
        cliEJB.Atualizar(usuarioLogado.getIdCliente());
        MensageFactory.info("Dados atualizados com sucesso!", "");
    }

    public void parTela(String val) {
        keyword = val;
    }

    public List<CarrinhoEmpresa> getItensDoCarrinhoPorEmpresa() {
        itensDoCarrinhoPorEmpresa = new ArrayList<CarrinhoEmpresa>();
        if (!carrinho.isEmpty()) {
            List<Empresa> empresasNoCarrinho = new ArrayList<Empresa>();
            for (Carrinho c : carrinho) {
                if (empresasNoCarrinho.indexOf(c.getProduto().getIdEmpresa()) < 0) {
                    empresasNoCarrinho.add(c.getProduto().getIdEmpresa());
                }
            }
            for (Empresa e : empresasNoCarrinho) {
                List<Carrinho> cars = new ArrayList<Carrinho>();
                for (Carrinho c : carrinho) {
                    if (c.getProduto().getIdEmpresa().getIdEmpresa().equals(e.getIdEmpresa())) {
                        cars.add(c);
                    }
                }
                CarrinhoEmpresa ce = new CarrinhoEmpresa();
                ce.setEmpresa(e);
                ce.setCarrinhos(cars);
                itensDoCarrinhoPorEmpresa.add(ce);
            }
        }
        return itensDoCarrinhoPorEmpresa;
    }

    public List<Compra> listarComprasUsuario() {
        return cEJB.listarComprasPorUsuario(usuarioLogado);
    }

    public List<Pedido> listarPedidosUsuario() {
        return cEJB.listarPedidosPorUsuario(usuarioLogado);
    }

    public List<Pedido> listarPedidos() {
        return cEJB.listarPedidosPorUsuario(usuarioLogado);
    }

    public List<Compra> listarCompraPorLote(String lote) {
        return cEJB.listarComprasPorLote(lote);
    }

    public void setItensDoCarrinhoPorEmpresa(List<CarrinhoEmpresa> itensDoCarrinhoPorEmpresa) {
        this.itensDoCarrinhoPorEmpresa = itensDoCarrinhoPorEmpresa;
    }

    public EnderecoEntrega getEnderecoEntrega() {
        return enderecoEntrega;
    }

    public void setEnderecoEntrega(EnderecoEntrega enderecoEntrega) {
        this.enderecoEntrega = enderecoEntrega;
    }

    public String getBuscaCorrente() {
        return buscaCorrente;
    }

    public void setBuscaCorrente(String buscaCorrente) {
        this.buscaCorrente = buscaCorrente;
    }

    public Cliente getCliente() {
        return cliente;
    }

    public void setCliente(Cliente cliente) {
        this.cliente = cliente;
    }

    public String getNovaSenha() {
        return novaSenha;
    }

    public void setNovaSenha(String novaSenha) {
        this.novaSenha = novaSenha;
    }

    public String getSenhaAtual() {
        return senhaAtual;
    }

    public void setSenhaAtual(String senhaAtual) {
        this.senhaAtual = senhaAtual;
    }

    public String getKeyword() {
        return keyword;
    }

    public void setKeyword(String keyword) {
        this.keyword = keyword;
    }
}
