/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ux.view;

import com.ux.controller.ComentarioEJB;
import com.ux.model.Comentario;
import com.ux.model.Empresa;
import com.ux.util.MensageFactory;
import java.io.Serializable;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

/**
 *
 * @author Renato
 */
@ManagedBean
@ViewScoped
public class ComentarioMB implements Serializable {

    /**
     * Creates a new instance of ComentarioMB
     */
    @EJB
    ComentarioEJB cEJB;
    private Comentario comentario;
    private Comentario comentarioResposta;

    public ComentarioMB() {
        comentario = new Comentario();
        comentarioResposta = new Comentario();
    }

    public List<Comentario> comentariosSemResposta(Empresa e) {
        return cEJB.listarComentariosSemRespostas(e);
    }

    public Comentario getComentario() {
        return comentario;
    }

    public void setComentario(Comentario comentario) {
        this.comentario = comentario;
    }

    public void selecionarComentario(Comentario comentario) {
        comentarioResposta.setIdProduto(comentario.getIdProduto());
        comentarioResposta.setIdCliente(comentario.getIdCliente());
        this.comentario = comentario;
    }

    public void enviarComentario() {
        comentarioResposta.setDtComentario(new Date());
        comentarioResposta.setTipo("R");
//        cEJB.inserirComentarioResposta(comentarioResposta);
        this.comentario.setIdComentarioReplica(comentarioResposta);
        cEJB.Atualizar(this.comentario);
        this.comentario = new Comentario();
        comentarioResposta = new Comentario();
        MensageFactory.info("Resposta enviada com sucesso!", null);
    }

    public void excluirComentario(Comentario c) {
        cEJB.Excluir(c, c.getIdComentario());
        MensageFactory.info("O comentário foi removido com sucesso!", null);
    }

    public Comentario getComentarioResposta() {
        return comentarioResposta;
    }

    public void setComentarioResposta(Comentario comentarioResposta) {
        this.comentarioResposta = comentarioResposta;
    }

    public String subMensagem(String s) {
        if (s.length() > 23) {
            return s.substring(0, 23) + "...";
        } else {
            return s.substring(0, s.length()) + "...";
        }
    }

    public String calcularDiferencaPostagem(Date dtPostagem) {
        long hora = 60;     //minutos
        long dia = 1440;    //minutos

        Calendar c = Calendar.getInstance();
        c.setTime(dtPostagem);

        Calendar a = Calendar.getInstance(); // data atual
        long minutos = ((a.getTime().getTime() / 60000) - (c.getTime().getTime() / 60000));
        

        if (minutos < 60) {
            return minutos + " minutos atrás.";
        } else if (minutos >= 60 && minutos < 1440) {
            return minutos / 60 + " horas atrás.";
        } else if (minutos >= 1440) {
//            System.out.println(minutos);
//            System.out.println(60);
//            System.out.println(1440);
            return ((minutos / 60) / 24) + " dias atrás";
        } else {
            return "";
        }
    }
}
