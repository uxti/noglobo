/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ux.view;

import com.ux.controller.UsuarioEJB;
import com.ux.model.Usuario;
import com.ux.util.UXUtil;
import java.io.Serializable;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.net.URI;
import java.net.URISyntaxException;
import javax.ejb.EJB;
import javax.faces.FacesException;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.bean.SessionScoped;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;

/**
 *
 * @author Renato
 */
@ManagedBean
@SessionScoped
public class SessaoMB implements Serializable{

    @EJB
    UsuarioEJB uEJB;
    private Usuario usuario;
    private Usuario usuarioLogado;
    private boolean loggedIn;
    public BigDecimal valorMin, valorMax; 
    public String ordenacaoBusca;
    
    

    public SessaoMB() {
        usuario = new Usuario();
        usuarioLogado = new Usuario();
        valorMin = new BigDecimal(BigInteger.ZERO);
        valorMax = new BigDecimal(BigInteger.ZERO);
        ordenacaoBusca = "";        
    }
    
    

    public String logar() {
        System.out.println(usuario.getEmail());
        usuarioLogado = uEJB.getUserByName(usuario.getEmail(), usuario.getSenha());
        if (usuarioLogado.getEmail() != null) {
            loggedIn = true;
            return "/Restrito/index.xhtml?faces-redirect=true";
        } else {
            FacesMessage msg = new FacesMessage("Login error!", "ERROR MSG");
            msg.setSeverity(FacesMessage.SEVERITY_ERROR);
            FacesContext.getCurrentInstance().addMessage(null, msg);
            return "";
        }
    }

    public String doLogout() {
        loggedIn = false;
        // Set logout message
        FacesMessage msg = new FacesMessage("Logout success!", "INFO MSG");
        msg.setSeverity(FacesMessage.SEVERITY_INFO);
        FacesContext.getCurrentInstance().addMessage(null, msg);
        return "/index.xhtml";
    }

    public String applicationUri(String compare) {
        try {
            compare = "Restrito/" + compare;
            FacesContext ctxt = FacesContext.getCurrentInstance();
            ExternalContext ext = ctxt.getExternalContext();
            URI uri = new URI(ext.getRequestScheme(), null, ext.getRequestServerName(), ext.getRequestServerPort(), ext.getRequestContextPath(), ext.getRequestServletPath(), null);

            //            System.out.println("Compare: " + compare + "\n");
//            System.out.println("URL: " + uri.toASCIIString());
            if (uri.toASCIIString().contains(compare)) {
                return "active";
            } else {
                return null;
            }
        } catch (URISyntaxException e) {
            throw new FacesException(e);
        }
    }

    public String applicationUri() {
        try {
            FacesContext ctxt = FacesContext.getCurrentInstance();
            ExternalContext ext = ctxt.getExternalContext();
            URI uri = new URI(ext.getRequestContextPath(), ext.getRequestServletPath(), null);
//            System.out.println(uri.toString());
            String s = uri.toString().replace("/noGlobo:/Restrito/", "").replace("/", "").replace("index.xhtml", "").replace("noglobo:Restrito", "");
//            System.out.println(s);
            return s;
        } catch (URISyntaxException e) {
            throw new FacesException(e);
        }
    }

    public String checarURL(String url2) throws URISyntaxException {
//        FacesContext ctxt = FacesContext.getCurrentInstance();
//        ExternalContext ext = ctxt.getExternalContext();
//        URI uri = new URI(ext.getRequestScheme(), null, ext.getRequestServerName(), ext.getRequestServerPort(), ext.getRequestContextPath(), ext.getRequestServletPath(), null);
//        String urll = uri.toString();
        if (url2.contains("/Restrito/Restrito/")) {
            url2.replace("/Restrito/Restrito", "/Restrito/");
            return url2;
        } else {
            return url2;
        }
    }

    public String formatarMoedaView(BigDecimal val) {
        return UXUtil.formatarMoeda(val);
    }

    public Usuario getUsuario() {
        return usuario;
    }

    public void setUsuario(Usuario usuario) {
        this.usuario = usuario;
    }

    public Usuario getUsuarioLogado() {
        return usuarioLogado;
    }

    public void setUsuarioLogado(Usuario usuarioLogado) {
        this.usuarioLogado = usuarioLogado;
    }

    public boolean isLoggedIn() {
        return loggedIn;
    }

    public void setLoggedIn(boolean loggedIn) {
        this.loggedIn = loggedIn;
    }

    public BigDecimal getValorMin() {
        return valorMin;
    }

    public void setValorMin(BigDecimal valorMin) {
        this.valorMin = valorMin;
    }

    public BigDecimal getValorMax() {
        return valorMax;
    }

    public void setValorMax(BigDecimal valorMax) {
        this.valorMax = valorMax;
    }

    public String getOrdenacaoBusca() {
        return ordenacaoBusca;
    }

    public void setOrdenacaoBusca(String ordenacaoBusca) {
        this.ordenacaoBusca = ordenacaoBusca;
    }
    
}
