/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ux.view;

import br.com.moip.Authentication;
import br.com.moip.Moip;
import br.com.moip.MoipException;
import br.com.moip.OAuth;
import br.com.moip.ValidationException;
import br.com.moip.resource.Customer;
import br.com.moip.resource.Order;
import br.com.moip.resource.Payment;
import br.com.moip.resource.structure.Boleto;
import br.com.moip.resource.structure.CreditCard;
import br.com.moip.resource.structure.Holder;
import br.com.moip.resource.structure.Item;
import br.com.moip.resource.structure.Phone;
import br.com.moip.resource.structure.TaxDocument;
import com.ux.config.Config;
import com.ux.controller.CidadeEJB;
import com.ux.controller.CompraEJB;
import com.ux.controller.EstadoEJB;
import com.ux.model.Cidade;
import com.ux.model.Compra;
import com.ux.model.Empresa;
import com.ux.model.Produto;
import com.ux.model.Usuario;
import com.ux.pojo.Carrinho;
import com.ux.pojo.CarrinhoEmpresa;
import com.ux.pojo.CartaoCredito;
import com.ux.pojo.Endereco;
import com.ux.pojo.EnderecoEntrega;
import com.ux.pojo.FreteWS;
import com.ux.config.Configuracoes;
import com.ux.controller.EntregaEJB;
import com.ux.controller.ProdutoEJB;
import com.ux.model.Entrega;
import com.ux.util.MensageFactory;
import com.ux.util.UXUtil;
import java.io.IOException;
import java.io.Serializable;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.net.ConnectException;
import java.net.MalformedURLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletRequest;
import org.apache.commons.mail.EmailException;
import org.primefaces.context.RequestContext;

/**
 *
 * @author Renato
 */
@ManagedBean
@SessionScoped
public class CarrinhoMB implements Serializable {

    @EJB
    CompraEJB cEJB;
    @EJB
    CidadeEJB cidEJB;
    @EJB
    EstadoEJB eEJB;
    @EJB
    ProdutoEJB pEJB;
    @EJB
    EntregaEJB entEJB;
    private List<CarrinhoEmpresa> itensDoCarrinhoPorEmpresa;
    private CarrinhoEmpresa itemComprando;
    private Endereco enderecoWS;
    private EnderecoEntrega enderecoEntrega;
    private String tipoServico;
    private String cep;
    private List<FreteWS> fretesDisponiveis;
    private FreteWS freteSelecionado;
    private Usuario usuarioLogado;
    private String linkBoleto;
    private List<Carrinho> carrinho;
    private List<Carrinho> cars;
    private List<Produto> produtosVejaTambem;
    private CartaoCredito creditCard;
    private Integer numeroParcelas;
    Authentication auth = new OAuth(Config.oAuth);
    private Boolean retirada;
    private String telefone;
    private BigDecimal custoEntregaDelivery;
    private Entrega bairroSelecionad2;

//    String chavePublica = "-----BEGIN PUBLIC KEY-----MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAgv0H+6/1EdUOy7yPVhKq7KlxHuCkij5W1StiSDHwengFNZr3+VM9SmbItSvAMZZN5HNG0yR1Dbt1Coz7bqbZCN6GfYAGOwQdkScOGnbjLiSdV+8I0de7B2z9TofJ1bKDvbBYmzzhVl2MzEC6ygMjCxCAyRHVcr4LM/wkDiavrVasZZ0qN7tr1d5+K+0g92Yop+UgLeUq4aueuCotzQNJ/sltOmT+hPl0pzCQr0zQ5x36aivqMERcWwgoEsVbaAbcTDKCafjB3WkaGV2vB+rDA5aoHiF4zr+VqfA8d5nDnIaw7oxZ6EXK1XqJE1DvakXQBEYRT1C6r3X2y64tzHv1UQIDAQAB-----END PUBLIC KEY-----";
    /**
     * Creates a new instance of CarrinhoMB
     */
    public CarrinhoMB() {
        itensDoCarrinhoPorEmpresa = new ArrayList<CarrinhoEmpresa>();
        enderecoWS = new Endereco();
        enderecoEntrega = new EnderecoEntrega();
        usuarioLogado = new Usuario();
        cars = new ArrayList<Carrinho>();
        carrinho = new ArrayList<Carrinho>();
        creditCard = new CartaoCredito();
        itemComprando = new CarrinhoEmpresa();
        freteSelecionado = new FreteWS();
        fretesDisponiveis = new ArrayList<>();
        custoEntregaDelivery = new BigDecimal(BigInteger.ZERO);
        bairroSelecionad2 = new Entrega();
//        creditCard.setHash(chavePublica);
    }

    public void preCarregarUsuario(Usuario usuarioLogado) {
        if (usuarioLogado != null) {
            this.usuarioLogado = usuarioLogado;
        }
        creditCard = new CartaoCredito();
        if (itensDoCarrinhoPorEmpresa.size() > 0) {
            produtosVejaTambem = pEJB.listarProdutosReferentesPorEmpresaPatrocinadas(2);
        }
    }

    public List<Entrega> bairros(Empresa emp) {
        System.out.println("oi");
        System.out.println(entEJB.listarEntregas(emp).size());
        return entEJB.listarEntregas(emp);
    }

    public void consultarFreteDelivery(List<Carrinho> carrinho, String cep, Empresa emp) throws MalformedURLException, IOException {
        if (cep.equals("_____-___") || cep.isEmpty()) {
            MensageFactory.error("Informe o CEP!", "");
        } else {
            enderecoEntrega = new EnderecoEntrega();
            Endereco e = new Endereco();
            try {
                e = e.pesquisarEnderecoPorCep(cep);
                for (Carrinho c : carrinho) {
                    c.setCep(cep);
                    c.setEndereco(e);
                    c.setTipoFrete("Entrega");

                }
                enderecoEntrega.setBairro(e.getBairro());
                enderecoEntrega.setCidade(e.getCidade());
                enderecoEntrega.setLogradouro(e.getLogradouro());
                enderecoEntrega.setTipo_logradouro(e.getTipo_logradouro());
                enderecoEntrega.setUf(e.getUf());
                enderecoEntrega.setCep(cep);
                for (CarrinhoEmpresa item : itensDoCarrinhoPorEmpresa) {
                    if (item.getCarrinhos().equals(carrinho)) {
                        item.setCep(cep);
                    }
                }
                if (enderecoEntrega.getCidade().equals(emp.getIdCidade().getNome())) {
                } else {
                    MensageFactory.warn("Atenção! Não fazemos entrega para esta cidade.", "");
                }
                freteSelecionado = new FreteWS();
                freteSelecionado.setValor(emp.getValorEntrega().toString());
            } catch (ConnectException ex) {
                MensageFactory.warn("Atenção! Algum problema com a conexao. Tente novamente!", "");
            }


        }
    }

    public void consultarEnderecoDelivery(List<Carrinho> carrinho, String cep) throws MalformedURLException, IOException {
        if (cep.equals("_____-___")) {
            MensageFactory.error("Informe o CEP!", "");
        } else {
            enderecoEntrega = new EnderecoEntrega();
            for (Carrinho c : carrinho) {
                Endereco e = new Endereco();
                e = e.pesquisarEnderecoPorCep(cep);
                c.setEndereco(e);
                c.setCep(cep);
                c.setTipoFrete("Entrega");
//                c.setTipoFrete(verificaTipoFrete(tipoServico));
                enderecoWS = e;
                enderecoEntrega.setBairro(e.getBairro());
                enderecoEntrega.setCidade(e.getCidade());
                enderecoEntrega.setLogradouro(e.getLogradouro());
                enderecoEntrega.setTipo_logradouro(e.getTipo_logradouro());
                enderecoEntrega.setUf(e.getUf());
                enderecoEntrega.setCep(cep);
            }
            for (Carrinho c : carrinho) {
                //System.out.println(c.getTipoFrete());
            }
        }
    }

    public void consultarFreteCarrinho2(List<Carrinho> carrinho, String cep) throws MalformedURLException, IOException {
        if (cep.equals("_____-___")) {
            MensageFactory.error("Informe o CEP!", "");
        } else {
            enderecoEntrega = new EnderecoEntrega();
            fretesDisponiveis = new ArrayList<>();
            freteSelecionado = new FreteWS();
            this.cep = cep;
            FreteWS f2 = new FreteWS();
            if (!cep.contains("_") && !cep.isEmpty()) {
                BigDecimal altura = new BigDecimal(BigInteger.ZERO), largura = new BigDecimal(BigInteger.ZERO),
                        comprimento = new BigDecimal(BigInteger.ZERO), diametro = new BigDecimal(BigInteger.ZERO);
                BigDecimal valorDeclarado = new BigDecimal(BigInteger.ZERO), peso = new BigDecimal(BigInteger.ZERO);
                String cepEmpresa = "", tipoEncomenda = "";

                for (Carrinho c : carrinho) {
                    peso = peso.add(c.getProduto().getPesoKg().multiply(new BigDecimal(c.getQuantidade())));
                    valorDeclarado = valorDeclarado.add(c.getProduto().getPreco().multiply(new BigDecimal(c.getQuantidade())));
                    altura = altura.add(c.getProduto().getAlturaCm().multiply(new BigDecimal(c.getQuantidade())));
                    largura = largura.add(c.getProduto().getLarguraCm());
                    comprimento = comprimento.add(c.getProduto().getComprimentoCm());
                    diametro = diametro.add(c.getProduto().getDiametroCm());
                    cepEmpresa = c.getProduto().getIdEmpresa().getCep();
                    if (carrinho.size() > 1) {
                        tipoEncomenda = "1";
                    } else {
                        tipoEncomenda = c.getProduto().getFormatoEncomenda();
                    }
                }
                //Validações
                if (peso.longValue() > 30) {
                    MensageFactory.warn("Atenção o peso excedeu os 30 Kilos! Remova algum item e faça-o em outro pedido.", "");
                    return;
                }
                if (comprimento.longValue() < 16) {
                    comprimento = new BigDecimal(16);
                } else if (comprimento.longValue() > 105) {
                    MensageFactory.warn("Atenção comprimento excedeu os 105 centímetros. Remova algum item e faça-o em outro pedido.", "");
                    return;
                }
                if (largura.longValue() < 11) {
                    largura = new BigDecimal(11);
                } else if (largura.longValue() > 105) {
                    MensageFactory.warn("Atenção a largura excedeu os 105 centímetros. Remova algum item e faça-o em outro pedido.", "");
                    return;
                }
                if (altura.longValue() < 2) {
                    altura = new BigDecimal(2);
                } else if (altura.longValue() > 105) {
                    MensageFactory.warn("Atenção a altura excedeu os 105 centímetros. Remova algum item e faça-o em outro pedido.", "");
                    return;
                }
                if (comprimento.add(largura).add(altura).longValue() > 200) {
                    MensageFactory.warn("Atenção a soma dos diametros excedeu os 200 centímetros. Remova algum item e faça-o em outro pedido.", "");
                    return;
                }


                FreteWS f = new FreteWS();
                Map<String, Object> params = new LinkedHashMap<String, Object>();
                params.put("nCdEmpresa", "16034449"); //nao é obrigatorio
                params.put("sDsSenha", "21478552"); // nao é obrigatorio

                params.put("sCdMaoPropria", "s");
                params.put("sCdAvisoRecebimento", "n");
                params.put("StrRetorno", "xml");
                params.put("nCdServico", "40010,41106");
                //Obrigatorios
                params.put("sCepOrigem", cepEmpresa);
                //System.out.println("cep selecionado " + cep);
                params.put("sCepDestino", cep.replace("-", ""));
                params.put("nVlPeso", peso.toString().replace(".00", ""));
                params.put("nCdFormato", tipoEncomenda.toString().replace(".00", ""));
                params.put("nVlComprimento", comprimento.toString().replace(".00", ""));
                params.put("nVlAltura", altura.toString().replace(".00", ""));
                params.put("nVlLargura", largura.toString().replace(".00", ""));
                params.put("nVlDiametro", diametro.toString().replace(".00", ""));
                params.put("nVlValorDeclarado", (valorDeclarado.toString().replace(".00", "")));
//            c.setFrete(f.consultarFrete(params));
                System.out.println(params);

                try {
                    for (FreteWS fs : f.consultarFreteAutomaticoPACSEDEX(params).getcServico()) {
                        //System.out.println(fs.getValor());
                        //System.out.println(fs.getPrazoEntrega());
                        if (fretesDisponiveis.size() < 2 && !"0,00".equals(fs.getValor())) {
                            System.out.println("adicionou");
                            fretesDisponiveis.add(fs);
                        } else {
                            MensageFactory.warn("As medidas permitidas pelo correio foram excedidas!", "");
                            break;
                        }
                    }
                } catch (java.net.ConnectException e) {
                    MensageFactory.warn("Verifique sua conexao com a internet!", "");
                }
                //System.out.println(f.getErro());
                for (Carrinho c : carrinho) {

                    if (f.getErro() == null) {
                        //System.out.println("setando endereço");
                        Endereco e = new Endereco();
                        e = e.pesquisarEnderecoPorCep(cep);
                        c.setEndereco(e);
                        c.setCep(cep);
//                c.setTipoFrete(verificaTipoFrete(tipoServico));
                        enderecoWS = e;
                        enderecoEntrega.setBairro(e.getBairro());
                        enderecoEntrega.setCidade(e.getCidade());
                        enderecoEntrega.setLogradouro(e.getLogradouro());
                        enderecoEntrega.setTipo_logradouro(e.getTipo_logradouro());
                        enderecoEntrega.setUf(e.getUf());
                        enderecoEntrega.setCep(cep);
                        //System.out.println(enderecoEntrega.getCep());
                    }
                    f2 = f;
                }
            }
            if (f2.getErro() == null) {
                MensageFactory.info("Frete calculado com sucesso!", null);
            } else {
                MensageFactory.warn("Não foi possível calcular o frete!", null);
            }
        }
    }

    public void consultarFreteCarrinho(List<Carrinho> carrinho, String cep) throws MalformedURLException, IOException {
        enderecoEntrega = new EnderecoEntrega();
        fretesDisponiveis = new ArrayList<FreteWS>();
        freteSelecionado = new FreteWS();
        FreteWS f2 = new FreteWS();
        if (!cep.contains("_") && !cep.isEmpty()) {
            for (Carrinho c : carrinho) {
                FreteWS f = new FreteWS();
                Map<String, Object> params = new LinkedHashMap<String, Object>();
                params.put("nCdEmpresa", "16034449"); //nao é obrigatorio
                params.put("sDsSenha", "21478552"); // nao é obrigatorio

                params.put("sCdMaoPropria", "s");
                params.put("sCdAvisoRecebimento", "n");
                params.put("StrRetorno", "xml");
                params.put("nCdServico", "40010,41106");
                //Obrigatorios
                params.put("sCepOrigem", c.getProduto().getIdEmpresa().getCep().replace("-", ""));
//                System.out.println("cep selecionado " + cep);
                params.put("sCepDestino", cep.replace("-", ""));
                params.put("nVlPeso", c.getProduto().getPesoKg().toString().replace(".00", ""));
                params.put("nCdFormato", c.getProduto().getFormatoEncomenda().toString().replace(".00", ""));
                params.put("nVlComprimento", c.getProduto().getComprimentoCm().toString().replace(".00", ""));
                params.put("nVlAltura", c.getProduto().getAlturaCm().toString().replace(".00", ""));
                params.put("nVlLargura", c.getProduto().getLarguraCm().toString().replace(".00", ""));
                params.put("nVlDiametro", c.getProduto().getDiametroCm().toString().replace(".00", ""));
                params.put("nVlValorDeclarado", (c.getProduto().getPreco().multiply(new BigDecimal(c.getQuantidade())).toString().replace(".00", "")));
//            c.setFrete(f.consultarFrete(params));
                for (FreteWS fs : f.consultarFreteAutomaticoPACSEDEX(params).getcServico()) {
//                    System.out.println(fs.getValor());
//                    System.out.println(fs.getPrazoEntrega());
                    if (fretesDisponiveis.size() < 2) {
//                        System.out.println("adicionou");
                        fretesDisponiveis.add(fs);
                    }
                }
                if (f.getErro() == null) {
                    Endereco e = new Endereco();
                    e = e.pesquisarEnderecoPorCep(cep);
                    c.setEndereco(e);
                    c.setCep(cep);
//                c.setTipoFrete(verificaTipoFrete(tipoServico));
                    enderecoWS = e;
                    enderecoEntrega.setBairro(e.getBairro());
                    enderecoEntrega.setCidade(e.getCidade());
                    enderecoEntrega.setLogradouro(e.getLogradouro());
                    enderecoEntrega.setTipo_logradouro(e.getTipo_logradouro());
                    enderecoEntrega.setUf(e.getUf());
                    enderecoEntrega.setCep(cep);
//                    System.out.println(enderecoEntrega.getCep());
                }
                f2 = f;
            }

            if (f2.getErro() == null) {
                MensageFactory.info("Frete calculado com sucesso!", null);
            } else {
                MensageFactory.warn("Não foi possível calcular o frete!", null);
            }
        }
    }
    private String previousPage = null;

    public void removerItemCarrinho(int index, int indexCE, Carrinho c) {
//          System.out.println("Vamos remover o produto " + c.getProduto().getNome());
//          UIViewRoot viewRoot = FacesContext.getCurrentInstance().getViewRoot();
//          String id = viewRoot.getViewId();
//          System.out.println("ID " + id);
        try {
            CarrinhoEmpresa ce = itensDoCarrinhoPorEmpresa.get(indexCE);
            ce.getCarrinhos().remove(index);
//            System.out.println(ce.getCarrinhos().size());
            if (ce.getCarrinhos().isEmpty()) {
                itensDoCarrinhoPorEmpresa.remove(indexCE);
//                System.out.println(itensDoCarrinhoPorEmpresa);
            }
            int in = 0;
            for (Carrinho cc : carrinho) {
                if (cc.equals(c)) {
                    break;
                } else {
                    in++;
                }
            }
            carrinho.remove(in);
            HttpServletRequest req = (HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext().getRequest();
            String url = req.getRequestURL().toString();
            RequestContext rc = RequestContext.getCurrentInstance();
            rc.execute("window.location.href='" + url + "'");
        } catch (Exception e) {
            System.out.println(e);
        }
    }

    public void vercep(String cep) {
        System.out.println(cep + "eeeeeeeeeee");
    }

    public void pagarComCartaoCredito(List<Carrinho> cs, int index) throws EmailException {
        if (UXUtil.isValidCardNumber(creditCard.getNumber())) {
//            if (UXUtil.isCPF(creditCard.getTaxDocument())) {
            List<Item> itensMoip = new ArrayList<Item>();
            String customID = String.valueOf(new Random().nextInt()).replace("-", "") + String.valueOf(new Date().getTime());
            Compra cDados = new Compra();
            String oauth = "";
            for (Carrinho carrinho : cs) {
                Compra c = new Compra();
                c.setFormaPagamento("Cartão de Crédito");
                c.setNumParcela(numeroParcelas);
                c.setLote(customID);
                c.setDtCompra(new Date());
                c.setIdUsuario(usuarioLogado);
                c.setIdEmpresa(carrinho.getEmpresa());
                c.setIdProduto(carrinho.getProduto());
                c.setValidoAte(UXUtil.addDia(new Date(), 6));
                c.setResponsavel(telefone);
                if (carrinho.getRetirada() == true) {
                    c.setCodigoRetirada(usuarioLogado.getIdCliente().getPrimeiroNome().substring(0, 1).toUpperCase() + usuarioLogado.getIdCliente().getSobrenome().substring(0, 1).toUpperCase() + UXUtil.getLastTree(c.getLote()));
                    c.setRetirar(true);
                    c.setTipoFrete("Retirada");
                }
//                    System.out.println(c.getTipoFrete());
                if (carrinho.getTipoFrete().equals("Entrega")) {
                    c.setValorFrete(new BigDecimal(carrinho.getProduto().getIdEmpresa().getValorEntrega().toString().replace(",", ".")));
                    c.setTipoFrete("Entrega");
                    c.setValidoAte(UXUtil.addDia(new Date(), 1));
                    c.setFrete("Entrega " + c.getValorFrete() + "");
                } else if (carrinho.getTipoFrete().equals("Retirada")) {
                    c.setValorFrete(new BigDecimal(BigInteger.ZERO.toString().replace(",", ".")));
                    c.setTipoFrete("Retirada");
                    c.setValidoAte(UXUtil.addDia(new Date(), 1));
                    c.setFrete("Retirada do produto " + new BigDecimal(BigInteger.ZERO) + "");
                } else {
                    if (carrinho.getRetirada() == false) {
                        c.setFrete(verificaTipoFrete(freteSelecionado.getCodigo()) + " " + freteSelecionado.getValor() + " Entrega em " + freteSelecionado.getPrazoEntrega() + " dias úteis!");
                        c.setValorFrete(new BigDecimal(freteSelecionado.getValor().replace(",", ".")));
                    }
                }
                c.setValorCompra(carrinho.getProduto().getPreco());
                if (carrinho.getRetirada() == false) {
                    c.setValorTotal(totalFinal(cs));

                    c.setBairroEntrega(enderecoEntrega.getBairro());
                    c.setNumeroEntrega(enderecoEntrega.getNumero());
                    c.setCepEntrega(enderecoEntrega.getCep());
                    c.setComplementoEntrega(enderecoEntrega.getComplemento());
                    c.setEnderecoEntrega(enderecoEntrega.getLogradouro());
                    if (cidEJB.pesquisarCidadePorNome(enderecoEntrega.getCidade()).size() == 0) {
                        Cidade cidade = new Cidade();
                        cidade.setNome(enderecoEntrega.getCidade());
                        cidade.setIdEstado(eEJB.pesquisarEstado(enderecoEntrega.getUf()).get(0));
                        cidEJB.Salvar(cidade);
                        c.setIdCidadeEntrega(cidade);
                    } else {
                        c.setIdCidadeEntrega(cidEJB.pesquisarCidadePorNome(enderecoEntrega.getCidade()).get(0));
                    }
                    c.setIdEstadoEntrega(eEJB.pesquisarEstado(enderecoEntrega.getUf()).get(0));
                } else {
                    if (c.getValorTotal() == null) {
                        c.setValorTotal(BigDecimal.ZERO);
                    }
                    c.setValorTotal(totalFinal(cs));
                }
                c.setIdEmpresa(carrinho.getProduto().getIdEmpresa());
                c.setStatus("Aguardando pagamento");


                c.setQuantidade(new BigDecimal(carrinho.getQuantidade()));
                cDados = c;
                cEJB.Atualizar(c);
                Item i = new Item();
                i.setProduct(c.getIdProduto().getNome());
                i.setPrice(converterValorEmInteger(c.getValorTotal()));
                i.setQuantity(converterValorEmInteger(c.getQuantidade()));
                itensMoip.add(i);
                oauth = carrinho.getProduto().getIdEmpresa().getOauth();
                //System.out.println("Oauth" + carrinho.getProduto().getIdEmpresa().getOauth());
            }

//        BigDecimal valorNoGlobo = ((cDados.getValorTotal().multiply(new BigDecimal(BigInteger.ONE))).divide(new BigDecimal(100)));
//        String saida = valorNoGlobo.toString();
//        int saida2 = Integer.parseInt(saida.replace(".", "").replace(",", ""));

            Customer customer = new Customer();
            customer.setFullname(usuarioLogado.getIdCliente().getPrimeiroNome() + " " + usuarioLogado.getIdCliente().getSobrenome());
            customer.setEmail(usuarioLogado.getEmail());
            customer.setBirthDate(UXUtil.formataDataYYYYMMDD(usuarioLogado.getIdCliente().getDtNascimento()));


            Authentication authentication = new OAuth(oauth);
            //System.out.println("Oauth" + oauth);
            Moip moip = new Moip(authentication, Config.endpoint);

//        System.out.println("Oauth Loja: " + oauth);
//        System.out.println("CustomID " + customID);
//        System.out.println("Valor " + converterValorEmInteger(totalFinal(cs)));
//        System.out.println("Nome " + usuarioLogado.getIdCliente().getPrimeiroNome());
//        System.out.println("Email " + usuarioLogado.getIdCliente().getEmail());
//        System.out.println("Cpf " + usuarioLogado.getIdCliente().getCpfcnpj());
//        System.out.println("IdMoip " + Config.idMoip);

            Order order = moip.orders().setOwnId("noglobo_" + customID)
                    .addItem("Compra referente ao lote " + customID, 1, customID, converterValorEmInteger(totalFinal(cs)))
                    .setShippingAmount(0)
                    .setCustomer(moip.customers().setOwnId("noglobo_" + customID)
                    .setFullname(usuarioLogado.getIdCliente().getPrimeiroNome() + " " + usuarioLogado.getIdCliente().getSobrenome())
                    .setEmail(usuarioLogado.getIdCliente().getEmail())
                    .setBirthDate("1988-12-30")
                    .setTaxDocument(usuarioLogado.getIdCliente().getCpfcnpj().replace(".", "").replace("-", ""))
                    .setPhone("11", "00000000", "55"))
                    .addReceiver(Config.idMoip, (converterValorEmInteger(totalFinal(cs)) / 100), "SECONDARY")
                    .create();
            cEJB.atualizarComprasPorLote(customID, order.getId());
            Order orderPayment = moip.orders().get(order.getId());

//        System.out.println("Numero de parcelas " + numeroParcelas);
            try {
                Payment payment = orderPayment.payments();
                payment.setInstallmentCount(numeroParcelas);
                //Dados do cartão
                CreditCard c = new CreditCard();
                c.setBrand(creditCard.getBrand().toUpperCase());
                c.setCvc(creditCard.getCvc());
                c.setExpirationMonth(creditCard.getExpirationMonth());
                c.setExpirationYear(creditCard.getExpirationYear());
                c.setNumber(creditCard.getNumber().replace(" ", ""));
                System.out.println(creditCard.getNumber().replace(" ", ""));

                Holder h = new Holder();
                h.setFullname(creditCard.getFullname());
                TaxDocument td = new TaxDocument();
                td.setNumber(usuarioLogado.getIdCliente().getCpfcnpj().replace(".", "").replace("-", ""));
                //td.setNumber("08754903602");
                td.setType("CPF");
                h.setTaxDocument(td);
                h.setBirthdate("1988-12-30");
                Phone p = new Phone();
                p.setAreaCode("11");
                p.setCountryCode("55");
                p.setNumber("00000000");
                h.setPhone(p);
                c.setHolder(h);
                payment.setCreditCard(c);
                System.out.println("Bandeira: " + c.getBrand());
                System.out.println("CVC: " + c.getCvc());
                System.out.println("Mes Exp: " + c.getExpirationMonth());
                System.out.println("Ano Exp: " + c.getExpirationYear());
                System.out.println("Numero: " + c.getNumber());
                System.out.println("Holder: " + c.getHolder());
                System.out.println("Pagamento : " + payment);
                payment.execute();
            } catch (ValidationException e) {
                System.out.println(e);
                MensageFactory.warn(e.getErrors().get(0).getDescription(), null);
            } catch (MoipException me) {
                System.out.println(me);
            }


            List<Payment> pays = moip.orders().get(order.getId()).getPayments();
            if (pays.size() > 0) {
                String tosave = pays.get(0).getId();
                cEJB.atualizarPagamentoComprasPorLote(customID, tosave, "");
                try {
                    if (cDados.getIdEmpresa().getSegmento().equals("Delivery")) {
                        UXUtil.enviarEmailConfigurado(usuarioLogado.getEmail(), usuarioLogado.getIdCliente().getPrimeiroNome(), "Recebemos o seu pedido Nº " + customID,
                                Configuracoes.mensagemDelivery(cDados, cEJB.listarComprasPorLote(cDados.getLote())));
                    } else {
                        UXUtil.enviarEmailConfigurado(usuarioLogado.getEmail(), usuarioLogado.getIdCliente().getPrimeiroNome(), "Recebemos o seu pedido Nº " + customID,
                                Configuracoes.mensagem(cDados, cEJB.listarComprasPorLote(cDados.getLote())));
                    }
                } catch (Exception e) {
                    System.out.println(e);
                    System.out.println("Não foi possivel enviar o email, verificar portas ");
                }

                MensageFactory.info("Pagamento efetuado com sucesso!", null);
                String satusPed = pays.get(0).getStatus();
                cEJB.atualizarStatusPorLote(satusPed, customID);
                for (Carrinho c : cs) {
                    carrinho.remove(c);
                }
                itemComprando = new CarrinhoEmpresa();
                cep = "";
                tipoServico = "";
                freteSelecionado = new FreteWS();
                fretesDisponiveis = new ArrayList<FreteWS>();
                enderecoWS = new Endereco();
                enderecoEntrega = new EnderecoEntrega();
                linkBoleto = "";
                itemComprando = new CarrinhoEmpresa();
                creditCard = new CartaoCredito();
            }
//            } else {
//                MensageFactory.warn("Atenção o CPF informado não é válido!", "");
//            }
        } else {
            MensageFactory.warn("Atenção o número do cartão de crédito digitado não é válido!", "");
        }

    }

    public void atualizarStatusCompras(Usuario usuario) {
        List<Compra> csAtualizar = cEJB.comprasParaAtualizarStatus(usuario);
        Moip moip = new Moip(auth, Config.endpoint);
        for (Compra compra : csAtualizar) {
//            System.out.println(compra.getOrderCode());
//            System.out.println(compra.getPaymentCode());
            if (compra.getPaymentCode() != null) {
                Payment payment = moip.orders().get(compra.getOrderCode()).payments().get(compra.getPaymentCode());
//                System.out.println(payment.getStatus());
                if (payment.getStatus().equals("AUTHORIZED")) {
                    compra.setStatus("Pago");
                    compra.setSituacaoEntrega("Aguardando Envio");
                    cEJB.Atualizar(compra);
                }
            }

        }
    }

    public void pagarComBoleto(List<Carrinho> cs, int index) throws IOException {

        List<Item> itensMoip = new ArrayList<Item>();
        String customID = String.valueOf(new Random().nextInt()).replace("-", "") + String.valueOf(new Date().getTime());
//            System.out.println(usuarioLogado.getIdCliente().getPrimeiroNome());
        String oauth = "";
        for (Carrinho carrinho : cs) {
            Compra c = new Compra();
            c.setFormaPagamento("Boleto");
            c.setNumParcela(1);
            c.setLote(customID);
            c.setDtCompra(new Date());
            c.setIdUsuario(usuarioLogado);
            c.setIdEmpresa(carrinho.getEmpresa());
            c.setIdProduto(carrinho.getProduto());
            c.setValorFrete(new BigDecimal(freteSelecionado.getValor().replace(",", ".")));
            c.setValorCompra(carrinho.getProduto().getPreco());
            c.setValorTotal(totalFinal(cs));
            c.setBairroEntrega(enderecoEntrega.getBairro());
            c.setNumeroEntrega(enderecoEntrega.getNumero());
            c.setCepEntrega(enderecoEntrega.getCep());
            c.setComplementoEntrega(enderecoEntrega.getComplemento());
            c.setEnderecoEntrega(enderecoEntrega.getLogradouro());
            c.setIdEmpresa(carrinho.getProduto().getIdEmpresa());
            c.setStatus("Aguardando pagamento");
            c.setValidoAte(UXUtil.addDia(new Date(), 6));
            if (cidEJB.pesquisarCidadePorNome(enderecoEntrega.getCidade()).size() == 0) {
                Cidade cidade = new Cidade();
                cidade.setNome(enderecoEntrega.getCidade());
                cidade.setIdEstado(eEJB.pesquisarEstado(enderecoEntrega.getUf()).get(0));
                cidEJB.Salvar(cidade);
                c.setIdCidadeEntrega(cidade);
            } else {
                c.setIdCidadeEntrega(cidEJB.pesquisarCidadePorNome(enderecoEntrega.getCidade()).get(0));
            }
            c.setIdEstadoEntrega(eEJB.pesquisarEstado(enderecoEntrega.getUf()).get(0));
            c.setQuantidade(new BigDecimal(carrinho.getQuantidade()));
            cEJB.Atualizar(c);
            Item i = new Item();
            i.setProduct(c.getIdProduto().getNome());
            i.setPrice(converterValorEmInteger(c.getValorTotal()));
            i.setQuantity(converterValorEmInteger(c.getQuantidade()));
            itensMoip.add(i);
            oauth = carrinho.getProduto().getIdEmpresa().getOauth();
        }

        Customer customer = new Customer();
        customer.setFullname(usuarioLogado.getIdCliente().getPrimeiroNome());
        customer.setEmail(usuarioLogado.getEmail());
        customer.setBirthDate(UXUtil.formataDataYYYYMMDD(usuarioLogado.getIdCliente().getDtNascimento()));
//            Moip moip = new Moip(auth, endpoint); //carrega do noglobo, o correto é carregar do cliente o noglobo é secundario

        //Autenticou no moip
        Authentication authentication = new OAuth(oauth);
        Moip moip = new Moip(authentication, Config.endpoint);
        //Criou o pedido

//        System.out.println("Oauth Loja: " + oauth);
//        System.out.println("CustomID " + customID);
//        System.out.println("Valor " + converterValorEmInteger(totalFinal(cs)));
//        System.out.println("Nome " + usuarioLogado.getIdCliente().getPrimeiroNome());
//        System.out.println("Email " + usuarioLogado.getIdCliente().getEmail());
//        System.out.println("Cpf " + usuarioLogado.getIdCliente().getCpfcnpj());
//        System.out.println("IdMoip " + Config.idMoip);


        Order order = moip.orders().setOwnId("noglobo_" + customID)
                .addItem("Compra referente ao lote " + customID, 1, customID, converterValorEmInteger(totalFinal(cs)))
                .setShippingAmount(0)
                .setCustomer(moip.customers().setOwnId("noglobo_" + customID)
                .setFullname(usuarioLogado.getIdCliente().getPrimeiroNome() + " " + usuarioLogado.getIdCliente().getSobrenome())
                .setEmail(usuarioLogado.getIdCliente().getEmail())
                .setBirthDate("1988-12-30")
                .setTaxDocument(usuarioLogado.getIdCliente().getCpfcnpj())
                .setPhone("11", "00000000", "55"))
                .addReceiver(Config.idMoip, (converterValorEmInteger(totalFinal(cs)) / 100), "SECONDARY")
                .create();
        //Atualizou no banco
        cEJB.atualizarComprasPorLote(customID, order.getId());

        System.out.println("Order " + order.getId());

        //Criou o pagamento em boleto
        Boleto boleto = new Boleto();
        boleto.setExpirationDate(UXUtil.formataDataYYYYMMDD(UXUtil.addDia(new Date(), 7)));
        boleto.setFirstInstructionLine("Pagamento referente ao pedido " + customID + ".");
        boleto.setSecondInstructionLine("noGlobo agradece a preferencia.");
        boleto.setThirdInstructionLine("Pagamento segurado pelo MoIP");
        Order orderPayment = moip.orders().get(order.getId());
        Payment payment = orderPayment.payments()
                .setBoleto(boleto)
                .execute();
//            System.out.println(payment.getId());
        Payment payment2 = moip.orders().get(orderPayment.getId()).payments().get(payment.getId());

        //Atualizou o status das compras
        cEJB.atualizarPagamentoComprasPorLote(customID, payment.getId(), payment2.get_links().getPayBoleto().getRedirectHref());
        linkBoleto = payment2.get_links().getPayBoleto().getRedirectHref();
        MensageFactory.info("Sua compra foi efetuada com sucesso! Imprima o boleto no link!", null);

        for (Carrinho c : cs) {
            carrinho.remove(c);
        }
        String linkRedirect = linkBoleto.trim();
        cep = "";
        tipoServico = "";
        freteSelecionado = new FreteWS();
        fretesDisponiveis = new ArrayList<FreteWS>();
        enderecoWS = new Endereco();
        enderecoEntrega = new EnderecoEntrega();
        linkBoleto = "";
        itemComprando = new CarrinhoEmpresa();
//        ExternalContext externalContext = FacesContext.getCurrentInstance().getExternalContext();
//        externalContext.redirect(linkRedirect.trim());
        RequestContext rc = RequestContext.getCurrentInstance();
        rc.execute("window.open('" + linkRedirect.trim() + "/print', '_blank');");
    }

    public void redirecionar() {
        RequestContext rc = RequestContext.getCurrentInstance();
        rc.execute("window.open('http://www.example.com/?ReportID=1', '_blank');");
    }

    public void addProdutoCarrinho(Produto p, int quantidade) throws IOException {
        Carrinho c = new Carrinho();
        c.setProduto(p);
        c.setQuantidade(quantidade == 0 ? 1 : quantidade);
        carrinho.add(c);
        MensageFactory.info("Item adicionado!", null);

        Map<String, String> params = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap();
        if (params.size() > 0) {
            if (params.get("redirecionar").equals("S")) {
                FacesContext.getCurrentInstance().getExternalContext().redirect(FacesContext.getCurrentInstance().getExternalContext().getRequestContextPath() + "/Carrinho");
            }
        }
    }

    public Integer converterValorEmInteger(BigDecimal b) {
        if (b != null) {
            String val = b.toString();
            String saida = val.replace(".", "").replace(",", "");
//            System.out.println(saida);
            return Integer.parseInt(saida);
        } else {
            return 1;
        }
    }

    public String verificaTipoFrete(String num) {
        if (num.equals("40010")) {
            return "SEDEX";
        } else if (num.equals("40045")) {
            return "SEDEX a Cobrar";
        } else if (num.equals("41106")) {
            return "PAC";
        } else {
            return "";
        }
    }

    public BigDecimal subTotal(List<Carrinho> cs) {
        BigDecimal total = new BigDecimal(BigInteger.ZERO);
        try {
            for (Carrinho c : cs) {
                total = total.add(c.getProduto().getPreco().multiply(new BigDecimal(c.getQuantidade())));
            }
            return total;
        } catch (Exception e) {
            return new BigDecimal(BigInteger.ZERO);
        }
    }

    public BigDecimal totalFrete(List<Carrinho> cs) {
        BigDecimal total = new BigDecimal(BigInteger.ZERO);
        try {
            for (Carrinho c : cs) {
                total = total.add(new BigDecimal(c.getFrete().getValor().replace(",", ".")).multiply(new BigDecimal(c.getQuantidade())));
            }
            return total;
        } catch (Exception e) {
            return new BigDecimal(BigInteger.ZERO);
        }
    }

    public BigDecimal totalFinal(List<Carrinho> cs, Empresa emp) {
        BigDecimal subTotal = new BigDecimal(BigInteger.ZERO);
        subTotal = subTotal(cs);
        BigDecimal totalFrete = new BigDecimal(BigInteger.ZERO);
        try {
            if (emp.getSegmento().equals("Delivery")) {
                if (cs.get(0).getRetirada() == true) {
                    totalFrete = new BigDecimal(BigInteger.ZERO);
                } else {
//                    totalFrete = emp.getValorEntrega();
                    totalFrete = cs.get(0).getEntrega().getCusto();
                }
            } else {
                totalFrete = new BigDecimal(freteSelecionado.getValor().replace(",", "."));
            }
        } catch (Exception e) {
        }
        return subTotal.add(totalFrete);
    }

    public void ThisIsRetirada(List<Carrinho> cs, Boolean tipo, CarrinhoEmpresa ce) {
        ce.setRetirada(tipo);
        for (Carrinho c : cs) {
            c.setRetirada(tipo);
        }
        atualizarTelaLimpaFrete(null);
        freteSelecionado = new FreteWS();
        freteSelecionado.setValor("");

    }

    public void carregarValorCarrinhos(CarrinhoEmpresa ce) {
        for (Carrinho c : ce.getCarrinhos()) {
            c.setEntrega(ce.getEntrega());
        }
    }

    public boolean cidadeEntrega(Empresa emp) {
        if (enderecoEntrega.getCidade() != null) {
            if (enderecoEntrega.getCidade().equals(emp.getIdCidade().getNome())) {
                return true;
            } else {
                MensageFactory.warn("Atenção! Não fazemos entrega para esta cidade.", "");
                return false;
            }
        } else {
            return false;
        }
    }

    public BigDecimal totalFinal(List<Carrinho> cs) {
        BigDecimal subTotal = new BigDecimal(BigInteger.ZERO);
        subTotal = subTotal(cs);
        BigDecimal totalFrete = new BigDecimal(BigInteger.ZERO);
        try {
//            System.out.println("teve aqui");
//            System.out.println(cs.get(0).getProduto().getIdEmpresa().getSegmento());
            if (cs.get(0).getProduto().getIdEmpresa().getSegmento().equals("Delivery")) {
                if (cs.get(0).getRetirada() == true) {
                    totalFrete = new BigDecimal(BigInteger.ZERO);
                } else {
                    if (cs.get(0).getEntrega().getCusto() != null) {
                        totalFrete = cs.get(0).getEntrega().getCusto();
                    } else {
                        totalFrete = new BigDecimal(BigInteger.ZERO);
                    }
                }
            } else {
                System.out.println(cs.get(0).getCep());
                if ((cs.get(0).getCep() == null || cs.get(0).getCep().equals("")) || fretesDisponiveis.isEmpty()) {
                    totalFrete = new BigDecimal(BigInteger.ZERO);
                } else {
                    totalFrete = new BigDecimal(freteSelecionado.getValor().replace(",", "."));
                }

            }
        } catch (Exception e) {
        }
        return subTotal.add(totalFrete);
    }

    public BigDecimal totalFinal2(Boolean retirada, String cep, List<Carrinho> cs, Empresa emp) {
        BigDecimal subTotal = new BigDecimal(BigInteger.ZERO);
        subTotal = subTotal(cs);
        BigDecimal totalFrete = new BigDecimal(BigInteger.ZERO);
        try {
            if (cs.get(0).getProduto().getIdEmpresa().getSegmento().equals("Delivery")) {
                if (retirada) {
                    totalFrete = new BigDecimal(BigInteger.ZERO);
                } else {
                    if (cep.equals("")) {
                        totalFrete = new BigDecimal(BigInteger.ZERO);
                    } else {
                        totalFrete = new BigDecimal(freteSelecionado.getValor().replace(",", "."));
                    }

                }
            } else {
                totalFrete = new BigDecimal(freteSelecionado.getValor().replace(",", "."));
            }
        } catch (Exception e) {
        }
        return subTotal.add(totalFrete);
    }

    public List<CarrinhoEmpresa> getItensDoCarrinhoPorEmpresa() {
        itensDoCarrinhoPorEmpresa = new ArrayList<CarrinhoEmpresa>();
        if (!carrinho.isEmpty()) {
            List<Empresa> empresasNoCarrinho = new ArrayList<Empresa>();
            for (Carrinho c : carrinho) {
                if (empresasNoCarrinho.indexOf(c.getProduto().getIdEmpresa()) < 0) {
                    empresasNoCarrinho.add(c.getProduto().getIdEmpresa());
                }
            }
            for (Empresa e : empresasNoCarrinho) {
                List<Carrinho> cars = new ArrayList<Carrinho>();
                for (Carrinho c : carrinho) {
                    if (c.getProduto().getIdEmpresa().getIdEmpresa().equals(e.getIdEmpresa())) {
                        cars.add(c);
                    }
                }
                CarrinhoEmpresa ce = new CarrinhoEmpresa();
                ce.setEmpresa(e);
                ce.setCarrinhos(cars);
                itensDoCarrinhoPorEmpresa.add(ce);
            }
        }
        return itensDoCarrinhoPorEmpresa;
    }

    public String carregarItensComprar(int index) {
        System.out.println(index);
        itemComprando = itensDoCarrinhoPorEmpresa.get(index);

        for (Carrinho c : itemComprando.getCarrinhos()) {
            if (c.getRetirada() == true) {
                itemComprando.setRetirada(true);
                freteSelecionado.setValor(c.getEntrega().getCusto().toString());
                break;
            }
        }
        System.out.println(itemComprando.getRetirada());
        itemComprando.setCep(cep);
        return "compra.xhtml?faces-redirect=true";
//        RequestContext rc = RequestContext.getCurrentInstance();
//        rc.execute("window.open('compra');");
    }

    public void redirecionar(String url) throws IOException {
        UXUtil.redirecionar(url);
    }

    public void atualizarTelaLimpaFrete(String cep) {
        if (cep == null || cep.equals("_____-___") || cep.isEmpty() || cep.equals("")) {
            itemComprando.setCep("");
            fretesDisponiveis = new ArrayList();
            freteSelecionado.setValor("");
            enderecoEntrega = new EnderecoEntrega();
        }
    }

    public void carregarUsuario(Usuario u) {
        usuarioLogado = u;
    }

    public void setItensDoCarrinhoPorEmpresa(List<CarrinhoEmpresa> itensDoCarrinhoPorEmpresa) {
        this.itensDoCarrinhoPorEmpresa = itensDoCarrinhoPorEmpresa;
    }

    public Endereco getEnderecoWS() {
        return enderecoWS;
    }

    public void setEnderecoWS(Endereco enderecoWS) {
        this.enderecoWS = enderecoWS;
    }

    public EnderecoEntrega getEnderecoEntrega() {
        return enderecoEntrega;
    }

    public void setEnderecoEntrega(EnderecoEntrega enderecoEntrega) {
        this.enderecoEntrega = enderecoEntrega;
    }

    public String getTipoServico() {
        return tipoServico;
    }

    public void setTipoServico(String tipoServico) {
        this.tipoServico = tipoServico;
    }

    public String getCep() {
        return cep;
    }

    public void setCep(String cep) {
        this.cep = cep;
    }

    public List<FreteWS> getFretesDisponiveis() {
        return fretesDisponiveis;
    }

    public void setFretesDisponiveis(List<FreteWS> fretesDisponiveis) {
        this.fretesDisponiveis = fretesDisponiveis;
    }

    public FreteWS getFreteSelecionado() {
        return freteSelecionado;
    }

    public void setFreteSelecionado(FreteWS freteSelecionado) {
        System.out.println("Testando");
        this.freteSelecionado = freteSelecionado;
    }

    public Usuario getUsuarioLogado() {
        return usuarioLogado;
    }

    public void setUsuarioLogado(Usuario usuarioLogado) {
        this.usuarioLogado = usuarioLogado;
    }

    public String getLinkBoleto() {
        return linkBoleto;
    }

    public void setLinkBoleto(String linkBoleto) {
        this.linkBoleto = linkBoleto;
    }

    public List<Carrinho> getCarrinho() {
        return carrinho;
    }

    public void setCarrinho(List<Carrinho> carrinho) {
        this.carrinho = carrinho;
    }

    public List<Carrinho> getCars() {
        return cars;
    }

    public void setCars(List<Carrinho> cars) {
        this.cars = cars;
    }

    public CartaoCredito getCreditCard() {
        return creditCard;
    }

    public void setCreditCard(CartaoCredito creditCard) {
        this.creditCard = creditCard;
    }

    public Integer getNumeroParcelas() {
        return numeroParcelas;
    }

    public void setNumeroParcelas(Integer numeroParcelas) {
        this.numeroParcelas = numeroParcelas;
    }

    public List<Integer> listarParcelas() {
//        BigDecimal total = totalFinal(cs);
        List<Integer> ps = new ArrayList<Integer>();
        for (int i = 1; i < 13; i++) {
            ps.add(i);
        }
        return ps;
    }

    public CarrinhoEmpresa getItemComprando() {
        return itemComprando;
    }

    public void setItemComprando(CarrinhoEmpresa itemComprando) {
        this.itemComprando = itemComprando;
    }

    public Boolean getRetirada() {
        return retirada;
    }

    public void setRetirada(Boolean retirada) {
        this.retirada = retirada;
    }

    public String getTelefone() {
        return telefone;
    }

    public void setTelefone(String telefone) {
        this.telefone = telefone;
    }

    public List<Produto> getProdutosVejaTambem() {
        return produtosVejaTambem;
    }

    public void setProdutosVejaTambem(List<Produto> produtosVejaTambem) {
        this.produtosVejaTambem = produtosVejaTambem;
    }

    public BigDecimal getCustoEntregaDelivery() {
        return custoEntregaDelivery;
    }

    public void setCustoEntregaDelivery(BigDecimal custoEntregaDelivery) {
        this.custoEntregaDelivery = custoEntregaDelivery;
    }

    public Entrega getBairroSelecionad2() {
        return bairroSelecionad2;
    }

    public void setBairroSelecionad2(Entrega bairroSelecionad2) {
        this.bairroSelecionad2 = bairroSelecionad2;
    }
}
