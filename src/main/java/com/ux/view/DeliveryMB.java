/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ux.view;

import com.ux.config.Configuracoes;
import com.ux.controller.CompraEJB;
import com.ux.controller.PedidoEJB;
import com.ux.model.Compra;
import com.ux.model.Empresa;
import com.ux.pojo.Pedido;
import com.ux.util.MensageFactory;
import com.ux.util.UXUtil;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.List;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import org.apache.commons.mail.EmailException;

/**
 *
 * @author Renato
 */
@ManagedBean
@ViewScoped
public class DeliveryMB {

     /**
      * Creates a new instance of DeliveryMB
      */
     @EJB
     PedidoEJB pEJB;
     @EJB
     CompraEJB cEJB;
     private Pedido pedido;

     public DeliveryMB() {
          pedido = new Pedido();
     }

     public List<Pedido> listarPedisoAbertos(Empresa empresa) {
//          System.out.println("Empresa " + empresa.getNome());
          return pEJB.listarPedidos(empresa);
     }

     public void ver(Empresa empresa) {
//          System.out.println("Empresa " + empresa.getNome());
          System.out.println(pEJB.listarPedidos(empresa).size());
     }

     public void confirmarPedido(Pedido p) throws EmailException {
          System.out.println("eita");
          List<Compra> listaCompras = cEJB.listarComprasPorLote(p.getLote());
          for (Compra c : listaCompras) {
               if (c.getRetirar() == null) {
                    c.setRetirar(false);
               }
               if (c.getRetirar()) {
                    c.setSituacaoEntrega("Retirado");
               } else {
                    c.setSituacaoEntrega("Enviado");
               }
               cEJB.Atualizar(c);
          }
          if (p.getCodRetirada() == null || "".equals(p.getCodRetirada())) {
               UXUtil.enviarEmailConfigurado(p.getUsuario().getEmail(), p.getUsuario().getIdCliente().getPrimeiroNome(), "Envio de Produtos " + p.getEmpresa().getNome(), Configuracoes.mensagemEnvioDelivery(p, cEJB.listarComprasPorLote(p.getLote())));
          } else {
               UXUtil.enviarEmailConfigurado(p.getUsuario().getEmail(), p.getUsuario().getIdCliente().getPrimeiroNome(), "Envio de Produtos " + p.getEmpresa().getNome(), Configuracoes.mensagemRetiradaDelivery(p, cEJB.listarComprasPorLote(p.getLote())));
          }
          MensageFactory.info("O Pedido foi finalizado com sucesso!", null);
     }

     public List<Compra> listarComprasPorLote(String lote) {
          return cEJB.listarComprasPorLote(lote);
     }

     public void teste() throws IOException {
          InputStream is = FacesContext.getCurrentInstance().getExternalContext().getResourceAsStream("/WEB-INF/email/EnvioDelivery.html");
          BufferedReader reader = new BufferedReader(new InputStreamReader(is));
          String line = "";
          while ((line = reader.readLine()) != null) {
               System.out.println(line);
          }
     }

     public Pedido getPedido() {
          return pedido;
     }

     public void setPedido(Pedido pedido) {
          this.pedido = pedido;
     }
}
