/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ux.view;

import com.ux.controller.TipoEmpresaEJB;
import com.ux.model.TipoEmpresa;
import com.ux.util.MensageFactory;
import java.util.List;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

/**
 *
 * @author Renato
 */
@ManagedBean
@ViewScoped
public class TipoEmpresaMB {

    @EJB
    TipoEmpresaEJB teEJB;
    private TipoEmpresa tipoEmpresa;

    public void novo() {
        tipoEmpresa = new TipoEmpresa();
    }

    public void salvar() {
        if (tipoEmpresa.getIdTipoEmpresa() == null) {
            try {
                teEJB.Salvar(tipoEmpresa);
                MensageFactory.info("Salvo com sucesso!", "");
                novo();
            } catch (Exception e) {
                MensageFactory.warn("Erro ao tentar salvar!", null);
                System.out.println(e);
            }
        } else {
            try {
                teEJB.Atualizar(tipoEmpresa);
                MensageFactory.info("Atualizado com sucesso!", "");
                novo();
            } catch (Exception e) {
                MensageFactory.warn("Erro ao tentar atualizar!", null);
                System.out.println(e);
            }
        }
    }

    public List<TipoEmpresa> listarTiposEmpresa() {
        return teEJB.ListarTodos();
    }

    public void excluir() {
        try {
            teEJB.Excluir(tipoEmpresa, tipoEmpresa.getIdTipoEmpresa());
            MensageFactory.info("Excluído com sucesso!", null);
            novo();
        } catch (Exception e) {
            MensageFactory.warn("Erro ao tentar excluir!", null);
            System.out.println(e);
        }
    }

    public TipoEmpresaMB() {
        novo();
    }

    public TipoEmpresa getTipoEmpresa() {
        return tipoEmpresa;
    }

    public void setTipoEmpresa(TipoEmpresa tipoEmpresa) {
        this.tipoEmpresa = tipoEmpresa;
    }
}
