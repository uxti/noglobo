/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ux.view;

import com.ux.config.Config;
import com.ux.controller.CidadeEJB;
import com.ux.controller.ClienteEJB;
import com.ux.controller.UsuarioEJB;
import com.ux.model.Cliente;
import com.ux.model.Perfil;
import com.ux.model.Usuario;
import com.ux.util.MensageFactory;
import com.ux.util.Security;
import com.ux.util.UXUtil;
import java.io.File;
import java.io.IOException;
import java.util.Date;
import java.util.List;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import org.apache.commons.io.FileUtils;
import org.primefaces.context.RequestContext;
import org.primefaces.event.FileUploadEvent;

/**
 *
 * @author Renato
 */
@ManagedBean
@ViewScoped
public class UsuarioMB {

    @EJB
    UsuarioEJB uEJB;
    @EJB
    CidadeEJB cEJB;
    @EJB
    ClienteEJB ciEJB;
    private Usuario usuario;
    private Cliente cliente;
    private String senha = "", cidade = "", email = "";
    private Boolean inserindo = Boolean.TRUE, salvar = Boolean.TRUE;

    public UsuarioMB() {
        novo();
        cliente = new Cliente();
    }

    public void cadastrarUsuario() {
        if (inserindo) {
            novo();
            inserindo = Boolean.FALSE;
        } else {
            try {
                if (UXUtil.isCPF(cliente.getCpfcnpj())) {
                    if (validarCampos()) {
                        email = "";
                        email = cliente.getEmail();
                        cliente.setDtCadastro(new Date());
                        // setado fixo para patos de minas / MG, para temporariamente não afetar nas buscas.
                        cliente.setIdCidade(cEJB.SelecionarPorID(1));
                        cliente.setIdEstado(cEJB.SelecionarPorID(1).getIdEstado());
                        usuario.setEmail(cliente.getEmail());
                        usuario.setUsk(Security.encrypt(usuario.getEmail()));
                        usuario.setSenha(getSenha());
                        usuario.setInativo(true);
                        uEJB.cadastrarUsuario(usuario, cliente);
                        MensageFactory.info("Ótimo! Cadastro realizado. Agora é só digitar sua senha e Entrar.", null);
                        usuario = new Usuario();
                        cliente = new Cliente();
                        inserindo = Boolean.TRUE;
                        RequestContext.getCurrentInstance().execute("$('#frmCompra\\\\:emailLogin').val('" + email + "');");
                        RequestContext.getCurrentInstance().execute("$('#frmLogin\\\\:emailLogin').val('" + email + "');");
                    } else {
                        MensageFactory.error("Verifique os campos obrigátorios.", null);
                    }
                } else {
                    MensageFactory.error("CPF inválido.", null);
                }

            } catch (Exception e) {
                MensageFactory.warn("Não foi possível cadastrar! Verifique os dados informados.", null);
                System.out.println(e);
            }
        }
    }

    public boolean validarCampos() {
        if (uEJB.verificarEmailExiste(cliente.getEmail())) {
            MensageFactory.error("Email já cadastrado.", null);
            return false;
        } else {
            System.out.println("CPF: " + cliente.getCpfcnpj());
            if (ciEJB.verificarCPFExiste(cliente.getCpfcnpj())) {
                MensageFactory.error("CPF já cadastrado", null);
                return false;
            }
        }
        return true;
    }

    public void verificarLogin(Cliente c) {
        if (c != null) {
            if (UXUtil.isCPF(c.getCpfcnpj())) {
                ciEJB.Atualizar(c);
                RequestContext.getCurrentInstance().execute("$('#dlgPagamentos').modal('show')");
            }
        } else {
            MensageFactory.error("Faça o login para realizar o pagamento.", "");
            RequestContext.getCurrentInstance().execute("$('html, body').animate({scrollTop: 500}, 'slow')");
        }
    }

    public Boolean validarCPF(String CPF) {
        return UXUtil.isCPF(CPF);
    }

    public void verificarEmailExiste() {
        if (uEJB.verificarEmailExiste(usuario.getEmail())) {
            MensageFactory.warn("Este email já se encontra em uso! Informe um email diferente.", null);
        }
    }

    public List<Perfil> listaPerfis() {
        return uEJB.listarPerfis();
    }

    public void upload(FileUploadEvent event) throws IOException {
        if (usuario.getIdUsuario() == null) {
            System.out.println(new File(Config.caminhoUsuario + uEJB.afterID()).mkdir());
            FileUtils.copyFile(new File(Config.indexModel), new File(Config.caminhoUsuario + uEJB.afterID() + "\\index.xhtml"));
            usuario.setImagem(UXUtil.upload(event, Config.caminhoUsuario + uEJB.afterID() + "\\", 400, 400));
        } else {
            if (!new File(Config.caminhoUsuario + usuario.getIdUsuario()).exists()) {
                new File(Config.caminhoUsuario + usuario.getIdUsuario()).mkdir();
                FileUtils.copyFile(new File(Config.indexModel), new File(Config.caminhoUsuario + usuario.getIdUsuario() + "\\index.xhtml"));
            }
            usuario.setImagem(UXUtil.upload(event, Config.caminhoUsuario + usuario.getIdUsuario() + "\\", 400, 400));
        }

    }

    public String verificaIdImage() {
        if (usuario.getIdUsuario() == null) {
            return uEJB.afterID().toString();
        } else {
            return usuario.getIdUsuario().toString();
        }
    }

    public void novo() {
        usuario = new Usuario();
    }

    public void salvar() throws Exception {
        usuario.setSenha(Security.encrypt(usuario.getSenha()));
        usuario.setUsk(Security.encrypt(usuario.getEmail()));
        System.out.println("O usk " + usuario.getUsk());
        if (usuario.getIdUsuario() == null) {
            try {
                uEJB.Salvar(usuario);
                MensageFactory.info("Usuário salvo com sucesso!", null);
            } catch (Exception e) {
                MensageFactory.error("Não foi possível salvar o usuário!", null);
                System.out.println(e);
            }
        } else {
            try {
                uEJB.Atualizar(usuario);
                MensageFactory.info("Usuário atualizado com sucesso!", null);
            } catch (Exception e) {
                MensageFactory.error("Não foi possível atualizar o usuário!", null);
                System.out.println(e);
            }
        }
    }

    public void excluir(Usuario u) {
        try {
            uEJB.Excluir(u, u.getIdUsuario());
            MensageFactory.info("Usuário excluído com sucesso!", null);
        } catch (Exception e) {
            MensageFactory.error("Não foi possível excluir o usuário!", null);
            System.out.println(e);
        }
    }

    public List<Usuario> listarUsuarios() {
        return uEJB.ListarTodos();
    }

    public Usuario getUsuario() throws Exception {

        return usuario;
    }

    public void setUsuario(Usuario usuario) throws Exception {
        if (usuario.getSenha() != null) {
            usuario.setSenha(Security.decrypt(usuario.getSenha()));
        }
        this.usuario = usuario;
    }

    public Cliente getCliente() {
        return cliente;
    }

    public void setCliente(Cliente cliente) {
        this.cliente = cliente;
    }

    public Boolean getInserindo() {
        return inserindo;
    }

    public void setInserindo(Boolean Inserindo) {
        this.inserindo = Inserindo;
    }

    public String getSenha() {
        return senha;
    }

    public void setSenha(String senha) {
        this.senha = senha;
    }

    public String getCidade() {
        return cidade;
    }

    public void setCidade(String cidade) {
        this.cidade = cidade;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Boolean getSalvar() {
        return salvar;
    }

    public void setSalvar(Boolean salvar) {
        this.salvar = salvar;
    }
}
