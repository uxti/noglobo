/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ux.view;

import com.ux.config.Config;
import com.ux.controller.BuscaEJB;
import com.ux.controller.CategoriaEJB;
import com.ux.controller.CidadeEJB;
import com.ux.controller.ComentarioEJB;
import com.ux.controller.EmpresaEJB;
import com.ux.controller.ProdutoEJB;
import com.ux.controller.UsuarioEJB;
import com.ux.model.Cidade;
import com.ux.model.Cliente;
import com.ux.model.Comentario;
import com.ux.model.Empresa;
import com.ux.model.Foto;
import com.ux.model.Produto;
import com.ux.pojo.Endereco;
import com.ux.pojo.FreteWS;
import com.ux.util.MensageFactory;
import com.ux.util.UXUtil;
import java.io.File;
import java.io.IOException;
import java.io.Serializable;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.net.MalformedURLException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ThreadLocalRandom;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import org.apache.commons.lang3.text.WordUtils;
import org.primefaces.context.RequestContext;

/**
 *
 * @author Renato
 */
@ManagedBean
@ViewScoped
public class BuscaMB implements Serializable {

    @EJB
    ProdutoEJB pEJB;
    @EJB
    EmpresaEJB eEJB;
    @EJB
    UsuarioEJB uEJB;
    @EJB
    CidadeEJB cEJB;
    @EJB
    ComentarioEJB comeEJB;
    @EJB
    BuscaEJB bEJB;
    @EJB
    CategoriaEJB catEJB;
    private List<Produto> produtos, produtosVejaTambem;
    private List<Empresa> empresas;
    private List<String> buscasRelacionadas, buscasRelacionadasEmpresas;
    private String parametro, cidade, tipo, enderecoAtual, busca;
    private Produto produtoSelecionado;
    private Comentario comentario;
    private List<FreteWS> fretesDisponiveis;
    private String url;
    public BigDecimal valorMin, valorMax;
    public String ordenacaoBusca, segmentoBusca;
    private Empresa empresaPatrocinada;

    public BuscaMB() {
        produtos = new ArrayList<>();
        produtosVejaTambem = new ArrayList<>();
        empresas = new ArrayList<>();
        produtoSelecionado = new Produto();
        empresaSelecionada = new Empresa();
        fretes = new FreteWS();
        endereco = new Endereco();
        comentario = new Comentario();
        fretesDisponiveis = new ArrayList<FreteWS>();
        buscasRelacionadas = new ArrayList<String>();
        quantidade = 1;
        valorMin = new BigDecimal(BigInteger.ZERO);
        valorMax = new BigDecimal(BigInteger.ZERO);
        buscasRelacionadasEmpresas = new ArrayList<String>();
        empresaPatrocinada = new Empresa();
    }

    @PostConstruct
    public void init() {
        parametro = "";
        if (!listarCidadesParaBusca().isEmpty()) {
            cidade = listarCidadesParaBusca().get(0).getSlug();
        } else {
            cidade = listarCidadesParaBusca().get(0).getSlug();
        }
        tipo = "produtos";
        Map<String, String> params = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap();
        if (params.isEmpty()) {
            setarValores();
        } else {
            parametro = params.get("q");
            cidade = params.get("cidade");
            tipo = params.get("tipo");
        }
    }

    public void confirmarRegistroMoipCode() {
        Map<String, String> params = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap();
        String code = params.get("code");
        String json = "";
    }

    public void setarValores() {
        // Atualmente o parametro cidade irá ser passado como padrão Patos de Minas, para evitar processamento desnecessário, 
        // sendo que o noGlobo só trabalha com empresas de Patos
        if (cidade == null) {
            cidade = "patos-de-minas";
        }
        if (tipo == null) {
            tipo = "produtos";
        }

    }

    public String retornaSlug(String texto) {
        String saida = UXUtil.formatString(texto);
        if (saida.startsWith("-") || saida.startsWith(" ")) {
            saida = saida.substring(1, saida.length());
        }
        return saida;
    }

    public String reFormataSlug(String texto) {
        return UXUtil.reFormatString(texto);
    }

    public Endereco getEndereco() {
        return endereco;
    }

    public void setEndereco(Endereco endereco) {
        this.endereco = endereco;
    }
    private Empresa empresaSelecionada;
    private String cepConsultar;
    private String tipoServico;
    private int quantidade;
    private com.ux.pojo.FreteWS fretes;
    private Endereco endereco;
    private String paramAnterior;

    public Produto getProdutoSelecionado() {
        return produtoSelecionado;
    }

    public void setProdutoSelecionado(Produto produtoSelecionado) {
        this.produtoSelecionado = produtoSelecionado;
    }

    public String dataEntrega(String qntDias) {
        return UXUtil.formataDataDDMMYYYY(UXUtil.addDia(new Date(), Integer.parseInt(qntDias))).toString();
    }

    public List<Produto> listarProdutosPromocao() {
        return pEJB.listarProdutosEmPromocao();
    }

    public String substring(String val) {
        if (val.length() > 15) {
            return val.substring(0, 12) + "...";
        } else {
            return val.substring(0, val.length());
        }
    }

    public void setarParametro() {
        parametro = busca;
    }

    public void carregarParametro() {
        parametro = busca;
    }

    public void direcionar() throws IOException {
        Map<String, String> params = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap();
        if (params.get("q") != null) {
            if (busca == null) {
                busca = reFormataSlug(params.get("q"));
            }
            if (cidade == null) {
                setarValores();
            }

            parametro = UXUtil.rtrim(UXUtil.ltrim(busca));
            parametro = retornaSlug(parametro);
            // Redirecionamento de URL
            url = "\"" + FacesContext.getCurrentInstance().getExternalContext().getRequestContextPath() + "/" + "@" + "/" + getCidade() + "/" + getTipo() + "/" + parametro.toLowerCase() + "\" ";
            ExternalContext externalContext = FacesContext.getCurrentInstance().getExternalContext();
            String urll = FacesContext.getCurrentInstance().getExternalContext().getRequestContextPath() + "/" + "@" + "/" + getCidade() + "/" + getTipo() + "/" + parametro.toLowerCase();
            System.out.println(urll);
            externalContext.redirect(urll);
        }
    }

    public void buscar() throws IOException {
        // MAP dos parametros
        Map<String, String> params = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap();
        Set<String> nomeParametros = params.keySet();

        System.out.println(params.size());
        //System.out.println(params);
        if (params == null || params.size() == 0) {
            url = "\"" + FacesContext.getCurrentInstance().getExternalContext().getRequestContextPath() + "\" ";
            FacesContext.getCurrentInstance().getExternalContext().redirect(FacesContext.getCurrentInstance().getExternalContext().getRequestContextPath());
        } else {
            switch (params.size()) {
                case 0: {
                    url = "\"" + FacesContext.getCurrentInstance().getExternalContext().getRequestContextPath() + "\" ";
                    FacesContext.getCurrentInstance().getExternalContext().redirect(FacesContext.getCurrentInstance().getExternalContext().getRequestContextPath());
                }
                case 1: {
                    cidade = params.get("cidade");
                    if (cidade.equals("@")) {
                        url = "\"" + FacesContext.getCurrentInstance().getExternalContext().getRequestContextPath() + "\" ";
                        FacesContext.getCurrentInstance().getExternalContext().redirect(FacesContext.getCurrentInstance().getExternalContext().getRequestContextPath());
                    } else {
                        url = "\"" + FacesContext.getCurrentInstance().getExternalContext().getRequestContextPath() + "/" + "@" + "/" + getCidade() + "\" ";
                    }
                    parametro = "%";
                    tipo = "produtos";

                    break;
                }
                case 2: {
                    if (params.get("tipo") != null && params.get("cidade") != null) {
                        cidade = pegarParametro(params, "cidade");
                        tipo = pegarParametro(params, "tipo");
                        parametro = "%";
                        url = "\"" + FacesContext.getCurrentInstance().getExternalContext().getRequestContextPath() + "/" + "@" + "/" + getCidade() + "/" + getTipo() + "\" ";
                    }
                    break;
                }
                case 3: {
                    cidade = pegarParametro(params, "cidade");
                    tipo = pegarParametro(params, "tipo");
                    parametro = reFormataSlug(pegarParametro(params, "q"));
                    busca = parametro;
                    url = "\"" + FacesContext.getCurrentInstance().getExternalContext().getRequestContextPath() + "/" + "@" + "/" + getCidade() + "/" + getTipo() + "/" + getParametro().toLowerCase() + "\" ";
                    break;
                }
            };
//            System.out.println(busca);
            // Modifica a URL em execução
            RequestContext.getCurrentInstance().execute("mudarUrl('Busca', " + url + ")");

            // Busca as informações de acordo com os parametros setados acima.
            if (tipo.equals("empresas")) {
                if (paramAnterior != parametro) {
                    empresas = new ArrayList<Empresa>();
                    empresas = eEJB.pesquisarEmpresas(parametro, tipo, cidade);
                    montaBuscaRelacionadaEmpresas();
                } else {
                    paramAnterior = parametro;
                }

            } else if (tipo.equals("servicos")) {
                if (paramAnterior != parametro) {
                    produtos = new ArrayList<Produto>();
                    if (parametro != null || parametro != "") {
                        produtos = pEJB.pesquisarServicos(parametro, tipo, cidade, busca);
                    }
                    paramAnterior = parametro;
                } else {
                    paramAnterior = parametro;
                }
            } else {
                if (paramAnterior != parametro) {
                    produtos = new ArrayList<Produto>();
                    if (parametro != null || parametro != "") {
                        System.out.println("pesquisa de produtos " + parametro);
                        produtos = pEJB.pesquisarProdutos(parametro, tipo, cidade, busca);
                    }
                    montaBuscaRelacionadaProduto();
                    paramAnterior = parametro;
                } else {
                    paramAnterior = parametro;
                }
            }

            //ordena e filtra a listagem de produtos
            filtararProdutosPorValor();
            ordernarLista();

            //ordena e filtra a listagem de empresas
            filtararEmpresasPorSegmento();
            ordernarListaEmpresas();
        }
        paramAnterior = parametro;
    }

    public void filtararEmpresasPorSegmento() {
        List<Empresa> listaOriginal = empresas;
        List<Empresa> novaLista = new ArrayList<Empresa>();
        if (segmentoBusca != null) {

            if (segmentoBusca.equals("Serviços/Contato")) {
                for (Empresa e : empresas) {
                    if (e.getSegmento().equals("Serviços/Contato")) {
                        novaLista.add(e);
                    }
                }
                empresas = new ArrayList<Empresa>();
                empresas.addAll(novaLista);
            } else if (segmentoBusca.equals("Produtos")) {
//                System.out.println("ta so nos produtos");
                for (Empresa e : empresas) {
                    if (e.getSegmento().equals("Produtos")) {
                        novaLista.add(e);
                    }
                }
                empresas = new ArrayList<Empresa>();
                empresas.addAll(novaLista);
            } else if (segmentoBusca.equals("Delivery")) {
                for (Empresa e : empresas) {
                    if (e.getSegmento().equals("Delivery")) {
                        novaLista.add(e);
                    }
                }
                empresas = new ArrayList<Empresa>();
                empresas.addAll(novaLista);
            } else {
                empresas = listaOriginal;
            }
        }
    }

    public void filtararProdutosPorValor() {
        List<Produto> listaOriginal = produtos;
        List<Produto> novaLista = new ArrayList<Produto>();
        if (valorMin == null) {
            valorMin = new BigDecimal(BigInteger.ZERO);
        }
        if (valorMax == null) {
            valorMax = new BigDecimal(BigInteger.ZERO);
        }
        if (valorMin.longValue() > 0 && valorMax.longValue() > 0) {
            for (Produto p : produtos) {
                if (p.getPreco().longValue() >= valorMin.longValue() && p.getPreco().longValue() <= valorMax.longValue()) {
                    novaLista.add(p);
                }
            }
            produtos = novaLista;
        } else if ((valorMin.longValue() == 0 || valorMin == null) && valorMax.longValue() > 0) {
            for (Produto p : produtos) {
                if (p.getPreco().longValue() <= valorMax.longValue()) {
                    novaLista.add(p);
                }
            }
            produtos = new ArrayList<Produto>();
            produtos.addAll(novaLista);
//            System.out.println(produtos.size());
        } else if (valorMin.longValue() > 0 && (valorMax.longValue() == 0 || valorMax == null)) {
            for (Produto p : produtos) {
                if (p.getPreco().longValue() >= valorMin.longValue()) {
                    novaLista.add(p);
                }
            }
            produtos = novaLista;
        } else {
            produtos = listaOriginal;
        }
    }

    public void ordernarLista() {
        if (ordenacaoBusca != null) {
            if (ordenacaoBusca.equals("MAV")) {
                Collections.sort(produtos, new Comparator<Produto>() {
                    @Override
                    public int compare(Produto pro1, Produto pro2) {
                        return pro2.getCompraList().size() - pro1.getCompraList().size();
                    }
                });
//                System.out.println("mais vendidos");
            } else if (ordenacaoBusca.equals("MEP")) {
                Collections.sort(produtos, new Comparator<Produto>() {
                    @Override
                    public int compare(Produto pro1, Produto pro2) {
                        return pro1.getPreco().intValue() - pro2.getPreco().intValue();
                    }
                });
//                System.out.println("menor preço");
            } else if (ordenacaoBusca.equals("MAP")) {
                Collections.sort(produtos, new Comparator<Produto>() {
                    @Override
                    public int compare(Produto pro1, Produto pro2) {
                        return pro2.getPreco().intValue() - pro1.getPreco().intValue();
                    }
                });
//                System.out.println("maior preço");
            }
        }
    }

    public void ordernarListaEmpresas() {
        if (ordenacaoBusca != null) {
            if (ordenacaoBusca.equals("AZ")) {
                Collections.sort(empresas, new Comparator<Empresa>() {
                    @Override
                    public int compare(Empresa emp1, Empresa emp2) {
                        int res = emp1.getNome().trim().compareTo(emp2.getNome().trim());
                        return res;
                    }
                });
                for (Empresa e : empresas) {
                    System.out.println(e.getNome());
                }
            } else if (ordenacaoBusca.equals("ZA")) {
                Collections.sort(empresas, new Comparator<Empresa>() {
                    @Override
                    public int compare(Empresa emp1, Empresa emp2) {
                        int res = emp2.getNome().trim().compareTo(emp1.getNome().trim());
//                        int res = String.CASE_INSENSITIVE_ORDER.compare(emp2.getNome(), emp1.getNome());
//                        if (res == 0) {
//                            res = emp2.getNome().compareTo(emp1.getNome());
//                        }
                        return res;
                    }
                });
            } else {
            }
        }
    }

    public Empresa linksPatrocinados() {
        return eEJB.listarEmpresasPatrocinadas(parametro);
    }

    public String escreveImagem(Integer id, String path) {
        return id + "/" + path;


    }

    public void montaBuscaRelacionadaEmpresas() {
        List<String> ls = new ArrayList<String>();
        if (!empresas.isEmpty()) {
            Collections.shuffle(empresas);
            Empresa e = empresas.get(0);
            String keys = e.getTagKeywords();
//            System.out.println(keys);
            if (keys != null) {
                if (keys.isEmpty() || keys.equals("")) {
//                System.out.println("veio no vazio");
                    ls.addAll(eEJB.listarTagsEmpresas(busca));
                } else {
                    ls = UXUtil.delimitar(keys, ",");
                }
            } else {
                ls.addAll(eEJB.listarTagsEmpresas(busca));
            }
            buscasRelacionadasEmpresas = ls;
        }
    }

    public void montaBuscaRelacionadaProduto() {
        buscasRelacionadas = new ArrayList<String>();
        if (!produtos.isEmpty()) {
            int rand = ThreadLocalRandom.current().nextInt(produtos.size());
            if (produtos.get(rand).getIdCategoria() != null) {
                buscasRelacionadas = UXUtil.delimitar(produtos.get(rand).getIdCategoria().getTags(), ",");
            } else {
                buscasRelacionadas = catEJB.listarTags(busca);
            }
        } else {
            buscasRelacionadas = catEJB.listarTags(busca);
        }

        Collections.shuffle(buscasRelacionadas);
        if (buscasRelacionadas.size() > 3) {
            ArrayList<String> ss = new ArrayList<String>();
            ss.add(buscasRelacionadas.get(0));
            ss.add(buscasRelacionadas.get(1));
            ss.add(buscasRelacionadas.get(2));
            buscasRelacionadas = new ArrayList<String>();
            buscasRelacionadas.addAll(ss);
        } else if (buscasRelacionadas.isEmpty()) {
            buscasRelacionadas.addAll(catEJB.getNamesByRelacionados(busca));
        }
    }

    public String substrRelacionada(String s) {
//        System.out.println(s.length());
        if (s.length() > 13) {
            return s.substring(0, 13);
        } else {
            return s;
        }
    }

    public String pegarParametro(Map<String, String> listaParametros, String busca) {
        return listaParametros.get(busca) == null ? "%" : listaParametros.get(busca);
    }

    public class ProdutoComparator implements Comparator<Produto> {

        @Override
        public int compare(Produto o1, Produto o2) {
            return o1.getPreco().subtract(o2.getPreco()).intValue();
        }
    }

    public Integer totalProdutos() {
        Double d = pEJB.getTotalReg();
        return d.intValue();
    }

    public List<Integer> inteiros() {
        List<Integer> is = new ArrayList<Integer>();
        for (int i = 1; i <= pEJB.getNumPaginas(); i++) {
            is.add(i);
        }
        return is;
    }

    public List<Empresa> getEmpresas() {
        return empresas;
    }

    public void setEmpresas(List<Empresa> empresas) {
        this.empresas = empresas;
    }

    public void consultarFrete() throws MalformedURLException, IOException {
        fretesDisponiveis = new ArrayList<FreteWS>();
        FreteWS f = new FreteWS();
        Map<String, Object> params = new LinkedHashMap<String, Object>();
        params.put("nCdEmpresa", ""); //nao é obrigatorio
        params.put("sDsSenha", ""); // nao é obrigatorio

        params.put("sCdMaoPropria", "s");
        params.put("sCdAvisoRecebimento", "n");
        params.put("StrRetorno", "xml");


        params.put("nCdServico", "40010,41106");
        System.out.println(tipoServico);
        //Obrigatorios
        params.put("sCepOrigem", produtoSelecionado.getIdEmpresa().getCep().replace("-", ""));
        params.put("sCepDestino", cepConsultar.replace("-", ""));
        params.put("nVlPeso", produtoSelecionado.getPesoKg().toString().replace(".00", ""));
        params.put("nCdFormato", (produtoSelecionado.getFormatoEncomenda() == null ? 1 : produtoSelecionado.getFormatoEncomenda()).toString().replace(".00", ""));
        params.put("nVlComprimento", produtoSelecionado.getComprimentoCm().toString().replace(".00", ""));
        params.put("nVlAltura", produtoSelecionado.getAlturaCm().toString().replace(".00", ""));
        params.put("nVlLargura", produtoSelecionado.getLarguraCm().toString().replace(".00", ""));
        params.put("nVlDiametro", produtoSelecionado.getDiametroCm().toString().replace(".00", ""));
        params.put("nVlValorDeclarado", (produtoSelecionado.getPreco().multiply(new BigDecimal(quantidade == 0 ? 1 : quantidade)).toString().replace(".00", "")));
//        System.out.println(params);
        for (FreteWS fs : f.consultarFreteAutomaticoPACSEDEX(params).getcServico()) {
            if (!"0".equals(fs.getErro())) {
//                System.out.println(fs.getErro());
//                System.out.println(fs.getMsgErro());
//                System.out.println(fs.getValor());
//                System.out.println(fs.getPrazoEntrega());
//                MensageFactory.warn(fs.getMsgErro(), "");
            } else {
//                System.out.println(fs.getErro());
//                System.out.println(fs.getMsgErro());
//                System.out.println(fs.getValor());
//                System.out.println(fs.getPrazoEntrega());
                fretesDisponiveis.add(fs);
            }
        }
//        System.out.println(fretes.getPrazoEntrega());
          Endereco e = new Endereco();
          endereco = e.pesquisarEnderecoPorCep(cepConsultar);
//          System.out.println("Fretes Disponiveis " + fretesDisponiveis.size());
     }

    public String[] retornaTags(String tags) {
        String[] listaTags = new String[tags.split(",").length];
        listaTags = tags.split(",");
        return listaTags;
    }

    public void enviarComentario(Cliente c) {
        comentario.setDtComentario(new Date());
        comentario.setIdCliente(c);
        comentario.setIdProduto(produtoSelecionado);
        comentario.setTipo("C");
        comeEJB.Salvar(comentario);
        MensageFactory.info("Seu comentário foi enviado.", "Aguarde até a resposta do vendedor!");
        comentario = new Comentario();
        comentario.setComentario("");
    }

    public int getQuantidade() {
        return quantidade;
    }

    public void setQuantidade(int quantidade) {
        this.quantidade = quantidade;
    }

     public void produtoSelecionadoView() {
          Map<String, String> params = FacesContext.getCurrentInstance().
                  getExternalContext().getRequestParameterMap();
          String parameterOne = params.get("q");
          produtoSelecionado = pEJB.getProdutoBySlug(parameterOne);
          if (produtoSelecionado.getIdCategoria() != null) {
               produtosVejaTambem = pEJB.listarProdutosReferentesPorEmpresa(produtoSelecionado.getIdProduto(), produtoSelecionado.getIdCategoria().getIdCategoria().toString(), produtoSelecionado.getIdEmpresa().getIdEmpresa(), 3);
//               System.out.println(produtosVejaTambem.size());
          }
     }

    public void empresaSelecionadaView() {
        Map<String, String> params = FacesContext.getCurrentInstance().
                getExternalContext().getRequestParameterMap();
        String parameterOne = params.get("q");
        empresaSelecionada = eEJB.getEmpresaBySlug(parameterOne);
    }

     public void trocarFoto(Foto f) {
//          System.out.println(f.getUrl());
          produtoSelecionado.setFotoPrincipal(f.getUrl());
//          System.out.println(produtoSelecionado.getFotoPrincipal());
     }

    public void verEndereco() {
        System.out.println(enderecoAtual);
    }

    public List<Produto> getProdutos() {
        return produtos;
    }

    public void setProdutos(List<Produto> produtos) {
        this.produtos = produtos;
    }

    public String getParametro() {
        return parametro;
    }

    public void setParametro(String parametro) {
        this.parametro = parametro;
    }

    public String getCidade() {
        return cidade;
    }

    public void setCidade(String cidade) {
        this.cidade = cidade;
    }

    public Empresa getEmpresaSelecionada() {
        return empresaSelecionada;
    }

    public void setEmpresaSelecionada(Empresa empresaSelecionada) {
        this.empresaSelecionada = empresaSelecionada;
    }

    public String getCepConsultar() {
        return cepConsultar;
    }

    public void setCepConsultar(String cepConsultar) {
        this.cepConsultar = cepConsultar;
    }

    public FreteWS getFretes() {
        return fretes;
    }

    public void setFretes(FreteWS fretes) {
        this.fretes = fretes;
    }

    public String getTipoServico() {
        return tipoServico;
    }

    public void setTipoServico(String tipoServico) {
        this.tipoServico = tipoServico;
    }

    public List<Cidade> listarCidadesParaBusca() {
        return cEJB.listarCidadesComEmpresas();
    }

    public Comentario getComentario() {
        return comentario;
    }

    public void setComentario(Comentario comentario) {
        this.comentario = comentario;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public String getEnderecoAtual() {
        return enderecoAtual;
    }

     public void setEnderecoAtual(String enderecoAtual) {
//          System.out.println("Setou o " + enderecoAtual);
          this.enderecoAtual = enderecoAtual;
     }

    public List<FreteWS> getFretesDisponiveis() {
        return fretesDisponiveis;
    }

    public void setFretesDisponiveis(List<FreteWS> fretesDisponiveis) {
        this.fretesDisponiveis = fretesDisponiveis;
    }

    public String verificaTipoFrete(String num) {
        if (num.equals("40010")) {
            return "SEDEX";
        } else if (num.equals("40045")) {
            return "SEDEX a Cobrar";
        } else if (num.equals("41106")) {
            return "PAC";
        } else {
            return "";
        }
    }

    public List<String> getBuscasRelacionadas() {
        return buscasRelacionadas;
    }

    public void setBuscasRelacionadas(List<String> buscasRelacionadas) {
        this.buscasRelacionadas = buscasRelacionadas;
    }

     public void handleKeyEvent() {
          parametro = parametro.toUpperCase();
//          System.out.println(parametro);
     }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public List<String> autoCompleteBusca(String query) {
        List<String> resultados = new ArrayList<String>();
        if (tipo == null) {
            tipo = "produtos";
        }
        if (tipo.equals("produtos")) {
            resultados = bEJB.pesquisarSugestoesBuscaProdutos(query);
        } else {
            if (tipo.equals("servicos")) {
                resultados = bEJB.pesquisarSugestoesBuscaServiços(query);
            } else {
                resultados = bEJB.pesquisarSugestoesBuscaEmpresas(query);
            }
        }
        resultados.add(0, query);
        return resultados;
    }

    public String verificarImagem(Integer id, String foto) {
        if (new File((Config.caminhoProduto) + id + "//" + foto).exists()) {
            return "/upload/Produto/" + id + "/" + foto;
        } else {
            return "/upload/no-image.jpg";
        }
    }

    public String verificarImagemEmpresa(Integer id, String foto) {
        if (new File((Config.caminhoEmpresa) + id + "//" + foto).exists()) {
            return "/upload/Empresa/" + id + "/" + foto;
        } else {
            return "/upload/no-image.jpg";
        }
    }

    public String verificarImagemUsuario(Integer id, String foto) {
        if (new File((Config.caminhoUsuario) + id + "//" + foto).exists()) {
            return "/upload/Usuario/" + id + "/" + foto;
        } else {
            return "/upload/no-image.jpg";
        }
    }

    public String retornaEndereco(Empresa emp) {
        String enderecos = "";
        if (emp != null) {
            enderecos = WordUtils.capitalize(emp.getTipoLogradouro() == null ? "" : emp.getTipoLogradouro() + " ") + emp.getLogradouro() + ", " + emp.getNumero() + ". " + emp.getBairro();
            if (!enderecos.substring(enderecos.length() - 2, enderecos.length()).equals("MG")) {
                enderecos += ". " + emp.getIdCidade().getNome() + " - " + emp.getIdEstado().getSigla().toUpperCase();
            }
        }        
        return enderecos;
    }

    public String retornaTelefone(Empresa emp) {
        return UXUtil.retornaTelefone(emp);
    }

    public String retornaCodigoProduto(Integer codigo, int qtdZeros, String caracter) {
        return UXUtil.retornaNumeroComZerosAEsquerda(codigo.toString(), qtdZeros, caracter);
    }

    public String getBusca() {
        return busca;
    }

    public void setBusca(String buscaOriginal) {
        this.busca = buscaOriginal;
    }

    public BigDecimal getValorMin() {
        return valorMin;
    }

    public void setValorMin(BigDecimal valorMin) {
        this.valorMin = valorMin;
    }

    public BigDecimal getValorMax() {
        return valorMax;
    }

    public void setValorMax(BigDecimal valorMax) {
        this.valorMax = valorMax;
    }

    public String getOrdenacaoBusca() {
        return ordenacaoBusca;
    }

    public void setOrdenacaoBusca(String ordenacaoBusca) {
        this.ordenacaoBusca = ordenacaoBusca;
    }

    public String getSegmentoBusca() {
        return segmentoBusca;
    }

    public void setSegmentoBusca(String segmentoBusca) {
        this.segmentoBusca = segmentoBusca;
    }

    public List<String> getBuscasRelacionadasEmpresas() {
        return buscasRelacionadasEmpresas;
    }

    public void setBuscasRelacionadasEmpresas(List<String> buscasRelacionadasEmpresas) {
        this.buscasRelacionadasEmpresas = buscasRelacionadasEmpresas;
    }

    public Empresa getEmpresaPatrocinada() {
        return empresaPatrocinada;
    }

    public void setEmpresaPatrocinada(Empresa empresaPatrocinada) {
        this.empresaPatrocinada = empresaPatrocinada;
    }

    public List<Produto> getProdutosVejaTambem() {
        return produtosVejaTambem;
    }

    public void setProdutosVejaTambem(List<Produto> produtosVejaTambem) {
        this.produtosVejaTambem = produtosVejaTambem;
    }
}
