/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ux.view;

import com.ux.config.Config;
import com.ux.controller.BannerEJB;
import com.ux.model.Banner;
import com.ux.util.MensageFactory;
import com.ux.util.UXUtil;
import java.io.File;
import java.io.IOException;
import java.util.List;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import org.apache.commons.io.FileUtils;
import org.primefaces.context.RequestContext;
import org.primefaces.event.FileUploadEvent;

/**
 *
 * @author Renato
 */
@ManagedBean
@ViewScoped
public class BannerMB {

    @EJB
    BannerEJB bEJB;
    private Banner banner;

    public BannerMB() {
        banner = new Banner();
    }

    public void salvar() {
        if (banner.getIdBanner() == null) {
            bEJB.Salvar(banner);
            novo();
            MensageFactory.info("Banner salvo com sucesso!", null);
        } else {
            bEJB.Atualizar(banner);
            MensageFactory.info("Banner atualizado com sucesso!", null);
            novo();
        }
    }

    public void excluir() {
        try {
            bEJB.Excluir(banner, banner.getIdBanner());
            MensageFactory.info("Banner excluído com sucesso!", null);
            novo();
        } catch (Exception e) {
            MensageFactory.warn("Não foi possível excluir o banner!", null);
            System.out.println(e);
        }
    }

    public void novo() {
        banner = new Banner();
    }

    public void upload(FileUploadEvent event) throws IOException {
        if (banner.getIdBanner() == null) {
            System.out.println(new File(Config.caminhoBanner + bEJB.afterID()).mkdir());
            FileUtils.copyFile(new File(Config.indexModel), new File(Config.caminhoBanner + bEJB.afterID() + "\\index.xhtml"));
            banner.setBanner(UXUtil.upload(event, Config.caminhoBanner + bEJB.afterID() + "\\"));
        } else {
            if (!new File(Config.caminhoBanner + banner.getIdBanner()).exists()) {
                new File(Config.caminhoBanner + banner.getIdBanner()).mkdir();
                FileUtils.copyFile(new File(Config.indexModel), new File(Config.caminhoBanner + banner.getIdBanner() + "\\index.xhtml"));
            }
            banner.setBanner(UXUtil.upload(event, Config.caminhoBanner + banner.getIdBanner() + "\\"));
        }
//        RequestContext rc = RequestContext.getCurrentInstance();
//        rc.update(":frmBanner:image");
    }

    public String verificaIdImage() {
        if (banner.getIdBanner() == null) {
            return bEJB.afterID().toString();
        } else {
            return banner.getIdBanner().toString();
        }
    }

    public List<Banner> listarBanner() {
        return bEJB.ListarTodos();
    }

    public Banner getBanner() {
        return banner;
    }

    public void setBanner(Banner banner) {
        this.banner = banner;
    }
}
