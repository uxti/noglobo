/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ux.view;

import com.ux.controller.CategoriaEJB;
import com.ux.model.Categoria;
import com.ux.util.MensageFactory;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

/**
 *
 * @author JOAOPAULO
 */
@ManagedBean
@ViewScoped
public class CategoriaMB implements Serializable {

    @EJB
    CategoriaEJB cEJB;
    Categoria categoria;
    private List<String> SubCategorias;

    public CategoriaMB() {
        novo();
    }

    public void novo() {
        categoria = new Categoria();
        SubCategorias = new ArrayList<String>();
    }

    public List<Categoria> listarCategorias() {
        return cEJB.ListarTodos();
    }

    public void salvar() {
        System.out.println(categoria.getTags());
        if (categoria.getIdCategoria() == null) {
            try {
                cEJB.Salvar(categoria);
                novo();
                MensageFactory.info("Salvo com sucesso!", null);
            } catch (Exception e) {
                MensageFactory.info("Erro ao tentar salvar", null);
                System.out.println("O erro foi: " + e);
            }
        } else {
            try {
                cEJB.salvarCategoria(categoria);
                novo();
                MensageFactory.info("Editado com sucesso", null);
            } catch (Exception e) {
                MensageFactory.info("Erro ao tentar Editar", null);
                System.out.println("O erro foi: " + e);
            }
        }
    }

    public void excluir() {
        try {
            cEJB.Excluir(categoria, categoria.getIdCategoria());
            novo();
            MensageFactory.info("Excluído com sucesso!", null);
        } catch (Exception e) {
            MensageFactory.error("Erro ao tentar excluir!", null);
            System.out.println("Erro ocorrido é: " + e);
        }
    }

    //Aqui os get e set
    public Categoria getCategoria() {
        return categoria;
    }

    

    public void setCategoria(Categoria categoria) {
        this.categoria = categoria;
    }

    public List<String> getSubCategorias() {
        return SubCategorias;
    }

    public void setSubCategorias(List<String> SubCategorias) {
        this.SubCategorias = SubCategorias;
    }
}
