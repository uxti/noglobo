/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ux.view;

import com.ux.controller.EnderecoEJB;
import com.ux.controller.UsuarioEJB;
import com.ux.controller.VendasEJB;
import com.ux.model.Compra;
import com.ux.config.Configuracoes;
import com.ux.controller.CompraEJB;
import com.ux.model.Empresa;
import com.ux.pojo.Pedido;
import com.ux.util.MensageFactory;
import com.ux.util.UXUtil;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import org.apache.commons.mail.EmailException;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;

/**
 *
 * @author Renato
 */
@ManagedBean
@ViewScoped
public class VendasMB implements Serializable {

    @EJB
    VendasEJB vEJB;
    private Compra compra;
    @EJB
    EnderecoEJB eEJB;
    @EJB
    UsuarioEJB uEJB;
    @EJB
    CompraEJB cEJB;
    private LazyDataModel<Pedido> vendasLazy;
    private List<Pedido> listaVendas;
    private Empresa empresaSessao;
    List<Compra> comprasSelecionadas;

    public VendasMB() {
        compra = new Compra();
        empresaSessao = new Empresa();
        comprasSelecionadas = new ArrayList<Compra>();
        vendasLazy = new LazyDataModel<Pedido>() {
            private static final long serialVersionUID = 1L;

            @Override
            public List<Pedido> load(int first, int pageSize, String sortField, SortOrder sortOrder, Map<String, Object> filters) {
                String Clausula = "";
                StringBuffer sf = new StringBuffer();
                int contar = 0;
                for (Iterator<String> it = filters.keySet().iterator(); it.hasNext();) {
                    String filterProperty = it.next(); // table column name = field name  
                    Object filterValue = filters.get(filterProperty);
                    if (contar == 0) {
//                              if (sf.toString().contains("where")) {
                        sf.append(" and p." + filterProperty + " like'%" + filterValue + "%'");
//                              }
                    } else {
                        sf.append(" and p." + filterProperty + " like'%" + filterValue + "%' ");
                    }
                    contar = contar + 1;
                }
                if (sortOrder.toString().contains("ASCENDING") && sortField != null) {
                    sf.append(" order by p." + sortField + " asc");
                }
                if (sortOrder.toString().contains("DESCENDING") && sortField != null) {
                    sf.append(" order by p." + sortField + " desc");
                }
                Clausula = sf.toString();

                if (Clausula.contains("_")) {
                    Clausula = Clausula.replace("_", "");
                }
                if (Clausula.contains("() -")) {
                    Clausula = Clausula.replace("() -", "");
                }
                if (Clausula.contains("..")) {
                    Clausula = Clausula.replace("..", "");
                }
                setRowCount(Integer.parseInt(vEJB.contarComprasRegistro(Clausula).toString()));
                listaVendas = vEJB.listarComprasLazyModeWherePedido(first, pageSize, Clausula, empresaSessao);
                return listaVendas;
            }
        };
    }

    public boolean checkEntrega(String situacao) {
        if (situacao.equals("Aguardando Retirada") || situacao.equals("Aguardando Envio")) {
            return false;
        } else {
            return true;
        }
    }

    @PostConstruct
    public void atualizarStatusCompras() {
//        atualizarStatusCompras();
    }

    public void carregarEmpresaSessao(Empresa emp) {
        empresaSessao = emp;
    }

    public void atualizarEntrega() throws EmailException {
        Compra aleatorio = new Compra();
        for (Compra compra : comprasSelecionadas) {
            compra.setSituacaoEntrega("Enviado");
            vEJB.atualizar(compra);
            aleatorio = compra;
        }
        MensageFactory.info("Venda atualizada!", null);
        UXUtil.enviarEmailConfigurado(aleatorio.getIdUsuario().getEmail(), aleatorio.getIdUsuario().getIdCliente().getPrimeiroNome(), "noGlobo - Envio do pedido " + aleatorio.getLote(), Configuracoes.mensagemEnvioMercadoria(aleatorio, cEJB.listarComprasPorLote(aleatorio.getLote())));
        compra = new Compra();
    }

    public void selecionarPedidos(String lote) {
        comprasSelecionadas = cEJB.listarComprasPorLote(lote);
    }

    public List<Compra> listarVendasAguardandoEnvio() {
        try {
            return vEJB.listarComprasAguardandoEnvio();
        } catch (Exception e) {
            return new ArrayList<Compra>();
        }
    }

    public List<Compra> listarVendasAguardandoRetirada() {
        try {
            return vEJB.listarComprasAguardandoRetirada();
        } catch (Exception e) {
            return new ArrayList<Compra>();
        }
    }

    public Integer totalComprasAguardandoRetirada(Empresa emp) {
        return vEJB.comprasAguardandoRetirada(emp);
    }
    public Integer totalComprasAguardandoEnvio(Empresa emp) {
        return vEJB.comprasAguardandoEnvio(emp);
    }
    public Integer totalDeVendas(Empresa emp) {
        return vEJB.totalDeVendas(emp);
    }

    public String enderecoEntrega() {
        if (comprasSelecionadas.isEmpty()) {
            return "";
        } else {
            return comprasSelecionadas.get(0).getEnderecoEntrega() + ", " + comprasSelecionadas.get(0).getNumeroEntrega() + ". " + comprasSelecionadas.get(0).getBairroEntrega() + " - " + comprasSelecionadas.get(0).getIdCidadeEntrega().getNome();
        }
    }

    public List<Compra> listarVendas() {
        return vEJB.listarVendas();
    }

    public Compra getCompra() {
        return compra;
    }

    public void setCompra(Compra compra) {
        this.compra = compra;
    }

    public LazyDataModel<Pedido> getVendasLazy() {
        return vendasLazy;
    }

    public void setVendasLazy(LazyDataModel<Pedido> vendasLazy) {
        this.vendasLazy = vendasLazy;
    }

    public List<Pedido> getListaVendas() {
        return listaVendas;
    }

    public void setListaVendas(List<Pedido> listaVendas) {
        this.listaVendas = listaVendas;
    }

    public List<Compra> getComprasSelecionadas() {
        return comprasSelecionadas;
    }

    public void setComprasSelecionadas(List<Compra> comprasSelecionadas) {
        this.comprasSelecionadas = comprasSelecionadas;
    }
}
