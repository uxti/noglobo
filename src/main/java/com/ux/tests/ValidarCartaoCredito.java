/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ux.tests;

/**
 *
 * @author Renato
 */
public class ValidarCartaoCredito {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        if(isValidCardNumber("6011719017465260")){
            System.out.println("true");
        }else{
            System.out.println("false");
        }
    }
    
    public static boolean isValidCardNumber(String ccNumber) {

        try {
            ccNumber = ccNumber.replaceAll("\\D", "");
            char[] ccNumberArry = ccNumber.toCharArray();

            int checkSum = 0;
            for (int i = ccNumberArry.length - 1; i >= 0; i--) {

                char ccDigit = ccNumberArry[i];

                if ((ccNumberArry.length - i) % 2 == 0) {
                    int doubleddDigit = Character.getNumericValue(ccDigit) * 2;
                    checkSum += (doubleddDigit % 9 == 0 && doubleddDigit != 0) ? 9 : doubleddDigit % 9;

                } else {
                    checkSum += Character.getNumericValue(ccDigit);
                }

            }

            return (checkSum != 0 && checkSum % 10 == 0);

        } catch (Exception e) {

            e.printStackTrace();

        }

        return false;
    }
}
