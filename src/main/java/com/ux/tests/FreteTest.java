/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ux.tests;

import com.ux.pojo.FreteWS;
import com.ux.pojo.ServicosFreteWS;
import java.io.IOException;
import java.net.MalformedURLException;
import java.util.LinkedHashMap;
import java.util.Map;

/**
 *
 * @author Renato
 */
public class FreteTest {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws MalformedURLException, IOException {
        FreteWS f = new FreteWS(); //importar da pojo
        Map<String, Object> params = new LinkedHashMap<String, Object>();
        params.put("nCdEmpresa", ""); //nao é obrigatorio
        params.put("sDsSenha", ""); // nao é obrigatorio

        params.put("sCdMaoPropria", "s");
        params.put("sCdAvisoRecebimento", "n");
        params.put("StrRetorno", "xml");
        params.put("nCdServico", "40010,41106");

        //Obrigatorios
        params.put("sCepOrigem", "43820080");
        params.put("sCepDestino", "38706308");
        params.put("nVlPeso", "1");
        params.put("nCdFormato", "1");
        params.put("nVlComprimento", "16");
        params.put("nVlAltura", "16");
        params.put("nVlLargura", "16");
        params.put("nVlDiametro", "0");
        params.put("nVlValorDeclarado", "19");

//        f = f.consultarFrete(params);
//        System.out.println("Valor "+f.getValor());
//        System.out.println("Prazo "+f.getPrazoEntrega()+" dias");
        ServicosFreteWS sf = f.consultarFreteAutomaticoPACSEDEX(params);
        for(FreteWS fs : sf.getcServico()){
            System.out.println(fs.getPrazoEntrega());
        }

    }
}
