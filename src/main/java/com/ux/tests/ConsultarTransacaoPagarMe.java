/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ux.tests;

import com.ux.util.UXUtil;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.io.StringWriter;
import java.io.UnsupportedEncodingException;
import java.io.Writer;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.AbstractMap;
import java.util.LinkedHashMap;
import java.util.Map;
import org.primefaces.json.JSONException;
import org.primefaces.json.JSONObject;

/**
 *
 * @author Renato
 */
public class ConsultarTransacaoPagarMe {

    private static final String api_key = "ak_test_JZpSPt6QUpOe217mbG8Epi9epAeg8m";
    private static final String valor = "1000";
    
    private static final String token = "test_transaction_bKlUj0dPQW6kWSiMFwgKsUN4NH69qh";

   
    public static void main(String[] args) throws MalformedURLException, UnsupportedEncodingException, IOException, JSONException {
        Map<String, Object> params = new LinkedHashMap<String, Object>();
        params.put("amount", valor);
        params.put("api_key", api_key);

        String uri = "https://api.pagar.me/1/transactions/?token" + token + "/capture" + UXUtil.curl(params);
        URL url = new URL(uri);
        HttpURLConnection connection = (HttpURLConnection) url.openConnection();
        connection.setRequestMethod("GET");
        connection.setConnectTimeout(15000);
        connection.connect();
        String responseJson = UXUtil.inputStreamToString(connection.getInputStream()).replace("[", "").replace("]", "");
        JSONObject object = new JSONObject(responseJson);
        System.out.println("O ID  QUE SERA PERSISTIDO NO BANCO P/ CONSULTAR NO PAGAR.ME == "+object.get("id"));

    }
}
