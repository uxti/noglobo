/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ux.tests;

import com.ux.util.UXUtil;
import java.io.IOException;
import java.net.MalformedURLException;

/**
 *
 * @author Renato
 */
public class CriarConta {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws MalformedURLException, IOException {
        String json = "{    \"email\": {        \"address\": \"contato@uxsolucoes.com.br\"    },    \"person\": {        \"name\": \"Runscope\",        \"lastName\": \"Random 9123\",        \"taxDocument\": {            \"type\": \"CPF\",            \"number\": \"742.520.863-61\"        },        \"birthDate\": \"1990-01-01\",        \"phone\": {            \"countryCode\": \"55\",            \"areaCode\": \"11\",            \"number\": \"965213244\"        },        \"address\": {            \"street\": \"Av. Brigadeiro Faria Lima\",            \"streetNumber\": \"2927\",            \"district\": \"Itaim\",            \"zipCode\": \"01234-000\",            \"city\": \"São Paulo\",            \"state\": \"SP\",            \"country\": \"BRA\"        }    },    \"type\": \"MERCHANT\"}";
        String saida = UXUtil.executarPost("https://sandbox.moip.com.br/v2/accounts", "OAuth gjlbnjd9s541r361hj52e03jm4pksw9", json);
        System.out.println(saida);
    }
}
