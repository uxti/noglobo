/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ux.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Renato
 */
@Entity
@Table(name = "empresa")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Empresa.findAll", query = "SELECT e FROM Empresa e"),
    @NamedQuery(name = "Empresa.findByIdEmpresa", query = "SELECT e FROM Empresa e WHERE e.idEmpresa = :idEmpresa"),
    @NamedQuery(name = "Empresa.findBySegmento", query = "SELECT e FROM Empresa e WHERE e.segmento = :segmento"),
    @NamedQuery(name = "Empresa.findByNome", query = "SELECT e FROM Empresa e WHERE e.nome = :nome"),
    @NamedQuery(name = "Empresa.findByCnpj", query = "SELECT e FROM Empresa e WHERE e.cnpj = :cnpj"),
    @NamedQuery(name = "Empresa.findByIe", query = "SELECT e FROM Empresa e WHERE e.ie = :ie"),
    @NamedQuery(name = "Empresa.findByIm", query = "SELECT e FROM Empresa e WHERE e.im = :im"),
    @NamedQuery(name = "Empresa.findByLogo", query = "SELECT e FROM Empresa e WHERE e.logo = :logo"),
    @NamedQuery(name = "Empresa.findByCapa", query = "SELECT e FROM Empresa e WHERE e.capa = :capa"),
    @NamedQuery(name = "Empresa.findBySlug", query = "SELECT e FROM Empresa e WHERE e.slug = :slug"),
    @NamedQuery(name = "Empresa.findByAtivo", query = "SELECT e FROM Empresa e WHERE e.ativo = :ativo"),
    @NamedQuery(name = "Empresa.findByBloqueado", query = "SELECT e FROM Empresa e WHERE e.bloqueado = :bloqueado"),
    @NamedQuery(name = "Empresa.findByDiasFechados", query = "SELECT e FROM Empresa e WHERE e.diasFechados = :diasFechados"),
    @NamedQuery(name = "Empresa.findByHorarioFuncionamentoInicio", query = "SELECT e FROM Empresa e WHERE e.horarioFuncionamentoInicio = :horarioFuncionamentoInicio"),
    @NamedQuery(name = "Empresa.findByHorarioFuncionamentoFinal", query = "SELECT e FROM Empresa e WHERE e.horarioFuncionamentoFinal = :horarioFuncionamentoFinal"),
    @NamedQuery(name = "Empresa.findBySite", query = "SELECT e FROM Empresa e WHERE e.site = :site"),
    @NamedQuery(name = "Empresa.findBySiteCompra", query = "SELECT e FROM Empresa e WHERE e.siteCompra = :siteCompra"),
    @NamedQuery(name = "Empresa.findByFacebook", query = "SELECT e FROM Empresa e WHERE e.facebook = :facebook"),
    @NamedQuery(name = "Empresa.findByGooglePlus", query = "SELECT e FROM Empresa e WHERE e.googlePlus = :googlePlus"),
    @NamedQuery(name = "Empresa.findByTwitter", query = "SELECT e FROM Empresa e WHERE e.twitter = :twitter"),
    @NamedQuery(name = "Empresa.findByTelefone", query = "SELECT e FROM Empresa e WHERE e.telefone = :telefone"),
    @NamedQuery(name = "Empresa.findByDddTelefone", query = "SELECT e FROM Empresa e WHERE e.dddTelefone = :dddTelefone"),
    @NamedQuery(name = "Empresa.findBySegundoTelefone", query = "SELECT e FROM Empresa e WHERE e.segundoTelefone = :segundoTelefone"),
    @NamedQuery(name = "Empresa.findByDddSegundoTelefone", query = "SELECT e FROM Empresa e WHERE e.dddSegundoTelefone = :dddSegundoTelefone"),
    @NamedQuery(name = "Empresa.findByTelefoneWhatsapp", query = "SELECT e FROM Empresa e WHERE e.telefoneWhatsapp = :telefoneWhatsapp"),
    @NamedQuery(name = "Empresa.findByDdTelefoneWhatsapp", query = "SELECT e FROM Empresa e WHERE e.ddTelefoneWhatsapp = :ddTelefoneWhatsapp"),
    @NamedQuery(name = "Empresa.findByEmail", query = "SELECT e FROM Empresa e WHERE e.email = :email"),
    @NamedQuery(name = "Empresa.findByEmailVendas", query = "SELECT e FROM Empresa e WHERE e.emailVendas = :emailVendas"),
    @NamedQuery(name = "Empresa.findByEmailPagamento", query = "SELECT e FROM Empresa e WHERE e.emailPagamento = :emailPagamento"),
    @NamedQuery(name = "Empresa.findByJanelaConfirmacaoMenorIdade", query = "SELECT e FROM Empresa e WHERE e.janelaConfirmacaoMenorIdade = :janelaConfirmacaoMenorIdade"),
    @NamedQuery(name = "Empresa.findByCep", query = "SELECT e FROM Empresa e WHERE e.cep = :cep"),
    @NamedQuery(name = "Empresa.findByTipoLogradouro", query = "SELECT e FROM Empresa e WHERE e.tipoLogradouro = :tipoLogradouro"),
    @NamedQuery(name = "Empresa.findByLogradouro", query = "SELECT e FROM Empresa e WHERE e.logradouro = :logradouro"),
    @NamedQuery(name = "Empresa.findByNumero", query = "SELECT e FROM Empresa e WHERE e.numero = :numero"),
    @NamedQuery(name = "Empresa.findByBairro", query = "SELECT e FROM Empresa e WHERE e.bairro = :bairro"),
    @NamedQuery(name = "Empresa.findByComplemento", query = "SELECT e FROM Empresa e WHERE e.complemento = :complemento"),
    @NamedQuery(name = "Empresa.findByEnviarSms", query = "SELECT e FROM Empresa e WHERE e.enviarSms = :enviarSms"),
    @NamedQuery(name = "Empresa.findByCreditosSms", query = "SELECT e FROM Empresa e WHERE e.creditosSms = :creditosSms"),
    @NamedQuery(name = "Empresa.findByEnviarEmail", query = "SELECT e FROM Empresa e WHERE e.enviarEmail = :enviarEmail"),
    @NamedQuery(name = "Empresa.findByTokenSmsMobi", query = "SELECT e FROM Empresa e WHERE e.tokenSmsMobi = :tokenSmsMobi"),
    @NamedQuery(name = "Empresa.findByCredencialSmsMobi", query = "SELECT e FROM Empresa e WHERE e.credencialSmsMobi = :credencialSmsMobi"),
    @NamedQuery(name = "Empresa.findByPrincipalUserSmsMobi", query = "SELECT e FROM Empresa e WHERE e.principalUserSmsMobi = :principalUserSmsMobi"),
    @NamedQuery(name = "Empresa.findByLojaSugerida", query = "SELECT e FROM Empresa e WHERE e.lojaSugerida = :lojaSugerida"),
    @NamedQuery(name = "Empresa.findByLinkPatrocinado", query = "SELECT e FROM Empresa e WHERE e.linkPatrocinado = :linkPatrocinado"),
    @NamedQuery(name = "Empresa.findByCpf", query = "SELECT e FROM Empresa e WHERE e.cpf = :cpf"),
    @NamedQuery(name = "Empresa.findByIdMoip", query = "SELECT e FROM Empresa e WHERE e.idMoip = :idMoip"),
    @NamedQuery(name = "Empresa.findByLoginMoip", query = "SELECT e FROM Empresa e WHERE e.loginMoip = :loginMoip"),
    @NamedQuery(name = "Empresa.findBySenhaMoip", query = "SELECT e FROM Empresa e WHERE e.senhaMoip = :senhaMoip"),
    @NamedQuery(name = "Empresa.findByOauth", query = "SELECT e FROM Empresa e WHERE e.oauth = :oauth"),
    @NamedQuery(name = "Empresa.findByValorEntrega", query = "SELECT e FROM Empresa e WHERE e.valorEntrega = :valorEntrega"),
    @NamedQuery(name = "Empresa.findByEcommerce", query = "SELECT e FROM Empresa e WHERE e.ecommerce = :ecommerce"),
    @NamedQuery(name = "Empresa.findByTempoEntrega", query = "SELECT e FROM Empresa e WHERE e.tempoEntrega = :tempoEntrega"),
    @NamedQuery(name = "Empresa.findByCodigoRetirada", query = "SELECT e FROM Empresa e WHERE e.codigoRetirada = :codigoRetirada"),
    @NamedQuery(name = "Empresa.findByConfirmacaoMoip", query = "SELECT e FROM Empresa e WHERE e.confirmacaoMoip = :confirmacaoMoip")})
public class Empresa implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id_empresa")
    private Integer idEmpresa;
    @Size(max = 50)
    @Column(name = "segmento")
    private String segmento;
    @Size(max = 100)
    @Column(name = "nome")
    private String nome;
    @Size(max = 20)
    @Column(name = "cnpj")
    private String cnpj;
    @Size(max = 25)
    @Column(name = "ie")
    private String ie;
    @Size(max = 30)
    @Column(name = "im")
    private String im;
    @Size(max = 255)
    @Column(name = "logo")
    private String logo;
    @Size(max = 255)
    @Column(name = "capa")
    private String capa;
    @Size(max = 255)
    @Column(name = "slug")
    private String slug;
    @Column(name = "ativo")
    private Boolean ativo;
    @Column(name = "bloqueado")
    private Boolean bloqueado;
    @Size(max = 255)
    @Column(name = "dias_fechados")
    private String diasFechados;
    @Column(name = "horario_funcionamento_inicio")
    @Temporal(TemporalType.TIME)
    private Date horarioFuncionamentoInicio;
    @Column(name = "horario_funcionamento_final")
    @Temporal(TemporalType.TIME)
    private Date horarioFuncionamentoFinal;
    @Lob
    @Size(max = 65535)
    @Column(name = "texto")
    private String texto;
    @Lob
    @Size(max = 65535)
    @Column(name = "termos_uso")
    private String termosUso;
    @Lob
    @Size(max = 65535)
    @Column(name = "slogan")
    private String slogan;
    @Size(max = 255)
    @Column(name = "site")
    private String site;
    @Size(max = 255)
    @Column(name = "site_compra")
    private String siteCompra;
    @Lob
    @Size(max = 65535)
    @Column(name = "tag_description")
    private String tagDescription;
    @Lob
    @Size(max = 65535)
    @Column(name = "tag_keywords")
    private String tagKeywords;
    @Size(max = 255)
    @Column(name = "facebook")
    private String facebook;
    @Size(max = 255)
    @Column(name = "google_plus")
    private String googlePlus;
    @Size(max = 255)
    @Column(name = "twitter")
    private String twitter;
    @Size(max = 100)
    @Column(name = "telefone")
    private String telefone;
    @Size(max = 5)
    @Column(name = "ddd_telefone")
    private String dddTelefone;
    @Size(max = 100)
    @Column(name = "segundo_telefone")
    private String segundoTelefone;
    @Size(max = 5)
    @Column(name = "ddd_segundo_telefone")
    private String dddSegundoTelefone;
    @Size(max = 100)
    @Column(name = "telefone_whatsapp")
    private String telefoneWhatsapp;
    @Size(max = 5)
    @Column(name = "dd_telefone_whatsapp")
    private String ddTelefoneWhatsapp;
    // @Pattern(regexp="[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?", message="E-mail inválido")//if the field contains email address consider using this annotation to enforce field validation
    @Size(max = 200)
    @Column(name = "email")
    private String email;
    @Size(max = 255)
    @Column(name = "email_vendas")
    private String emailVendas;
    @Size(max = 200)
    @Column(name = "email_pagamento")
    private String emailPagamento;
    @Lob
    @Size(max = 65535)
    @Column(name = "token_seguranca")
    private String tokenSeguranca;
    @Column(name = "janela_confirmacao_menor_idade")
    private Boolean janelaConfirmacaoMenorIdade;
    @Size(max = 30)
    @Column(name = "cep")
    private String cep;
    @Size(max = 20)
    @Column(name = "tipo_logradouro")
    private String tipoLogradouro;
    @Size(max = 255)
    @Column(name = "logradouro")
    private String logradouro;
    @Size(max = 50)
    @Column(name = "numero")
    private String numero;
    @Size(max = 50)
    @Column(name = "bairro")
    private String bairro;
    @Size(max = 255)
    @Column(name = "complemento")
    private String complemento;
    @Lob
    @Size(max = 65535)
    @Column(name = "texto_devolucao")
    private String textoDevolucao;
    @Lob
    @Size(max = 65535)
    @Column(name = "texto_rodape")
    private String textoRodape;
    @Column(name = "enviar_sms")
    private Boolean enviarSms;
    @Column(name = "creditos_sms")
    private Integer creditosSms;
    @Column(name = "enviar_email")
    private Boolean enviarEmail;
    @Size(max = 6)
    @Column(name = "token_sms_mobi")
    private String tokenSmsMobi;
    @Size(max = 40)
    @Column(name = "credencial_sms_mobi")
    private String credencialSmsMobi;
    @Size(max = 50)
    @Column(name = "principal_user_sms_mobi")
    private String principalUserSmsMobi;
    @Column(name = "loja_sugerida")
    private Boolean lojaSugerida;
    @Column(name = "link_patrocinado")
    private Boolean linkPatrocinado;
    @Size(max = 15)
    @Column(name = "cpf")
    private String cpf;
    @Size(max = 200)
    @Column(name = "id_moip")
    private String idMoip;
    @Size(max = 200)
    @Column(name = "login_moip")
    private String loginMoip;
    @Size(max = 200)
    @Column(name = "senha_moip")
    private String senhaMoip;
    @Size(max = 255)
    @Column(name = "oauth")
    private String oauth;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "valor_entrega")
    private BigDecimal valorEntrega;
    @Column(name = "ecommerce")
    private Boolean ecommerce;
    @Size(max = 255)
    @Column(name = "tempo_entrega")
    private String tempoEntrega;
    @Size(max = 255)
    @Column(name = "codigo_retirada")
    private String codigoRetirada;
    @Lob
    @Size(max = 65535)
    @Column(name = "link_senha")
    private String linkSenha;
    @Column(name = "confirmacao_moip")
    private Boolean confirmacaoMoip;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idEmpresa")
    private List<Compra> compraList;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idEmpresa")
    private List<Produto> produtoList;
    @OneToMany(mappedBy = "idEmpresa")
    private List<Usuario> usuarioList;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idEmpresa")
    private List<Entrega> entregaList;
    @JoinColumn(name = "id_cidade", referencedColumnName = "id_cidade")
    @ManyToOne
    private Cidade idCidade;
    @JoinColumn(name = "id_usuario", referencedColumnName = "id_usuario")
    @ManyToOne
    private Usuario idUsuario;
    @JoinColumn(name = "id_estado", referencedColumnName = "id_estado")
    @ManyToOne
    private Estado idEstado;
    @JoinColumn(name = "id_tipo_empresa", referencedColumnName = "id_tipo_empresa")
    @ManyToOne
    private TipoEmpresa idTipoEmpresa;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idEmpresa")
    private List<Pagamento> pagamentoList;
    @OneToMany(mappedBy = "idEmpresa")
    private List<Foto> fotoList;

    public Empresa() {
    }

    public Empresa(Integer idEmpresa) {
        this.idEmpresa = idEmpresa;
    }

    public Integer getIdEmpresa() {
        return idEmpresa;
    }

    public void setIdEmpresa(Integer idEmpresa) {
        this.idEmpresa = idEmpresa;
    }

    public String getSegmento() {
        return segmento;
    }

    public void setSegmento(String segmento) {
        this.segmento = segmento;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getCnpj() {
        return cnpj;
    }

    public void setCnpj(String cnpj) {
        this.cnpj = cnpj;
    }

    public String getIe() {
        return ie;
    }

    public void setIe(String ie) {
        this.ie = ie;
    }

    public String getIm() {
        return im;
    }

    public void setIm(String im) {
        this.im = im;
    }

    public String getLogo() {
        return logo;
    }

    public void setLogo(String logo) {
        this.logo = logo;
    }

    public String getCapa() {
        return capa;
    }

    public void setCapa(String capa) {
        this.capa = capa;
    }

    public String getSlug() {
        return slug;
    }

    public void setSlug(String slug) {
        this.slug = slug;
    }

    public Boolean getAtivo() {
        return ativo;
    }

    public void setAtivo(Boolean ativo) {
        this.ativo = ativo;
    }

    public Boolean getBloqueado() {
        return bloqueado;
    }

    public void setBloqueado(Boolean bloqueado) {
        this.bloqueado = bloqueado;
    }

    public String getDiasFechados() {
        return diasFechados;
    }

    public void setDiasFechados(String diasFechados) {
        this.diasFechados = diasFechados;
    }

    public Date getHorarioFuncionamentoInicio() {
        return horarioFuncionamentoInicio;
    }

    public void setHorarioFuncionamentoInicio(Date horarioFuncionamentoInicio) {
        this.horarioFuncionamentoInicio = horarioFuncionamentoInicio;
    }

    public Date getHorarioFuncionamentoFinal() {
        return horarioFuncionamentoFinal;
    }

    public void setHorarioFuncionamentoFinal(Date horarioFuncionamentoFinal) {
        this.horarioFuncionamentoFinal = horarioFuncionamentoFinal;
    }

    public String getTexto() {
        return texto;
    }

    public void setTexto(String texto) {
        this.texto = texto;
    }

    public String getTermosUso() {
        return termosUso;
    }

    public void setTermosUso(String termosUso) {
        this.termosUso = termosUso;
    }

    public String getSlogan() {
        return slogan;
    }

    public void setSlogan(String slogan) {
        this.slogan = slogan;
    }

    public String getSite() {
        return site;
    }

    public void setSite(String site) {
        this.site = site;
    }

    public String getSiteCompra() {
        return siteCompra;
    }

    public void setSiteCompra(String siteCompra) {
        this.siteCompra = siteCompra;
    }

    public String getTagDescription() {
        return tagDescription;
    }

    public void setTagDescription(String tagDescription) {
        this.tagDescription = tagDescription;
    }

    public String getTagKeywords() {
        return tagKeywords;
    }

    public void setTagKeywords(String tagKeywords) {
        this.tagKeywords = tagKeywords;
    }

    public String getFacebook() {
        return facebook;
    }

    public void setFacebook(String facebook) {
        this.facebook = facebook;
    }

    public String getGooglePlus() {
        return googlePlus;
    }

    public void setGooglePlus(String googlePlus) {
        this.googlePlus = googlePlus;
    }

    public String getTwitter() {
        return twitter;
    }

    public void setTwitter(String twitter) {
        this.twitter = twitter;
    }

    public String getTelefone() {
        return telefone;
    }

    public void setTelefone(String telefone) {
        this.telefone = telefone;
    }

    public String getDddTelefone() {
        return dddTelefone;
    }

    public void setDddTelefone(String dddTelefone) {
        this.dddTelefone = dddTelefone;
    }

    public String getSegundoTelefone() {
        return segundoTelefone;
    }

    public void setSegundoTelefone(String segundoTelefone) {
        this.segundoTelefone = segundoTelefone;
    }

    public String getDddSegundoTelefone() {
        return dddSegundoTelefone;
    }

    public void setDddSegundoTelefone(String dddSegundoTelefone) {
        this.dddSegundoTelefone = dddSegundoTelefone;
    }

    public String getTelefoneWhatsapp() {
        return telefoneWhatsapp;
    }

    public void setTelefoneWhatsapp(String telefoneWhatsapp) {
        this.telefoneWhatsapp = telefoneWhatsapp;
    }

    public String getDdTelefoneWhatsapp() {
        return ddTelefoneWhatsapp;
    }

    public void setDdTelefoneWhatsapp(String ddTelefoneWhatsapp) {
        this.ddTelefoneWhatsapp = ddTelefoneWhatsapp;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getEmailVendas() {
        return emailVendas;
    }

    public void setEmailVendas(String emailVendas) {
        this.emailVendas = emailVendas;
    }

    public String getEmailPagamento() {
        return emailPagamento;
    }

    public void setEmailPagamento(String emailPagamento) {
        this.emailPagamento = emailPagamento;
    }

    public String getTokenSeguranca() {
        return tokenSeguranca;
    }

    public void setTokenSeguranca(String tokenSeguranca) {
        this.tokenSeguranca = tokenSeguranca;
    }

    public Boolean getJanelaConfirmacaoMenorIdade() {
        return janelaConfirmacaoMenorIdade;
    }

    public void setJanelaConfirmacaoMenorIdade(Boolean janelaConfirmacaoMenorIdade) {
        this.janelaConfirmacaoMenorIdade = janelaConfirmacaoMenorIdade;
    }

    public String getCep() {
        return cep;
    }

    public void setCep(String cep) {
        this.cep = cep;
    }

    public String getTipoLogradouro() {
        return tipoLogradouro;
    }

    public void setTipoLogradouro(String tipoLogradouro) {
        this.tipoLogradouro = tipoLogradouro;
    }

    public String getLogradouro() {
        return logradouro;
    }

    public void setLogradouro(String logradouro) {
        this.logradouro = logradouro;
    }

    public String getNumero() {
        return numero;
    }

    public void setNumero(String numero) {
        this.numero = numero;
    }

    public String getBairro() {
        return bairro;
    }

    public void setBairro(String bairro) {
        this.bairro = bairro;
    }

    public String getComplemento() {
        return complemento;
    }

    public void setComplemento(String complemento) {
        this.complemento = complemento;
    }

    public String getTextoDevolucao() {
        return textoDevolucao;
    }

    public void setTextoDevolucao(String textoDevolucao) {
        this.textoDevolucao = textoDevolucao;
    }

    public String getTextoRodape() {
        return textoRodape;
    }

    public void setTextoRodape(String textoRodape) {
        this.textoRodape = textoRodape;
    }

    public Boolean getEnviarSms() {
        return enviarSms;
    }

    public void setEnviarSms(Boolean enviarSms) {
        this.enviarSms = enviarSms;
    }

    public Integer getCreditosSms() {
        return creditosSms;
    }

    public void setCreditosSms(Integer creditosSms) {
        this.creditosSms = creditosSms;
    }

    public Boolean getEnviarEmail() {
        return enviarEmail;
    }

    public void setEnviarEmail(Boolean enviarEmail) {
        this.enviarEmail = enviarEmail;
    }

    public String getTokenSmsMobi() {
        return tokenSmsMobi;
    }

    public void setTokenSmsMobi(String tokenSmsMobi) {
        this.tokenSmsMobi = tokenSmsMobi;
    }

    public String getCredencialSmsMobi() {
        return credencialSmsMobi;
    }

    public void setCredencialSmsMobi(String credencialSmsMobi) {
        this.credencialSmsMobi = credencialSmsMobi;
    }

    public String getPrincipalUserSmsMobi() {
        return principalUserSmsMobi;
    }

    public void setPrincipalUserSmsMobi(String principalUserSmsMobi) {
        this.principalUserSmsMobi = principalUserSmsMobi;
    }

    public Boolean getLojaSugerida() {
        return lojaSugerida;
    }

    public void setLojaSugerida(Boolean lojaSugerida) {
        this.lojaSugerida = lojaSugerida;
    }

    public Boolean getLinkPatrocinado() {
        return linkPatrocinado;
    }

    public void setLinkPatrocinado(Boolean linkPatrocinado) {
        this.linkPatrocinado = linkPatrocinado;
    }

    public String getCpf() {
        return cpf;
    }

    public void setCpf(String cpf) {
        this.cpf = cpf;
    }

    public String getIdMoip() {
        return idMoip;
    }

    public void setIdMoip(String idMoip) {
        this.idMoip = idMoip;
    }

    public String getLoginMoip() {
        return loginMoip;
    }

    public void setLoginMoip(String loginMoip) {
        this.loginMoip = loginMoip;
    }

    public String getSenhaMoip() {
        return senhaMoip;
    }

    public void setSenhaMoip(String senhaMoip) {
        this.senhaMoip = senhaMoip;
    }

    public String getOauth() {
        return oauth;
    }

    public void setOauth(String oauth) {
        this.oauth = oauth;
    }

    public BigDecimal getValorEntrega() {
        return valorEntrega;
    }

    public void setValorEntrega(BigDecimal valorEntrega) {
        this.valorEntrega = valorEntrega;
    }

    public Boolean getEcommerce() {
        return ecommerce;
    }

    public void setEcommerce(Boolean ecommerce) {
        this.ecommerce = ecommerce;
    }

    public String getTempoEntrega() {
        return tempoEntrega;
    }

    public void setTempoEntrega(String tempoEntrega) {
        this.tempoEntrega = tempoEntrega;
    }

    public String getCodigoRetirada() {
        return codigoRetirada;
    }

    public void setCodigoRetirada(String codigoRetirada) {
        this.codigoRetirada = codigoRetirada;
    }

    public String getLinkSenha() {
        return linkSenha;
    }

    public void setLinkSenha(String linkSenha) {
        this.linkSenha = linkSenha;
    }

    public Boolean getConfirmacaoMoip() {
        return confirmacaoMoip;
    }

    public void setConfirmacaoMoip(Boolean confirmacaoMoip) {
        this.confirmacaoMoip = confirmacaoMoip;
    }

    @XmlTransient
    public List<Compra> getCompraList() {
        return compraList;
    }

    public void setCompraList(List<Compra> compraList) {
        this.compraList = compraList;
    }

    @XmlTransient
    public List<Produto> getProdutoList() {
        return produtoList;
    }

    public void setProdutoList(List<Produto> produtoList) {
        this.produtoList = produtoList;
    }

    @XmlTransient
    public List<Usuario> getUsuarioList() {
        return usuarioList;
    }

    public void setUsuarioList(List<Usuario> usuarioList) {
        this.usuarioList = usuarioList;
    }

    @XmlTransient
    public List<Entrega> getEntregaList() {
        return entregaList;
    }

    public void setEntregaList(List<Entrega> entregaList) {
        this.entregaList = entregaList;
    }

    public Cidade getIdCidade() {
        return idCidade;
    }

    public void setIdCidade(Cidade idCidade) {
        this.idCidade = idCidade;
    }

    public Usuario getIdUsuario() {
        return idUsuario;
    }

    public void setIdUsuario(Usuario idUsuario) {
        this.idUsuario = idUsuario;
    }

    public Estado getIdEstado() {
        return idEstado;
    }

    public void setIdEstado(Estado idEstado) {
        this.idEstado = idEstado;
    }

    public TipoEmpresa getIdTipoEmpresa() {
        return idTipoEmpresa;
    }

    public void setIdTipoEmpresa(TipoEmpresa idTipoEmpresa) {
        this.idTipoEmpresa = idTipoEmpresa;
    }

    @XmlTransient
    public List<Pagamento> getPagamentoList() {
        return pagamentoList;
    }

    public void setPagamentoList(List<Pagamento> pagamentoList) {
        this.pagamentoList = pagamentoList;
    }

    @XmlTransient
    public List<Foto> getFotoList() {
        return fotoList;
    }

    public void setFotoList(List<Foto> fotoList) {
        this.fotoList = fotoList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idEmpresa != null ? idEmpresa.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Empresa)) {
            return false;
        }
        Empresa other = (Empresa) object;
        if ((this.idEmpresa == null && other.idEmpresa != null) || (this.idEmpresa != null && !this.idEmpresa.equals(other.idEmpresa))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.ux.model.Empresa[ idEmpresa=" + idEmpresa + " ]";
    }
    
}
