/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ux.model;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Renato
 */
@Entity
@Table(name = "frete")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Frete.findAll", query = "SELECT f FROM Frete f"),
    @NamedQuery(name = "Frete.findByIdFrete", query = "SELECT f FROM Frete f WHERE f.idFrete = :idFrete"),
    @NamedQuery(name = "Frete.findByCodigo", query = "SELECT f FROM Frete f WHERE f.codigo = :codigo"),
    @NamedQuery(name = "Frete.findByDescricao", query = "SELECT f FROM Frete f WHERE f.descricao = :descricao"),
    @NamedQuery(name = "Frete.findByCodigoFretePagseguro", query = "SELECT f FROM Frete f WHERE f.codigoFretePagseguro = :codigoFretePagseguro")})
public class Frete implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id_frete")
    private Integer idFrete;
    @Size(max = 100)
    @Column(name = "codigo")
    private String codigo;
    @Size(max = 100)
    @Column(name = "descricao")
    private String descricao;
    @Size(max = 2)
    @Column(name = "codigo_frete_pagseguro")
    private String codigoFretePagseguro;
    @OneToMany(mappedBy = "idFrete")
    private List<Compra> compraList;
    @JoinColumn(name = "id_produto", referencedColumnName = "id_produto")
    @ManyToOne
    private Produto idProduto;

    public Frete() {
    }

    public Frete(Integer idFrete) {
        this.idFrete = idFrete;
    }

    public Integer getIdFrete() {
        return idFrete;
    }

    public void setIdFrete(Integer idFrete) {
        this.idFrete = idFrete;
    }

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public String getCodigoFretePagseguro() {
        return codigoFretePagseguro;
    }

    public void setCodigoFretePagseguro(String codigoFretePagseguro) {
        this.codigoFretePagseguro = codigoFretePagseguro;
    }

    @XmlTransient
    public List<Compra> getCompraList() {
        return compraList;
    }

    public void setCompraList(List<Compra> compraList) {
        this.compraList = compraList;
    }

    public Produto getIdProduto() {
        return idProduto;
    }

    public void setIdProduto(Produto idProduto) {
        this.idProduto = idProduto;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idFrete != null ? idFrete.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Frete)) {
            return false;
        }
        Frete other = (Frete) object;
        if ((this.idFrete == null && other.idFrete != null) || (this.idFrete != null && !this.idFrete.equals(other.idFrete))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.ux.model.Frete[ idFrete=" + idFrete + " ]";
    }
    
}
