/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ux.model;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Renato
 */
@Entity
@Table(name = "parametrizacao")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Parametrizacao.findAll", query = "SELECT p FROM Parametrizacao p"),
    @NamedQuery(name = "Parametrizacao.findByIdParametrizacao", query = "SELECT p FROM Parametrizacao p WHERE p.idParametrizacao = :idParametrizacao"),
    @NamedQuery(name = "Parametrizacao.findByMetaDescription", query = "SELECT p FROM Parametrizacao p WHERE p.metaDescription = :metaDescription"),
    @NamedQuery(name = "Parametrizacao.findByMetaKeywords", query = "SELECT p FROM Parametrizacao p WHERE p.metaKeywords = :metaKeywords"),
    @NamedQuery(name = "Parametrizacao.findByMetaCanonical", query = "SELECT p FROM Parametrizacao p WHERE p.metaCanonical = :metaCanonical"),
    @NamedQuery(name = "Parametrizacao.findByMetaApplicationName", query = "SELECT p FROM Parametrizacao p WHERE p.metaApplicationName = :metaApplicationName"),
    @NamedQuery(name = "Parametrizacao.findByTitle", query = "SELECT p FROM Parametrizacao p WHERE p.title = :title"),
    @NamedQuery(name = "Parametrizacao.findByBannerPrincipal", query = "SELECT p FROM Parametrizacao p WHERE p.bannerPrincipal = :bannerPrincipal"),
    @NamedQuery(name = "Parametrizacao.findByLinkFace", query = "SELECT p FROM Parametrizacao p WHERE p.linkFace = :linkFace"),
    @NamedQuery(name = "Parametrizacao.findByLinkTwitter", query = "SELECT p FROM Parametrizacao p WHERE p.linkTwitter = :linkTwitter"),
    @NamedQuery(name = "Parametrizacao.findByLinkInstagran", query = "SELECT p FROM Parametrizacao p WHERE p.linkInstagran = :linkInstagran"),
    @NamedQuery(name = "Parametrizacao.findByLinkGplus", query = "SELECT p FROM Parametrizacao p WHERE p.linkGplus = :linkGplus"),
    @NamedQuery(name = "Parametrizacao.findByBannerBusca1", query = "SELECT p FROM Parametrizacao p WHERE p.bannerBusca1 = :bannerBusca1"),
    @NamedQuery(name = "Parametrizacao.findByBannerBusca2", query = "SELECT p FROM Parametrizacao p WHERE p.bannerBusca2 = :bannerBusca2"),
    @NamedQuery(name = "Parametrizacao.findByBannerBusca3", query = "SELECT p FROM Parametrizacao p WHERE p.bannerBusca3 = :bannerBusca3"),
    @NamedQuery(name = "Parametrizacao.findByCep", query = "SELECT p FROM Parametrizacao p WHERE p.cep = :cep"),
    @NamedQuery(name = "Parametrizacao.findByTipoLogradouro", query = "SELECT p FROM Parametrizacao p WHERE p.tipoLogradouro = :tipoLogradouro"),
    @NamedQuery(name = "Parametrizacao.findByLogradouro", query = "SELECT p FROM Parametrizacao p WHERE p.logradouro = :logradouro"),
    @NamedQuery(name = "Parametrizacao.findByNumero", query = "SELECT p FROM Parametrizacao p WHERE p.numero = :numero"),
    @NamedQuery(name = "Parametrizacao.findByBairro", query = "SELECT p FROM Parametrizacao p WHERE p.bairro = :bairro"),
    @NamedQuery(name = "Parametrizacao.findByComplemento", query = "SELECT p FROM Parametrizacao p WHERE p.complemento = :complemento"),
    @NamedQuery(name = "Parametrizacao.findByTelefones", query = "SELECT p FROM Parametrizacao p WHERE p.telefones = :telefones"),
    @NamedQuery(name = "Parametrizacao.findByEmail", query = "SELECT p FROM Parametrizacao p WHERE p.email = :email"),
    @NamedQuery(name = "Parametrizacao.findByCodigoCorreios", query = "SELECT p FROM Parametrizacao p WHERE p.codigoCorreios = :codigoCorreios"),
    @NamedQuery(name = "Parametrizacao.findBySenhaCorreios", query = "SELECT p FROM Parametrizacao p WHERE p.senhaCorreios = :senhaCorreios"),
    @NamedQuery(name = "Parametrizacao.findByKeyApiPagamento", query = "SELECT p FROM Parametrizacao p WHERE p.keyApiPagamento = :keyApiPagamento"),
    @NamedQuery(name = "Parametrizacao.findByKeyCriptografiaPagamento", query = "SELECT p FROM Parametrizacao p WHERE p.keyCriptografiaPagamento = :keyCriptografiaPagamento")})
public class Parametrizacao implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id_parametrizacao")
    private Integer idParametrizacao;
    @Size(max = 255)
    @Column(name = "meta_description")
    private String metaDescription;
    @Size(max = 255)
    @Column(name = "meta_keywords")
    private String metaKeywords;
    @Size(max = 255)
    @Column(name = "meta_canonical")
    private String metaCanonical;
    @Size(max = 255)
    @Column(name = "meta_application_name")
    private String metaApplicationName;
    @Size(max = 255)
    @Column(name = "title")
    private String title;
    @Size(max = 255)
    @Column(name = "banner_principal")
    private String bannerPrincipal;
    @Lob
    @Size(max = 65535)
    @Column(name = "texto_radape")
    private String textoRadape;
    @Size(max = 255)
    @Column(name = "link_face")
    private String linkFace;
    @Size(max = 255)
    @Column(name = "link_twitter")
    private String linkTwitter;
    @Size(max = 255)
    @Column(name = "link_instagran")
    private String linkInstagran;
    @Size(max = 255)
    @Column(name = "link_gplus")
    private String linkGplus;
    @Lob
    @Size(max = 65535)
    @Column(name = "descricao")
    private String descricao;
    @Lob
    @Size(max = 65535)
    @Column(name = "descricao_beneficios")
    private String descricaoBeneficios;
    @Lob
    @Size(max = 65535)
    @Column(name = "descricao_contato")
    private String descricaoContato;
    @Size(max = 255)
    @Column(name = "banner_busca1")
    private String bannerBusca1;
    @Size(max = 255)
    @Column(name = "banner_busca2")
    private String bannerBusca2;
    @Size(max = 255)
    @Column(name = "banner_busca3")
    private String bannerBusca3;
    @Size(max = 9)
    @Column(name = "cep")
    private String cep;
    @Size(max = 20)
    @Column(name = "tipo_logradouro")
    private String tipoLogradouro;
    @Size(max = 255)
    @Column(name = "logradouro")
    private String logradouro;
    @Size(max = 50)
    @Column(name = "numero")
    private String numero;
    @Size(max = 50)
    @Column(name = "bairro")
    private String bairro;
    @Size(max = 255)
    @Column(name = "complemento")
    private String complemento;
    @Size(max = 255)
    @Column(name = "telefones")
    private String telefones;
    // @Pattern(regexp="[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?", message="E-mail inválido")//if the field contains email address consider using this annotation to enforce field validation
    @Size(max = 200)
    @Column(name = "email")
    private String email;
    @Lob
    @Size(max = 65535)
    @Column(name = "termo_uso")
    private String termoUso;
    @Lob
    @Size(max = 65535)
    @Column(name = "termo_privacidade")
    private String termoPrivacidade;
    @Size(max = 255)
    @Column(name = "codigo_correios")
    private String codigoCorreios;
    @Size(max = 255)
    @Column(name = "senha_correios")
    private String senhaCorreios;
    @Size(max = 255)
    @Column(name = "key_api_pagamento")
    private String keyApiPagamento;
    @Size(max = 255)
    @Column(name = "key_criptografia_pagamento")
    private String keyCriptografiaPagamento;

    public Parametrizacao() {
    }

    public Parametrizacao(Integer idParametrizacao) {
        this.idParametrizacao = idParametrizacao;
    }

    public Integer getIdParametrizacao() {
        return idParametrizacao;
    }

    public void setIdParametrizacao(Integer idParametrizacao) {
        this.idParametrizacao = idParametrizacao;
    }

    public String getMetaDescription() {
        return metaDescription;
    }

    public void setMetaDescription(String metaDescription) {
        this.metaDescription = metaDescription;
    }

    public String getMetaKeywords() {
        return metaKeywords;
    }

    public void setMetaKeywords(String metaKeywords) {
        this.metaKeywords = metaKeywords;
    }

    public String getMetaCanonical() {
        return metaCanonical;
    }

    public void setMetaCanonical(String metaCanonical) {
        this.metaCanonical = metaCanonical;
    }

    public String getMetaApplicationName() {
        return metaApplicationName;
    }

    public void setMetaApplicationName(String metaApplicationName) {
        this.metaApplicationName = metaApplicationName;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getBannerPrincipal() {
        return bannerPrincipal;
    }

    public void setBannerPrincipal(String bannerPrincipal) {
        this.bannerPrincipal = bannerPrincipal;
    }

    public String getTextoRadape() {
        return textoRadape;
    }

    public void setTextoRadape(String textoRadape) {
        this.textoRadape = textoRadape;
    }

    public String getLinkFace() {
        return linkFace;
    }

    public void setLinkFace(String linkFace) {
        this.linkFace = linkFace;
    }

    public String getLinkTwitter() {
        return linkTwitter;
    }

    public void setLinkTwitter(String linkTwitter) {
        this.linkTwitter = linkTwitter;
    }

    public String getLinkInstagran() {
        return linkInstagran;
    }

    public void setLinkInstagran(String linkInstagran) {
        this.linkInstagran = linkInstagran;
    }

    public String getLinkGplus() {
        return linkGplus;
    }

    public void setLinkGplus(String linkGplus) {
        this.linkGplus = linkGplus;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public String getDescricaoBeneficios() {
        return descricaoBeneficios;
    }

    public void setDescricaoBeneficios(String descricaoBeneficios) {
        this.descricaoBeneficios = descricaoBeneficios;
    }

    public String getDescricaoContato() {
        return descricaoContato;
    }

    public void setDescricaoContato(String descricaoContato) {
        this.descricaoContato = descricaoContato;
    }

    public String getBannerBusca1() {
        return bannerBusca1;
    }

    public void setBannerBusca1(String bannerBusca1) {
        this.bannerBusca1 = bannerBusca1;
    }

    public String getBannerBusca2() {
        return bannerBusca2;
    }

    public void setBannerBusca2(String bannerBusca2) {
        this.bannerBusca2 = bannerBusca2;
    }

    public String getBannerBusca3() {
        return bannerBusca3;
    }

    public void setBannerBusca3(String bannerBusca3) {
        this.bannerBusca3 = bannerBusca3;
    }

    public String getCep() {
        return cep;
    }

    public void setCep(String cep) {
        this.cep = cep;
    }

    public String getTipoLogradouro() {
        return tipoLogradouro;
    }

    public void setTipoLogradouro(String tipoLogradouro) {
        this.tipoLogradouro = tipoLogradouro;
    }

    public String getLogradouro() {
        return logradouro;
    }

    public void setLogradouro(String logradouro) {
        this.logradouro = logradouro;
    }

    public String getNumero() {
        return numero;
    }

    public void setNumero(String numero) {
        this.numero = numero;
    }

    public String getBairro() {
        return bairro;
    }

    public void setBairro(String bairro) {
        this.bairro = bairro;
    }

    public String getComplemento() {
        return complemento;
    }

    public void setComplemento(String complemento) {
        this.complemento = complemento;
    }

    public String getTelefones() {
        return telefones;
    }

    public void setTelefones(String telefones) {
        this.telefones = telefones;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getTermoUso() {
        return termoUso;
    }

    public void setTermoUso(String termoUso) {
        this.termoUso = termoUso;
    }

    public String getTermoPrivacidade() {
        return termoPrivacidade;
    }

    public void setTermoPrivacidade(String termoPrivacidade) {
        this.termoPrivacidade = termoPrivacidade;
    }

    public String getCodigoCorreios() {
        return codigoCorreios;
    }

    public void setCodigoCorreios(String codigoCorreios) {
        this.codigoCorreios = codigoCorreios;
    }

    public String getSenhaCorreios() {
        return senhaCorreios;
    }

    public void setSenhaCorreios(String senhaCorreios) {
        this.senhaCorreios = senhaCorreios;
    }

    public String getKeyApiPagamento() {
        return keyApiPagamento;
    }

    public void setKeyApiPagamento(String keyApiPagamento) {
        this.keyApiPagamento = keyApiPagamento;
    }

    public String getKeyCriptografiaPagamento() {
        return keyCriptografiaPagamento;
    }

    public void setKeyCriptografiaPagamento(String keyCriptografiaPagamento) {
        this.keyCriptografiaPagamento = keyCriptografiaPagamento;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idParametrizacao != null ? idParametrizacao.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Parametrizacao)) {
            return false;
        }
        Parametrizacao other = (Parametrizacao) object;
        if ((this.idParametrizacao == null && other.idParametrizacao != null) || (this.idParametrizacao != null && !this.idParametrizacao.equals(other.idParametrizacao))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.ux.model.Parametrizacao[ idParametrizacao=" + idParametrizacao + " ]";
    }
    
}
