/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ux.model;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Renato
 */
@Entity
@Table(name = "banner")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Banner.findAll", query = "SELECT b FROM Banner b"),
    @NamedQuery(name = "Banner.findByIdBanner", query = "SELECT b FROM Banner b WHERE b.idBanner = :idBanner"),
    @NamedQuery(name = "Banner.findByBanner", query = "SELECT b FROM Banner b WHERE b.banner = :banner"),
    @NamedQuery(name = "Banner.findByLink", query = "SELECT b FROM Banner b WHERE b.link = :link"),
    @NamedQuery(name = "Banner.findByPosicao", query = "SELECT b FROM Banner b WHERE b.posicao = :posicao"),
    @NamedQuery(name = "Banner.findByAtivo", query = "SELECT b FROM Banner b WHERE b.ativo = :ativo")})
public class Banner implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id_banner")
    private Integer idBanner;
    @Size(max = 255)
    @Column(name = "banner")
    private String banner;
    @Size(max = 255)
    @Column(name = "link")
    private String link;
    @Size(max = 100)
    @Column(name = "posicao")
    private String posicao;
    @Column(name = "ativo")
    private Boolean ativo;

    public Banner() {
    }

    public Banner(Integer idBanner) {
        this.idBanner = idBanner;
    }

    public Integer getIdBanner() {
        return idBanner;
    }

    public void setIdBanner(Integer idBanner) {
        this.idBanner = idBanner;
    }

    public String getBanner() {
        return banner;
    }

    public void setBanner(String banner) {
        this.banner = banner;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }

    public String getPosicao() {
        return posicao;
    }

    public void setPosicao(String posicao) {
        this.posicao = posicao;
    }

    public Boolean getAtivo() {
        return ativo;
    }

    public void setAtivo(Boolean ativo) {
        this.ativo = ativo;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idBanner != null ? idBanner.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Banner)) {
            return false;
        }
        Banner other = (Banner) object;
        if ((this.idBanner == null && other.idBanner != null) || (this.idBanner != null && !this.idBanner.equals(other.idBanner))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.ux.model.Banner[ idBanner=" + idBanner + " ]";
    }
    
}
