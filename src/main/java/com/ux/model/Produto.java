/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ux.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Renato
 */
@Entity
@Table(name = "produto")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Produto.findAll", query = "SELECT p FROM Produto p"),
    @NamedQuery(name = "Produto.findByIdProduto", query = "SELECT p FROM Produto p WHERE p.idProduto = :idProduto"),
    @NamedQuery(name = "Produto.findByNome", query = "SELECT p FROM Produto p WHERE p.nome = :nome"),
    @NamedQuery(name = "Produto.findByPreco", query = "SELECT p FROM Produto p WHERE p.preco = :preco"),
    @NamedQuery(name = "Produto.findByPrecoPromocional", query = "SELECT p FROM Produto p WHERE p.precoPromocional = :precoPromocional"),
    @NamedQuery(name = "Produto.findByFotoPrincipal", query = "SELECT p FROM Produto p WHERE p.fotoPrincipal = :fotoPrincipal"),
    @NamedQuery(name = "Produto.findByAtivo", query = "SELECT p FROM Produto p WHERE p.ativo = :ativo"),
    @NamedQuery(name = "Produto.findByPromocao", query = "SELECT p FROM Produto p WHERE p.promocao = :promocao"),
    @NamedQuery(name = "Produto.findByEncomenda", query = "SELECT p FROM Produto p WHERE p.encomenda = :encomenda"),
    @NamedQuery(name = "Produto.findByFormatoEncomenda", query = "SELECT p FROM Produto p WHERE p.formatoEncomenda = :formatoEncomenda"),
    @NamedQuery(name = "Produto.findByComprimentoCm", query = "SELECT p FROM Produto p WHERE p.comprimentoCm = :comprimentoCm"),
    @NamedQuery(name = "Produto.findByAlturaCm", query = "SELECT p FROM Produto p WHERE p.alturaCm = :alturaCm"),
    @NamedQuery(name = "Produto.findByLarguraCm", query = "SELECT p FROM Produto p WHERE p.larguraCm = :larguraCm"),
    @NamedQuery(name = "Produto.findByDiametroCm", query = "SELECT p FROM Produto p WHERE p.diametroCm = :diametroCm"),
    @NamedQuery(name = "Produto.findByPesoKg", query = "SELECT p FROM Produto p WHERE p.pesoKg = :pesoKg"),
    @NamedQuery(name = "Produto.findByServicoAdicional", query = "SELECT p FROM Produto p WHERE p.servicoAdicional = :servicoAdicional"),
    @NamedQuery(name = "Produto.findByAvisoRecebimento", query = "SELECT p FROM Produto p WHERE p.avisoRecebimento = :avisoRecebimento"),
    @NamedQuery(name = "Produto.findByValorFreteFixo", query = "SELECT p FROM Produto p WHERE p.valorFreteFixo = :valorFreteFixo"),
    @NamedQuery(name = "Produto.findBySlug", query = "SELECT p FROM Produto p WHERE p.slug = :slug"),
    @NamedQuery(name = "Produto.findByDisponibilidade", query = "SELECT p FROM Produto p WHERE p.disponibilidade = :disponibilidade"),
    @NamedQuery(name = "Produto.findByQuantidade", query = "SELECT p FROM Produto p WHERE p.quantidade = :quantidade"),
    @NamedQuery(name = "Produto.findByMaximoQuantidade", query = "SELECT p FROM Produto p WHERE p.maximoQuantidade = :maximoQuantidade"),
    @NamedQuery(name = "Produto.findByMaxQuantidadeCompra", query = "SELECT p FROM Produto p WHERE p.maxQuantidadeCompra = :maxQuantidadeCompra"),
    @NamedQuery(name = "Produto.findByTipo", query = "SELECT p FROM Produto p WHERE p.tipo = :tipo"),
    @NamedQuery(name = "Produto.findByFrete", query = "SELECT p FROM Produto p WHERE p.frete = :frete")})
public class Produto implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id_produto")
    private Integer idProduto;
    @Size(max = 255)
    @Column(name = "nome")
    private String nome;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "preco")
    private BigDecimal preco;
    @Column(name = "preco_promocional")
    private BigDecimal precoPromocional;
    @Size(max = 255)
    @Column(name = "foto_principal")
    private String fotoPrincipal;
    @Column(name = "ativo")
    private Boolean ativo;
    @Column(name = "promocao")
    private Boolean promocao;
    @Lob
    @Size(max = 65535)
    @Column(name = "descricao")
    private String descricao;
    @Lob
    @Size(max = 65535)
    @Column(name = "pagina")
    private String pagina;
    @Column(name = "encomenda")
    private Boolean encomenda;
    @Size(max = 1)
    @Column(name = "formato_encomenda")
    private String formatoEncomenda;
    @Column(name = "comprimento_cm")
    private BigDecimal comprimentoCm;
    @Column(name = "altura_cm")
    private BigDecimal alturaCm;
    @Column(name = "largura_cm")
    private BigDecimal larguraCm;
    @Column(name = "diametro_cm")
    private BigDecimal diametroCm;
    @Column(name = "peso_kg")
    private BigDecimal pesoKg;
    @Size(max = 1)
    @Column(name = "servico_adicional")
    private String servicoAdicional;
    @Size(max = 1)
    @Column(name = "aviso_recebimento")
    private String avisoRecebimento;
    @Column(name = "valor_frete_fixo")
    private BigDecimal valorFreteFixo;
    @Size(max = 255)
    @Column(name = "slug")
    private String slug;
    @Size(max = 30)
    @Column(name = "disponibilidade")
    private String disponibilidade;
    @Column(name = "quantidade")
    private BigDecimal quantidade;
    @Column(name = "maximo_quantidade")
    private BigDecimal maximoQuantidade;
    @Column(name = "max_quantidade_compra")
    private BigDecimal maxQuantidadeCompra;
    @Lob
    @Size(max = 65535)
    @Column(name = "resumo")
    private String resumo;
    @Size(max = 100)
    @Column(name = "tipo")
    private String tipo;
    @Size(max = 100)
    @Column(name = "frete")
    private String frete;
    @Lob
    @Size(max = 65535)
    @Column(name = "meta_tag")
    private String metaTag;
    @OneToMany(mappedBy = "idProduto")
    private List<Tags> tagsList;
    @OneToMany(mappedBy = "idProduto")
    private List<Compra> compraList;
    @JoinColumn(name = "id_categoria", referencedColumnName = "id_categoria")
    @ManyToOne
    private Categoria idCategoria;
    @JoinColumn(name = "id_sub_categoria", referencedColumnName = "id_sub_categoria")
    @ManyToOne
    private SubCategoria idSubCategoria;
    @JoinColumn(name = "id_empresa", referencedColumnName = "id_empresa")
    @ManyToOne(optional = false)
    private Empresa idEmpresa;
    @OneToMany(mappedBy = "idProduto")
    private List<Comentario> comentarioList;
    @OneToMany(mappedBy = "idProduto")
    private List<Frete> freteList;
    @OneToMany(mappedBy = "idProduto")
    private List<Foto> fotoList;

    public Produto() {
    }

    public Produto(Integer idProduto) {
        this.idProduto = idProduto;
    }

    public Integer getIdProduto() {
        return idProduto;
    }

    public void setIdProduto(Integer idProduto) {
        this.idProduto = idProduto;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public BigDecimal getPreco() {
        return preco;
    }

    public void setPreco(BigDecimal preco) {
        this.preco = preco;
    }

    public BigDecimal getPrecoPromocional() {
        return precoPromocional;
    }

    public void setPrecoPromocional(BigDecimal precoPromocional) {
        this.precoPromocional = precoPromocional;
    }

    public String getFotoPrincipal() {
        return fotoPrincipal;
    }

    public void setFotoPrincipal(String fotoPrincipal) {
        this.fotoPrincipal = fotoPrincipal;
    }

    public Boolean getAtivo() {
        return ativo;
    }

    public void setAtivo(Boolean ativo) {
        this.ativo = ativo;
    }

    public Boolean getPromocao() {
        return promocao;
    }

    public void setPromocao(Boolean promocao) {
        this.promocao = promocao;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public String getPagina() {
        return pagina;
    }

    public void setPagina(String pagina) {
        this.pagina = pagina;
    }

    public Boolean getEncomenda() {
        return encomenda;
    }

    public void setEncomenda(Boolean encomenda) {
        this.encomenda = encomenda;
    }

    public String getFormatoEncomenda() {
        return formatoEncomenda;
    }

    public void setFormatoEncomenda(String formatoEncomenda) {
        this.formatoEncomenda = formatoEncomenda;
    }

    public BigDecimal getComprimentoCm() {
        return comprimentoCm;
    }

    public void setComprimentoCm(BigDecimal comprimentoCm) {
        this.comprimentoCm = comprimentoCm;
    }

    public BigDecimal getAlturaCm() {
        return alturaCm;
    }

    public void setAlturaCm(BigDecimal alturaCm) {
        this.alturaCm = alturaCm;
    }

    public BigDecimal getLarguraCm() {
        return larguraCm;
    }

    public void setLarguraCm(BigDecimal larguraCm) {
        this.larguraCm = larguraCm;
    }

    public BigDecimal getDiametroCm() {
        return diametroCm;
    }

    public void setDiametroCm(BigDecimal diametroCm) {
        this.diametroCm = diametroCm;
    }

    public BigDecimal getPesoKg() {
        return pesoKg;
    }

    public void setPesoKg(BigDecimal pesoKg) {
        this.pesoKg = pesoKg;
    }

    public String getServicoAdicional() {
        return servicoAdicional;
    }

    public void setServicoAdicional(String servicoAdicional) {
        this.servicoAdicional = servicoAdicional;
    }

    public String getAvisoRecebimento() {
        return avisoRecebimento;
    }

    public void setAvisoRecebimento(String avisoRecebimento) {
        this.avisoRecebimento = avisoRecebimento;
    }

    public BigDecimal getValorFreteFixo() {
        return valorFreteFixo;
    }

    public void setValorFreteFixo(BigDecimal valorFreteFixo) {
        this.valorFreteFixo = valorFreteFixo;
    }

    public String getSlug() {
        return slug;
    }

    public void setSlug(String slug) {
        this.slug = slug;
    }

    public String getDisponibilidade() {
        return disponibilidade;
    }

    public void setDisponibilidade(String disponibilidade) {
        this.disponibilidade = disponibilidade;
    }

    public BigDecimal getQuantidade() {
        return quantidade;
    }

    public void setQuantidade(BigDecimal quantidade) {
        this.quantidade = quantidade;
    }

    public BigDecimal getMaximoQuantidade() {
        return maximoQuantidade;
    }

    public void setMaximoQuantidade(BigDecimal maximoQuantidade) {
        this.maximoQuantidade = maximoQuantidade;
    }

    public BigDecimal getMaxQuantidadeCompra() {
        return maxQuantidadeCompra;
    }

    public void setMaxQuantidadeCompra(BigDecimal maxQuantidadeCompra) {
        this.maxQuantidadeCompra = maxQuantidadeCompra;
    }

    public String getResumo() {
        return resumo;
    }

    public void setResumo(String resumo) {
        this.resumo = resumo;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public String getFrete() {
        return frete;
    }

    public void setFrete(String frete) {
        this.frete = frete;
    }

    public String getMetaTag() {
        return metaTag;
    }

    public void setMetaTag(String metaTag) {
        this.metaTag = metaTag;
    }

    @XmlTransient
    public List<Tags> getTagsList() {
        return tagsList;
    }

    public void setTagsList(List<Tags> tagsList) {
        this.tagsList = tagsList;
    }

    @XmlTransient
    public List<Compra> getCompraList() {
        return compraList;
    }

    public void setCompraList(List<Compra> compraList) {
        this.compraList = compraList;
    }

    public Categoria getIdCategoria() {
        return idCategoria;
    }

    public void setIdCategoria(Categoria idCategoria) {
        this.idCategoria = idCategoria;
    }

    public SubCategoria getIdSubCategoria() {
        return idSubCategoria;
    }

    public void setIdSubCategoria(SubCategoria idSubCategoria) {
        this.idSubCategoria = idSubCategoria;
    }

    public Empresa getIdEmpresa() {
        return idEmpresa;
    }

    public void setIdEmpresa(Empresa idEmpresa) {
        this.idEmpresa = idEmpresa;
    }

    @XmlTransient
    public List<Comentario> getComentarioList() {
        return comentarioList;
    }

    public void setComentarioList(List<Comentario> comentarioList) {
        this.comentarioList = comentarioList;
    }

    @XmlTransient
    public List<Frete> getFreteList() {
        return freteList;
    }

    public void setFreteList(List<Frete> freteList) {
        this.freteList = freteList;
    }

    @XmlTransient
    public List<Foto> getFotoList() {
        return fotoList;
    }

    public void setFotoList(List<Foto> fotoList) {
        this.fotoList = fotoList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idProduto != null ? idProduto.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Produto)) {
            return false;
        }
        Produto other = (Produto) object;
        if ((this.idProduto == null && other.idProduto != null) || (this.idProduto != null && !this.idProduto.equals(other.idProduto))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.ux.model.Produto[ idProduto=" + idProduto + " ]";
    }
    
}
