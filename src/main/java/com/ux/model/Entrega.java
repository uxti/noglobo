/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ux.model;

import java.io.Serializable;
import java.math.BigDecimal;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Renato
 */
@Entity
@Table(name = "entrega")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Entrega.findAll", query = "SELECT e FROM Entrega e"),
    @NamedQuery(name = "Entrega.findByIdTaxa", query = "SELECT e FROM Entrega e WHERE e.idTaxa = :idTaxa"),
    @NamedQuery(name = "Entrega.findByCusto", query = "SELECT e FROM Entrega e WHERE e.custo = :custo")})
public class Entrega implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id_taxa")
    private Integer idTaxa;
    @Lob
    @Size(max = 65535)
    @Column(name = "bairro")
    private String bairro;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "custo")
    private BigDecimal custo;
    @Column(name = "naoentrega")
    private Boolean naoentrega;
    @JoinColumn(name = "id_empresa", referencedColumnName = "id_empresa")
    @ManyToOne
    private Empresa idEmpresa;

    public Entrega() {
    }

    public Entrega(Integer idTaxa) {
        this.idTaxa = idTaxa;
    }

    public Integer getIdTaxa() {
        return idTaxa;
    }

    public void setIdTaxa(Integer idTaxa) {
        this.idTaxa = idTaxa;
    }

    public String getBairro() {
        return bairro;
    }

    public void setBairro(String bairro) {
        this.bairro = bairro;
    }

    public BigDecimal getCusto() {
        return custo;
    }

    public void setCusto(BigDecimal custo) {
        this.custo = custo;
    }

    public Empresa getIdEmpresa() {
        return idEmpresa;
    }

    public void setIdEmpresa(Empresa idEmpresa) {
        this.idEmpresa = idEmpresa;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idTaxa != null ? idTaxa.hashCode() : 0);
        return hash;
    }

    public Boolean getNaoentrega() {
        return naoentrega;
    }

    public void setNaoentrega(Boolean naoentrega) {
        this.naoentrega = naoentrega;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Entrega)) {
            return false;
        }
        Entrega other = (Entrega) object;
        if ((this.idTaxa == null && other.idTaxa != null) || (this.idTaxa != null && !this.idTaxa.equals(other.idTaxa))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.ux.model.Entrega[ idTaxa=" + idTaxa + " ]";
    }
}
