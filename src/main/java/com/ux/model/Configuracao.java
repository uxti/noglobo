/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ux.model;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Renato
 */
@Entity
@Table(name = "configuracao")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Configuracao.findAll", query = "SELECT c FROM Configuracao c"),
    @NamedQuery(name = "Configuracao.findByIdConfiguracao", query = "SELECT c FROM Configuracao c WHERE c.idConfiguracao = :idConfiguracao"),
    @NamedQuery(name = "Configuracao.findByTitle", query = "SELECT c FROM Configuracao c WHERE c.title = :title"),
    @NamedQuery(name = "Configuracao.findBySlogan", query = "SELECT c FROM Configuracao c WHERE c.slogan = :slogan"),
    @NamedQuery(name = "Configuracao.findByMetatagDescription", query = "SELECT c FROM Configuracao c WHERE c.metatagDescription = :metatagDescription"),
    @NamedQuery(name = "Configuracao.findByMissao", query = "SELECT c FROM Configuracao c WHERE c.missao = :missao"),
    @NamedQuery(name = "Configuracao.findByVisao", query = "SELECT c FROM Configuracao c WHERE c.visao = :visao"),
    @NamedQuery(name = "Configuracao.findByValores", query = "SELECT c FROM Configuracao c WHERE c.valores = :valores"),
    @NamedQuery(name = "Configuracao.findByNossosServicosTexto1", query = "SELECT c FROM Configuracao c WHERE c.nossosServicosTexto1 = :nossosServicosTexto1"),
    @NamedQuery(name = "Configuracao.findByNossosServicosTexto2", query = "SELECT c FROM Configuracao c WHERE c.nossosServicosTexto2 = :nossosServicosTexto2"),
    @NamedQuery(name = "Configuracao.findByNossosServicosTexto3", query = "SELECT c FROM Configuracao c WHERE c.nossosServicosTexto3 = :nossosServicosTexto3"),
    @NamedQuery(name = "Configuracao.findByEmailContato", query = "SELECT c FROM Configuracao c WHERE c.emailContato = :emailContato"),
    @NamedQuery(name = "Configuracao.findByCep", query = "SELECT c FROM Configuracao c WHERE c.cep = :cep"),
    @NamedQuery(name = "Configuracao.findByTipoLogradouro", query = "SELECT c FROM Configuracao c WHERE c.tipoLogradouro = :tipoLogradouro"),
    @NamedQuery(name = "Configuracao.findByLogradouro", query = "SELECT c FROM Configuracao c WHERE c.logradouro = :logradouro"),
    @NamedQuery(name = "Configuracao.findByNumero", query = "SELECT c FROM Configuracao c WHERE c.numero = :numero"),
    @NamedQuery(name = "Configuracao.findByBairro", query = "SELECT c FROM Configuracao c WHERE c.bairro = :bairro"),
    @NamedQuery(name = "Configuracao.findByComplemento", query = "SELECT c FROM Configuracao c WHERE c.complemento = :complemento"),
    @NamedQuery(name = "Configuracao.findByTelefones", query = "SELECT c FROM Configuracao c WHERE c.telefones = :telefones"),
    @NamedQuery(name = "Configuracao.findByCodigoCorreios", query = "SELECT c FROM Configuracao c WHERE c.codigoCorreios = :codigoCorreios"),
    @NamedQuery(name = "Configuracao.findByEmailNotificacao", query = "SELECT c FROM Configuracao c WHERE c.emailNotificacao = :emailNotificacao"),
    @NamedQuery(name = "Configuracao.findByHostNotificacao", query = "SELECT c FROM Configuracao c WHERE c.hostNotificacao = :hostNotificacao"),
    @NamedQuery(name = "Configuracao.findByNomeNotificacao", query = "SELECT c FROM Configuracao c WHERE c.nomeNotificacao = :nomeNotificacao"),
    @NamedQuery(name = "Configuracao.findBySenhaNotificacao", query = "SELECT c FROM Configuracao c WHERE c.senhaNotificacao = :senhaNotificacao"),
    @NamedQuery(name = "Configuracao.findByBannerTopoPromocao", query = "SELECT c FROM Configuracao c WHERE c.bannerTopoPromocao = :bannerTopoPromocao"),
    @NamedQuery(name = "Configuracao.findByBannerLateralPromocao1", query = "SELECT c FROM Configuracao c WHERE c.bannerLateralPromocao1 = :bannerLateralPromocao1"),
    @NamedQuery(name = "Configuracao.findByBannerLateralPromocao2", query = "SELECT c FROM Configuracao c WHERE c.bannerLateralPromocao2 = :bannerLateralPromocao2"),
    @NamedQuery(name = "Configuracao.findByBannerBusca1", query = "SELECT c FROM Configuracao c WHERE c.bannerBusca1 = :bannerBusca1"),
    @NamedQuery(name = "Configuracao.findByBannerBusca2", query = "SELECT c FROM Configuracao c WHERE c.bannerBusca2 = :bannerBusca2")})
public class Configuracao implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id_configuracao")
    private Integer idConfiguracao;
    @Lob
    @Size(max = 65535)
    @Column(name = "ofensas")
    private String ofensas;
    @Size(max = 100)
    @Column(name = "title")
    private String title;
    @Size(max = 255)
    @Column(name = "slogan")
    private String slogan;
    @Size(max = 160)
    @Column(name = "metatag_description")
    private String metatagDescription;
    @Lob
    @Size(max = 65535)
    @Column(name = "metatag_keywords")
    private String metatagKeywords;
    @Lob
    @Size(max = 65535)
    @Column(name = "quem_somos")
    private String quemSomos;
    @Size(max = 255)
    @Column(name = "missao")
    private String missao;
    @Size(max = 255)
    @Column(name = "visao")
    private String visao;
    @Size(max = 255)
    @Column(name = "valores")
    private String valores;
    @Lob
    @Size(max = 65535)
    @Column(name = "nossos_servicos")
    private String nossosServicos;
    @Size(max = 120)
    @Column(name = "nossos_servicos_texto_1")
    private String nossosServicosTexto1;
    @Size(max = 120)
    @Column(name = "nossos_servicos_texto_2")
    private String nossosServicosTexto2;
    @Size(max = 120)
    @Column(name = "nossos_servicos_texto_3")
    private String nossosServicosTexto3;
    @Lob
    @Size(max = 65535)
    @Column(name = "privacidade")
    private String privacidade;
    @Lob
    @Size(max = 65535)
    @Column(name = "termos_uso")
    private String termosUso;
    @Size(max = 255)
    @Column(name = "email_contato")
    private String emailContato;
    @Lob
    @Size(max = 65535)
    @Column(name = "facebook")
    private String facebook;
    @Lob
    @Size(max = 65535)
    @Column(name = "google_plus")
    private String googlePlus;
    @Lob
    @Size(max = 65535)
    @Column(name = "instagram")
    private String instagram;
    @Lob
    @Size(max = 65535)
    @Column(name = "twitter")
    private String twitter;
    @Size(max = 9)
    @Column(name = "cep")
    private String cep;
    @Size(max = 20)
    @Column(name = "tipo_logradouro")
    private String tipoLogradouro;
    @Size(max = 255)
    @Column(name = "logradouro")
    private String logradouro;
    @Size(max = 50)
    @Column(name = "numero")
    private String numero;
    @Size(max = 50)
    @Column(name = "bairro")
    private String bairro;
    @Size(max = 255)
    @Column(name = "complemento")
    private String complemento;
    @Size(max = 255)
    @Column(name = "telefones")
    private String telefones;
    @Size(max = 255)
    @Column(name = "codigo_correios")
    private String codigoCorreios;
    @Size(max = 255)
    @Column(name = "email_notificacao")
    private String emailNotificacao;
    @Size(max = 255)
    @Column(name = "host_notificacao")
    private String hostNotificacao;
    @Size(max = 255)
    @Column(name = "nome_notificacao")
    private String nomeNotificacao;
    @Size(max = 100)
    @Column(name = "senha_notificacao")
    private String senhaNotificacao;
    @Size(max = 255)
    @Column(name = "banner_topo_promocao")
    private String bannerTopoPromocao;
    @Size(max = 255)
    @Column(name = "banner_lateral_promocao_1")
    private String bannerLateralPromocao1;
    @Size(max = 255)
    @Column(name = "banner_lateral_promocao_2")
    private String bannerLateralPromocao2;
    @Size(max = 255)
    @Column(name = "banner_busca_1")
    private String bannerBusca1;
    @Size(max = 255)
    @Column(name = "banner_busca_2")
    private String bannerBusca2;

    public Configuracao() {
    }

    public Configuracao(Integer idConfiguracao) {
        this.idConfiguracao = idConfiguracao;
    }

    public Integer getIdConfiguracao() {
        return idConfiguracao;
    }

    public void setIdConfiguracao(Integer idConfiguracao) {
        this.idConfiguracao = idConfiguracao;
    }

    public String getOfensas() {
        return ofensas;
    }

    public void setOfensas(String ofensas) {
        this.ofensas = ofensas;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getSlogan() {
        return slogan;
    }

    public void setSlogan(String slogan) {
        this.slogan = slogan;
    }

    public String getMetatagDescription() {
        return metatagDescription;
    }

    public void setMetatagDescription(String metatagDescription) {
        this.metatagDescription = metatagDescription;
    }

    public String getMetatagKeywords() {
        return metatagKeywords;
    }

    public void setMetatagKeywords(String metatagKeywords) {
        this.metatagKeywords = metatagKeywords;
    }

    public String getQuemSomos() {
        return quemSomos;
    }

    public void setQuemSomos(String quemSomos) {
        this.quemSomos = quemSomos;
    }

    public String getMissao() {
        return missao;
    }

    public void setMissao(String missao) {
        this.missao = missao;
    }

    public String getVisao() {
        return visao;
    }

    public void setVisao(String visao) {
        this.visao = visao;
    }

    public String getValores() {
        return valores;
    }

    public void setValores(String valores) {
        this.valores = valores;
    }

    public String getNossosServicos() {
        return nossosServicos;
    }

    public void setNossosServicos(String nossosServicos) {
        this.nossosServicos = nossosServicos;
    }

    public String getNossosServicosTexto1() {
        return nossosServicosTexto1;
    }

    public void setNossosServicosTexto1(String nossosServicosTexto1) {
        this.nossosServicosTexto1 = nossosServicosTexto1;
    }

    public String getNossosServicosTexto2() {
        return nossosServicosTexto2;
    }

    public void setNossosServicosTexto2(String nossosServicosTexto2) {
        this.nossosServicosTexto2 = nossosServicosTexto2;
    }

    public String getNossosServicosTexto3() {
        return nossosServicosTexto3;
    }

    public void setNossosServicosTexto3(String nossosServicosTexto3) {
        this.nossosServicosTexto3 = nossosServicosTexto3;
    }

    public String getPrivacidade() {
        return privacidade;
    }

    public void setPrivacidade(String privacidade) {
        this.privacidade = privacidade;
    }

    public String getTermosUso() {
        return termosUso;
    }

    public void setTermosUso(String termosUso) {
        this.termosUso = termosUso;
    }

    public String getEmailContato() {
        return emailContato;
    }

    public void setEmailContato(String emailContato) {
        this.emailContato = emailContato;
    }

    public String getFacebook() {
        return facebook;
    }

    public void setFacebook(String facebook) {
        this.facebook = facebook;
    }

    public String getGooglePlus() {
        return googlePlus;
    }

    public void setGooglePlus(String googlePlus) {
        this.googlePlus = googlePlus;
    }

    public String getInstagram() {
        return instagram;
    }

    public void setInstagram(String instagram) {
        this.instagram = instagram;
    }

    public String getTwitter() {
        return twitter;
    }

    public void setTwitter(String twitter) {
        this.twitter = twitter;
    }

    public String getCep() {
        return cep;
    }

    public void setCep(String cep) {
        this.cep = cep;
    }

    public String getTipoLogradouro() {
        return tipoLogradouro;
    }

    public void setTipoLogradouro(String tipoLogradouro) {
        this.tipoLogradouro = tipoLogradouro;
    }

    public String getLogradouro() {
        return logradouro;
    }

    public void setLogradouro(String logradouro) {
        this.logradouro = logradouro;
    }

    public String getNumero() {
        return numero;
    }

    public void setNumero(String numero) {
        this.numero = numero;
    }

    public String getBairro() {
        return bairro;
    }

    public void setBairro(String bairro) {
        this.bairro = bairro;
    }

    public String getComplemento() {
        return complemento;
    }

    public void setComplemento(String complemento) {
        this.complemento = complemento;
    }

    public String getTelefones() {
        return telefones;
    }

    public void setTelefones(String telefones) {
        this.telefones = telefones;
    }

    public String getCodigoCorreios() {
        return codigoCorreios;
    }

    public void setCodigoCorreios(String codigoCorreios) {
        this.codigoCorreios = codigoCorreios;
    }

    public String getEmailNotificacao() {
        return emailNotificacao;
    }

    public void setEmailNotificacao(String emailNotificacao) {
        this.emailNotificacao = emailNotificacao;
    }

    public String getHostNotificacao() {
        return hostNotificacao;
    }

    public void setHostNotificacao(String hostNotificacao) {
        this.hostNotificacao = hostNotificacao;
    }

    public String getNomeNotificacao() {
        return nomeNotificacao;
    }

    public void setNomeNotificacao(String nomeNotificacao) {
        this.nomeNotificacao = nomeNotificacao;
    }

    public String getSenhaNotificacao() {
        return senhaNotificacao;
    }

    public void setSenhaNotificacao(String senhaNotificacao) {
        this.senhaNotificacao = senhaNotificacao;
    }

    public String getBannerTopoPromocao() {
        return bannerTopoPromocao;
    }

    public void setBannerTopoPromocao(String bannerTopoPromocao) {
        this.bannerTopoPromocao = bannerTopoPromocao;
    }

    public String getBannerLateralPromocao1() {
        return bannerLateralPromocao1;
    }

    public void setBannerLateralPromocao1(String bannerLateralPromocao1) {
        this.bannerLateralPromocao1 = bannerLateralPromocao1;
    }

    public String getBannerLateralPromocao2() {
        return bannerLateralPromocao2;
    }

    public void setBannerLateralPromocao2(String bannerLateralPromocao2) {
        this.bannerLateralPromocao2 = bannerLateralPromocao2;
    }

    public String getBannerBusca1() {
        return bannerBusca1;
    }

    public void setBannerBusca1(String bannerBusca1) {
        this.bannerBusca1 = bannerBusca1;
    }

    public String getBannerBusca2() {
        return bannerBusca2;
    }

    public void setBannerBusca2(String bannerBusca2) {
        this.bannerBusca2 = bannerBusca2;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idConfiguracao != null ? idConfiguracao.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Configuracao)) {
            return false;
        }
        Configuracao other = (Configuracao) object;
        if ((this.idConfiguracao == null && other.idConfiguracao != null) || (this.idConfiguracao != null && !this.idConfiguracao.equals(other.idConfiguracao))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.ux.model.Configuracao[ idConfiguracao=" + idConfiguracao + " ]";
    }
    
}
