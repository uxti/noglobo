/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ux.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Renato
 */
@Entity
@Table(name = "compra")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Compra.findAll", query = "SELECT c FROM Compra c"),
    @NamedQuery(name = "Compra.findByIdCompra", query = "SELECT c FROM Compra c WHERE c.idCompra = :idCompra"),
    @NamedQuery(name = "Compra.findByDtCompra", query = "SELECT c FROM Compra c WHERE c.dtCompra = :dtCompra"),
    @NamedQuery(name = "Compra.findByQuantidade", query = "SELECT c FROM Compra c WHERE c.quantidade = :quantidade"),
    @NamedQuery(name = "Compra.findByValorCompra", query = "SELECT c FROM Compra c WHERE c.valorCompra = :valorCompra"),
    @NamedQuery(name = "Compra.findByValorTotal", query = "SELECT c FROM Compra c WHERE c.valorTotal = :valorTotal"),
    @NamedQuery(name = "Compra.findByEnderecoEntrega", query = "SELECT c FROM Compra c WHERE c.enderecoEntrega = :enderecoEntrega"),
    @NamedQuery(name = "Compra.findByNumeroEntrega", query = "SELECT c FROM Compra c WHERE c.numeroEntrega = :numeroEntrega"),
    @NamedQuery(name = "Compra.findByCepEntrega", query = "SELECT c FROM Compra c WHERE c.cepEntrega = :cepEntrega"),
    @NamedQuery(name = "Compra.findByComplementoEntrega", query = "SELECT c FROM Compra c WHERE c.complementoEntrega = :complementoEntrega"),
    @NamedQuery(name = "Compra.findByTipoFrete", query = "SELECT c FROM Compra c WHERE c.tipoFrete = :tipoFrete"),
    @NamedQuery(name = "Compra.findByValorFrete", query = "SELECT c FROM Compra c WHERE c.valorFrete = :valorFrete"),
    @NamedQuery(name = "Compra.findByResponsavel", query = "SELECT c FROM Compra c WHERE c.responsavel = :responsavel"),
    @NamedQuery(name = "Compra.findByLote", query = "SELECT c FROM Compra c WHERE c.lote = :lote"),
    @NamedQuery(name = "Compra.findByTransactionCode", query = "SELECT c FROM Compra c WHERE c.transactionCode = :transactionCode"),
    @NamedQuery(name = "Compra.findByTokenAutorizacao", query = "SELECT c FROM Compra c WHERE c.tokenAutorizacao = :tokenAutorizacao"),
    @NamedQuery(name = "Compra.findByReferencePagseguro", query = "SELECT c FROM Compra c WHERE c.referencePagseguro = :referencePagseguro"),
    @NamedQuery(name = "Compra.findByCodigoRastreamentoEntrega", query = "SELECT c FROM Compra c WHERE c.codigoRastreamentoEntrega = :codigoRastreamentoEntrega"),
    @NamedQuery(name = "Compra.findByBairroEntrega", query = "SELECT c FROM Compra c WHERE c.bairroEntrega = :bairroEntrega"),
    @NamedQuery(name = "Compra.findByFormaPagamento", query = "SELECT c FROM Compra c WHERE c.formaPagamento = :formaPagamento"),
    @NamedQuery(name = "Compra.findByFrete", query = "SELECT c FROM Compra c WHERE c.frete = :frete"),
    @NamedQuery(name = "Compra.findByLinkBoleto", query = "SELECT c FROM Compra c WHERE c.linkBoleto = :linkBoleto"),
    @NamedQuery(name = "Compra.findByNumParcela", query = "SELECT c FROM Compra c WHERE c.numParcela = :numParcela"),
    @NamedQuery(name = "Compra.findByOrderCode", query = "SELECT c FROM Compra c WHERE c.orderCode = :orderCode"),
    @NamedQuery(name = "Compra.findByPaymentCode", query = "SELECT c FROM Compra c WHERE c.paymentCode = :paymentCode"),
    @NamedQuery(name = "Compra.findBySituacaoEntrega", query = "SELECT c FROM Compra c WHERE c.situacaoEntrega = :situacaoEntrega"),
    @NamedQuery(name = "Compra.findByStatus", query = "SELECT c FROM Compra c WHERE c.status = :status"),
    @NamedQuery(name = "Compra.findByValidoAte", query = "SELECT c FROM Compra c WHERE c.validoAte = :validoAte"),
    @NamedQuery(name = "Compra.findByRetirar", query = "SELECT c FROM Compra c WHERE c.retirar = :retirar"),
    @NamedQuery(name = "Compra.findByCodigoRetirada", query = "SELECT c FROM Compra c WHERE c.codigoRetirada = :codigoRetirada")})
public class Compra implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id_compra")
    private Integer idCompra;
    @Column(name = "dt_compra")
    @Temporal(TemporalType.DATE)
    private Date dtCompra;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "quantidade")
    private BigDecimal quantidade;
    @Column(name = "valor_compra")
    private BigDecimal valorCompra;
    @Column(name = "valor_total")
    private BigDecimal valorTotal;
    @Size(max = 200)
    @Column(name = "endereco_entrega")
    private String enderecoEntrega;
    @Size(max = 50)
    @Column(name = "numero_entrega")
    private String numeroEntrega;
    @Size(max = 20)
    @Column(name = "cep_entrega")
    private String cepEntrega;
    @Size(max = 200)
    @Column(name = "complemento_entrega")
    private String complementoEntrega;
    @Size(max = 200)
    @Column(name = "tipo_frete")
    private String tipoFrete;
    @Column(name = "valor_frete")
    private BigDecimal valorFrete;
    @Size(max = 200)
    @Column(name = "responsavel")
    private String responsavel;
    @Size(max = 255)
    @Column(name = "lote")
    private String lote;
    @Size(max = 255)
    @Column(name = "transaction_code")
    private String transactionCode;
    @Size(max = 255)
    @Column(name = "token_autorizacao")
    private String tokenAutorizacao;
    @Size(max = 255)
    @Column(name = "reference_pagseguro")
    private String referencePagseguro;
    @Size(max = 255)
    @Column(name = "codigo_rastreamento_entrega")
    private String codigoRastreamentoEntrega;
    @Size(max = 255)
    @Column(name = "bairro_entrega")
    private String bairroEntrega;
    @Size(max = 50)
    @Column(name = "forma_pagamento")
    private String formaPagamento;
    @Size(max = 100)
    @Column(name = "frete")
    private String frete;
    @Size(max = 255)
    @Column(name = "link_boleto")
    private String linkBoleto;
    @Column(name = "num_parcela")
    private Integer numParcela;
    @Size(max = 255)
    @Column(name = "order_code")
    private String orderCode;
    @Size(max = 255)
    @Column(name = "payment_code")
    private String paymentCode;
    @Size(max = 255)
    @Column(name = "situacao_entrega")
    private String situacaoEntrega;
    @Size(max = 100)
    @Column(name = "status")
    private String status;
    @Column(name = "valido_ate")
    @Temporal(TemporalType.DATE)
    private Date validoAte;
    @Column(name = "retirar")
    private Boolean retirar;
    @Size(max = 255)
    @Column(name = "codigo_retirada")
    private String codigoRetirada;
    @JoinColumn(name = "id_cidade_entrega", referencedColumnName = "id_cidade")
    @ManyToOne
    private Cidade idCidadeEntrega;
    @JoinColumn(name = "id_usuario", referencedColumnName = "id_usuario")
    @ManyToOne
    private Usuario idUsuario;
    @JoinColumn(name = "id_empresa", referencedColumnName = "id_empresa")
    @ManyToOne(optional = false)
    private Empresa idEmpresa;
    @JoinColumn(name = "id_estado_entrega", referencedColumnName = "id_estado")
    @ManyToOne
    private Estado idEstadoEntrega;
    @JoinColumn(name = "id_produto", referencedColumnName = "id_produto")
    @ManyToOne
    private Produto idProduto;
    @JoinColumn(name = "id_pagamento", referencedColumnName = "id_pagamento")
    @ManyToOne
    private Pagamento idPagamento;
    @JoinColumn(name = "id_frete", referencedColumnName = "id_frete")
    @ManyToOne
    private Frete idFrete;
    @JoinColumn(name = "id_status", referencedColumnName = "id_status")
    @ManyToOne
    private Status idStatus;

    public Compra() {
    }

    public Compra(Integer idCompra) {
        this.idCompra = idCompra;
    }

    public Integer getIdCompra() {
        return idCompra;
    }

    public void setIdCompra(Integer idCompra) {
        this.idCompra = idCompra;
    }

    public Date getDtCompra() {
        return dtCompra;
    }

    public void setDtCompra(Date dtCompra) {
        this.dtCompra = dtCompra;
    }

    public BigDecimal getQuantidade() {
        return quantidade;
    }

    public void setQuantidade(BigDecimal quantidade) {
        this.quantidade = quantidade;
    }

    public BigDecimal getValorCompra() {
        return valorCompra;
    }

    public void setValorCompra(BigDecimal valorCompra) {
        this.valorCompra = valorCompra;
    }

    public BigDecimal getValorTotal() {
        return valorTotal;
    }

    public void setValorTotal(BigDecimal valorTotal) {
        this.valorTotal = valorTotal;
    }

    public String getEnderecoEntrega() {
        return enderecoEntrega;
    }

    public void setEnderecoEntrega(String enderecoEntrega) {
        this.enderecoEntrega = enderecoEntrega;
    }

    public String getNumeroEntrega() {
        return numeroEntrega;
    }

    public void setNumeroEntrega(String numeroEntrega) {
        this.numeroEntrega = numeroEntrega;
    }

    public String getCepEntrega() {
        return cepEntrega;
    }

    public void setCepEntrega(String cepEntrega) {
        this.cepEntrega = cepEntrega;
    }

    public String getComplementoEntrega() {
        return complementoEntrega;
    }

    public void setComplementoEntrega(String complementoEntrega) {
        this.complementoEntrega = complementoEntrega;
    }

    public String getTipoFrete() {
        return tipoFrete;
    }

    public void setTipoFrete(String tipoFrete) {
        this.tipoFrete = tipoFrete;
    }

    public BigDecimal getValorFrete() {
        return valorFrete;
    }

    public void setValorFrete(BigDecimal valorFrete) {
        this.valorFrete = valorFrete;
    }

    public String getResponsavel() {
        return responsavel;
    }

    public void setResponsavel(String responsavel) {
        this.responsavel = responsavel;
    }

    public String getLote() {
        return lote;
    }

    public void setLote(String lote) {
        this.lote = lote;
    }

    public String getTransactionCode() {
        return transactionCode;
    }

    public void setTransactionCode(String transactionCode) {
        this.transactionCode = transactionCode;
    }

    public String getTokenAutorizacao() {
        return tokenAutorizacao;
    }

    public void setTokenAutorizacao(String tokenAutorizacao) {
        this.tokenAutorizacao = tokenAutorizacao;
    }

    public String getReferencePagseguro() {
        return referencePagseguro;
    }

    public void setReferencePagseguro(String referencePagseguro) {
        this.referencePagseguro = referencePagseguro;
    }

    public String getCodigoRastreamentoEntrega() {
        return codigoRastreamentoEntrega;
    }

    public void setCodigoRastreamentoEntrega(String codigoRastreamentoEntrega) {
        this.codigoRastreamentoEntrega = codigoRastreamentoEntrega;
    }

    public String getBairroEntrega() {
        return bairroEntrega;
    }

    public void setBairroEntrega(String bairroEntrega) {
        this.bairroEntrega = bairroEntrega;
    }

    public String getFormaPagamento() {
        return formaPagamento;
    }

    public void setFormaPagamento(String formaPagamento) {
        this.formaPagamento = formaPagamento;
    }

    public String getFrete() {
        return frete;
    }

    public void setFrete(String frete) {
        this.frete = frete;
    }

    public String getLinkBoleto() {
        return linkBoleto;
    }

    public void setLinkBoleto(String linkBoleto) {
        this.linkBoleto = linkBoleto;
    }

    public Integer getNumParcela() {
        return numParcela;
    }

    public void setNumParcela(Integer numParcela) {
        this.numParcela = numParcela;
    }

    public String getOrderCode() {
        return orderCode;
    }

    public void setOrderCode(String orderCode) {
        this.orderCode = orderCode;
    }

    public String getPaymentCode() {
        return paymentCode;
    }

    public void setPaymentCode(String paymentCode) {
        this.paymentCode = paymentCode;
    }

    public String getSituacaoEntrega() {
        return situacaoEntrega;
    }

    public void setSituacaoEntrega(String situacaoEntrega) {
        this.situacaoEntrega = situacaoEntrega;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Date getValidoAte() {
        return validoAte;
    }

    public void setValidoAte(Date validoAte) {
        this.validoAte = validoAte;
    }

    public Boolean getRetirar() {
        return retirar;
    }

    public void setRetirar(Boolean retirar) {
        this.retirar = retirar;
    }

    public String getCodigoRetirada() {
        return codigoRetirada;
    }

    public void setCodigoRetirada(String codigoRetirada) {
        this.codigoRetirada = codigoRetirada;
    }

    public Cidade getIdCidadeEntrega() {
        return idCidadeEntrega;
    }

    public void setIdCidadeEntrega(Cidade idCidadeEntrega) {
        this.idCidadeEntrega = idCidadeEntrega;
    }

    public Usuario getIdUsuario() {
        return idUsuario;
    }

    public void setIdUsuario(Usuario idUsuario) {
        this.idUsuario = idUsuario;
    }

    public Empresa getIdEmpresa() {
        return idEmpresa;
    }

    public void setIdEmpresa(Empresa idEmpresa) {
        this.idEmpresa = idEmpresa;
    }

    public Estado getIdEstadoEntrega() {
        return idEstadoEntrega;
    }

    public void setIdEstadoEntrega(Estado idEstadoEntrega) {
        this.idEstadoEntrega = idEstadoEntrega;
    }

    public Produto getIdProduto() {
        return idProduto;
    }

    public void setIdProduto(Produto idProduto) {
        this.idProduto = idProduto;
    }

    public Pagamento getIdPagamento() {
        return idPagamento;
    }

    public void setIdPagamento(Pagamento idPagamento) {
        this.idPagamento = idPagamento;
    }

    public Frete getIdFrete() {
        return idFrete;
    }

    public void setIdFrete(Frete idFrete) {
        this.idFrete = idFrete;
    }

    public Status getIdStatus() {
        return idStatus;
    }

    public void setIdStatus(Status idStatus) {
        this.idStatus = idStatus;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idCompra != null ? idCompra.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Compra)) {
            return false;
        }
        Compra other = (Compra) object;
        if ((this.idCompra == null && other.idCompra != null) || (this.idCompra != null && !this.idCompra.equals(other.idCompra))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.ux.model.Compra[ idCompra=" + idCompra + " ]";
    }
    
}
