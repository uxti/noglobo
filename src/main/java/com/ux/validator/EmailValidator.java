/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ux.validator;

import com.ux.controller.EmpresaEJB;
import javax.ejb.EJB;
import java.util.Map;
import java.util.regex.Pattern;
import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.validator.FacesValidator;
import javax.faces.validator.Validator;
import javax.faces.validator.ValidatorException;
import org.primefaces.validate.ClientValidator;

/**
 *
 * @author Renato
 */
@FacesValidator("custom.emailValidator")
public class EmailValidator implements Validator, ClientValidator {

    @EJB
    EmpresaEJB eEJB;

    @Override
    public void validate(FacesContext context, UIComponent component, Object value) throws ValidatorException {
        if (eEJB.verificarEmailExiste(value.toString())) {
            throw new ValidatorException(new FacesMessage(FacesMessage.SEVERITY_ERROR, "Email já se encontra em uso!",
                    "Este email já se encontra em uso! Informe um email diferente."));
        }
    }

    @Override
    public Map<String, Object> getMetadata() {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public String getValidatorId() {
        throw new UnsupportedOperationException("Not supported yet.");
    }
}
