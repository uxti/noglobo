/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ux.pojo;

import com.ux.model.Empresa;
import com.ux.model.Entrega;
import com.ux.model.Produto;

/**
 *
 * @author Renato
 */
public class Carrinho {

    private Produto produto;
    private Empresa empresa;
    private Endereco endereco;
    private int quantidade;
    private FreteWS frete;
    private String cep;
    private String tipoFrete;
    private Boolean retirada;
    private String telefone;
    private Entrega entrega;

    public Carrinho() {
        produto = new Produto();
        empresa = new Empresa();
        endereco = new Endereco();
        quantidade = 1;
        frete = new FreteWS();
        cep = "";
        tipoFrete = "";
        telefone = "";
        entrega = new Entrega();
    }

    public Entrega getEntrega() {
        return entrega;
    }

    public void setEntrega(Entrega entrega) {
        this.entrega = entrega;
    }

    public Produto getProduto() {
        return produto;
    }

    public void setProduto(Produto produto) {
        this.produto = produto;
    }

    public int getQuantidade() {
        return quantidade;
    }

    public void setQuantidade(int quantidade) {
        this.quantidade = quantidade;
    }

    public FreteWS getFrete() {
        return frete;
    }

    public void setFrete(FreteWS frete) {
        this.frete = frete;
    }

    public Empresa getEmpresa() {
        return empresa;
    }

    public void setEmpresa(Empresa empresa) {
        this.empresa = empresa;
    }

    public Endereco getEndereco() {
        return endereco;
    }

    public void setEndereco(Endereco endereco) {
        this.endereco = endereco;
    }

    public String getCep() {
        return cep;
    }

    public void setCep(String cep) {
        this.cep = cep;
    }

    public String getTipoFrete() {
        return tipoFrete;
    }

    public void setTipoFrete(String tipoFrete) {
        this.tipoFrete = tipoFrete;
    }

    public Boolean getRetirada() {
        if (retirada == null) {
            retirada = false;
        }
        return retirada;
    }

    public void setRetirada(Boolean retirada) {
        this.retirada = retirada;
    }

    public String getTelefone() {
        return telefone;
    }

    public void setTelefone(String telefone) {
        this.telefone = telefone;
    }
}
