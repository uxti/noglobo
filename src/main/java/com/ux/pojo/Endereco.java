/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ux.pojo;

import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.io.xml.DomDriver;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;

/**
 *
 * @author Renato
 */
public class Endereco {

    private String resultado;
    private String resultado_txt;
    private String uf;
    private String cidade;
    private String bairro;
    private String tipo_logradouro;
    private String logradouro;
 

    public String getResultado() {
        return resultado;
    }

    public void setResultado(String resultado) {
        this.resultado = resultado;
    }

    public String getResultado_txt() {
        return resultado_txt;
    }

    public void setResultado_txt(String resultado_txt) {
        this.resultado_txt = resultado_txt;
    }

    public String getUf() {
        return uf;
    }

    public void setUf(String uf) {
        this.uf = uf;
    }

    public String getCidade() {
        return cidade;
    }

    public void setCidade(String cidade) {
        this.cidade = cidade;
    }

    public String getBairro() {
        return bairro;
    }

    public void setBairro(String bairro) {
        this.bairro = bairro;
    }

    public String getTipo_logradouro() {
        return tipo_logradouro;
    }

    public void setTipo_logradouro(String tipo_logradouro) {
        this.tipo_logradouro = tipo_logradouro;
    }

    public String getLogradouro() {
        return logradouro;
    }

    public void setLogradouro(String logradouro) {
        this.logradouro = logradouro;
    }

  
    
    

    public Endereco pesquisarEnderecoPorCep(String cep) throws MalformedURLException, IOException {
        cep = cep.replace("-", "").trim();
        URL url;
        url = new URL("http://cep.republicavirtual.com.br/web_cep.php?cep=" + cep);
        URLConnection conn = url.openConnection();
        XStream read = new XStream(new DomDriver());
        read.alias("webservicecep", Endereco.class);
        Endereco e = (Endereco) read.fromXML(conn.getInputStream());
        return e;
    }
}
