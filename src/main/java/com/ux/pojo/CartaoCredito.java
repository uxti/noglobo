/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ux.pojo;

/**
 *
 * @author Renato
 */
public class CartaoCredito {

    private String expirationMonth;
    private String expirationYear;
    private String number;
    private String cvc;
    private String brand;
    private String first6;
    private String last4;
    private String hash;
    private String fullname;
    private String birthdate;
    private String taxDocument;
    private String phone;

    public CartaoCredito() {
        this.expirationMonth = "";
        this.expirationYear = "";
        this.number = "";
        this.cvc = "";
        this.brand = "";
        this.first6 = "";
        this.last4 = "";
        this.hash = "";
        this.fullname = "";
        this.birthdate = "";
        this.taxDocument = "";
        this.phone = "";
    }

    
    
    public CartaoCredito(String expirationMonth, String expirationYear, String number, String cvc, String brand, String first6, String last4, String hash, String fullname, String birthdate, String taxDocument, String phone) {
        this.expirationMonth = expirationMonth;
        this.expirationYear = expirationYear;
        this.number = number;
        this.cvc = cvc;
        this.brand = brand;
        this.first6 = first6;
        this.last4 = last4;
        this.hash = hash;
        this.fullname = fullname;
        this.birthdate = birthdate;
        this.taxDocument = taxDocument;
        this.phone = phone;
    }

    
    
   
    public String getExpirationMonth() {
        return expirationMonth;
    }

    public void setExpirationMonth(String expirationMonth) {
        this.expirationMonth = expirationMonth;
    }

    public String getExpirationYear() {
        return expirationYear;
    }

    public void setExpirationYear(String expirationYear) {
        this.expirationYear = expirationYear;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String getCvc() {
        return cvc;
    }

    public void setCvc(String cvc) {
        this.cvc = cvc;
    }

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public String getFirst6() {
        return first6;
    }

    public void setFirst6(String first6) {
        this.first6 = first6;
    }

    public String getLast4() {
        return last4;
    }

    public void setLast4(String last4) {
        this.last4 = last4;
    }

    public String getHash() {
        return hash;
    }

    public void setHash(String hash) {
        this.hash = hash;
    }

    public String getFullname() {
        return fullname;
    }

    public void setFullname(String fullname) {
        this.fullname = fullname;
    }

    public String getBirthdate() {
        return birthdate;
    }

    public void setBirthdate(String birthdate) {
        this.birthdate = birthdate;
    }

    public String getTaxDocument() {
        return taxDocument;
    }

    public void setTaxDocument(String taxDocument) {
        this.taxDocument = taxDocument;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }


}
