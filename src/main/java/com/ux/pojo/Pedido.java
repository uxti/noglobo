/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ux.pojo;

import com.ux.model.Empresa;
import com.ux.model.Usuario;
import java.math.BigDecimal;
import java.util.Date;

/**
 *
 * @author Renato
 */
public class Pedido {

    private Empresa empresa;
    private Usuario usuario;
    private BigDecimal valor;
    private BigDecimal frete;
    private String lote;
    private Date dt_venda;
    private String status;
    private String situacaoEntrega;
    private String codRetirada;
    private String telefone;

    public Empresa getEmpresa() {
        return empresa;
    }

    public void setEmpresa(Empresa empresa) {
        this.empresa = empresa;
    }

    public String getLote() {
        return lote;
    }

    public void setLote(String lote) {
        this.lote = lote;
    }

    public Date getDt_venda() {
        return dt_venda;
    }

    public void setDt_venda(Date dt_venda) {
        this.dt_venda = dt_venda;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getSituacaoEntrega() {
        return situacaoEntrega;
    }

    public void setSituacaoEntrega(String situacaoEntrega) {
        this.situacaoEntrega = situacaoEntrega;
    }

    public BigDecimal getValor() {
        return valor;
    }

    public void setValor(BigDecimal valor) {
        this.valor = valor;
    }

    public String getCodRetirada() {
        return codRetirada;
    }

    public void setCodRetirada(String codRetirada) {
        this.codRetirada = codRetirada;
    }

    public Usuario getUsuario() {
        return usuario;
    }

    public void setUsuario(Usuario usuario) {
        this.usuario = usuario;
    }

    public BigDecimal getFrete() {
        return frete;
    }

    public void setFrete(BigDecimal frete) {
        this.frete = frete;
    }

    public String getTelefone() {
        return telefone;
    }

    public void setTelefone(String telefone) {
        this.telefone = telefone;
    }
    
    

    public String verificaSituacao() {
        if (status.equals("IN_ANALYSIS")) {
            return "label label-warning";
        } else if (status.equals("Cancelada")) {
            return "label label-danger";
        } else if (status.equals("Pago")) {
            return "label label-success";
        } else if (status.equals("Aguardando pagamento")) {
            return "label label-warning";
        } else {
            return "label label-default";
        }
    }

    public String verificaEntrega() {
        if (situacaoEntrega.equals("Aguardando Envio") || situacaoEntrega.equals("Aguardando Retirada")) {
            return "label label-warning";
        } else if (situacaoEntrega.equals("Enviado") || situacaoEntrega.equals("Retirado")) {
            return "label label-success";
        } else {
            return "";
        }
    }
}
