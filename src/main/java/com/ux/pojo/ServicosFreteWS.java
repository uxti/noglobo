/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ux.pojo;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamImplicit;
import java.util.List;

/**
 *
 * @author Renato
 */
@XStreamAlias("Servicos")
public class ServicosFreteWS {

    @XStreamImplicit
    private List<FreteWS> cServico;

    public List<FreteWS> getcServico() {
        return cServico;
    }

    public void setcServico(List<FreteWS> cServico) {
        this.cServico = cServico;
    }
}
