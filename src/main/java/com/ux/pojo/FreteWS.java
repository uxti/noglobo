/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ux.pojo;

import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.io.xml.DomDriver;
import com.ux.util.UXUtil;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.regex.Pattern;

/**
 *
 * @author Renato
 */
@XStreamAlias("Servico")
public class FreteWS {

    private String cServico;
    private String Codigo;
    private String Valor;
    private String PrazoEntrega;
    private String ValorSemAdicionais;
    private String ValorMaoPropria;
    private String ValorAvisoRecebimento;
    private String ValorValorDeclarado;
    private String EntregaDomiciliar;
    private String EntregaSabado;
    private String MsgErro;
    private String Erro;

    public String getcServico() {
        return cServico;
    }

    public void setcServico(String cServico) {
        this.cServico = cServico;
    }

    public String getCodigo() {
        return Codigo;
    }

    public void setCodigo(String Codigo) {
        this.Codigo = Codigo;
    }

    public String getValor() {
        return Valor;
    }

    public void setValor(String Valor) {
        this.Valor = Valor;
    }

    public String getPrazoEntrega() {
        return PrazoEntrega;
    }

    public void setPrazoEntrega(String PrazoEntrega) {
        this.PrazoEntrega = PrazoEntrega;
    }

    public String getValorSemAdicionais() {
        return ValorSemAdicionais;
    }

    public void setValorSemAdicionais(String ValorSemAdicionais) {
        this.ValorSemAdicionais = ValorSemAdicionais;
    }

    public String getValorMaoPropria() {
        return ValorMaoPropria;
    }

    public void setValorMaoPropria(String ValorMaoPropria) {
        this.ValorMaoPropria = ValorMaoPropria;
    }

    public String getValorAvisoRecebimento() {
        return ValorAvisoRecebimento;
    }

    public void setValorAvisoRecebimento(String ValorAvisoRecebimento) {
        this.ValorAvisoRecebimento = ValorAvisoRecebimento;
    }

    public String getValorValorDeclarado() {
        return ValorValorDeclarado;
    }

    public void setValorValorDeclarado(String ValorValorDeclarado) {
        this.ValorValorDeclarado = ValorValorDeclarado;
    }

    public String getEntregaDomiciliar() {
        return EntregaDomiciliar;
    }

    public void setEntregaDomiciliar(String EntregaDomiciliar) {
        this.EntregaDomiciliar = EntregaDomiciliar;
    }

    public String getEntregaSabado() {
        return EntregaSabado;
    }

    public void setEntregaSabado(String EntregaSabado) {
        this.EntregaSabado = EntregaSabado;
    }

    public String getMsgErro() {
        return MsgErro;
    }

    public void setMsgErro(String MsgErro) {
        this.MsgErro = MsgErro;
    }

    public String getErro() {
        return Erro;
    }

    public void setErro(String Erro) {
        this.Erro = Erro;
    }

    public FreteWS consultarFrete(Map<String, Object> params) throws MalformedURLException, IOException {
        URL url;
        String urll = "http://ws.correios.com.br/calculador/CalcPrecoPrazo.aspx?" + UXUtil.curl(params);
        System.out.println(urll);
        url = new URL(urll);
        StringBuilder xml = new StringBuilder();
        BufferedReader reader = new BufferedReader(new InputStreamReader(url.openStream(), "UTF-8"));
        for (String line; (line = reader.readLine()) != null;) {
            xml.append(line);
        }

        String xmlSaida = "";
        xmlSaida = xml.toString().replace("<Servicos>", "").replace("</Servicos>", "");
        XStream read = new XStream(new DomDriver());
        read.alias("cServico", FreteWS.class);
        FreteWS e = (FreteWS) read.fromXML(xmlSaida);
        return e;
    }

    public ServicosFreteWS consultarFreteAutomaticoPACSEDEX(Map<String, Object> params) throws MalformedURLException, UnsupportedEncodingException, IOException {
        URL url;
        String urll = "http://ws.correios.com.br/calculador/CalcPrecoPrazo.aspx?" + UXUtil.curl(params);
//        System.out.println(urll);
        url = new URL(urll);
        StringBuilder xml = new StringBuilder();
        BufferedReader reader = new BufferedReader(new InputStreamReader(url.openStream(), "UTF-8"));
        for (String line; (line = reader.readLine()) != null;) {
            xml.append(line);
        }

        XStream xStream = new XStream();
        xStream.processAnnotations(ServicosFreteWS.class);
        ServicosFreteWS servicosFreteWS = (ServicosFreteWS) xStream.fromXML(xml.toString());
        return servicosFreteWS;
    }
}
