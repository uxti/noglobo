/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ux.pojo;

import com.ux.model.Empresa;
import com.ux.model.Entrega;
import java.util.List;

/**
 *
 * @author Renato
 */
public class CarrinhoEmpresa {

    private Empresa empresa;
    private List<Carrinho> carrinhos;
    private Endereco endereco;
    private String cep;
    private Boolean retirada;
    private Entrega entrega;
    

    public Empresa getEmpresa() {
        return empresa;
    }

    public void setEmpresa(Empresa empresa) {
        this.empresa = empresa;
    }

    public List<Carrinho> getCarrinhos() {
        return carrinhos;
    }

    public void setCarrinhos(List<Carrinho> carrinhos) {
        this.carrinhos = carrinhos;
    }

    public Endereco getEndereco() {
        return endereco;
    }

    public void setEndereco(Endereco endereco) {
        this.endereco = endereco;
    }

    public String getCep() {
        return cep;
    }

    public void setCep(String cep) {
        this.cep = cep;
    }

    public Boolean getRetirada() {
        if(retirada==null){
            retirada = false;
        }
        return retirada;
    }

    public void setRetirada(Boolean retirada) {
        this.retirada = retirada;
    }

    public Entrega getEntrega() {
        return entrega;
    }

    public void setEntrega(Entrega entrega) {
        this.entrega = entrega;
    }
    
    
    
    
    
}
