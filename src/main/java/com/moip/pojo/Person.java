/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.moip.pojo;


/**
 *
 * @author Renato
 */
public class Person {

    private String name;
    private String lastName;
    private TaxDocument taxDocument;
    private String birthDate;
    private Phone phone;
    private Address address;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }
    
    

    public TaxDocument getTaxDocument() {
        return taxDocument;
    }

    public void setTaxDocument(TaxDocument taxDocument) {
        this.taxDocument = taxDocument;
    }

    public String getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(String birthDate) {
        this.birthDate = birthDate;
    }

    public Phone getPhone() {
        return phone;
    }

    public void setPhone(Phone phone) {
        this.phone = phone;
    }

    public Address getAddress() {
        return address;
    }

    public void setAddress(Address address) {
        this.address = address;
    }
    
    
}
