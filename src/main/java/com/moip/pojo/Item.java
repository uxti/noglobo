/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.moip.pojo;

/**
 *
 * @author Renato
 */
public class Item {

    private String product;
    private int quantity;
    private String detail;
    private int price;

    public Item(String product, int quantity, String detail, int price) {
        this.product = product;
        this.quantity = quantity;
        this.detail = detail;
        this.price = price;
    }

   

    public String getProduct() {
        return product;
    }

    public void setProduct(String product) {
        this.product = product;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public String getDetail() {
        return detail;
    }

    public void setDetail(String detail) {
        this.detail = detail;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }
}
