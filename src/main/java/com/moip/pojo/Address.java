/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.moip.pojo;


/**
 *
 * @author Renato
 */
public class Address {
    private String street;
    private String streetNumber;
    private String district;
    private String zipCode;
    private String zipcode;
    private String city;
    private String state;
    private String country;

    public Address(String street, String streetNumber, String district, String zipCode, String zipcode, String city, String state, String country) {
        this.street = street;
        this.streetNumber = streetNumber;
        this.district = district;
        this.zipCode = zipCode;
        this.zipcode = zipcode;
        this.city = city;
        this.state = state;
        this.country = country;
    }
    
    

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public String getStreetNumber() {
        return streetNumber;
    }

    public void setStreetNumber(String streetNumber) {
        this.streetNumber = streetNumber;
    }

    public String getDistrict() {
        return district;
    }

    public void setDistrict(String district) {
        this.district = district;
    }

    public String getZipCode() {
        return zipCode;
    }

    public void setZipCode(String zipCode) {
        this.zipCode = zipCode;
    }

    public String getZipcode() {
        return zipcode;
    }

    public void setZipcode(String zipcode) {
        this.zipcode = zipcode;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }
    
    
    
}
