/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.moip.pojo;

import java.util.List;

/**
 *
 * @author Renato
 */
public class Receivers {

    private List<MoipAccount> moipAccount;
    private String type;
    private Amount amount;

    public Receivers(List<MoipAccount> moipAccount, String type, Amount amount) {
        this.moipAccount = moipAccount;
        this.type = type;
        this.amount = amount;
    }

    public Receivers() {
    }
    
    
    
    public List<MoipAccount> getMoipAccount() {
        return moipAccount;
    }

    public void setMoipAccount(List<MoipAccount> moipAccount) {
        this.moipAccount = moipAccount;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Amount getAmount() {
        return amount;
    }

    public void setAmount(Amount amount) {
        this.amount = amount;
    }
    
    
}
