/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.moip.pojo;

/**
 *
 * @author Renato
 */
public class Amount {

    private String currency;
    private int percentual;

    public Amount(int percentual) {
        this.percentual = percentual;
    }

    public Amount(String currency) {
        this.currency = currency;
    }

    public Amount() {
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public int getPercentual() {
        return percentual;
    }

    public void setPercentual(int percentual) {
        this.percentual = percentual;
    }
}
