/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.moip.pojo;

/**
 *
 * @author Renato
 */
public class MoipAccount {

    private String id;

    public MoipAccount() {
    }

    public MoipAccount(String id) {
        this.id = id;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}
