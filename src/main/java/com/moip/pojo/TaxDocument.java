/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.moip.pojo;


/**
 *
 * @author Renato
 */
public class TaxDocument {
    private String type;
    private String number;

    public TaxDocument(String type, String number) {
        this.type = type;
        this.number = number;
    }
    
    

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }
    
    
}
