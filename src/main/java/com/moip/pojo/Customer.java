/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.moip.pojo;

/**
 *
 * @author Renato
 */
public class Customer {

    private String ownId;
    private String fullname;
    private String email;
    private String birthDate;
    private TaxDocument taxDocument;
    private Phone phone;

    public Customer(String ownId, String fullname, String email, String birthDate, TaxDocument taxDocument, Phone phone) {
        this.ownId = ownId;
        this.fullname = fullname;
        this.email = email;
        this.birthDate = birthDate;
        this.taxDocument = taxDocument;
        this.phone = phone;
    }

    public Customer() {
    }

    public String getOwnId() {
        return ownId;
    }

    public void setOwnId(String ownId) {
        this.ownId = ownId;
    }

    public String getFullname() {
        return fullname;
    }

    public void setFullname(String fullname) {
        this.fullname = fullname;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(String birthDate) {
        this.birthDate = birthDate;
    }

    public TaxDocument getTaxDocument() {
        return taxDocument;
    }

    public void setTaxDocument(TaxDocument taxDocument) {
        this.taxDocument = taxDocument;
    }

    public Phone getPhone() {
        return phone;
    }

    public void setPhone(Phone phone) {
        this.phone = phone;
    }
}
